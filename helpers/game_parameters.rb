# encoding: utf-8

module GameParameters

  def set_game_parameters players, characters, districts, parameters, player
    $player = player
    # Liste des Joueurs
    @players_name = players
    $nb_players = players.length
    @players = []

    # liste des cartes 'Personnage'
    $char_list = characters
    $nb_char = $char_list.length

    # Liste des cartes 'Quartier'
    $dist_list = districts

    # Paramètres de 'Table'
    $begin_sold = parameters[:begin_sold] # Définit la somme de départ de chaque Joueur
    $begin_nb_cards = parameters[:begin_nb_cards] # Définit le nombre de cartes distribuées à chaque Joueur
    $max_dist = parameters[:max_dist] # Nombre maximal de Quartier à construire pour remporter la partie
    $easy_draw = parameters[:easy_draw] # Définit si on applique la variante permettant de choisir la carte à défausser parmis toutes ses cartes

    # Paramètres des 'Personnages'
    $kill_crown = parameters[:kill_crown] # Définit si l'assassinat du 'Roi' entraîne la perte de la couronne
    $end_killed_char = parameters[:end_killed_char] # Définit si le Personnage assassiné se révèle à la fin du tour
    $total_bewitch = parameters[:total_bewitch] # Définit si la 'Sorcière' récupère la totalité du tour du Joueur ou seulement son pouvoir
    $bewitch_crown = parameters[:bewitch_crown] # Définit si la 'Sorcière' récupère la couronne en ensorcellant le 'Roi'
    $build_bewitch = parameters[:build_bewitch] # Définit si la 'Sorcière' peut construire lors de son tour
    #todo Implémenter l'appel du 'Bailli' en dernier
    $end_owner = parameters[:end_owner] # Définit si le 'Bailli' est appelé à la fin
    $sorcerer_revealed_cards = parameters[:sorcerer_revealed_cards] # Définit si le 'Sorcier' peut voir les cartes lors du choix
    $toss_king = parameters[:toss_king] # Définit si on tire au hasard le 'Roi' lorsqu'il se retrouve parmis les personnages défaussés
    $killing_king = parameters[:killing_king] # Définit si le "Roi" peut-être assassiné
    $oblige_to_enthrone = parameters[:oblige_to_enthrone] # Définit si l''Empereur' est obligé d'intrôniser un Joueur au début de son tour
    $all_wealthy_pay = parameters[:all_wealthy_pay] # Définit si tous les Joueurs les plus riches payent l''Abbe'
    $ending_turn_mercenary = parameters[:ending_turn_mercenary] # Définit si le 'Condottiere' peut détruire le 'Quartier' d'un Joueur ayant achevé sa 'Citadelle'
    $pay_diff = parameters[:pay_diff] # Définit si le 'Diplomate' paye la différence entre si son 'Quartier' a un coût inférieur à celui de sa victime

    # Paramètres des 'Quartiers'
    $treasure_add = parameters[:treasure_add ] # Définit si le coût de la construction du 'Quartier' 'Trésor Impérial' est compris dans le décompte final ou pas
    #todo Implémenter le fait qu'un 'Quartier' détruit par la 'Poudrière' n'empêche pas la fin de la partie si le Joueur avait fini sa 'Citadelle'
    $end_powder = parameters[:end_powder]  # Définit si la destruction d'un 'Quartier' dans une 'Citadelle' achevée par la 'Poudrière' repousse la fin de la partie
  end

  #def set_game_parameters
  #  #$player = 1
  #  $nb_players = 8
  #  @players_name = %w(Philippe Mélina Romain Nadège Thomas Théophane Yohan Michel)
  #  @players = []
  #
  #  # Paramètres de 'Table'
  #  $begin_sold = 2 # Définit la somme de départ de chaque Joueur
  #  $begin_nb_cards = 4 # Définit le nombre de cartes distribuées à chaque Joueur
  #  $max_dist = 8 # Nombre maximal de Quartier à construire pour remporter la partie
  #  $easy_draw = false # Définit si on applique la variante permettant de choisir la carte à défausser parmis toutes ses cartes
  #
  #  # Paramètres des 'Personnages'
  #  $kill_crown = true # Définit si l'assassinat du 'Roi' entraîne la perte de la couronne
  #  $end_killed_char = false # Définit si le Personnage assassiné se révèle à la fin du tour
  #  $total_bewitch = false # Définit si la 'Sorcière' récupère la totalité du tour du Joueur ou seulement son pouvoir
  #  $bewitch_crown = false # Définit si la 'Sorcière' récupère la couronne en ensorcellant le 'Roi'
  #  $build_bewitch = true # Définit si la 'Sorcière' peut construire lors de son tour
  #  $end_owner = false # Définit si le 'Bailli' est appelé à la fin
  #  $sorcerer_revealed_cards = false # Définit si le 'Sorcier' peut voir les cartes lors du choix
  #  $toss_king = true # Définit si on tire au hasard le 'Roi' lorsqu'il se retrouve parmis les personnages défaussés
  #  $killing_king = true # Définit si le "Roi" peut-être assassiné
  #  $oblige_to_enthrone = true # Définit si l''Empereur' est obligé d'intrôniser un Joueur au début de son tour
  #  $all_wealthy_pay = false # Définit si tous les Joueurs les plus riches payent l''Abbe'
  #  $ending_turn_mercenary = false # Définit si le 'Condottiere' peut détruire le 'Quartier' d'un Joueur ayant achevé sa 'Citadelle'
  #  $pay_diff = true # Définit si le 'Diplomate' paye la différence entre si son 'Quartier' a un coût inférieur à celui de sa victime
  #
  #  # Paramètres des 'Quartiers'
  #  $treasure_add = false # Définit si le coût de la construction du 'Quartier' 'Trésor Impérial' est compris dans le décompte final ou pas
  #  $end_powder = false  # Définit si la destruction d'un 'Quartier' dans une 'Citadelle' achevée par la 'Poudrière' repousse la fin de la partie
  #
  #  $char_list = %w(assassin voleur magicien roi abbe marchand architecte diplomate reine)
  #  $nb_char = $char_list.length
  #  #$char_list = %w(assassin voleur magicien roi eveque marchand architecte condottiere reine)
  #  #$char_list = %w(sorciere bailli sorcier empereur abbe alchimiste navigateur diplomate artiste)
  #  $dist_list = %w(beffroi bibliotheque carriere cimetiere cour_des_miracles donjon dracoport ecole_de_magie fabrique fontaine_aux_souhaits grande_muraille hopital hospice laboratoire manufacture musee observatoire parc phare poudriere salle_des_cartes salle_du_trone tresor_imperial universite)
  #end
end