# encoding: utf-8

require 'Qt4'
require './gui/game/display_card'
require './gui/game/display_cards'
require './helpers/parameters'

class ZoomCard < DisplayCard

  def initialize parent=nil
    super(parent)
    hide
  end

  def add_card card
    super(card)
    # Définit le contenu de la bulle d'aide
    @card.setToolTip "Clic 'Droit' pour réduire la carte."
  end

  def del_card
    # Définit le contenu de la bulle d'aide
    @card.setToolTip "Clic 'Droit' pour agrandir la carte."
    super()
  end
end

class ZoomPlayerHand < DisplayCards

  def initialize
    super()
    hide
  end

  def mouseDoubleClickEvent event
    if isVisible
      scene.font.hide
      scene.zoom_player_hand.hide
      scene.zoom_player_hand.del_cards
    end
  end
end

module ToggleFunctions

  def action_toggle scene
    if scene.action.isVisible
      scene.font.hide
      scene.action.hide
    else
      scene.font.show unless scene.font.isVisible
      if scene.zoom_card.isVisible
        scene.zoom_card.del_card
        scene.zoom_card.hide
      end
      if scene.zoom_player_hand.isVisible
        scene.zoom_player_hand.del_cards
        scene.zoom_player_hand.hide
      end
      until scene.chat.nil?
        scene.chat.hide if scene.chat.isVisible
      end
      scene.history.hide if scene.history.isVisible
      scene.disp_char_power.hide
      scene.action.show
    end
  end

  def char_power_toggle scene
    if scene.disp_char_power.isVisible
      scene.font.hide
      scene.disp_char_power.hide
    else
      scene.font.show unless scene.font.isVisible
      if scene.zoom_card.isVisible
        scene.zoom_card.del_card
        scene.zoom_card.hide
      end
      if scene.zoom_player_hand.isVisible
        scene.zoom_player_hand.del_cards
        scene.zoom_player_hand.hide
      end
      until scene.chat.nil?
        scene.chat.hide if scene.chat.isVisible
      end
      scene.history.hide if scene.history.isVisible
      scene.action.hide
      scene.disp_char_power.show
    end
  end

  def chat_toggle scene
    if scene.chat.isVisible
      scene.font.hide
      scene.chat.hide
    else
      scene.font.show unless scene.font.isVisible
      if scene.zoom_card.isVisible
        scene.zoom_card.del_card
        scene.zoom_card.hide
      end
      if scene.zoom_player_hand.isVisible
        scene.zoom_player_hand.del_cards
        scene.zoom_player_hand.hide
      end
      scene.disp_char_power.hide if scene.disp_char_power.isVisible
      scene.history.hide if scene.history.isVisible
      scene.action.hide
      scene.chat.show
    end
  end

  def history_toggle scene
    if scene.history.isVisible
      scene.font.hide
      scene.history.hide
    else
      scene.font.show unless scene.font.isVisible
      if scene.zoom_card.isVisible
        scene.zoom_card.del_card
        scene.zoom_card.hide
      end
      if scene.zoom_player_hand.isVisible
        scene.zoom_player_hand.del_cards
        scene.zoom_player_hand.hide
      end
      scene.disp_char_power.hide if scene.disp_char_power.isVisible
      until scene.chat.nil?
        scene.chat.hide if scene.chat.isVisible
      end
      scene.action.hide
      scene.history.show
    end
  end
end