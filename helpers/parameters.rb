# encoding: utf-8

module Parameters

  def set_colors
    $red = Qt::Color.new 210, 0, 0
    $green = Qt::Color.new 50, 205, 50
    $dimgrey = Qt::Color.new 50, 50, 50
    $grey = Qt::Color.new 150, 150, 150, 255
    $main_font_color = Qt::Color.new 236, 174, 96, 255

    $player_colors = []
    $player_colors << Qt::Color.new(128, 0, 0) # Rouge
    $player_colors << Qt::Color.new(0, 0, 156) # Bleu
    $player_colors << Qt::Color.new(0, 100, 0) # Vert
    $player_colors << Qt::Color.new(238, 154, 0) # Orange
    $player_colors << Qt::Color.new(135, 31, 120) # Violet
    $player_colors << Qt::Color.new(255, 99, 71) # Tomate
    $player_colors << Qt::Color.new(139, 69, 19) # Brun
    $player_colors << Qt::Color.new(112, 147, 219) # Bleu clair

    $player_rgb_colors = []
    for player_color in $player_colors
      $player_rgb_colors << "#{player_color.red},#{player_color.green},#{player_color.blue}"
    end
  end

  def set_polices
    $main_font = "BlackChancery"
    $script_font = "Note this"
    $indicator_font = "Times"
    Qt::FontDatabase.addApplicationFont "./font/BlackChancery.ttf"
    Qt::FontDatabase.addApplicationFont "./font/Note_this.ttf"
    Qt::FontDatabase.addApplicationFont "./font/Gondola_SD.ttf"
  end

  def set_dimensions
    # Initialisation des ratios pour contrôler le zoom
    $zoom_out_ratio = 1/$ratio.to_f
    $zoom_in_ratio = $ratio.to_f
    # Définition des dimensions d'une carte
    $card_height = $scene_height / 2
    $card_width = $card_height * $card_ratio
    # Calcul des mesures d'une aire de jeu
    $area_width = $card_width*$zoom_out_ratio*5 + $card_space*5
    $area_height = $card_height*$zoom_out_ratio*2 + $card_space*2 + $card_height*$zoom_out_ratio/2
    # Calcul d'un coté d'une aire de jeu inclinée à 45°
    $rotate_area_wide = ($area_width / Math.sqrt(2)).ceil + ($area_height / Math.sqrt(2)).ceil
    # Calcul de l'espace horizontal entre chaque aire de jeu
    $area_space = (($scene_width - $rotate_area_wide*2 - $area_width*2).to_f/5).ceil
    $rotate_area_space = ($scene_height - $rotate_area_wide*2 ) / 3
    # Calcul de la marge
    $border_space = $card_height*$zoom_out_ratio/2 + $card_space
  end

end