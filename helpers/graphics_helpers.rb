# encoding: utf-8

require 'Qt4'

class TextBanner < Qt::GraphicsItem

  attr_reader :width, :height

  def initialize txt_label, txt_color, parent=nil
    super(parent)
    init_label txt_label, txt_color
    set_background
    set_positions
    setZValue 2
  end

  def boundingRect
    Qt::RectF.new 0, 0, @width, @height
  end

  # Affichage du nom/pseudo du joueur
  def init_label txt_label, txt_color
    @label = Qt::GraphicsTextItem.new txt_label, self
    font = Qt::Font.new $main_font, $icon_wide
    @label.setFont font
    @label.setDefaultTextColor txt_color
    @label.setZValue 2
  end

  # Affichage du fond
  def set_background
    scroll_banner_pix = Qt::Pixmap.new "./img/scroll_banner.png"
    @scroll_banner = Qt::GraphicsPixmapItem.new scroll_banner_pix, self
    @scroll_banner.setTransformationMode Qt::SmoothTransformation
    @scroll_banner.setZValue 1
  end

  def set_positions
    @width = (@label.boundingRect.width*1.2).ceil
    @height = (@label.boundingRect.height*1.34).ceil
    @width_ratio = @width / @scroll_banner.boundingRect.width
    @height_ratio = @height / @scroll_banner.boundingRect.height
    @scroll_banner.scale @width_ratio, @height_ratio
    x = (@label.boundingRect.width*0.1).ceil
    y = (@label.boundingRect.height*0.17).ceil
    @label.setPos x, y
  end
end