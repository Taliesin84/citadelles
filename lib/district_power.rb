# encoding: utf-8

module DistrictPower

  def allowed_dist_power (player)
    if player.citadel.find {|q| q.name == "Laboratoire"} and !((player.citadel.find {|q| q.name == "Laboratoire"}).power_used)
      laboratory_power player
    end
    if player.citadel.find {|q| q.name == "Manufacture"} and !((player.citadel.find {|q| q.name == "Manufacture"}).power_used)
      factory_power player
    end
  end

  # Application du pouvoir de la Manufacture
  def laboratory_power(player)
    # On vérifie que le Joueur a des cartes Quartier à défausser
    unless player.hand.empty?
      begin
        begin
          puts "Souhaitez-vous vous défausser d'une carte Quartier en échange d'une pièce d'or ? (O/N)"
          choice = gets.strip.to_s.upcase
          unless %W(O N).include? choice
            raise "Veuillez répondre par O ou N"
          end
        rescue
          puts "Erreur : #{$!}\n\n"
          retry
        end
        begin
          puts "Quelle carte Quartier souhaitez-vous défausser ?\n\n"
          player.show_hand
          puts "A - Annuler"
          district = get.strip
          district_to_i = 0
          district_to_i = district.match(/\d{1,}/)[0].to_i unless district.match(/\d{1,}/).nil?
          if (district_to_i < 1) or (district_to_i > player.hand+1) or (district == "A")
            raise "Le chiffre saisit doit être celui d'une de vos cartes Quartier !"
          end
          if district.match(/\?/)
            player.hand[district_to_i-1].show_power
          end
        rescue
          puts "Erreur : #{$!}\n\n"
          retry
        end
      end while choice.match(/\?/)
      unless district == "A"
        # On défausse la carte Quartier sélectionnée dans la pile de défausse des Quartiers
        @discarding << player.hand[district_to_i-1]
        # On enlève la carte Quartier sélectionnée de la main du Joueur
        player.hand.delete_at(district_to_i-1)
        # On crédite le coût de la défausse
        player.sold += 1
        # On indique que le pouvoir du Quartier a été utilisé pour ce tour
        (player.find {|q| q.name == "Laboratoire"}).power_used = true
        # On affiche le nouveau solde du Joueur
        puts "Votre solde est de : #{player.sold} pièces d'or.\n\n"
      end
    end
  end

  # Application du pouvoir de la Manufacture
  def factory_power(player)
    # On vérifie que le Joueur a les moyens d'utiliser le pouvoir de la Manufacture
    if player.sold >= 2
      begin
        puts "Souhaitez-vous payer 2 pièces d'or pour pouvoir piocher 3 cartes ? (O/N)"
        choice = gets.strip.to_s.upcase
        unless %W(O N).include? choice
          raise "Veuillez répondre par O ou N"
        end
      rescue
        puts "Erreur : #{$!}\n\n"
        retry
      end
      if choice == "O"
        # On débite le coût du tirage
        player.sold -= 2
        # On ajoute les 2 cartes Quartier piochées à la main du Joueur
        #todo prendre en compte le cas de la pioche vide
        player.hand += take_dist_cards(3)
        # On affiche la nouvelle main du Joueur
        player.show_hand
        # On indique que le pouvoir du Quartier a été utilisé pour ce tour
        (player.find {|q| q.name == "Manufacture"}).power_used = true
      end
    end
  end

  # Application du pouvoir du Cimetière
  def cemetery_power(dist)
    for p in @players
      # On vérifie qu'un Joueur a bâtit le Quartier Cimetière, qu'il n'est pas lui-même Condottiere et que son solde est suffisant
      if (p.citadel.find {|q| q.name == "Cimetière"}) and !(p.characters.find {|c| c.number == 8}) and (p.sold > 0)
        begin
          puts "Est-ce que le Joueur #{p.name} souhaite racheter..."
          puts "Quartier #{dist.cat} : #{dist.name} d'une valeur de #{dist.cost} pièces d'or"
          puts "...pour une pièce d'or ? (O/N)"
          choice = gets.strip.to_s.upcase
          unless %W(O N).include? choice
            raise "Veuillez répondre par O ou N"
          end
        rescue
          puts "Erreur : #{$!}\n\n"
          retry
        end
        # On ajoute le Quartier racheter à la main du Joueur
        if choice == "O"
          p.hand << dist
        end
      end
    end
  end

end