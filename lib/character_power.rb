# encoding: utf-8

module CharacterPower

  # Applique les pouvoirs d'un Personnage
  def char_power(player, char)
    case char.name
      when "Assassin"
        killing_char player
      when "Voleur"
        stolen_char player
      when "Magicien"
        change_cards player
      when "Roi"
        player.king = true
        player.rent("Noblesse")
      when "Evêque"
        player.protected = true
        player.rent("Religion")
      when "Marchand"
        player.sold += 1
        player.rent("Commerce")
      when "Architecte"
        player.architect =true
        player.hand + take_dist_cards(2)
      when "Condottiere"
        player.rent("Militaire")
        select_player_for_destruct player
      else
        # Pas de traitement
    end
  end

  # Selection du Personnage a assassiner
  def killing_char(player)
    begin
      puts "\nQuel personnage souhaitez-vous assassiner ?\n\n"
      # On enlève de la liste des choix les Personnages détenus par le joueur
      victims = []
      for c in @ref_char_list
        found = false
        for p in player.characters
          if p.name == c.name
            found = true
          end
        end
        unless found
          puts "#{c.number} - #{c.name}"
          victims << c.number
        end
      end
      victim = gets.strip.to_i
      unless victims.include? victim
        raise "Le chiffre saisit doit être celui d'un des personnages !"
      end
    rescue
      puts "Erreur : #{$!}\n\n"
      retry
    end
    kill victim
  end

  # Selection du Personnage a voler
  def stolen_char(player)
    begin
      puts "\nQuel personnage souhaitez-vous voler ?\n\n"
      # On enlève de la liste des choix les Personnages détenus par le joueur, l'Assassin qui est déjà passé et le Personnage assassiné
      victims = []
      for c in @ref_char_list
        found = false
        for p in player.characters
          if p.name == c.name
            found = true
          end
        end
        unless (%W(Assassin Voleur).include? c.name) or c.killed or found
          puts "#{c.number} - #{c.name}"
          victims << c.number
        end
      end
      victim = gets.strip.to_i
      unless victims.include? victim
        raise "Le chiffre saisi doit être celui d'un des personnages !"
      end
    rescue
      puts "Erreur : #{$!}\n\n"
      retry
    end
    steal victim
  end

  # Selection du type d'échange par le Magicien
  def change_cards(player)
    begin
      puts "\nQue voulez-vous faire ?"
      puts "1 - Echanger la totalité de vos cartes avec un autre joueur"
      puts "2 - Echanger des cartes avec la pioche"
      puts "R - Rien"
      action = gets.strip
      unless action.upcase == "R"
        action_to_i = 0
        action_to_i = action.match(/\d{1,}/)[0].to_i unless action.match(/\d{1,}/).nil?
        unless [1,2].include? action_to_i
          raise "Le chiffre saisit doit être celui d'une des 2 actions proposées !"
        end
      end
    rescue
      puts "Erreur : #{$!}\n\n"
      retry
    end
    if action_to_i == 1
      change_all_cards player
    elsif action_to_i == 2
      change_selected_cards player
    end
  end

  # Change toutes les cartes d'un joueur avec celles d'un autre
  def change_all_cards(player)
    begin
      puts "Avec quel joueur souhaitez-vous échanger vos cartes ?"
      i = 1
      victims = []
      for p in @players
        unless player.name == p.name
          puts "#{i} - #{p.name} qui a #{p.hand.length} cartes en main"
          victims << p
        end
      end
      victim = gets.strip.to_i
      if (victim < 1) or (victim > i)
        raise "Le chiffre saisit doit être celui d'un des joueurs !"
      end
    rescue
      puts "Erreur : #{$!}\n\n"
      retry
    end
    player_hand = player.hand
    player.hand = victims[i-1].hand
    victims[i-1].hand = player_hand
  end

  def change_selected_cards(player)
    choice = "O"
    new_cards = []
    until player.hand.empty? or choice != "O"
      begin
        puts "Quel carte souhaitez-vous échanger ?"
        i = 1
        for dist in player.hand
          puts "#{i} - Quartier #{dist.cat} : #{dist.name} d'une valeur de #{dist.cost} pièces d'or"
          i += 1
        end
        dist = gets.strip.to_i
        if (dist < 1) or (dist > i)
          raise "Le chiffre saisit doit être celui d'une de vos cartes Quartier !"
        end
      rescue
        puts "Erreur : #{$!}\n\n"
        retry
      end
      #todo prendre en compte le cas de la pioche vide
      card = take_dist_cards(1)
      new_cards += card
      puts "Vous avez pioché la carte Quartier suivante :"
      puts "Quartier #{card[0].cat} : #{card[0].name} d'une valeur de #{card[0].cost} pièces d'or."
      card[0].show_power
      puts "\n"
      # On met la carte sélectionnée dans la défausse
      @discarding << player.hand[dist-1]
      # On supprime la carte sélectionnée de la main du Joueur
      player.hand.delete_at(dist-1)
      begin
        # On permet au joueur d'échanger une autre carte'... ou pas !
        puts "Voulez-vous échanger une autre carte ? (O/N)"
        choice = gets.strip.to_s.upcase
        unless %W(O N).include? choice
          raise "Veuillez répondre par O ou N"
        end
      rescue
        puts "Erreur : #{$!}\n\n"
        retry
      end
    end
    player.hand += new_cards

    if player.hand.empty?
      puts "Vous n'avez pas de Quartiers a échanger !\n\n"
    end
  end

  # Détruit le quartier chez le joueur choisit
  def select_player_for_destruct(player)
    begin
      begin
        puts "Chez quel joueur souhaitez-vous détruire un Quartier ?"
        # On enlève de la liste des choix le joueur qui incarne le Condottiere
        victims = []
        for p in @players
          puts "#{p.number} - #{p.name}"
          victims << c.number
        end
        puts "\nA - Aucun"
        victim = gets.strip
        victim_to_i = 0
        victim_to_i = victim.match(/\d{1,}/)[0].to_i unless victim.match(/\d{1,}/).nil?
        unless (victims.include? victim_to_i) or (victim.upcase == "A")
          raise "Le chiffre saisit doit être celui d'un des Joueurs !"
        end
        if @players[victim_to_i-1].citadel.empty?
          raise "#{@players[victim-1]} ne possède aucun Quartier dans sa Citadelle !"
        end
        if victim.match(/\?/)
          @players[i-1].show_citadel(false)
        end
      rescue
        puts "Erreur : #{$!}\n\n"
        retry
      end
    end while choice.match(/\?/)
    unless victim.upcase == "A"
      select_dist_for_dest(player, @players[victim_to_i-1])
    end
  end

  def select_dist_for_destruct(player, victim)
    begin
      begin
        puts "Quel quartier souhaitez-vous détruire ?"
        victim.show_citadel(false, true)
        puts "\nC - Choisir la Citadelle d'un autre Joueur"
        district = gets.strip
        district_to_i = 0
        district_to_i = district.match(/\d{1,}/)[0].to_i unless district.match(/\d{1,}/).nil?
        if (district_to_i < 1) or (district_to_i >= victim.citadel.length) or (district.upcase != "C")
          raise "Le chiffre saisit doit être celui d'un des Quartiers de #{victim.name} !"
        end
        if district.match(/\?/)
          victim.citadel[district_to_i-1].show_power
        end
        destruct_cost = victim.citadel[district_to_i-1].cost-1
        # Vérifie si le Joueur victime a construit le Quartier Grande Muraille et que le Quartier visé n'est pas Grande Muraille'
        if (victim.citadel.find {|q| q.name == "Grande Muraille"}) and (victim.citadel[district_to_i-1].name != "Grande Muraille")
          destruct_cost += 1
        end
        # Vérifie que le Joueur a suffisamment de fonds pour détruire le Quartier qu'il a sélectionné
        if destruct_cost > player.sold
          raise "Vous n'avez pas assez d'argent pour détruire ce Quartier !"
        end
      rescue
        puts "Erreur : #{$!}\n\n"
        retry
      end
    end while district.match(/\?/)
    # Vérifie que le joueur n'a pas demandé de sélectionner un autre joueur'
    if district.upcase == "C"
      select_player_for_dest player
    else
      # On vérifie si un des Joueurs est à même de pouvoir utiliser le pouvoir du Cimetière
      cemetery_power victim.citadel[district_to_i-1]
      # On met le quartier détruit dans la défausse
      @discarding << victim.citadel[district_to_i-1]
      # On détruit le Quartier de la Citadelle du Joueur victime
      victim.citadel.delete_at[district_to_i-1]
      # On retire le prix de la destruction au Joueur incarnant le Condottiere
      player.sold -= destruct_cost
    end
  end

  # Tue un personnage
  def kill(char)
    for p in @players
      for c in p.characters
        if c.number == char
          c.kill
        end
      end
    end
    for c in @ref_char_list
      if c.number == char
        c.kill
      end
    end
  end

  # Vole un personnage
  def steal(char)
    for p in @players
      for c in p.characters
        if c.number == char
          c.steal
        end
      end
    end
  end
end