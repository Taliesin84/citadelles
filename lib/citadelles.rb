# encoding: utf-8
=begin
require './player'
require './character'
require './character_power'
require './district'
require './district_power'
=end

load './player.rb'
load './character.rb'
load './character_power.rb'
load './district.rb'
load './district_power.rb'

class Citadelles

  include CharacterPower
  include DistrictPower

  attr_reader :players, :hide_cards, :visible_cards, :round, :ref_char_list
  attr_accessor :dist_deck, :char_deck

  def initialize

    # Création des decks
    init_char_deck
    init_dist_deck
    #création de la défausse
    @discarding = []
    # Battage des cartes
    @dist_deck.shuffle! # plus rapide que : @dist_deck.sort_by! {rand}

    # Initialisation des joueurs
    init_players

    @round = 1

    until end_game?
      system 'clear'
      if @round == 1
        puts "-------------------"
        puts "| #{@round}er tour de jeu |"
        puts "-------------------\n\n"
      elsif @round < 10
        puts "--------------------"
        puts "| #{@round}ème tour de jeu |"
        puts "--------------------\n\n"
      else
        puts "---------------------"
        puts "| #{@round}ème tour de jeu |"
        puts "---------------------\n\n"
      end
      # Affichage des scores des Joueurs
      if round > 1
        puts "Voici les scores :\n\n"
        ranking = (@players.sort_by {|p| p.score}).reverse
        i = 1
        for p in ranking
          if i == 1
            puts "#{i}er - #{p.name} avec #{p.score} point(s) et #{p.citadel.length} Quartier(s) construit(s)\n\n"
          else
            puts "#{i}ème - #{p.name} avec #{p.score} point(s) et #{p.citadel.length} Quartier(s) construit(s)"
          end
          i += 1
        end
        puts "\n-------------------------------------------------------\n\n"
      end

      # Choix des Personnages par les Joueurs
      choose_chars

      # Appel des Personnages par le Roi
      i = 1
      while i <= 8
        puts "-------------------------------------------------------\n\n"
        puts "Le Roi appelle le Personnage #{@ref_char_list[i-1].name}.\n\n"

        # Vérifie que le personnage demandé n'a pas été défaussé
        if @hide_cards.find {|card| card.number == i}
          puts "Le Personnage #{@ref_char_list[i-1].name} a été défaussé !\n\n"
        else
          # On cherche quel Joueur possède le Personnage appelé
          player_char = search_player_char(i)
          player = player_char[0]
          char = player_char[1]

          # On vérifie que le Personnage appelé n'a pas été tué par l'Assassin
          if char.killed
            puts "Le #{@ref_char_list[i-1].name} a été tué par l'Assassin !\n\n"
          else
            # On vérifie si le Personnage n'a pas été volé par le Voleur
            if char.stolen
              puts "Votre Personnage a été volé par le Voleur !\n\n"
              thief = search_player_char(2)
              thief[0].sold += player.sold
              player.sold = 0
            end

            puts "C'est à #{player.name} de jouer.\n\n"
            if %W(Roi Evêque Marchand Architecte).include? @ref_char_list[i-1].name
              char_power(player, char)
            end
            puts "-------------------------------------------------------\n\n"
            player.show_infos
            puts "-------------------------------------------------------"
            # 1ère Phase : Application du pouvoir du Personnage

            if %W(Assassin Voleur Magicien).include? @ref_char_list[i-1].name
              char_power(player, char)
              puts "\n-------------------------------------------------------"
            end
            # Pouvoirs optionnels des Quartiers Laboratoire et Manufacture
            allowed_dist_power player

            # 2ème Phase : Argent vs. Carte
            puts " 1ère Phase"
            puts "-------------------------------------------------------\n\n"
            gold_vs_card player
            # Pouvoirs optionnels des Quartiers Laboratoire et Manufacture
            allowed_dist_power player

            puts "\n-------------------------------------------------------\n\n"
            player.show_infos
            puts "-------------------------------------------------------"
            puts " 2ème Phase"
            puts "-------------------------------------------------------\n\n"

            # 3ème Phase : Construction

            # On vérifie si le Personnage appelé est l'Architecte et s'il n'a pas été tué
            if (player.architect) and (char.name == "Architecte")
              # Le Joueur peut bâtir 3 quartiers
              r = 3
            else
              # Le Joueur ne peut bâtir qu'un seul Quartier
              r = 1
            end
            r.times {player.building}

            # Fin du tour : Affichage de la Citadelle du Joueur et de son score
            system "clear"
            player.show_infos
          end
        end
        #On passe à l'appel du Personnage suivant
        i += 1
      end
      # On remet en jeu les Personnages
      @char_deck += @ref_char_list
      # On pass au tour suivant
      @round += 1
      # On établit l'ordre de passage des Joueurs à partir du Roi
      until @players[0].king
        @players << @players[0]
        @players.shift
      end
      # On ré-initialise les indicateurs des Joueurs
      for p in @players
        p.re_init
        # On ré-initialise les pouvoirs des Quartiers (Laboratoire, Manufacture)
        for d in p.citadel
          d.power_used = false
        end
      end

    end

    # Affichage du classement final des Joueurs
    puts "\nLe vainqueur de la partie est :\n\n\n"
    ranking = (@players.sort_by {|p| p.score}).reverse
    i = 1
    for p in ranking
      if i == 1
        word = "*   #{i}er - #{p.name} avec #{p.score} point(s)   *"
        l = 1
        border = ""
        while l <= word.length
          border += "*"
        end
        puts border
        puts word
        puts border
        puts "\n\n"
      else
        puts "#{i}ème - #{p.name} avec #{p.score} point(s)"
      end
      i += 1
    end

  end

  # Vérifie si un ou plusieurs joueurs on construit 8 Quartiers à la fin du tour
  def end_game?
    end_game = false
    for p in @players
      end_game = true if p.citadel.length == 8
    end
    return end_game
  end

  # Initialisation des joueurs
  def init_players

    # Choix du nombre de joueurs
    begin
      puts "Nombre de joueurs (2-7):"
      nb_p = gets.strip.to_i
      if (nb_p < 2) or (nb_p > 7)
        raise "Le nombre de joueurs doit être compris entre 2 et 7 !"
      end
    rescue
      puts "Erreur : #{$!}"
      retry
    end

    # Création des Joueurs
    @players = []
    n = 1
    while n <= nb_p
      puts "Insérer le nom du Joueur #{n} :"
      name = gets.strip
      p = Player.new(name)
      p.hand = take_dist_cards(4)
      @players << p
      n += 1
    end
  end

  # Permet de trouver le joueur qui possède le personnage appelé
  def search_player_char(i_char)
    begin
      found = false
      x = 0
      while (x < @players.length) and !found
        y = 0
        while (y < @players[x].characters.length) and !found
          if @players[x].characters[y].number == i_char
            found = true
            return [@players[x], @players[x].characters[y]]
          else
            y += 1
          end
        end
        x += 1
      end
      unless found
        raise "Le personnage n'appartient à aucun joueur !"
      end
    rescue
      puts "Erreur : #{$!}\n\n"
    end
  end

  # Phase de jeu ouvrant le choix de prendre de l'or ou tirer des cartes'
  def gold_vs_card(player)

    # Menu permettant de choisir entre l'or et les cartes
    begin
      puts "Quelle action souhaitez-vous effectuer ?\n\n"
      puts "1 - Prendre 2 pièces d'or à la banque."
      if player.citadel.find {|q| q.name == "Observatoire"}
        puts "2 - Piocher 3 cartes Quartier, en choisir une et se défausser des 2 autres."
      elsif player.citadel.find {|q| q.name == "Bibliothèque"}
        puts "2 - Piocher 2 cartes Quartier."
      else
        puts "2 - Piocher 2 cartes Quartier, en choisir une et se défausser de l'autre."
      end

      choice = gets.strip.to_i
      unless [1,2].include? choice
        raise "Le chiffre saisit doit être celui d'une des 2 actions proposées !"
      end
    rescue
      puts "Erreur : #{$!}\n\n"
      retry
    end

    if choice == 1
      # Ajout des pièces d'or à la banque du joueur'
      player.sold += 2

    elsif choice == 2
      # Tirage de 3 cartes Quartier si le Joueur à bâtit le Quartier Observatoire, 2 sinon
      nb = 2
      nb = 3 if player.citadel.find {|q| q.name == "Observatoire"}

      #todo prendre en compte le cas de la pioche vide
      picked_cards = take_dist_cards(nb)

      # On vérifie si le Joueur n'a pas bâtit le Quartier Bibliothèque'
      if player.citadel.find {|q| q.name == "Bibliothèque"}
        player.hand += picked_cards
      else
        # Choix d'une des 2 cartes Quartier
        begin
          puts "Quelle carte souhaitez-vous conserver ?\n\n"
          i = 0
          while i < picked_cards.length-1
            puts "#{i+1} - Quartier #{picked_cards[i].cat} : #{picked_cards[i].name} d'une valeur de #{picked_cards[i].cost} pièces d'or"
            puts "    Pouvoir : #{picked_cards[i].show_power}"
            i += 1
          end
          choice = gets.strip.to_i
          if (choice < 1) or (choice > nb)
            raise "Le chiffre saisit doit être celui d'une des 2 cartes tirées !"
          end
        rescue
          puts "Erreur : #{$!}\n\n"
          retry
        end

        # Ajout de la carte Quartier choisie à la main du joueur
        player.hand << picked_cards[choice-1]
        # Suppression de la carte choisie des 2 ou 3 cartes tirées
        picked_cards.delete_at(choice-1)
        # Remise des cartes écartées dans la défausse
        @discarding += picked_cards
      end
    end
  end

  # Choix des personnages
  def choose_chars

    nb_p = @players.length

    # Retrait du premier personnage face cachée au hasard
    r = rand(@char_deck.length)
    @hide_cards = []
    @hide_cards << @char_deck[r]
    @char_deck.delete_at(r)

    # Retrait de d'un ou deux personnages faces visibles pour une partie respective de 5 ou 4 joueurs
    t = 0
    if nb_p == 4
      t = 2
    elsif nb_p == 5
      t = 1
    end
    @visible_cards = []
    t.times {
      begin
        r = rand(@char_deck.length)
      end while (@char_deck[r].name == "Roi")
      @visible_cards << @char_deck[r]
      @char_deck.delete_at(r)
    }

    # Choix des personnages par les joueurs
    i = 99
    while @char_deck.length > 1
      unless @visible_cards.empty?
        for c in @visible_cards
          puts "Le Personnage #{@visible_cards[r].name} a été défaussé !\n\n"
        end
      end
      if i >= nb_p-1
        i = 0
      else
        i += 1
      end
      take_char(@players[i])
      system 'clear'
    end

    # Choix avec le dernier perso et le perso écarté par le premier joueur pour le 7ème joueur
    if nb_p == 7
      @char_deck << @hide_cards.pop
      @char_deck.sort_by! {|character| character.number}
      unless @visible_cards.empty?
        for c in @visible_cards
          puts "Le Personnage #{@visible_cards[r].name} a été défaussé !\n\n"
        end
      end
      take_char(@players[i+1])
    end
    # Retrait du dernier personnage face caché
    @hide_cards << @char_deck[0]
    @char_deck.clear
  end

  # Choix d'un personnage
  def take_char(player)
    begin
      begin
        puts "C'est à #{player.name} de choisir un personnage parmis ceux restants :\n\n"
        for c in @char_deck
          puts "#{c.number} - #{c.name}"
        end
        choice = gets.strip
        choice_to_i = 0
        choice_to_i = choice.match(/\d{1,}/)[0].to_i unless choice.match(/\d{1,}/).nil?
        found = false
        for c in @char_deck
          if choice_to_i == c.number
            found = true
          end
        end
        unless found
          raise "Le chiffre saisi doit être celui d'un des personnages restants !"
        end
        # On vérifie si le joueur ne veut pas juste s'informer du pouvoir du personnage'
        if choice.match(/\?/)
          n = 0
          while @char_deck[n].number != choice_to_i
            n += 1
          end
          puts "Le pouvoir spéciale de #{@char_deck[n].name} est :\n\n"
          puts @char_deck[n].show_power
        end
      rescue
        puts "Erreur : #{$!}\n\n"
        retry
      end
    end while choice.match(/\?/)

    # On donne la carte Personnage choisit par le joueur
    player.characters << take_char_card(choice_to_i)
  end

  # Récupération d'une carte Personnage
  def take_char_card(i)
    n = 0
    while @char_deck[n].number != i
      n += 1
    end
    char_card = @char_deck[n]
    @char_deck.delete_at(n)
    return char_card
  end

  # Pioche d'un nombre nb de cartes Quartier
  def take_dist_cards(nb)
    # On remet la défausse dans la pile de cartes Quartier si cette dernière ne possède plus assez de carte pour répondre à la demande
    if @dist_deck.length < nb
      # On mélange la défausse
      @discarding.shuffle!
      # On rajoute la défausse à la pile de manière à conserver les dernières cartes sur le haut de la pile
      @discarding.concat @dist_deck
      @dist_deck = @discarding
      # On vide la défausse
      @discarding = []
    end
    # S'il n'y a plus assez de cartes Quartier en jeu on prend ce qu'on peut
    if @dist_deck.length < nb
      nb = @dist_deck.length
    end
    # On récupère les cartes du haut de la pile de cartes Quartier
    picked_dist_cards = @dist_deck.drop(@dist_deck.length-nb)
    # On retire les cartes tirées du haut de la pile de cartes Quartier
    @dist_deck.slice!(@dist_deck.length-nb, @dist_deck.length)
    # On renvoit les cartes Quartier tirées
    return picked_dist_cards
  end

  # Création du deck des Personnages
  def init_char_deck
    # Rôles de base
    assassin = Character.new(1, "Assassin")
    thief = Character.new(2, "Voleur")
    wizard = Character.new(3, "Magicien")
    king = Character.new(4, "Roi", "Noblesse")
    priest = Character.new(5, "Evêque", "Religion")
    seller = Character.new(6, "Marchand", "Commerce")
    architect = Character.new(7, "Architecte")
    mercenary = Character.new(8, "Condottiere", "Soldatesque")

    # Création d'une liste de référence des personnages
    @ref_char_list = [assassin, thief, wizard, king, priest, seller, architect, mercenary]
    # Création du deck de personnages
    @char_deck = [assassin, thief, wizard, king, priest, seller, architect, mercenary]
  end

  # Création du deck des Quartiers
  def init_dist_deck
    # Quartiers Religieux (bleu)
    temple = District.new("Religion", "Temple", 1)  # 3
    church = District.new("Religion", "Eglise", 2)  # 3
    monastery = District.new("Religion", "Monastère", 3)  # 4
    cathedral = District.new("Religion", "Cathédrale", 5)  # 2

    # Quartiers Commerciaux (vert)
    tavern = District.new("Commerce", "Taverne", 1) # 5
    market = District.new("Commerce", "Marché", 2) # 4
    shop = District.new("Commerce", "Echoppe", 2) # 4
    bar = District.new("Commerce", "Comptoir", 3) # 3
    harbor = District.new("Commerce", "Port", 4) # 3
    hall = District.new("Commerce", "Hôtel de Ville", 5) # 2

    # Quartiers Nobles (jaune)
    manor = District.new("Noblesse", "Manoir", 3) # 5
    castle = District.new("Noblesse", "Château", 4) # 5
    palace = District.new("Noblesse", "Palais", 5) # 2

    # Quartiers Militaires (rouge)
    tower = District.new("Militaire", "Tour de Guet", 1) # 3
    jail = District.new("Militaire", "Prison", 2) # 3
    barrack  = District.new("Caserne", "Caserne", 3) # 3
    stronghold  = District.new("Militaire", "Forteresse", 5) # 3

    # Quartiers Prestigieux (violet)
    miracle = District.new("Prestige", "Cour des miracles", 2)
    dungeon = District.new("Prestige", "Donjon", 3)
    treasure = District.new("Prestige", "Trésor Impérial", 4)
    fountain = District.new("Prestige", "Fontaine aux souhaits", 5)
    laboratory = District.new("Prestige", "Laboratoire", 5)
    cemetery = District.new("Prestige", "Cimetière", 5)
    quarry = District.new("Prestige", "Carrière", 5)
    factory = District.new("Prestige", "Manufacture", 5)
    observatory = District.new("Prestige", "Observatoire", 5)
    school = District.new("Prestige", "Ecole de Magie", 6)
    library = District.new("Prestige", "Bibliothèque", 6)
    wall = District.new("Prestige", "Grande Muraille", 6)
    university = District.new("Prestige", "Université", 6)
    dragon = District.new("Prestige", "Dracoport", 6)

    # Création du deck de quartiers
    @dist_deck = []
    2.times {
      @dist_deck << cathedral
      @dist_deck << hall
      @dist_deck << palace
    }
    3.times {
      @dist_deck << temple
      @dist_deck << church
      @dist_deck << bar
      @dist_deck << harbor
      @dist_deck << tower
      @dist_deck << jail
      @dist_deck << barrack
      @dist_deck << stronghold
    }
    4.times {
      @dist_deck << monastery
      @dist_deck << market
      @dist_deck << shop
    }
    5.times {
      @dist_deck << tavern
      @dist_deck << manor
      @dist_deck << castle
    }
    @dist_deck << miracle
    @dist_deck << dungeon
    @dist_deck << treasure
    @dist_deck << fountain
    @dist_deck << laboratory
    @dist_deck << cemetery
    @dist_deck << quarry
    @dist_deck << factory
    @dist_deck << observatory
    @dist_deck << school
    @dist_deck << library
    @dist_deck << wall
    @dist_deck << university
    @dist_deck << dragon
  end

end

c = Citadelles.new

