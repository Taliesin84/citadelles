# encoding: utf-8

class District

  CATEGORIES = ["Religion", "Commerce", "Noblesse", "Militaire", "Prestige"]

  attr_reader :cat, :name, :cost
  attr_accessor :power_used

  def initialize (cat, name, cost)
    @cat = cat
    @name = name
    @cost = cost
    @power_used = false
  end

  def show_power
    case name
      when "Cour des miracles"
        puts "Pour le décompte final des points, la Cour des miracles est considérée comme un quartier de la couleur de votre choix. Vous ne pouvez pas utiliser cette capacité si vous avez construit la cour des miracles au dernier tour du jeu."
      when "Donjon"
        puts "Le Donjon ne peut pas être détruit par le Condottiere."
      when "Trésor Impérial"
        puts "Le Trésor Impérial coûte 4 pièces d'or à bâtir, mais il rapporte autant de points que vous aurez de pièces d'or en votre possession en fin de partie."
      when "Fontaine aux souhaits"
        puts "En fin de partie, la Fontaine, outre les 5 points de victoire correspondant à son coût, rapporte 1 point de victoire supplémentaire par autre bâtiment violet en votre possession."
      when "Laboratoire"
        puts "Une fois par tour, vous pouvez vous défausser d'une carte quartier de votre main et recevoir une pièce d'or en contrepartie."
      when "Cimetière"
        puts "Lorsque le Condottiere détruit un quartier, vous pouvez payer une pièce d'or pour le prendre dans votre main. Vous ne pouvez pas faire cela si vous êtes vous-même Condottiere."
      when "Carrière"
        puts "Vous pouvez bâtir un bâtiment de même nom que l'un de vos bâtiments déjà en jeu. Vous ne perdez pas les bâtiments ainsi construits si la Carrière est détruite."
      when "Manufacture"
        puts "Une fois par tour, vous pouvez payer deux pièces d'or pour piocher trois cartes."
      when "Observatoire"
        puts "Si vous choisissez de piocher des cartes au début de votre tour, vous en piochez trois, en choisissez une et défaussez les deux autres."
      when "Ecole de Magie"
        puts "Pour la perception des revenus, l'Ecole de Magie est considérée comme un quartier de la couleur de votre choix. Elle vous rapporte donc si vous êtes Roi, Evêque, Marchand, Condottiere."
      when "Bibliothèque"
        puts "Si vous choisissez de piocher des cartes au début de votre tour, vous en piochez deux et les conservez toutes les deux."
      when "Grande Muraille"
        puts "Le prix à payer par la Condottiere pour détruire vos autres quartiers est augmenté de deux."
      when "Université", "Dracoport"
        puts "Cette réalisation de prestige coûte six pièces d'or à bâtir mais vaut huit points dans le décompte de fin de partie."
      else
        puts "Ce Quartier n'a aucun pouvoir particulier."
    end
  end
end