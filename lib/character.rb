# encoding: utf-8

class Character

  attr_reader :number, :name, :cat, :power, :killed, :stolen


  def initialize (number, name, cat="Neutre")
    @number = number
    @name = name
    @cat = cat
    @killed = false
    @stolen = false
  end

  # Lexique des pouvoirs des Personnages
  def show_power

    case @name
      when "Assassin"
        puts "L'Assassin peut tuer le personnage de son choix. Celui-ci ne jouera pas ce tour-ci."
      when "Voleur"
        puts "Le Voleur peut voler le trésor du personnage de son choix.\nIl ne peut voler ni l'Assassin ni un personnage assassiné.\nLe vol prendra effet au début du tour du personnage volé."
      when "Magicien"
        puts "Au choix :\nLe Magicien peut échanger la totalité de ses cartes avec le joueur de son choix.\nOu\nLe Magicien peut échanger des cartes de sa main contre le même nombre de cartes de la pioche."
      when "Roi"
        puts "Le Roi prend la carte couronne et choisira en premier son personnage au prochain tour.\nChaque quartier noble qu'il possède lui rapporte une pièce d'or."
      when "Evêque"
        puts "L'Evêque ne peut pas être attaqué par le Condottière.\nChaque quartier religieux qu'il possède lui rapporte une pièce d'or."
      when "Marchand"
        puts "Le Marchand reçoit une pièce d'or en plus au début de son tour.\nChaque quartier marchand qu'il possède lui rapporte une pièce d'or."
      when "Architecte"
        puts "L'Architecte pioche deux cartes quartier en plus.\nIl peut bâtir jusqu'à trois quartiers."
      when "Condottiere"
        puts "Le Condottiere peut détruire un quartier de son choix (y compris un des siens) en payant à la banque le coût de construction du quartier moins un.\nChaque quartier militaire qu'il possède lui rapporte une pièce d'or."
      else
        # Pas de traitement
    end

  end

  def kill
    @killed = true
  end

  def steal
    @stolen = true
  end

end