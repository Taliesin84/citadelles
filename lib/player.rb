# encoding: utf-8

class Player

  attr_reader :name, :citadel, :score
  attr_accessor :hand, :characters, :sold, :protected, :king, :architect

  def initialize (name)
    @name = name
    @citadel = []
    @hand = []
    @sold = 2
    @characters = []
    @protected = false
    @king = false
    @architect = false
    @laboratory = false
    @factory = false
    @score = 0
  end

  # Réinitialise tous les booléens des joueurs
  def re_init
    @characters = []
    @protected = false
    @king = false
    @architect = false
  end

  # Affiche les infos relatives au Joueur
  def show_infos
    show_citadel
    show_hand
    puts "Votre solde est de : #{@sold} pièces d'or.\n\n"
    set_score
    puts "Votre score est de : #{@score} point(s).\n\n"
  end


  # Phase de jeu permettant de contruire un Quartier dans sa Citadelle
  def building
    begin
      # On permet au joueur de construire... ou pas !
      puts "Voulez-vous construire ? (O/N)"
      choice = gets.strip.to_s.upcase
      unless %W(O N).include? choice
        raise "Veuillez répondre par O ou N"
      end
    rescue
      puts "Erreur : #{$!}\n\n"
      retry
    end
    puts "\n"
    if choice == "O"
      begin
        begin
          # On vérifie si le joueur a encore de la place dans sa Citadelle pour y construire un Quartier
          if @citadel.length == 8
            raise "Votre Citadelle contient déjà 8 Quartiers ! Vous ne pouvez donc plus y construire de Quartier !"
          end

          # Le joueur choisit la carte Quartier qu'il souhaite construire dans sa Citadelle
          puts "Quel Quartier souhaitez-vous construire ?\n\n"
          i = 1
          cards = []
          for card in @hand
            # A moins que le Joueur est bâtit une Carrière sur sa Citadelle on enlève les Quartiers de même nom que ceux déjà construit
            if (@citadel.find {|dist| dist.name == "Carrière"}) or !(@citadel.find {|dist| dist.name == @hand[i-1].name})
              puts "#{i} - Quartier #{card.cat} : #{card.name} coûte #{card.cost} pièces d'or"
              cards << card
              i += 1
            end
          end
          puts "\nA - Aucun"
          district = gets.strip
          unless district.upcase == "A"
            district_to_i = 0
            district_to_i = district.match(/\d{1,}/)[0].to_i unless district.match(/\d{1,}/).nil?
            if (district_to_i < 1) or (district_to_i >= i) or (district.upcase == "A")
              raise "Le chiffre saisit doit être celui d'une de vos cartes Quartier !"
            end
            # On vérifie si le joueur a mis un "?" avant ou après son choix et on affiche le pouvoir du Quartier choisit
            if district.match(/\?/)
              @hand[district_to_i].show_power
            end
            # On vérifie si le joueur a suffisamment d'argent pour construire le Quartier choisit
            if @hand[district_to_i-1].cost > @sold
              raise "Vous ne possédez pas assez d'argent pour construire ce Quartier !"
            end
          end
        rescue
          puts "Erreur : #{$!}\n\n"
          retry
        end
      end while district.match(/\?/)
      unless district.upcase == "A"
        build cards[district_to_i-1]
      end
    end
  end

  # Ajoute la carte Quartier de la main du joueur dans sa citadelle et en déduit son coût
  # @param i [District]
  def build(dist)
    @citadel << dist
    @hand.delete_at(@hand.index(dist))
    @sold -= dist.cost
  end

  # Donne la rente des quartiers correspondants au joueur
  def rent(type)
    money = 0
    for dist in @citadel
      # Vérifie la catégorie de Quartier pour savoir s'il rapporte
      # Vérifie le cas particulier de l'Ecole de Magie (cf. district.show_power)
      if (dist.cat == type) or (dist.name == "Ecole de Magie")
        money += 1
      end
    end
    puts "Vos Quartiers de catégorie #{type} vous rapporte : #{money} pièce(s) d'or !\n\n"
    @sold += money
  end

  # Etablit le score du Joueur
  def set_score
    color = []
    miracle = false
    @score = 0
    for dist in @citadel
      @score += dist.cost
      color << dist.cat
      case dist.name
        when "Cour des miracles"
          miracle = true
        when "Trésor Impérial"
          @score += @sold - 4
        when "Fontaine aux souhaits"
          for q in @citadel
            if (q.cat == "Prestige") and (q.name != dist.name)
              @score += 1
            end
          end
        when "Université", "Dracoport"
          @score += 2
        else
          # Pas de traitement
      end
    end
    if (color.uniq.length == 5) or (miracle and (color.uniq.length == 4))
      @score += 3
    end

  end

  # Afficher la Citadelle du joueur
  # our : vérifie si on souhaite afficher sa propre Citadelle ou celle d'un autre Joueur est adapte le texte en fonction
  # @param our [Boolean]
  # cond : vérifie que la requête est fait par le Condottiere si oui retire le Donjon des Quartiers à afficher
  # @param cond [Boolean]
  def show_citadel(our=true, cond=false)
    if @citadel.empty?
      if our
        puts "Votre Citadelle n'a pas encore de Quartiers construits !\n\n"
      else
        puts "La Citadelle de #{name} n'a pas encore de Quartiers construits !\n\n"
      end
    else
      if our
        puts "Votre Citadelle :\n\n"
      else
        puts "Voici la Citadelle de #{name} :\n\n"
      end
      i = 1
      for dist in @citadel
        # On n'affiche pas le Quartier Donjon au Condottiere si ce dernier a été construit
        unless (dist.name == "Donjon") and cond
          # On augmente le prix de destruction d'une pièce d'or pour le Condottiere si le Joueur visé à construit le Quartier Grande Muraille hormis pour la carte Grande Muraille elle-même
          # Cela revient à afficher le prix normal du Quartier
          if (cond and (@citadel.find {|q| q.name == "Grande Muraille"}) and (dist.name != "Grande Muraille")) or !cond
            puts "#{i} - Quartier #{dist.cat} : #{dist.name} d'une valeur de #{dist.cost} pièces d'or"
          else
            puts "#{i} - Quartier #{dist.cat} : #{dist.name} d'une valeur de #{dist.cost-1} pièces d'or"
          end
          i += 1
        end
      end
      puts "\n"
    end
  end

  # Afficher la main du joueur
  def show_hand
    if @hand.empty?
      puts "Vous n'avez pas de Quartiers dans votre main !\n\n"
    else
      puts "Votre main :\n\n"
      i = 1
      for dist in @hand
        puts "#{i} - Quartier #{dist.cat} : #{dist.name} d'une valeur de #{dist.cost} pièces d'or"
        i += 1
      end
      puts "\n"
    end
  end

end