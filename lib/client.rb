# encoding: utf-8

require 'socket'      # Sockets are in standard library

class Client

  attr_reader :session

  def initialize name, hostname='localhost', port=2000

    @session = TCPSocket.open(hostname, port)

    while !(@session.closed?) and (serverMessage = @session.gets)
      # Affichage des messages du Serveur
      puts serverMessage
      @session.puts name

      # Déconnexion si le message du Serveur contient Googbye
      if serverMessage =~ /!disconnect!/
        @session.close
      end
    end
  end

end

client = Client.new "Phil"