# encoding: utf-8

require "gserver"
require "socket"

class GameServer < GServer

  GAME_HOST = "127.0.0.1"
  GAME_PORT = 2000

  attr_reader :id, :name, :mdp, :host, :port, :players, :nb_j_max

  def initialize name, mdp='', port=GAME_PORT, host=GAME_HOST, nb_j_max=4, *args
    super(port, host, nb_j_max, *args)
    @id = nil
    @name = name
    @mdp = mdp
    @nb_j_max = nb_j_max
    @players = %w(hebergeur)
    main_server_sending
  end

  def serve io
    action = io.gets
    puts action
    case action
      when /!refresh!/
        if action.strip.split('!')[2].to_i == "nb_max_players"
          @nb_j_max = action.strip.split('!')[3]
          # Connexion au serveur central pour informer de la modification du nombre de joueurs maximal accepté
          socket = TCPSocket.open('192.168.0.111', 2010)
          socket.puts "!refresh!#{@id}|#{@players.length}|#{@nb_j_max}"
          puts socket.gets
          socket.close
        end
        puts action
        io.puts action
      when /!shutdown!/
        # Connexion au serveur central pour informer ce dernier de l'annulation du serveur de jeu
        socket = TCPSocket.open('192.168.0.111', 2010)
        socket.puts "!del_game!#{@id}"
        socket.gets
        socket.close
        stop
      else
        puts action
    end
  end

  # Fonction permettant le référencement de la partie dans le serveur Central
  def main_server_sending sc_host='192.168.0.111', sc_port=2010
    # On ouvre le socket pour communiquer avec le serveur Central
    socket = TCPSocket.open sc_host, sc_port
    # On envoit les informations relatives au serveur de jeu et à la partie
    socket.puts "!new!#{@name}|#{@mdp}|#{@host}|#{@port}|#{@players.length}|#{@nb_j_max}"
    server_msg = socket.gets
    # Récupération de l'identifiant attribué par le Serveur Central'
    @id = server_msg.split('!')[2].strip if server_msg =~ /!id!/
    #socket.close
  end
end