# encoding: utf-8

require "gserver"
require "benchmark"

class MainGServer < GServer

  MAIN_HOST = "0.0.0.0"
  MAIN_PORT = 2010

  attr_accessor :games

  def initialize(port=MAIN_PORT, host=MAIN_HOST, *args)
    super(port, host, *args)

    @games = {}
  end

  def serve io
    action = io.gets
    case action
      when /!new!/
        puts Benchmark.measure { add_game(io, action) }
        add_game io, action
      when /!get_games!/
        io.puts get_games io
      when /!refresh!/
        game = action.strip.split('!')[2].split('|')
        refresh_game io, game
      when /!del_game!/
        game_id = action.strip.split('!')[2]
        del_game io, game_id
      else
        puts "no action corresponding"
    end
  end

  # Function permettant d'ajouter le référencement d'un Serveur de jeu
  def add_game io, action
    game = action.strip.split('!')[2].split('|')
    id = gen_id
    @games[id] = game
    puts "Nouveau Serveur : #{id} - #{game[0]} - #{game[2]}/#{game[3]}"
    puts "Nous avons actuellement #{@games.size} serveur(s) de jeu connecté(s) !"

    # Attribution d'un identifiant unique au Serveur de Jeu
    io.puts "!id!#{id}"
  end

  # Supprime un serveur de jeu du référencement central
  def del_game io, game_id
    game = @games[game_id]
    # Suppression du référencement du Serveur de Jeu
    @games.delete(game_id)
    # Message au Serveur de Jeu
    io.puts "Serveur #{game[0]} supprimé !"
    # Messages d'informations du Serveur Central
    puts "Serveur #{game[0]} supprimé !"
    puts "Nous avons actuellement #{@games.size} serveur(s) de jeu connecté(s) !"
  end

  # Consultation de la liste des serveurs de jeu
  def get_games io
    for id, game in @games
      io.puts "#{id}|#{game[0]}|#{game[1]}|#{game[2]}|#{game[3]}|#{game[4]}|#{game[5]}"
    end
  end

  # Rafraichissement de la liste des serveurs de jeu
  def refresh_game io, game
    @games[game[0]][3] = game[1]
    @games[game[0]][4] = game[2]
    io.puts "Les informations de votre serveur ont été mises à jour !"
  end

  private

  # Générateur d'identifiant unique
  def gen_id
    begin
      o = [('a'..'z'),('A'..'Z'),(1..9)].map{|i| i.to_a}.flatten
      id = (0..8).map{ o[rand(o.length)] }.join
    end while @games.has_key? id
    return id
  end

end