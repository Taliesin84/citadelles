# encoding: utf-8

require 'eventmachine'

module TchatServer

  def post_init
    $clients_list ||= {}
    @id = self.object_id
    $clients_list.merge!({@id => self})
  end

  def receive_data data
    for client in $clients_list.values
      client.send_data data
    end
  end

  def unbind
    $clients_list.delete @id
    send_data "Au revoir !"
  end

end

#EventMachine::run do
#  host = '0.0.0.0'
#  port = 2001
#  EventMachine::start_server host, port, TchatServer
#  puts "Serveur de Tchat démarré sur #{host}:#{port}..."
#end
