# encoding: utf-8

require 'eventmachine'

module MainEventServer

  attr_reader :id

  def post_init
    send_data "Bienvenue sur le Serveur Central de jeu de Citadelles !"
    @id = self.object_id
  end

  def receive_data data
    case data
      when /!new!/
        add_game data
      when /!get_games!/
        send_data get_games
      when /!refresh!/
        game = data.split('!')[2].strip
        send_data refresh_game game
      when /quit/i
        close_connection
      else
        puts data
    end

    def unbind
      del_game
    end

  end

  # Function permettant d'ajouter le référencement d'un Serveur de jeu
  def add_game data
    @game = data.split('!')[2].split('|')
    $games[@id] = @game
    @main_server_ui.infos.append "Nouveau Serveur : #{@id} - #{@game[0]} - #{@game[1]}/#{@game[2]}"
    @main_server_ui.infos.append "Nous avons actuellement #{$games.size} serveurs de jeu connectés !"

    # Ensemble des messages envoyés au serveur de jeu
    send_data "!id!#{@id}"
    send_data "Votre partie a bien été validée !"
    send_data "Voici les informations relatives à votre serveur :"
    send_data "#{@id} - #{@game[0]} - #{@game[1]}/#{@game[2]}"
    send_data "!disconnect!"
  end

  # Fonction permettant de consulter la liste des serveurs de jeu
  def get_games
    for id, game in $games
      send_data "#{id}|#{game[0]}|#{game[1]}|#{game[2]}|#{game[3]}|#{game[4]}|#{game[5]}"
    end
    send_data "!disconnect!"
  end

  def refresh_game(game)
    $games[game[0]][3] = game[3]
    $games[game[0]][4] = game[4]
    send_data "!disconnect!"
  end

  def del_game
    @main_server_ui.infos.append "Serveur #{$games[@id][0]} supprimé !"
    $games.delete(@id)
    @main_server_ui.infos.append "Nous avons actuellement #{$games.size} serveurs de jeu connectés !"
  end

end
=begin
EventMachine::run do
  host = '0.0.0.0'
  port = 2010
  $games = {}
  EventMachine::start_server host, port, MainEventServer
  puts "Started Central Event Server on #{host}:#{port}..."
end
=end