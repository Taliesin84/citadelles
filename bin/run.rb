# encoding: utf-8

require 'Qt4'
require './gui/interface/main'
require 'resolv-replace'

app = Qt::Application.new(ARGV)

locale = Qt::Locale::system().name().split('_')[0]
qt_translator = Qt::Translator.new
qt_translator.load "qt_#{locale}", "./translations/#{locale}/"
app.installTranslator qt_translator

main = Main.new
main.show

app.exec