# encoding: utf-8

require 'Qt4'
require "./helpers/graphics_helpers"
require './gui/game/display_card'
require './gui/game/display_cards'
require './gui/game/card_selection'
require './gui/game/player_selection'
require './gui/game/submit_banner'
require './gui/game/submit_button'
require './gui/game/ranking'
require './gui/game/podium'
require './gui/game/action_panel_disp'
require './gui/game/routing'

class ActionPanel < Qt::GraphicsItem

  include ActionPanelDisp
  include Routing

  attr_reader :name
  attr_accessor :current_player, :current_char

  def initialize parent=nil
    super(parent)
    @nb_turn = 1
    @name = ""
    @i_char = 0
    @i_player = 0
    @current_char = nil
    @current_player = nil
    setZValue 10
  end

  def boundingRect
    Qt::RectF.new -$scene_width/2, -$scene_height/2, $scene_width, $scene_height
  end

  # Remet à zéro tous les indicateurs
  def init_indicators
    # On remet les indicateurs de parcours à zéro
    @i_char = 0
    @i_player = 0
    # On remet les indicateurs de palier à zéro
    @_gold_vs_card = false
    @_build = false
    # On re-initialise les indicateurs de consommation des pouvoirs des 'Quartiers'
    init_power_dist
  end

  # Re-initialise les indicateurs de consommation des pouvoirs des 'Quartiers'
  def init_power_dist
    lab_card = scene.dist_deck_dup.find {|dist| dist.id == "lab"}
    lab_card.power_used = false
    man_card = scene.dist_deck_dup.find {|dist| dist.id == "mnf"}
    man_card.power_used = false
  end

  # Réinitialise le Deck des 'Personnages'
  def init_char_deck
    # On réinitialise le Deck des 'Personnages'
    for char in scene.char_deck_dup
      char.re_init
      char.setParentItem nil
      char.hide
    end
    scene.char_deck = scene.char_deck_dup.dup
  end

  # Initialise les paramètres du tour
  def init_turn
    @i_player = 0
    init_char_deck
    # Réorganise l'ordre des Joueurs pour mettre le 'Roi' en première position
    until scene.players[0].king
      player = scene.players.shift
      scene.players << player
    end
    @current_player = scene.players[@i_player]
    @current_player.hand.hide
    @current_player.set_current true
    # Définit le nombre de 'Personnages' défaussés avant le premier choix des Joueurs
    if $nb_char == 8
      case $nb_players
        when 4
          nb_cards = 2
        when 5
          nb_cards = 1
        else
          nb_cards = 1
      end
    elsif $nb_char == 9
      case $nb_players
        when 4
          nb_cards = 3
        when 5
          nb_cards = 2
        when 6
          nb_cards = 1
        else
          nb_cards = 1
      end
    end
    # On défausse les cartes face visible
    (nb_cards-1).times {
      card = scene.char_deck[rand(scene.char_deck.length)]
      if card.id=="roi" and !$toss_king
        while card.id == "roi"
          card = scene.char_deck[rand(scene.char_deck.length)]
        end
      end
      discard_char card.id
    }
    # On défausse la carte secrète avant le début des choix des personnages par les Joueurs
    card = scene.char_deck[rand(scene.char_deck.length)]
    discard_char card.id

    if nb_cards == 1
      disp_secret_card
    else
      disp_discarded_card
    end
  end

  # Passe au tour suivant
  def next_turn
    # On incrémente l'indicateur du nombre de tour
    @nb_turn += 1
    scene.history.insert_turn @nb_turn
    # On remet les indicateurs à zéro
    init_indicators
    #On vérifie si le 'Roi' faisait partis de la 'Défausse'
    found_discard_king = scene.discard_char.cards.find {|char| char.id == "roi"}
    # On réinitialise le Deck des 'Personnages'
    scene.discard_char.cards = []
    for player in scene.players
      player.citadel.characters = []
    end
    for char in scene.char_deck_ref
      char.re_init
      char.setParentItem nil
      char.hide
    end
    init_char_deck
    # On initialise le nouveau tour
    if found_discard_king and $toss_king
      @current_player = scene.players[0]
      disp_turn_toss
    else
      disp_turn_choose_char
    end
  end

  # Permet de changer de joueur temporairement
  def toggle_player new_player_id
    @current_player.hand.hide
    @current_player.set_current false
    @current_player = scene.players.find {|player| player.id == new_player_id}
    @current_player.set_current true
  end

  # Définit le Joueur suivant
  def next_player
    @current_player.hand.hide
    @current_player.set_current false
    @i_player < $nb_players-1 ? @i_player+=1 : @i_player=0
    @current_player = scene.players[@i_player]
    @current_player.set_current true
  end

  # Appelle le prochain 'Personnage'
  def call_next_char
    @i_char += 1 unless @name == "disp_turn_call_char"
    # On remet les indicateurs de palier à zéro
    @_gold_vs_card = false
    @_build = false
    @nb_build = nil
    @nb_embellish = nil
    # On désactive le Joueur précédent
    unless @current_player.nil?
      @current_player.hand.hide
      @current_player.set_current false
    end
    # On vérifie qu'on n'a pas appelé tous les 'Personnages'
    if @i_char < $nb_char
      # On initialise la carte 'Personnage'
      unless @current_char.nil?
        @current_char.re_init
      end
      # On récupère la carte du prochain 'Personnage'
      @current_char = scene.char_deck_ref[@i_char]
      char_dup = scene.char_deck_dup.find {|char| char.id == @current_char.id}
      # On vérifie si le 'Personnage' appelé n'a pas été assassiné
      if char_dup.killed and $end_killed_char
        @current_player = nil
      else
        # On vérifie que le 'Personnage' appelé appartient à un Joueur
        if char_dup.parentItem.class.to_s == "Citadel"
          # On définit le Joueur appelé
          @current_player = scene.players.find {|player| player.citadel.characters.find {|char| char.id == @current_char.id}}
          @current_player.set_current true
          # On cherche la carte du 'Personnage' appelé dans la 'Citadelle' du Joueur correspondant afin de la révéler
          char = @current_player.citadel.characters.find {|char| char.id == @current_char.id}
          history_player_name = "<span style='color:rgb(#{$player_rgb_colors[@current_player.id-1]});'>#{@current_player.name}</span>"
        else
          # Si le 'Personnage' appelé est dans la défausse on ne définit aucun Joueur
          @current_player = nil
          # On cherche la carte du 'Personnage' appelé dans la 'Défausse' afin de la révéler
          char = scene.discard_char.cards.find {|char| char.id == @current_char.id}
          history_player_name = "Défaussé(e)"
        end
        # On révèle la carte aux autres Joueurs
        char.reveal
      end
      victim_action = nil
      # On transfert les indicateurs du 'Personnage' appelé à la carte de référence
      @current_char.killed = char_dup.killed
      victim_action = "Assassiné(e)" if char_dup.killed
      @current_char.stolen = char_dup.stolen
      victim_action = "Volé(e)" if char_dup.stolen
      @current_char.bewitched = char_dup.bewitched
      victim_action = "Ensorcelé(e)" if char_dup.bewitched
      # On ajoute l'appel à l'historique
      scene.history.insert_call @current_char.number, @current_char.name, history_player_name, victim_action
      @current_char.reveal
      # On invite le Joueur à jouer son tour
      king_call_player
    else
      # On vérifie s'il n'y a pas un 'Personnage' assassiné à révéler
      killed_char = scene.char_deck_dup.find {|char| char.killed}
      if $end_killed_char and !killed_char.nil?
        killed_char.reveal # Révélation de la carte du 'Personnage' assassiné
        disp_killed_player killed_char # Révélation du Joueur assassiné
      else
        redirect_results
      end
    end
  end

  # Retire la carte tirée des choix du tirage au sort
  def discard_toss_char char_id
    scene.char_deck.delete_if {|card| card.id == char_id}
  end

  # Défausse le 'Personnage'
  def discard_char char_id
    scene.discard_char.add_card char_id
  end

  # Réinitialise la 'Pioche' avec la 'Défausse'
  def re_inject_discarded_card
    discarded_card = scene.discard_char.cards[0]
    discarded_card.re_init
    discarded_card.setParentItem nil
    discarded_card.hide
    scene.char_deck << discarded_card
    scene.discard_char.cards = []
  end

  # Ajoute le 'Personnage' à la 'Citadelle' du Joueur
  def add_char char_id
    char = scene.char_deck.delete_at(scene.char_deck.find_index {|card| card.id == char_id})
    @current_player.citadel.add_character char
  end

  # Intrônise le Joueur
  def enthrone new_king_id
    found_sdt = nil
    for player in scene.players
      # On vérifie si le Joueur a bâtit le 'Quartier' 'Salle du Trône'
      found_sdt = player if player.citadel.districts.find {|dist| dist.id == "sdt"}
      if player.id == new_king_id
        player.king ? player_diff=false : player_diff=true
        player.enthrone
        # On ajoute l'action à l'historique
        scene.history.insert_action "<span style='color:rgb(#{$player_rgb_colors[player.id-1]});'>#{player.name}</span> récupère la couronne.", true
      else
        player.dethrone
      end
    end
    if player_diff and !found_sdt.nil?
      found_sdt.earn 1
      # On ajoute l'action à l'historique
      scene.history.insert_action "<span style='color:rgb(#{$player_rgb_colors[found_sdt.id-1]});'>#{found_sdt.name}</span> gagne 1 pièce d'or grâce à la 'Salle du trône'.", true
    end

  end

  # Applique la taxe religieuse de l''Abbe'
  def religious_tax apply
    a_wealthy_player = scene.players.max_by {|player| player.citadel.sold}
    wealthy_players = scene.players.find_all {|player| player.citadel.sold == a_wealthy_player.citadel.sold}
    unless wealthy_players.find {|player| player.id == @current_player.id}
      if wealthy_players.length > 1 and $all_wealthy_pay
        for wealthy_player in wealthy_players
          if apply
            wealthy_player.pay 1
            # On ajoute l'action à l'historique
            scene.history.insert_action "<span style='color:rgb(#{$player_rgb_colors[wealthy_player.id-1]});'>#{wealthy_player.name}</span> paie la taxe religieuse d'un montant d'une pièce d'or à #{@current_player.name}."
          end
        end
      elsif wealthy_players.length == 1
        if apply
          wealthy_players[0].pay 1
          # On ajoute l'action à l'historique
          scene.history.insert_action "<span style='color:rgb(#{$player_rgb_colors[wealthy_players[0].id-1]});'>#{wealthy_players[0].name}</span> paie la taxe religieuse d'un montant d'une pièce d'or à #{@current_player.name}."
        end
        @money += 1
      end
    end
  end

  # Définit les gains du Joueur
  def set_amount apply_religious_tax=false
    # On calcul combien rapporte le 'Personnage' en fonction de sa catégorie
    @money = 0
    for dist in @current_player.citadel.districts
      @money += 1 if dist.cat == @current_char.cat
    end
    @money += 1 if @current_player.citadel.districts.find {|dist| dist.id == "edm"}
    @money += 1 if @current_char.id == "marchand"
    religious_tax apply_religious_tax if @current_char.id == "abbe"
  end

  # Ajoute la somme au solde du Joueur
  def gold_earned money, player_id, msg_type, dist_name=""
    receiver = scene.players.find {|player| player.id == player_id}
    receiver.earn money
    # On ajoute l'action à l'historique
    case msg_type
      when "std"
        scene.history.insert_action "<span style='color:rgb(#{$player_rgb_colors[@current_player.id-1]});'>#{@current_player.name}</span> a choisi de prendre 2 pièces d'or."
      when "char"
        scene.history.insert_action "Le 'Personnage' #{@current_char.name.capitalize} rapporte #{money} pièce(s) d'or à <span style='color:rgb(#{$player_rgb_colors[receiver.id-1]});'>#{receiver.name}</span>."
      when "dist"
        scene.history.insert_action "Le 'Quartier' #{dist_name.capitalize} rapporte #{money} pièce d'or à <span style='color:rgb(#{$player_rgb_colors[receiver.id-1]});'>#{receiver.name}</span>."
      when "crown"
        scene.history.insert_action "<span style='color:rgb(#{$player_rgb_colors[receiver.id-1]});'>#{receiver.name}</span> a recu une pièce d'or du nouveau possesseur de la couronne."
      when "sailor"
        scene.history.insert_action "<sp an style='color:rgb(#{$player_rgb_colors[receiver.id-1]});'>#{receiver.name}</span> a choisit de prendre 4 pièces d'or'."
      when "discard"
        scene.history.insert_action "<span style='color:rgb(#{$player_rgb_colors[receiver.id-1]});'>#{receiver.name}</span> a choisit de défausser une carte 'Quartier' de sa main contre 1 pièce d'or'."
      else
        # Do nothing
    end
  end

  # Supprime la somme volée du solde du Joueur visé
  def gold_stolen money, thief_id
    # On débite le solde de la victime
    @current_player.pay money
    thief = scene.players.find {|player| player.citadel.characters.find{|char| char.id == thief_id}}
    # On crédite le solde du 'Voleur'
    thief.earn money
    # On ajoute l'action à l'historique
    scene.history.insert_action "<span style='color:rgb(#{$player_rgb_colors[thief.id-1]});'>#{thief.name}</span> a volé #{money} pièce(s) d'or à <span style='color:rgb(#{$player_rgb_colors[@current_player.id-1]});'>#{@current_player.name}</span>."
  end

  # Paye la somme
  def gold_spend money, player_id
    buyer = scene.players.find {|player| player.id == player_id}
    buyer.pay money
  end

  # Ajoute une carte à la 'Main du Joueur'
  def add_dist_to_hand dist_id
    @current_player.add_card dist_id
  end

  # Défausse la carte de la 'Main du Joueur'
  def discard_dist_from_hand card_id
    @current_player.hand.remove_card card_id
    scene.discard_dist.add_card card_id
  end

  # Défausse des cartes de la 'Pioche'
  def discard_dist_from_draw
    card = scene.draw_dist.draw_card
    scene.discard_dist.add_card card.id
  end

  # Ajoute une carte au 'Quartier' 'Musée'
  def discard_dist_to_museum card_id
    @current_player.hand.remove_card card_id
    museum = @current_player.citadel.districts.find {|dist| dist.id == "mus"}
    museum.add_card card_id
    # On ajoute l'action à l'historique
    scene.history.insert_action "<span style='color:rgb(#{$player_rgb_colors[@current_player.id-1]});'>#{@current_player.name}</span> a ajouté une carte 'Quartier' à son 'Musée'."
  end

  # Met à jour le marqueur d'assassinat sur le 'Personnage'
  def kill card_id
    char_dup = scene.char_deck_dup.find {|char| char.id == card_id}
    char_dup.killed = true
    # On ajoute l'action à l'historique
    scene.history.insert_action "<span style='color:rgb(#{$player_rgb_colors[@current_player.id-1]});'>#{@current_player.name}</span> assassine le 'Personnage' '#{char_dup.name.capitalize}'."
  end

  # Met à jour le marqueur d'ensorcelement sur le 'Personnage'
  def bewitch card_id
    char_dup = scene.char_deck_dup.find {|char| char.id == card_id}
    char_dup.bewitched = true
    # On ajoute l'action à l'historique
    scene.history.insert_action "<span style='color:rgb(#{$player_rgb_colors[@current_player.id-1]});'>#{@current_player.name}</span> ensorcèle le 'Personnage' '#{char_dup.name.capitalize}'."
  end

  # Met à jour le marqueur de vole sur le 'Personnage'
  def steal card_id
    char_dup = scene.char_deck_dup.find {|char| char.id == card_id}
    char_dup.stolen = true
    # On ajoute l'action à l'historique
    scene.history.insert_action "<span style='color:rgb(#{$player_rgb_colors[@current_player.id-1]});'>#{@current_player.name}</span> vole le 'Personnage' '#{char_dup.name.capitalize}'."
  end

  # Echange toutes les cartes de la 'Main du Joueur' avec celle d'un autre Joueur
  def exchange_all player_id
    victim = scene.players.find {|player| player.id == player_id}
    prev_victim_hand = victim.hand.cards.dup
    prev_current_player_hand = @current_player.hand.cards.dup
    for card in prev_current_player_hand
      @current_player.remove_card card.id
      victim.add_card card.id
    end
    for card in prev_victim_hand
      victim.remove_card card.id
      @current_player.add_card card.id
    end
    # On ajoute l'action à l'historique
    scene.history.insert_action "<span style='color:rgb(#{$player_rgb_colors[@current_player.id-1]});'>#{@current_player.name}</span> échange toutes sa main avec celle de <span style='color:rgb(#{$player_rgb_colors[victim.id-1]});'>#{victim.name}</span>."
  end

  # Echange la carte de la 'Main du Joueur' avec une carte de la 'Pioche'
  def exchange_draw card_id
    @current_player.discard_card card_id, scene.discard_dist
    # On ajoute l'action à l'historique
    scene.history.insert_action "<span style='color:rgb(#{$player_rgb_colors[@current_player.id-1]});'>#{@current_player.name}</span> a échangé une carte 'Quartier' de sa main avec la pioche."
  end

  # Détruit le 'Quartier'
  def destruct_dist card_id, pay=true
    dist = scene.dist_deck_dup.find {|card| card.id == card_id}
    if pay
      # On vérifie si le Joueur ne possède pas le 'Quartier' 'Grande Muraille'
      (dist.parentItem.districts.find {|card| card.id == "gm"}) ? great_wall=1 : great_wall=0
      destruct_cost = dist.cost+great_wall+dist.upgrade-1
      @current_player.pay destruct_cost
      # On ajoute l'action à l'historique
      victim_name = dist.parentItem.player_name
      victim_id = dist.parentItem.player_id
      scene.history.insert_action "<span style='color:rgb(#{$player_rgb_colors[@current_player.id-1]});'>#{@current_player.name}</span> a détruit le 'Quartier' '#{dist.name.capitalize}' de la 'Citadelle' de <span style='color:rgb(#{$player_rgb_colors[victim_id-1]});'>#{victim_name}</span> pour un coût de #{destruct_cost} pièce(s) d'or."
    else
      scene.history.insert_action "<span style='color:rgb(#{$player_rgb_colors[@current_player.id-1]});'>#{@current_player.name}</span> a détruit le 'Quartier' '#{dist.name.capitalize}' de la 'Citadelle' de <span style='color:rgb(#{$player_rgb_colors[victim_id-1]});'>#{victim_name}</span>."
    end
    dist.parentItem.destruct_dist dist
    # On réinitialise la carte de référence correspondante
    dist_ref = scene.dist_deck_ref.find {|card| card.id == card_id}
    dist_ref.re_init
    # On vérifie si le 'Quartier' détruit n'est pas le 'Beffroi'
    if dist.id == "bef"
      change_max_dist +1
      # On ajoute l'action à l'historique
      scene.history.insert_action "Le nombre de 'Quartiers' a bâtir pour achever la partie passe à #{$max_dist}.", true
    end
  end

  # Echange 2 'Quartiers' de 2 'Citadelles'
  def exchange_citadel_dist diplomat_dist_id, victim_dist_id
    diplomat_dist = scene.dist_deck_dup.find {|card| card.id == diplomat_dist_id}
    victim_dist = scene.dist_deck_dup.find {|card| card.id == victim_dist_id}
    victim = scene.players.find {|player| player.id == victim_dist.parentItem.player_id}
    # Vérifie si la variante du paiement de différence du coût est activée
    if $pay_diff
      # Calcul de la différence de coût entre les 2 cartes
      (victim_dist.parentItem.districts.find {|dist| dist.id == "gm"}) ? great_wall=1 : great_wall=0
      diff = victim_dist.cost + victim_dist.upgrade + great_wall - diplomat_dist.cost
      # On vérifie si le 'Quartier' échanger n'est pas plus onéreux
      if diff > 0
        # Paiement de la différence à la victime
        @current_player.pay diff
        victim.earn diff
      end
      # On ajoute l'action à l'historique
      scene.history.insert_action "<span style='color:rgb(#{$player_rgb_colors[@current_player.id-1]});'>#{@current_player.name}</span> a échangé son 'Quartier' '#{diplomat_dist.name.capitalize} avec le 'Quartier' '#{victim_dist.name.capitalize}' de <span style='color:rgb(#{$player_rgb_colors[victim.id-1]});'>#{victim.name}</span> pour un coût de #{diff} pièxces(s) d'or."
    else
      # On ajoute l'action à l'historique
      scene.history.insert_action "<span style='color:rgb(#{$player_rgb_colors[@current_player.id-1]});'>#{@current_player.name}</span> a échangé son 'Quartier' '#{diplomat_dist.name.capitalize} avec le 'Quartier' '#{victim_dist.name.capitalize}' de <span style='color:rgb(#{$player_rgb_colors[victim.id-1]});'>#{victim.name}</span>."
    end
    # On récupère les index des cartes pour les attribuer aux nouvelles cartes
    victim_i_card = victim_dist.i_card
    victim_dist.i_card = diplomat_dist.i_card
    diplomat_dist.i_card = victim_i_card
    # On échange les cartes
    @current_player.citadel.set_card_position victim_dist
    victim.citadel.set_card_position diplomat_dist
    # On met à jour les scores des 2 protagonistes
    @current_player.citadel.set_score
    victim.citadel.set_score
  end

  # Embellit un 'Quartier'
  def embellish_dist card_id
    dist = scene.dist_deck_dup.find {|card| card.id == card_id}
    dist_ref = scene.dist_deck_ref.find {|card| card.id == card_id}
    @current_player.pay 1
    dist.embellish
    dist_ref.embellish
    @current_player.citadel.set_score
    # On ajoute l'action à l'historique
    scene.history.insert_action "<span style='color:rgb(#{$player_rgb_colors[@current_player.id-1]});'>#{@current_player.name}</span> a embellit son 'Quartier' '#{dist_ref.name.capitalize}' d'une pièce d'or."
  end

  # Construit le 'Quartier'
  def build_dist card_id
    @current_player.build card_id
    dist_ref = scene.dist_deck_ref.find {|card| card.id == card_id}
    # On ajoute l'action à l'historique
    scene.history.insert_action "<span style='color:rgb(#{$player_rgb_colors[@current_player.id-1]});'>#{@current_player.name}</span> a bâtit le 'Quartier' '#{dist_ref.name.capitalize}'."
    # On récupère l'argent de la construction du 'Quartier ' si on incarne l''Alchimiste'
    #todo permettre l'utilisation du pouvoir de l'alchimiste par la sorcière
    if @current_char == "alchimiste" and !@current_char.bewitched
      @current_player.earn dist_ref.cost
      # On ajoute l'action à l'historique
      scene.history.insert_action "<span style='color:rgb(#{$player_rgb_colors[@current_player.id-1]});'>#{@current_player.name}</span> récupère les frais de sa construction d'un montant de #{dist_ref.cost} pièce(s) d'or."
    end
  end

  # Change le nombre de 'Quartiers' à bâtir pour remporter la victoire
  def change_max_dist i
    $max_dist += i
    # On réinitialise le dessin des zones de chaque 'Citadelle'
    for player in scene.players
      player.citadel.re_init_card_zone
    end
    # On ajoute l'action à l'historique
    scene.history.insert_action "Le nombre de 'Quartiers' a bâtir pour achever la partie passe à #{$max_dist}.", true
  end

  # Permet d'ajouter un message personnaliser à l'historique et gérer la synchronisation de se dernier avec les autres joueurs
  def add_history_action text, italic=false
    scene.history.insert_action text, italic
  end

  # Réinitialise l'écran après validation d'une action
  def ending_action
    for item in childItems
      # On vide tous les conteneurs
      if %w(CardSelection DisplayCard DisplayCards).include? item.class.to_s
        item.re_init
      end
      # On désolidarise chaque 'item' de son 'parent'
      item.setParentItem nil unless item == @head_txt
      # On supprime définitivement l'item de la mémoire'
      scene.removeItem item
    end
  end

end