# encoding: utf-8

require 'Qt4'
require './helpers/graphics_helpers'
require './helpers/table_helpers'
require './helpers/parameters'

class Toggle < Qt::GraphicsPixmapItem

  include ToggleFunctions

  def initialize
    super()
    # On charge l'icône du bouton de basculement
    toggle_pix = Qt::Pixmap.new "./img/toggle.png"
    setPixmap toggle_pix
    # On positionne le bouton de basculement
    x = ($scene_width/2) - toggle_pix.width
    y = ($scene_height/2) - toggle_pix.height
    setPos x, y
    setZValue 11
    # Définit le contenu de la bulle d'aide
    setToolTip "Basculement Table/Action(s)"
    setAcceptedMouseButtons Qt::LeftButton
    # On accepete de récupérer de manière indépendante les évènements de type survol de la souris
    setAcceptHoverEvents true
    # Création des effets associés
    create_hover_effect
  end

  # Gestionnaire des évènements de type clique souris
  def mousePressEvent event
    action_toggle scene
  end

  def hoverMoveEvent event
    hoverEnterEvent event
  end

  def hoverEnterEvent event
    setCursor Qt::Cursor.new(Qt::PointingHandCursor)
    @hover_circle.show
  end

  def hoverLeaveEvent event
    @hover_circle.hide
  end

  # Création des effets
  def create_hover_effect
    # Caractéristiques de la ligne du pourtour de l'icône lors d'un survol
    pen = Qt::Pen.new $red
    pen.setWidth 3
    pen.setStyle(Qt::SolidLine)
    pen.setCapStyle(Qt::RoundCap)
    pen.setJoinStyle(Qt::RoundJoin)

    # Dessine un pourtour autour de l'icône
    @hover_circle = Qt::GraphicsEllipseItem.new 19, 19, 90, 90, self
    @hover_circle.setPen pen
    @hover_circle.hide
  end

end

class HistoryButton < Qt::GraphicsPixmapItem

  include ToggleFunctions

  def initialize
    super()

    # On instancie le bouton de basculement comme n'ayant pas effectué de basculement
    @activate = false
    # On charge l'icône du bouton de basculement
    history_pix = Qt::Pixmap.new "./img/history.png"
    setPixmap history_pix
    # On positionne le bouton de basculement
    x = ($scene_width/2) - history_pix.width
    y = -$scene_height/2
    setPos x, y
    setZValue 11
    setToolTip "Historique de la partie"
    setAcceptedMouseButtons Qt::LeftButton
    # On accepete de récupérer de manière indépendante les évènements de type survol de la souris
    setAcceptHoverEvents true

    create_hover_effect
  end

  def reset
    @activate = false
  end

  # Gestionnaire des évènements de type clique souris
  def mousePressEvent event
    history_toggle scene
  end

  def hoverMoveEvent event
    hoverEnterEvent event
  end

  def hoverEnterEvent event
    setCursor Qt::Cursor.new(Qt::PointingHandCursor)
    @hover_circle.show
  end

  def hoverLeaveEvent event
    @hover_circle.hide
  end

  # Création des effets
  def create_hover_effect

    # Caractéristiques de la ligne du pourtour de l'icône lors d'un survol
    pen = Qt::Pen.new $red
    pen.setWidth 2
    pen.setStyle(Qt::SolidLine)
    pen.setCapStyle(Qt::RoundCap)
    pen.setJoinStyle(Qt::RoundJoin)

    # Dessine un pourtour autour de l'icône
    @hover_circle = Qt::GraphicsEllipseItem.new 9, 9, 45, 45, self
    @hover_circle.setPen pen
    @hover_circle.hide
  end
end

class ChatButton < Qt::GraphicsPixmapItem

  include ToggleFunctions

  def initialize
    super()

    # On instancie le bouton de basculement comme n'ayant pas effectué de basculement
    @activate = false
    # On charge l'icône du bouton de basculement
    chat_pix = Qt::Pixmap.new "./img/chat.png"
    chat_pix = chat_pix.scaled $scene_height/10, $scene_height/10, Qt::IgnoreAspectRatio, Qt::SmoothTransformation
    setPixmap chat_pix
    # On positionne le bouton de basculement
    x = -$scene_width/2 + 16
    y = ($scene_height/2) - boundingRect.height - 16
    setPos x, y
    setZValue 11
    setShapeMode 1
    setToolTip "Zone de Clavardage"
    setAcceptedMouseButtons Qt::LeftButton
    # On accepete de récupérer de manière indépendante les évènements de type survol de la souris
    setAcceptHoverEvents true

    create_hover_effect
  end

  def reset
    @activate = false
  end

  # Gestionnaire des évènements de type clique souris
  def mousePressEvent event
    chat_toggle scene
  end

  def hoverMoveEvent event
    hoverEnterEvent event
  end

  def hoverEnterEvent event
    setCursor Qt::Cursor.new(Qt::PointingHandCursor)
    @hover_border.show
  end

  def hoverLeaveEvent event
    @hover_border.hide
  end

  # Création des effets
  def create_hover_effect

    # Caractéristiques de la ligne du pourtour de l'icône lors d'un survol
    pen = Qt::Pen.new $red
    pen.setWidth 2
    pen.setStyle(Qt::SolidLine)
    pen.setCapStyle(Qt::RoundCap)
    pen.setJoinStyle(Qt::RoundJoin)

    # Dessine un pourtour autour de l'icône
    painter = Qt::PainterPath.new
    painter.addRoundedRect 0.0, 0.0, boundingRect.width.to_f, boundingRect.height.to_f, 5.0, 5.0
    @hover_border = Qt::GraphicsPathItem.new painter, self
    @hover_border.setPen pen
    @hover_border.hide
  end
end

class CharPowerButton < Qt::GraphicsPixmapItem

  include ToggleFunctions

  def initialize
    super()
    # On charge l'icône du bouton de basculement
    char_power_pix = Qt::Pixmap.new "./img/char_power.png"
    char_power_pix = char_power_pix.scaled $scene_height/10, $scene_height/10, Qt::IgnoreAspectRatio, Qt::SmoothTransformation
    setPixmap char_power_pix
    # On positionne le bouton de basculement
    x = -$scene_width/2 + 16
    y = -(($scene_height/2) - 16)
    setPos x, y
    setZValue 11
    setShapeMode 1
    setToolTip "Pouvoirs des Personnages"
    setAcceptedMouseButtons Qt::LeftButton
    # On accepete de récupérer de manière indépendante les évènements de type survol de la souris
    setAcceptHoverEvents true

    create_hover_effect
  end

  # Gestionnaire des évènements de type clique souris
  def mousePressEvent event
    char_power_toggle scene
  end

  def hoverMoveEvent event
    hoverEnterEvent event
  end

  def hoverEnterEvent event
    setCursor Qt::Cursor.new(Qt::PointingHandCursor)
    @hover_border.show
  end

  def hoverLeaveEvent event
    @hover_border.hide
  end

  # Création des effets
  def create_hover_effect

    # Caractéristiques de la ligne du pourtour de l'icône lors d'un survol
    pen = Qt::Pen.new $red
    pen.setWidth 2
    pen.setStyle(Qt::SolidLine)
    pen.setCapStyle(Qt::RoundCap)
    pen.setJoinStyle(Qt::RoundJoin)

    # Dessine un pourtour autour de l'icône
    painter = Qt::PainterPath.new
    painter.addRoundedRect 0.0, 0.0, boundingRect.width.to_f, boundingRect.height.to_f, 5.0, 5.0
    @hover_border = Qt::GraphicsPathItem.new painter, self
    @hover_border.setPen pen
    @hover_border.hide
  end
end