# encoding: utf-8

require 'Qt4'

class DispCharPower < Qt::GraphicsItem

  def initialize
    super()
    set_heading "Liste des pouvoirs des 'Personnages'"
    disp_cards
    setZValue 10
    # On empêche le traitement en indépendance de la scène des évènements souris
    setAcceptedMouseButtons Qt::NoButton
    hide
  end

  def boundingRect
    Qt::RectF.new -$scene_width/2, -$scene_height/2, $scene_width, $scene_height
  end

  # Définit La zone d'instruction
  def set_heading txt_head
    head = Qt::GraphicsTextItem.new txt_head, self
    head.setDefaultTextColor $main_font_color
    head_font = Qt::Font.new $main_font, $scene_height/20
    head.setFont head_font
    x = -head.boundingRect.width/2
    y = -$scene_height/2 + $scene_height/10
    head.setPos x, y
  end

  # Affiche les cartes d'aide
  def disp_cards
    card_1_pix = Qt::Pixmap.new "./img/cards/others/infos-1.png"
    card_1 = Qt::GraphicsPixmapItem.new card_1_pix, self
    card_1.setTransformationMode Qt::SmoothTransformation
    card_1.scale $card_height/card_1.boundingRect.width, $card_width/card_1.boundingRect.height
    card_1.setPos -$card_height-$area_space/2, -$card_width/2
    card_2_pix = Qt::Pixmap.new "./img/cards/others/infos-2.png"
    card_2 = Qt::GraphicsPixmapItem.new card_2_pix, self
    card_2.setTransformationMode Qt::SmoothTransformation
    card_2.scale $card_height/card_2.boundingRect.width, $card_width/card_2.boundingRect.height
    card_2.setPos $area_space/2, -$card_width/2
  end

end