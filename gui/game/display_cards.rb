# encoding: utf-8

require 'Qt4'

class DisplayCards < Qt::GraphicsItem

  attr_reader :cards

  def initialize parent=nil
    super(parent)
    @prev_parent = nil
    @cards = []
    # On positionne la zone de choix
    @_x = -(($scene_width - $card_width)/2)
    @_y = -$card_height/2
    setPos @_x, @_y
    setZValue 10
    setAcceptedMouseButtons Qt::LeftButton
    # On accepte de récupérer de manière indépendante les évènements de type survol de la souris
    setAcceptHoverEvents true
  end

  def boundingRect
    Qt::RectF.new 0, 0, $scene_width-$card_width, $card_height
  end

  # Ajoute la carte à l'item graphique et organise son affichage
  def add_cards cards, txt_toot_tip=""
    @prev_parent = cards[0].parentItem
    @cards = cards
    for card in cards
      card.re_init_effects
      # On associe la carte à la zone de choix
      card.setParentItem self
      # On agrandit la taille de la carte
      card.enlarge
      card.show
      card.reveal
      card.setToolTip txt_toot_tip
      if cards.length < 4
        indent = (($scene_width-$card_width) - ($card_width + ($card_space*$ratio+$card_width)*(cards.length-1)))/2
        x = indent + (($card_space*$ratio+$card_width)*(card.i_card-1))
      else
        x = (($scene_width-$card_width*2)/(cards.length-1)) * (card.i_card-1)
      end
      # utilisation de setPos au lieu de move_to afin de ne pas enregistrer les nouvelles coordonnées des cartes
      card.setPos x, 0
      card.setZValue card.i_card
    end
  end

  # Réinitialise la zone de choix
  def re_init
    for card in @cards
      card.re_init_effects
      card.reduce
      card.selected = false
      card.available
      card.hide
      card.i_card = 0
      card.setParentItem nil
      card.setToolTip ""
    end
    @cards = []
  end

  # Réinitialise la zone de choix
  def del_cards
    for card in @cards
      card.setParentItem @prev_parent
      card.reduce
      card.setPos card._x, card._y
      card.setZValue card.i_card
      card.setToolTip "Clic 'Droit' pour agrandir la carte\nDouble Clic 'Gauche' pour agrandir toute votre main" if self.class.to_s == "ZoomPlayerHand"
    end
  end

end

