# encoding: utf-8

require 'Qt4'

class DisplayText < Qt::GraphicsItem

  attr_reader :width, :height

  def initialize img_name, parent=nil
    super(parent)
    set_font img_name
    setZValue 10
    # On empêche le traitement en indépendance de la scène des évènements souris
    setAcceptedMouseButtons Qt::NoButton
    set_text_edit
    hide
  end

  def boundingRect
    Qt::RectF.new -$scene_width/2, -$scene_height/2, $scene_width, $scene_height
  end

  def set_font img_name
    font_pix = Qt::Pixmap.new "./img/#{img_name}.png"
    @height = $scene_height - $area_space
    @width = font_pix.width*height/font_pix.height
    font_pix = font_pix.scaled width, height, Qt::IgnoreAspectRatio, Qt::SmoothTransformation
    font = Qt::GraphicsPixmapItem.new font_pix, self
    if img_name == "chat_font"
      font.setPos -width*0.9/2, -height/2
    else
      font.setPos -width/2, -height/2
    end
  end

  def set_text_edit
    @proxy_text_edit = Qt::GraphicsProxyWidget.new self
    @text_edit = Qt::TextEdit.new
    @text_edit.objectName = "textEdit"
    @text_edit.geometry = Qt::Rect.new(-$card_width*1.1, -$card_height*0.6, $card_width*1.9, $card_height*1.1)
    @text_edit.styleSheet = "QTextEdit{background:transparent; border:none;}"
    font = Qt::Font.new
    font.family = $script_font
    font.pointSize = 16
    @text_edit.font = font
    @text_edit.frameShape = Qt::Frame::NoFrame
    @text_edit.readOnly = true
    @proxy_text_edit.setWidget @text_edit
  end
end