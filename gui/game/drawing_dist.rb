# encoding: utf-8

require 'Qt4'
require './gui/game/district_card'
require './gui/game/deck'

class DrawingDist < Qt::GraphicsItem

  include Deck

  attr_reader :cards

  def initialize
    super()

    # On créé toutes les cartes 'Quartier'
    init_deck
    shuffle_deck

    # On positionne la zone de la 'Pioche'
    @_x = -$area_width + $card_width*$zoom_out_ratio
    @_y = -$card_height * $zoom_out_ratio/2
    setPos @_x, @_y
    setZValue 1
    # Dessine la zone de la 'Pioche'
    paint_card_zone
  end

  def boundingRect
    Qt::RectF.new 0, 0, $card_width*1.5, $card_height
  end

  # Mélange les cartes de la 'Pioche'
  def shuffle_deck
    # Battage des cartes
    @cards.shuffle! # plus rapide que : @dist_deck.sort_by! {rand}
    i = 1
    for card in @cards
      card.move_to 0, 0, i, 0
      i += 1
    end
  end

  # Renvois la première carte de la 'Pioche' soit la dernière du tableau de référencement
  def draw_card
    re_init_deck if @cards.length == 0
    card = @cards[@cards.length-1]
    @cards.pop
    card
  end

  # Création du deck des Quartiers
  def init_deck
    @cards = create_dist_deck
    # On ajoute les cartes à la table
    for card in @cards
      card.setParentItem self
    end
  end

  # Réinitialisation du deck avec la défausse
  def re_init_deck
    @cards = scene.discard_dist.cards
    scene.discard_dist.cards = []
    for card in @cards
      card.setParentItem self
    end
    shuffle_deck
  end

  # Dessine la zone des emplacements des cartes de la 'Pioche'
  def paint_card_zone
    # Caractéristiques des lignes de la délimitation de la zone de Pioche
    pen = Qt::Pen.new(Qt::Color.new(150, 150, 150, 255))
    pen.setWidth 1
    pen.setStyle(Qt::DashLine)
    pen.setCapStyle(Qt::RoundCap)
    pen.setJoinStyle(Qt::RoundJoin)

    # Création du cadre de délimitation de la zone de Pioche
    painter = Qt::PainterPath.new
    painter.addRoundedRect 0, 0, $card_width*$zoom_out_ratio, $card_height*$zoom_out_ratio, 5, 5
    zone = Qt::GraphicsPathItem.new painter, self
    zone.setPen pen
    zone.setPos 0, 0
    # Définit le contenu de la bulle d'aide
    zone.setToolTip "Pioche"
  end
end