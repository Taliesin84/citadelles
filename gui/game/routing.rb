# encoding: utf-8

module Routing

  # Orchestration d'une phase de jeu
  def do_action submit=""
    case @name
      when "disp_turn_toss"
        if submit == "next"
          ending_action
          disp_player_turn_toss # Annonce du prochain Joueur à tirer une carte au sort
        end
      when "disp_toss"
        char_card = @choice_box.cards.find {|card| card.selected}
        ending_action
        disp_toss_char char_card # Affichage de la carte tirée par le Joueur
      when "disp_toss_char"
        if submit == "next"
          tossed_card = @disp_box.card
          discard_toss_char tossed_card.id
          if %(roi empereur).include? tossed_card.id
            ending_action
            enthrone @current_player.id
            disp_enthrone # Affichage du nouveau 'Roi'
          else
            ending_action
            next_player
            disp_player_turn_toss  # Annonce du prochain Joueur à tirer une carte au sort
          end
        end
      when "disp_player_turn_toss"
        if submit == "next"
          ending_action
          disp_toss # Affichage du deck des 'Personnages' pour le tirage au sort
        end
      when "disp_enthrone"
        if submit == "next"
          ending_action
          disp_turn_choose_char # Affichage de l'écran Palier de phase : Sélection des 'Personnages'
        end
      when "disp_turn_choose_char"
        if submit == "next"
          ending_action
          init_turn # Initialisation du nouveau tour
        end
      when "disp_discarded_card"
        if submit == "next"
          ending_action
          disp_player_turn_choose_char # Affichage des cartes 'Personnage' défaussées face visible
        end
      when "disp_player_turn_choose_char"
        if submit == "next"
          ending_action
          @current_player.hand.show
          if scene.discard_char.cards.find {|char| !char.revealed}
            player_index = scene.players.index {|player| player == @current_player}
            if (player_index == 6 and $nb_char == 8) or (player_index == 7 and $nb_char == 9)
              re_inject_discarded_card
            end
            choose_char # Affichage de l'écran permettant le choix du Personnage
          else
            # On défausse la première carte face cachée
            card = scene.char_deck[rand(scene.char_deck.length)]
            discard_char card.id
            disp_secret_card # Affichage de la carte 'Personnage' défaussée face cachée
          end
        end
      when "disp_secret_card"
        if submit == "next"
          ending_action
          if ($nb_players == 7 and $nb_char == 8) or ($nb_players == 8 and $nb_char == 9)
            player_index = scene.players.index {|player| player == @current_player}
            if (player_index == 6 and $nb_char == 8) or (player_index == 7 and $nb_char == 9)
              disp_turn_call_char
            else
              choose_char  # Affichage de l'écran permettant le choix du Personnage
            end
          else
            if scene.discard_char.cards.length == scene.discard_char.nb_cards
              disp_turn_call_char
            else
              choose_char  # Affichage de l'écran permettant le choix du Personnage
            end
          end
        end
      when "choose_char"
        if submit == "next"
          char_card = @choice_box.cards.find {|card| card.selected}
          ending_action
          add_char char_card.id
          if scene.char_deck.length > 1 or scene.players.find {|player| player.citadel.characters.empty?}
            if $nb_players == 2 and scene.char_deck.length < 6
              choose_discard_char
            else
              next_player
              disp_player_turn_choose_char
            end
          else
            # On défausse la dernière carte face cachée
            card = scene.char_deck[0]
            discard_char card.id
            disp_secret_card # Affichage de la carte 'Personnage' défaussée face cachée
          end
        end
      when "choose_discard_char"
        if submit == "next"
          char_card = @choice_box.cards.find {|card| card.selected}
          ending_action
          discard_char char_card.id
          next_player
          disp_player_turn_choose_char
        end
      when "disp_turn_call_char"
        if submit == "next"
          ending_action
          call_next_char
        end
      when "king_call_player"
        if submit == "next"
          ending_action
          # Définit si le joueur incarnant le 'Roi' pourra être intrôniser
          if @current_char.id == "roi" and !@current_player.nil?
            char_dup = scene.char_deck_dup.find {|char| char.id == "roi"}
            if !char_dup.killed or (char_dup.killed and !$kill_crown) or (char_dup.bewitched and !$bewitch_crown)
              enthrone @current_player.id
            end
          end
          # Si le 'Personnage' est incarné par un Joueur on affiche la main de ce dernier
          @current_player.hand.show unless @current_player.nil?
          if @current_player.nil?
            # On appelle le prochain 'Personnage'
            call_next_char
          elsif @current_char.killed
            # On vérifie si le Joueur possède a bâtit le quartier 'Hôpital'
            if @current_player.citadel.districts.find {|dist| dist.id == "hop"}
              gold_vs_card
            else
              call_next_char
            end
          elsif @current_char.stolen
            # On calcule la somme dérobée
            @money = @current_player.citadel.sold
            # On affiche la somme dérobée
            disp_gold_stolen
          elsif @current_char.bewitched and $total_bewitch
            witch_player = scene.players.find {|player| player.citadel.characters.find {|char| char.id == "sorciere"}}
            add_history_action "<span style='color:rgb(#{$player_rgb_colors[witch_player.id-1]});'>#{witch_player.name}</span> récupère le tour de jeu de <span style='color:rgb(#{$player_rgb_colors[@current_player.id-1]});'>#{@current_player.name}</span>", true
            toggle_player witch_player.id
            # On appelle le Joueur incarnant la 'Sorcière'
            witch_recall
          else
            gold_vs_card
          end
        end
      when "disp_gold_stolen"
        if submit == "next"
          ending_action
          gold_stolen @money, 'voleur'
          @money = 0
          if @current_char.bewitched and $total_bewitch
            witch_player = scene.players.find {|player| player.citadel.characters.find {|char| char.id == "sorciere"}}
            toggle_player witch_player.id
            witch_recall
          else
            gold_vs_card
          end
        end
      when "disp_gold_earned"
        if submit == "next"
          ending_action
          if @current_char.id == "bailli"
            unless @due_to_owner
              if @due_to_owner > 0
                first_player = scene.players.find {|player| player.citadel.characters.find {|char| %(assassin sorciere).include? char.id}}
                gold_spend @due_to_owner, first_player.id
                gold_earned @due_to_owner, @current_player.id, "char"
                @due_to_owner = nil
              end
            end
          else
            set_amount true
            gold_earned @money, @current_player.id, "char"
          end
          @money = 0
          case @current_char.id
            when "empereur"
              $oblige_to_enthrone ? emperor_player_choice : search_card_power
            when "architecte"
              disp_drawn_dist 2
            else
              search_card_power
          end
        end
      when "disp_gold_taxed"
        if submit == "next"
          ending_action
          gold_stolen @money, 'bailli'
          @money = 0
          dist = @current_player.citadel.districts[@current_player.citadel.districts.length-1]
          dist.id=="bef" ? belfry_power_choice : search_card_power
        end
      when "disp_gold_earned_with_hospice"
        if submit == "next"
          ending_action
          gold_earned 1, @current_player.id, "dist", "hospice"
          hospice = scene.dist_deck_dup.find {|dist| dist.id == "hos"}
          hospice.power_used = true
          search_card_power
        end
      when "char_power_choice"
        if submit == "check"
          char_power_routing @disp_box.card.id # On redirige vers l'écran correspondant au pouvoir du 'Personnage'
        elsif submit == "ban"
          ending_action
          redirect_phase
        end
      when "dist_power_choice"
        if submit == "check"
          dist = @disp_box.card
          ending_action
          dist_power_routing dist.id # On redirige vers l'écran correspondant au pouvoir du 'Quartier'
        elsif submit == "ban"
          ending_action
          redirect_phase
        end
      when "kill_choice"
        if submit == "prev"
          ending_action
          search_card_power
        elsif submit == "next"
          char_card = @choice_box.cards.find {|card| card.selected}
          ending_action
          kill char_card.id
          @current_char.power_used = true
          search_card_power
        end
      when "bewitch_choice"
        if submit == "prev"
          ending_action
          search_card_power
        elsif submit == "next"
          char_card = @choice_box.cards.find {|card| card.selected}
          ending_action
          bewitch char_card.id
          @current_char.power_used = true
          search_card_power
        end
      when "steal_choice"
        if submit == "prev"
          ending_action
          search_card_power
        elsif submit == "next"
          char_card = @choice_box.cards.find {|card| card.selected}
          ending_action
          steal char_card.id
          @current_char.power_used = true
          search_card_power
        end
      when "exchange_type_choice"
        case submit
          when "prev"
            ending_action
            search_card_power
          when "exchange_all"
            ending_action
            exchange_all_choice
          when "exchange_draw"
            ending_action
            exchange_draw_choice
          else
        end
      when "exchange_all_choice"
        if submit == "prev"
          ending_action
          exchange_type_choice
        elsif submit == "next"
          player = @choice_box.players.find {|player| player.selected}
          ending_action
          exchange_all player.player_id
          @current_char.power_used = true
          disp_exchange_hand
        end
      when "disp_exchange_hand"
        if submit == "next"
          ending_action
          search_card_power
        end
      when "exchange_draw_choice"
        if submit == "prev"
          ending_action
          exchange_type_choice
        elsif submit == "next"
          selected_cards = @choice_box.cards.find_all {|card| card.selected}
          ending_action
          for card in selected_cards
            exchange_draw card.id
          end
          @current_char.power_used = true
          disp_drawn_dist selected_cards.length
        end
      when "witch_recall"
        if submit == "next"
          ending_action
          enthrone @current_player.id if @current_char.id=="roi" and $bewitch_crown
          @current_player.hand.show
          redirect_call_player true
        end
      when "sorcerer_player_choice"
        if submit == "prev"
          ending_action
          search_card_power
        else
          victim = @choice_box.players.find {|player| player.selected}
          ending_action
          sorcerer_card_choice victim.player_id
        end
      when "sorcerer_card_choice"
        if submit == "prev"
          ending_action
          sorcerer_player_choice
        elsif submit == "next"
          card = @choice_box.cards.find {|card| card.selected}
          ending_action
          add_dist_to_hand card.id
          @current_char.power_used = true
          add_history_action "<span style='color:rgb(#{$player_rgb_colors[@current_player.id-1]});'>#{@current_player.name}</span> a pris une carte 'Quartier' de la main de <span style='color:rgb(#{$player_rgb_colors[card.parentItem.player_id-1]});'>#{card.parentItem.player_name}</span>."
          if @current_player.citadel.sold >= card.cost
            sorcerer_build_choice card.id
          elsif $sorcerer_revealed_cards
            search_card_power
          else
            sorcerer_disp_card card.id
          end
        end
      when "sorcerer_build_choice"
        if submit == "check"
          dist_id = @disp_box.card.id
          ending_action
          build_dist dist_id
          redirect_build card
        elsif submit == "ban"
          ending_action
          search_card_power
        end
      when "sorcerer_disp_card"
        if submit == "next"
          ending_action
          search_card_power
        end
      when "emperor_player_choice"
        if submit == "prev"
          ending_action
          search_card_power
        elsif submit == "next"
          @current_char.power_used = true
          player_label = @choice_box.players.find {|player| player.selected}
          ending_action
          new_king = scene.players.find {|player| player.id == player_label.player_id}
          add_history_action "<span style='color:rgb(#{$player_rgb_colors[@current_player.id-1]});'>#{@current_player.name}</span> a donné la couronne à <span style='color:rgb(#{$player_rgb_colors[player_label.id-1]});'>#{player_label.name}</span>."
          enthrone player_label.player_id
          if new_king.hand.cards.length == 0
            if new_king.citadel.sold > 0
              gold_spend 1, player_label.player_id
              @due_to_emperor = 1
              disp_emperor_gain
            else
              redirect_phase
            end
          else
            toggle_player player_label.player_id
            emperor_call_player
          end
        end
      when "emperor_call_player"
        if submit == "next"
          ending_action
          @current_player.hand.show
          if @current_player.citadel.sold > 0
            enthroned_player_choice
          else
            enthroned_player_dist_choice false
          end
        end
      when "enthroned_player_choice"
        if submit == "1_card"
          ending_action
          enthroned_player_dist_choice true
        elsif submit == "del_coins"
          ending_action
          gold_spend 1, @current_player.id
          @due_to_emperor = 1
          origin_player = scene.players.find {|player| player.citadel.characters.find {|char| char.id == "empereur"}}
          toggle_player origin_player.id
          emperor_recall
        end
      when "enthroned_player_dist_choice"
        if submit == "next"
          card = @choice_box.cards.find {|card| card.selected}
          ending_action
          @due_to_emperor = card
          origin_player = scene.players.find {|player| player.citadel.characters.find {|char| char.id == "empereur"}}
          toggle_player origin_player.id
          emperor_recall
        elsif submit == "prev"
          ending_action
          enthroned_player_choice
        end
      when "emperor_recall"
        if submit == "next"
          ending_action
          @current_player.hand.show
          disp_emperor_gain
        end
      when "disp_emperor_gain"
        if submit == "next"
          ending_action
          if @due_to_emperor.class.to_s == "Fixnum"
            gold_earned @due_to_emperor, @current_player.id, "crown"
          elsif @due_to_emperor.class.to_s == "DistrictCard"
            add_dist_to_hand @due_to_emperor.id
            add_history_action "<span style='color:rgb(#{$player_rgb_colors[@current_player.id-1]});'>#{@current_player.name}</span> a recu une carte 'Quartier' du nouveau possesseur de la couronne."
          end
          @due_to_emperor = nil
          search_card_power
        end
      when "sailor_choice"
        ending_action
        if submit == "4_cards"
          @current_char.power_used = true
          add_history_action "<span style='color:rgb(#{$player_rgb_colors[@current_player.id-1]});'>#{@current_player.name}</span> a choisit de piocher 4 cartes."
          disp_drawn_dist 4
        elsif submit == "coins"
          @current_char.power_used = true
          gold_earned 4, @current_player.id, "sailor"
          search_card_power
        end
      when "destruct_player_choice"
        if submit == "prev"
          ending_action
          search_card_power
        else
          player = @choice_box.players.find {|player| player.selected}
          ending_action
          destruct_dist_choice player.player_id
        end
      when "destruct_dist_choice"
        if submit == "prev"
          ending_action
          destruct_player_choice
        elsif submit == "next"
          card = @choice_box.cards.find {|card| card.selected}
          ending_action
          destruct_dist card.id
          @current_char.power_used = true
          cemetery_owner = scene.players.find {|player| player.districts.find {|dist| dist.id == "cim"}}
          if cemetery_owner.nil?
            search_card_power
          else
            if cemetery_owner.citadel.sold == 0
              search_card_power
            else
              toggle_player cemetery_owner.id
              cemetery_owner_call
            end
          end
        end
      when "powder_player_choice"
        if submit == "prev"
          ending_action
          search_card_power
        else
          player = @choice_box.players.find {|player| player.selected}
          ending_action
          powder_dist_choice player.player_id
        end
      when "powder_dist_choice"
        if submit == "prev"
          ending_action
          powder_player_choice
        elsif submit == "next"
          card = @choice_box.cards.find {|card| card.selected}
          ending_action
          destruct_dist "pou", false # Destruction de la 'Poudriere'
          destruct_dist card.id, false # Destruction du 'Quartier' de la victime
          search_card_power
        end
      when "cemetery_owner_call"
        if submit == "next"
          ending_action
          @current_player.hand.show
          disp_cemetery_power
        end
      when "disp_cemetery_power"
        if submit == "check"
          card = @disp_box.card
          ending_action
          gold_spend 1, @current_player.id
          add_dist_to_hand card.id
        elsif submit == "ban"
          ending_action
        end
        origin_player = scene.players.find {|player| player.characters.find {|char| char.id == "condottiere"}}
        toggle_player origin_player.id
        mercenary_recall
      when "mercenary_recall"
        if submit == "next"
          ending_action
          @current_player.hand.show
          search_card_power
        end
      when "diplomat_dist_choice"
        if submit == "prev"
          ending_action
          search_card_power
        elsif submit == "next"
          card = @choice_box.cards.find {|card| card.selected}
          ending_action
          @diplomat_dist = card
          diplomat_player_choice
        end
      when "diplomat_player_choice"
        if submit == "prev"
          ending_action
          diplomat_dist_choice
        else
          player = @choice_box.players.find {|player| player.selected}
          ending_action
          diplomat_victim_dist_choice player.player_id
        end
      when "diplomat_victim_dist_choice"
        if submit == "prev"
          ending_action
          diplomat_player_choice
        elsif submit == "next"
          card = @choice_box.cards.find {|card| card.selected}
          ending_action
          @current_char.power_used = true
          exchange_citadel_dist @diplomat_dist.id, card.id
          @diplomat_dist = nil
          search_card_power
        end
      when "embellish_dist_choice"
        if submit == "prev"
          ending_action
          search_card_power
        elsif submit == "next"
          card = @choice_box.cards.find {|card| card.selected}
          ending_action
          embellish_dist card.id
          @nb_embellish += 1 unless @nb_embellish.nil?
          @nb_embellish ||= 1
          @current_char.power_used = true if @nb_embellish == 2
          search_card_power
        end
      when "discard_dist_choice"
        if submit == "prev"
          ending_action
          search_card_power
        elsif submit == "next"
          card = @choice_box.cards.find {|card| card.selected}
          ending_action
          discard_dist_from_hand card.id
          gold_earned 1, @current_player.id, "discard"
          # On indique que le pouvoir de la carte a été consommé
          dist = scene.dist_deck_dup.find {|card| card.id == "lab"}
          dist.power_used = true
          search_card_power
        end
      when "museum_discard_dist_choice"
        if submit == "prev"
          ending_action
          search_card_power
        elsif submit == "next"
          card = @choice_box.cards.find {|card| card.selected}
          ending_action
          discard_dist_to_museum card.id
          # On indique que le pouvoir de la carte a été consommé
          dist = scene.dist_deck_dup.find {|card| card.id == "mus"}
          dist.power_used = true
          search_card_power
        end
      when "gold_vs_card"
        ending_action
        @_gold_vs_card = true
        case submit
          when "coins"
            gold_earned 2, @current_player.id, "std"
            @current_char.killed ? call_next_char : redirect_call_player
          when "2_cards"
            add_history_action "<span style='color:rgb(#{$player_rgb_colors[@current_player.id-1]});'>#{@current_player.name}</span> a choisi de prendre 2 cartes 'Quartier'."
            disp_drawn_dist 2
          when "2_to_1_card"
            add_history_action "<span style='color:rgb(#{$player_rgb_colors[@current_player.id-1]});'>#{@current_player.name}</span> a choisi de tirer 2 cartes 'Quartier' et dans défausser une."
            if $easy_draw
              2.times { add_dist_to_hand scene.draw_dist.cards[scene.draw_dist.cards.length-1].id }
              choose_dist_to_discard 2
            else
              choose_dist 2
            end
          when "3_to_1_cards"
            add_history_action "<span style='color:rgb(#{$player_rgb_colors[@current_player.id-1]});'>#{@current_player.name}</span> a choisi de tirer 3 cartes 'Quartier' et dans défausser 2."
            if $easy_draw
              3.times { add_dist_to_hand scene.draw_dist.cards[scene.draw_dist.cards.length-1].id }
              choose_dist_to_discard 3
            else
              choose_dist 3
            end
          else
        end
      when "disp_drawn_dist"
        if submit == "next"
          for card in @disp_box.cards
            add_dist_to_hand card.id
          end
          ending_action
          # On vérifie la phase d'utilisation de la fonction
          #todo Vérifier le bon routing de cette partie de code
          if @current_char.killed
            call_next_char
          else
            if @current_char.id == "architecte"
              search_card_power
            else
              redirect_call_player
            end
          end
        end
      when "disp_park_drawn_dist"
        if submit == "next"
          for card in @disp_box.cards
            add_dist_to_hand card.id
          end
          ending_action
          # On vérifie la phase d'utilisation de la fonction
          search_card_power
        end
      when "choose_dist"
        if submit == "next"
          for card_ref in @choice_box.cards
            card_dup = scene.dist_deck_dup.find {|card| card.id == card_ref.id}
            if card_ref.selected
              add_dist_to_hand card_dup.id
            else
              discard_dist_from_draw
            end
          end
          ending_action
          @current_char.killed ? call_next_char : redirect_call_player
        end
      when "choose_dist_to_discard"
        if submit == "next"
          for card_ref in @choice_box.cards
            card_dup = scene.dist_deck_dup.find {|card| card.id == card_ref.id}
            # On défausse la/les carte(s) sélectionnée(s)
            discard_dist_from_hand card_dup.id if card_ref.selected
          end
          ending_action
          @current_char.killed ? call_next_char : redirect_call_player
        end
      when "confirm_build"
        if submit == "build"
          ending_action
          build_choice
        elsif submit == "no_build"
          ending_action
          @_build = true
          search_card_power
        end
      when "build_choice"
        if submit == "prev"
          ending_action
          confirm_build
        elsif submit == "next"
          card = @choice_box.cards.find {|card| card.selected}
          ending_action
          build_dist card.id
          # On vérifie si le Joueur incarne l'Architecte'
          if @current_char.id == "architecte" and @current_player.citadel.sold > 0
            # On vérifie si le Joueur a bâtit moins de 3 'Quartiers'
            @nb_build += 1 unless @nb_build.nil?
            @nb_build ||= 1
            @_build = true if @nb_build == 3
          else
            @_build = true
          end
          redirect_build card
        end
      when "belfry_power_choice"
        ending_action
        if submit == "check"
          change_max_dist -1
        end
        search_card_power
      when "disp_power_card"
        if %w(clock skip).include? submit
          ending_action
          redirect_phase
        elsif submit == "next"
          card = @choice_box.cards.find {|card| card.selected}
          ending_action
          if card.class.to_s == "CharacterCard"
            char_power_routing card.id  # On redirige vers l'écran correspondant au pouvoir du 'Personnage' séléctionné
          end
          if card.class.to_s == "DistrictCard"
            dist_power_routing card.id # On redirige vers l'écran correspondant au pouvoir du 'Quartier' sélectionné
          end
        end
      when "disp_killed_player"
        if submit == "next"
          ending_action
          if @due_to_queen
            player_queen = scene.players {|player| player.citadel.characters.find {|char| char.id = "reine"}}
            disp_gold_earned_by_queen player_queen.name
          else
            redirect_results # Affichage du classement intermédiaire ou final
          end
        end
      when "disp_gold_earned_by_queen"
        if submit == "next"
          ending_action
          player_queen = scene.players.find {|player| player.citadel.characters.find {|char| char.id = "reine"}}
          player_queen.earn @due_to_queen
          add_history_action "<span style='color:rgb(#{$player_rgb_colors[player_queen.id-1]});'>#{player_queen.name}</span> a recu 3 pièces d'or pour avoir été assis à côté du 'Roi' en étant 'Reine'."
          @due_to_queen = nil
          redirect_results # Affichage du classement intermédiaire ou final
        end
      when "disp_ranking"
        if submit == "next"
          ending_action
          next_turn
        end
      when "disp_podium"
        if submit == "next"
          ending_action
          disp_ranking true # Affichage du classement final
        end
      else
    end
  end

  def char_power_routing char_id
    case char_id
      when "assassin"
        ending_action
        kill_choice
      when "sorciere"
        ending_action
        bewitch_choice
      when "voleur"
        ending_action
        steal_choice
      when "magicien"
        ending_action
        exchange_type_choice
      when "sorcier"
        ending_action
        sorcerer_player_choice
      when "empereur"
        ending_action
        emperor_player_choice
      when "navigateur"
        ending_action
        sailor_choice
      when "condottiere"
        ending_action
        destruct_player_choice
      when "diplomate"
        ending_action
        diplomat_dist_choice
      when "artiste"
        ending_action
        embellish_dist_choice
      else
    end
  end

  def dist_power_routing dist_id
    case dist_id
      when "lab"
        ending_action
        add_history_action "<span style='color:rgb(#{$player_rgb_colors[@current_player.id-1]});'>#{@current_player.name}</span> a défaussé une carte 'Quartier' contre une pièce d'or."
        discard_dist_choice
      when "mnf"
        ending_action
        gold_spend 2, @current_player.id
        # On indique que le pouvoir de la carte a été consommé
        dist = scene.dist_deck_dup.find {|card| card.id == "mnf"}
        dist.power_used = true
        add_history_action "<span style='color:rgb(#{$player_rgb_colors[@current_player.id-1]});'>#{@current_player.name}</span> a payer 2 pièces d'or pour piocher 3 cartes 'Quartier'."
        disp_drawn_dist 3
      when "mus"
        ending_action
        museum_discard_dist_choice
      when "pou"
        ending_action
        powder_player_choice
      else
    end
  end

  # Vérifie si le Joueur possède une carte dont le pouvoir nécessite un choix
  def search_card_power
    powered_cards = []
    #todo Prendre en compte la capabilité du Joueur d'appliquer les pouvoirs des cartes
    # On vérifie si le Joueur possède un 'Personnage' dont le pouvoir nécessite un choix et si ce dernier n'a pas déjà été utilisé
    unless @current_char.power_used
      case @current_char.id
        when "assassin", "voleur", "magicien", "sorcier", "empereur", "condottiere"
          powered_cards << @current_char
        when "sorciere"
          unless !$build_bewitch and @_build
            powered_cards << @current_char
          end
        when "empereur"
          unless $oblige_to_enthrone
            powered_cards << @current_char
          end
        when "navigateur"
          unless @build
            powered_cards << @current_char
          end
        when "diplomate"
          if @current_player.citadel.districts.length > 0
            powered_cards << @current_char
          end
        when "artiste"
          if @current_player.citadel.sold > 0
            powered_cards << @current_char
          end
        else
      end
    end
    # On vérifie si le Joueur possède la carte 'Laboratoire'
    lab_card = @current_player.citadel.districts.find {|dist| dist.id == "lab"}
    unless lab_card.nil?
      unless lab_card.power_used or @current_player.hand.cards.length < 1
        powered_cards << lab_card
      end
    end
    # On vérifie si le Joueur possède la carte 'Manufacture'
    man_card = @current_player.citadel.districts.find {|dist| dist.id == "mnf"}
    unless man_card.nil?
      unless man_card.power_used or @current_player.citadel.sold < 2
        powered_cards << man_card
      end
    end
    # On vérifie si le Joueur possède la carte 'Musée'
    mus_card = @current_player.citadel.districts.find {|dist| dist.id == "mus"}
    unless mus_card.nil?
      unless mus_card.power_used or @current_player.hand.cards.length == 0
        powered_cards << mus_card
      end
    end
    # On vérifie si le Joueur possède la carte 'Poudriere'
    pou_card = @current_player.citadel.districts.find {|dist| dist.id == "pou"}
    unless pou_card.nil?
      powered_cards << pou_card
    end
    # On redirige selon les cas
    if powered_cards.length > 1
      disp_power_card powered_cards
    elsif powered_cards.length == 1
      if powered_cards[0].class.to_s == "CharacterCard"
        char_power_choice
      elsif powered_cards[0].class.to_s == "DistrictCard"
        if powered_cards[0].id == "lab"
          dist_power_choice "lab"
        elsif powered_cards[0].id == "mnf"
          dist_power_choice "mnf"
        elsif powered_cards[0].id == "mus"
          dist_power_choice "mus"
        elsif powered_cards[0].id == "pou"
          dist_power_choice "pou"
        end
      end
    else
      redirect_phase
    end
  end

  # Redirige vers la suite logique des actions
  def redirect_phase
    if !@_gold_vs_card and !@_build
      gold_vs_card
    elsif @_gold_vs_card and !@_build
      if (@current_char.id == "sorciere" and @current_char.power_used and !$build_bewitch) or
          (@current_char.id == "navigateur" and @current_char.power_used)
        call_next_char
      else
        confirm_build
      end
    elsif @_gold_vs_card and @_build
      dist_power = []
      # Vérifie si le Joueur n'a pas batit le quartier 'Hospice'
      hospice = @current_player.citadel.districts.find {|dist| dist.id == "hos"}
      unless hospice.nil?
        if !hospice.power_used and @current_player.citadel.sold == 0
          dist_power << hospice
        end
      end
      # Vérifie si le Joueur n'a pas batit le quartier 'Parc'
      park = @current_player.citadel.districts.find {|dist| dist.id == "par"}
      unless park.nil?
        if !park.power_used and @current_player.hand.cards.length == 0
          dist_power << park
        end
      end
      if dist_power.length == 0
        call_next_char
      else
        if dist_power[0].id == "hos"
          disp_gold_earned_with_hospice
        elsif dist_power[0].id == "par"
          disp_park_drawn_dist
        end
      end
    end
  end

  # Aiguille vers la prochaine phase du tour
  def redirect_call_player witch_called=false
    if @current_char.bewitched and !$total_bewitch and !witch_called
      witch_player = scene.players.find {|player| player.citadel.characters.find {|char| char.id == "sorciere"}}
      toggle_player witch_player.id
      witch_recall
    else
      case @current_char.id
        when "bailli"
          if @due_to_owner.nil?
            search_card_power
          else
            if @due_to_owner > 0
              @money = @due_to_owner
              disp_gold_earned
            end
          end
        when "roi", "empereur", "eveque", "abbe", "marchand", "condottiere", "diplomate"
          set_amount
          disp_gold_earned
        when "architecte"
          add_history_action "<span style='color:rgb(#{$player_rgb_colors[@current_player.id-1]});'>#{@current_player.name}</span> a pioché 2 cartes 'Quartier'."
          disp_drawn_dist 2
        when "reine"
          queen_index = scene.players.index {|player| player.citadel.characters.find{|char| char.id == 'reine'}}
          king_index = scene.players.index {|player| player.king}
          king_index+1>=$nb_char ? king_index_up=0 : king_index_up=king_index+1
          king_index-1<0 ? king_index_down=$nb_char-1 : king_index_down=king_index-1
          # On ajoute 3 pièces d'or si la 'Reine' est aux côtés du 'Roi'
          if queen_index == king_index_up or queen_index == king_index_down
            king = scene.char_deck_dup.find {|char| char.id == "roi"}
            if king.killed
              @due_to_queen = 3
              search_card_power
            else
              @money += 3
              disp_gold_earned
            end
          else
            search_card_power
          end
        else
          search_card_power
      end
    end
  end

  def redirect_build card
    # On vérifie que le 'Bailli' fait partit du deck des 'Personnages'
    owner = scene.char_deck_dup.find {|char| char.id == "bailli"}
    if owner.nil? or @current_char.id == "bailli"
      card.id=="bef" ? belfry_power_choice : search_card_power
    else
      if owner.parentItem.class.to_s == "Citadel" and @current_player.citadel.sold > 0
        #todo Implémenter la retenue des impôts du 'Bailli' lorsque celui-ci est appelé à la fin
        if %w(assassin sorciere).include? @current_char.id
          # On retient la dette de l''Assassin' ou de la 'Sorcière' envers le 'Bailli'
          @due_to_owner ||= 1
          card.id=="bef" ? belfry_power_choice : search_card_power
        else
          @money = 1
          disp_gold_taxed
        end
      else
        card.id=="bef" ? belfry_power_choice : search_card_power
      end
    end

  end

  def redirect_results
    if scene.players.find {|player| player.citadel.districts.length == $max_dist}
      disp_podium # Affichage du podium final
    else
      disp_ranking # Affichage du classement intermédiaire
    end
  end

end