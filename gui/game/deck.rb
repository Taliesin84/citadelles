# encoding: utf-8

module Deck

  def create_dist_deck
    deck = []
    # Instanciation de toutes les cartes 'Quartier'
    deck << DistrictCard.new("cdm", "prestige", "cour_des_miracles", 2) if $dist_list.find {|dist| dist=="cour_des_miracles"}
    deck << DistrictCard.new("don", "prestige", "donjon", 3) if $dist_list.find {|dist| dist=="donjon"}
    deck << DistrictCard.new("ti", "prestige", "tresor_imperial", 4) if $dist_list.find {|dist| dist=="tresor_imperial"}
    deck << DistrictCard.new("car", "prestige", "carriere", 5) if $dist_list.find {|dist| dist=="carriere"}
    deck << DistrictCard.new("cim", "prestige", "cimetiere", 5) if $char_list.find {|char| char=="condottiere"} and $dist_list.find {|dist| dist=="cimetiere"}
    deck << DistrictCard.new("fas", "prestige", "fontaine_aux_souhaits", 5) if $dist_list.find {|dist| dist=="fontaine_aux_souhaits"}
    deck << DistrictCard.new("lab", "prestige", "laboratoire", 5) if $dist_list.find {|dist| dist=="laboratoire"}
    deck << DistrictCard.new("mnf", "prestige", "manufacture", 5) if $dist_list.find {|dist| dist=="manufacture"}
    deck << DistrictCard.new("obs", "prestige", "observatoire", 5) if $dist_list.find {|dist| dist=="observatoire"}
    deck << DistrictCard.new("bib", "prestige", "bibliotheque", 6) if $dist_list.find {|dist| dist=="bibliotheque"}
    deck << DistrictCard.new("dra", "prestige", "dracoport", 6) if $dist_list.find {|dist| dist=="dracoport"}
    deck << DistrictCard.new("edm", "prestige", "ecole_de_magie", 6) if $dist_list.find {|dist| dist=="ecole_de_magie"}
    deck << DistrictCard.new("gm", "prestige", "grande_muraille", 6) if $dist_list.find {|dist| dist=="grande_muraille"}
    deck << DistrictCard.new("uni", "prestige", "universite", 6) if $dist_list.find {|dist| dist=="universite"}
    # Cartes 'La Cité Sombre'
    #todo Implémenter la carte 'Phare'
    #deck << DistrictCard.new("pha", "prestige", "phare", 3) if $dist_list.find {|dist| dist=="phare"}
    deck << DistrictCard.new("pou", "prestige", "poudriere", 3) if $dist_list.find {|dist| dist=="poudriere"}
    deck << DistrictCard.new("hos", "prestige", "hospice", 4) if $dist_list.find {|dist| dist=="hospice"}
    deck << DistrictCard.new("mus", "prestige", "musee", 4) if $dist_list.find {|dist| dist=="musee"}
    deck << DistrictCard.new("bef", "prestige", "beffroi", 5) if $dist_list.find {|dist| dist=="beffroi"}
    deck << DistrictCard.new("fab", "prestige", "fabrique", 5) if $dist_list.find {|dist| dist=="fabrique"}
    deck << DistrictCard.new("sdc", "prestige", "salle_des_cartes", 5) if $dist_list.find {|dist| dist=="salle_des_cartes"}
    deck << DistrictCard.new("hop", "prestige", "hopital", 6) if $char_list.find {|char| char=="assassin"} and $dist_list.find {|dist| dist=="hopital"}
    deck << DistrictCard.new("par", "prestige", "parc", 6) if $dist_list.find {|dist| dist=="parc"}
    deck << DistrictCard.new("sdt", "prestige", "salle_du_trone", 6) if $dist_list.find {|dist| dist=="salle_du_trone"}
    i = 1
    2.times {
      deck << DistrictCard.new("cat#{i}", "religion", "cathedrale", 5)
      deck << DistrictCard.new("hdv#{i}", "commerce", "hotel_de_ville", 5)
      deck << DistrictCard.new("pal#{i}", "noblesse", "palais", 5)
      i += 1
    }
    i = 1
    3.times {
      deck << DistrictCard.new("tpl#{i}", "religion", "temple", 1)
      deck << DistrictCard.new("egl#{i}", "religion", "eglise", 2)
      deck << DistrictCard.new("com#{i}", "commerce", "comptoir", 3)
      deck << DistrictCard.new("por#{i}", "commerce", "port", 4)
      deck << DistrictCard.new("tdg#{i}", "militaire", "tour_de_guet", 1)
      deck << DistrictCard.new("pri#{i}", "militaire", "prison", 2)
      deck << DistrictCard.new("cas#{i}", "militaire", "caserne", 3)
      deck << DistrictCard.new("for#{i}", "militaire", "forteresse", 5)
      i += 1
    }
    i = 1
    4.times {
      deck << DistrictCard.new("mon#{i}", "religion", "monastere", 3)
      deck << DistrictCard.new("mar#{i}", "commerce", "marche", 2)
      deck << DistrictCard.new("ech#{i}", "commerce", "echoppe", 2)
      i += 1
    }
    i = 1
    5.times {
      deck << DistrictCard.new("tav#{i}", "commerce", "taverne", 1)
      deck << DistrictCard.new("man#{i}", "noblesse", "manoir", 3)
      deck << DistrictCard.new("cha#{i}", "noblesse", "chateau", 4)
      i += 1
    }
    deck
  end

  def create_char_deck
    deck = []
    for char in $char_list
      case char
        when "assassin"
          deck << CharacterCard.new(1, "assassin")
        when "sorciere"
          deck << CharacterCard.new(1, "sorciere")
        when "voleur"
          deck << CharacterCard.new(2, "voleur")
        when "bailli"
          deck << CharacterCard.new(2, "bailli")
        when "magicien"
          deck << CharacterCard.new(3, "magicien")
        when "sorcier"
          deck << CharacterCard.new(3, "sorcier")
        when "roi"
          deck << CharacterCard.new(4, "roi", "noblesse")
        when "empereur"
          deck << CharacterCard.new(4, "empereur", "noblesse")
        when "eveque"
          deck << CharacterCard.new(5, "eveque", "religion")
        when "abbe"
          deck << CharacterCard.new(5, "abbe", "religion")
        when "marchand"
          deck << CharacterCard.new(6, "marchand", "commerce")
        when "alchimiste"
          deck << CharacterCard.new(6, "alchimiste")
        when "architecte"
          deck << CharacterCard.new(7, "architecte")
        when "navigateur"
          deck << CharacterCard.new(7, "navigateur")
        when "condottiere"
          deck << CharacterCard.new(8, "condottiere", "militaire")
        when "diplomate"
          deck << CharacterCard.new(8, "diplomate", "militaire")
        when "reine"
          deck << CharacterCard.new(9, "reine")
        when "artiste"
          deck << CharacterCard.new(9, "artiste")
        else
      end
    end
    deck
  end

end