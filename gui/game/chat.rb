# encoding: utf-8

require 'Qt4'
require './gui/game/display_text'
require 'socket'

class Chat < DisplayText

  def initialize host, player, parent=nil
    super("chat_font", parent)
    @player = player
    set_input
    start_tchat_socket host
  end

  def set_input
    @proxy_input = Qt::GraphicsProxyWidget.new self
    @input = Qt::LineEdit.new
    @input.objectName = "textEdit"
    @input.geometry = Qt::Rect.new(-$card_width*0.85, $card_height*0.66, $card_width*1.5, $icon_wide)
    @input.styleSheet = "QLineEdit{background:transparent;}"
    font = Qt::Font.new
    font.family = $script_font
    font.pointSize = 16
    @input.font = font
    @input.readOnly = true
    @proxy_input.setWidget @input
  end

  # Ouvre un socket client avec le serveur de tchat
  def start_tchat_socket host
    # Désolidarisation de la récupération des messages du tchat du reste de l'application'
    @th_msg = Thread.new do
      begin
        # Ouverture d'un socket avec le Serveur de Tchat
        @tchat_socket = TCPSocket.open host, 2001
        @text_edit.append "<i>Connexion réussie !</i><br>"
        @input.readOnly = false

        while !(@tchat_socket.closed?) and (server_msg = @tchat_socket.gets)
          @text_edit.verticalScrollBar.value == @text_edit.verticalScrollBar.maximum ? slider_down=true : slider_down=false
          # Affichage des messages du Serveur
          @text_edit.append server_msg.strip
          # On met la scrollbar en bas si celle-ci l'était avant l'insertion du nouveau message
          @text_edit.verticalScrollBar.value = @text_edit.verticalScrollBar.maximum if slider_down
        end
      rescue
        puts "Erreur : #{$!}"
        Qt::MessageBox.critical self, "Critical", "Echec connexion au serveur de Tchat !"
        retry
      end
    end
  end

  def keyPressEvent event
    if event.key == Qt::Key_Return or event.key == Qt::Key_Enter
      @tchat_socket.puts "<b><span style='color:rgb(#{$player_rgb_colors[@player.id-1]});'>#{@player.name}</span></b> : #{@input.text}"
      @input.clear
    end
  end
end