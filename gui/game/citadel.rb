# encoding: utf-8

require 'Qt4'
require './gui/game/player_label'

class Citadel < Qt::GraphicsItem

  attr_reader :player_id, :player_name, :score, :crown, :districts
  attr_accessor :sold, :characters

  def initialize player_id, player_name
    super()

    @player_id = player_id
    @player_name = player_name
    @districts = []
    @characters = []
    @card_zones = []
    # Nombre de carte(s) dans la 'Main du Joueur'
    @nb_hand_cards = 0
    # Solde du Joueur
    @sold = $begin_sold
    # On ppositionne la zone de la "Citadelle" sur la table de jeu en fonction du nombre de joueurs
    set_position
    # On affice la Couronne
    display_crown
    # On affiche le 'Solde' du 'Joueur'
    display_sold
    # On affiche le nombre de cartes de la 'Main du Joueur'
    display_nb_cards
    # On dispose les indicateurs de jeu
    set_pos_indicators
    # On dessine les zones des cartes
    paint_card_zone
    #todo implémenter la récupération de l'avatar du joueur
    # On affiche l'avatar et le nom du Joueur
    set_player_label player_name
    # Score du Joueur
    set_score
  end

  def boundingRect
    Qt::RectF.new 0, -($card_height*$zoom_out_ratio + $card_space), $area_width, $area_height
  end

  def activate
    @player_label.activate
  end

  def deactivate
    @player_label.deactivate
  end

  def set_player_label player_name, avatar="avatar"
    @player_label = PlayerLabel.new @player_id, player_name, avatar, false, self

    # Positionnement du Label
    x = ($area_width - @player_label.width)/2
    y = $card_height*$zoom_out_ratio + $card_space
    @player_label.setPos x, y
    @player_label.setToolTip "Score : 0"
  end

  # Affichage de l'icône et de la valeur associé du solde du joueur
  def display_sold
    # Affichage de l'icône représentant le 'solde' du joueur
    money_pix = Qt::Pixmap.new "./img/money.png"
    money_pix = money_pix.scaled $icon_wide, $icon_wide, Qt::IgnoreAspectRatio, Qt::SmoothTransformation
    @coins = Qt::GraphicsPixmapItem.new money_pix, self
    # Définit le contenu de la bulle d'aide
    @coins.setToolTip "Solde du Joueur"
    # Affichage du montant du 'solde' du joueur
    @txt_sold = Qt::GraphicsTextItem.new @sold.to_s, self
    font = Qt::Font.new $indicator_font, $icon_wide/2
    @txt_sold.setFont font
  end

  # Met à jour l'indicateur du solde du joueur
  def refresh_sold
    @txt_sold.setPlainText @sold.to_s
  end

  # Affichage de l'icône et de la valeur associé du nombre de cartes de la 'Main du Joueur'
  def display_nb_cards
    # Affichage de l'icône représentant le nombre de cartes de la 'Main du Joueur'
    cards_pix = Qt::Pixmap.new "./img/cards.png"
    cards_pix = cards_pix.scaled $icon_wide, $icon_wide, Qt::IgnoreAspectRatio, Qt::SmoothTransformation
    @cards = Qt::GraphicsPixmapItem.new cards_pix, self
    # Définit le contenu de la bulle d'aide
    @cards.setToolTip "Nombre de cartes de la main du Joueur"
    # Affichage du montant du nombre de cartes de la 'Main du Joueur'
    @txt_nb_cards = Qt::GraphicsTextItem.new @nb_hand_cards.to_s, self
    font = Qt::Font.new $indicator_font, $icon_wide/2
    @txt_nb_cards.setFont font
  end

  # Met à jour l'indicateur du nombre de cartes dans la 'Main du Joueur'
  def refresh_nb_cards nb_cards
    @nb_hand_cards = nb_cards.to_i
    @txt_nb_cards.setPlainText nb_cards
  end

  def display_crown
    # Affichage de l'icône représentant le nombre de cartes de la 'Main du Joueur'
    crown_pix = Qt::Pixmap.new "./img/crown.png"
    crown_pix = crown_pix.scaled $card_width*$zoom_out_ratio, $icon_wide, Qt::IgnoreAspectRatio, Qt::SmoothTransformation
    @crown = Qt::GraphicsPixmapItem.new crown_pix, self
    # Définit le contenu de la bulle d'aide
    @crown.setToolTip "Longue vie au Roi !"
    @crown.hide
  end

  def set_pos_indicators
    y = -$card_space*2 - $card_height*$zoom_out_ratio - $icon_wide
    width = @cards.boundingRect.width+@txt_nb_cards.boundingRect.width+@coins.boundingRect.width+@txt_sold.boundingRect.width
    indent = ($area_width-$card_width*$zoom_out_ratio-$card_space-width)/3
    @crown.setPos 0, y
    x = $card_width*$zoom_out_ratio + $card_space + indent
    @coins.setPos x, y
    x += @coins.boundingRect.width
    @txt_sold.setPos x, y
    x += @txt_sold.boundingRect.width + indent
    @cards.setPos x, y
    x += @cards.boundingRect.width
    @txt_nb_cards.setPos x, y
  end

  # Déplace la carte 'Quartier' à sa place dans la cité
  def build_dist card_id
    # Si le Joueur pose le dernier Quartier à construire on vérifie s'il a été le premier
    if @districts.length+1 == $max_dist
      unless scene.players.find {|player| player.citadel.districts.length == $max_dist}
        player = scene.players.find {|player| player.id == @player_id}
        player.first = true
      end
    end
    # On récupère la carte correspondant à l'identifiant donné
    card = scene.dist_deck_dup.find {|card| card.id == card_id}
    # On vérifie si le Joueur a bâtit le quartier 'Fabrique' dans sa cité
    (@districts.find {|dist| dist.id == "fab"}) ? cost=card.cost-1 : cost=card.cost
    # On déduit du 'Solde' du joueur le coût de la construction
    @sold -= cost
    # On met à jour l'indicateur du 'Solde'
    refresh_sold
    # On ajoute la carte
    add_dist card
  end

  # Ajoute et positionne la carte sur le plateau
  def add_dist card
    # On insère la carte 'Quartier'
    @districts << card
    # On définit l'index de la carte 'Quartier' dans la 'Citadelle'
    card.i_card = @districts.length
    # On positionne la carte 'Quartier' dans la Citadelle
    set_card_position card
  end

  # Positionne la carte 'Quartier' dans la Citadelle
  def set_card_position card
    # On tourne la carte face découverte
    card.reveal unless card.revealed
    # On réduit la carte si son état précédent était agrandit
    card.reduce if card.zoomed
    # On définit la carte comme enfant de la 'Citadelle'
    card.setParentItem self
    # Définit le contenu de la bulle d'aide
    card.setToolTip "Clic 'Droit' pour agrandir la carte."
    # Calcul des coordonnées de la carte dans la 'Citadelle' en fonction de son arrivée
    if card.i_card > 4
      y = -@line_indent
      card.i_card -= 4
    end
    x = @character_indent + @district_indent*(card.i_card-1)
    # On positionne la carte 'Quartier' dans la Citadelle
    card.move_to x, y, 1
    # Mise à jour du Score
    set_score
  end

  # Détruit la carte 'Quartier' de sa place dans la cité
  def destruct_dist card
    # On défausse la carte 'Quartier' correspondante
    scene.discard_dist.add_card card.id
    # On supprime la carte du référencement de la "Citadelle" du Joueur
    @districts.delete_if {|dist| dist == card}
    # On réorganise les cartes
    new_districts = @districts.dup
    @districts = []
    for dist in new_districts
      add_dist dist
    end
    set_score
  end

  # Calcul le score du Joueur
  def set_score
    score = 0
    for dist in @districts
      # Pouvoir de la carte 'Trésor impérial'
      case dist.id
        when "ti"
          if $treasure_add
            score += dist.cost + dist.upgrade + @sold
          else
            score += @sold
          end
        when "mus"
          score += dist.cost + dist.museum_cards.length
        else
          score += dist.cost + dist.upgrade
      end
      # Pouvoir des cartes 'Dracoport' et 'Université'
      score += 2 if %w(dra uni).include? dist.id
    end
    # Pouvoir de la carte 'Fontaine aux souhaits'
    if @districts.find {|dist| dist.id == "fas"}
      nb_prestige = @districts.find_all {|dist| dist.cat == "prestige"}
      score += nb_prestige.length
    end
    cats = %w(commerce militaire noblesse prestige religion)
    nb_cat = 0
    for cat in cats
      nb_cat += 1 if @districts.find {|dist| dist.cat == cat}
    end
    #todo la 'Cour des miracles' n'est pas effective lors du tour de construction
    # Pouvoir de la carte 'Cour des miracles'
    nb_cat += 1 if @districts.find {|dist| dist.id == "cdm"}
    score += 3 if nb_cat >= 5
    if @districts.length == $max_dist
      player = scene.players.find {|player| player.id == @player_id}
      player.first ? score+=4 : score+=2
    end
    # Pouvoir de la carte 'Salle des cartes'
    score += @nb_hand_cards if @districts.find {|dist| dist.id == "sdc"}
    @score = score
    @player_label.setToolTip "Score : #@score"
  end

  # Place la carte 'Personnage' sur la zone de jeu du joueur
  def add_character card
    # On affiche la carte
    card.show
    # On définit la carte comme enfant de la 'Citadelle'
    card.setParentItem self
    # Définit le contenu de la bulle d'aide
    card.setToolTip "Clic 'Droit' pour agrandir la carte"
    # On met la carte face cachée
    card.un_reveal if card.revealed
    # On ajoute la carte 'Personnage' à la 'Citadelle'
    @characters << card
    card.i_card = @characters.length

    @characters.length < 2 ? y=0 : y=-@line_indent
    card.move_to 0, y
  end

  # Dessine les zones des emplacements des cartes de la 'Citadelle'
  def paint_card_zone
    # Caractéristiques des lignes des cadres de survol
    pen = Qt::Pen.new(Qt::Color.new(150, 150, 150))
    pen.setWidth 1
    pen.setStyle(Qt::DashLine)
    pen.setCapStyle(Qt::RoundCap)
    pen.setJoinStyle(Qt::RoundJoin)

    # Création du cadre de délimitation de la zone de la carte
    painter = Qt::PainterPath.new
    painter.addRoundedRect 0, 0, $card_width*$zoom_out_ratio, $card_height*$zoom_out_ratio, 5, 5

    # Valeur de décalage des cartes 'Quartier'
    @character_indent = $card_width*$zoom_out_ratio + $card_space*2
    @district_indent = $card_width*$zoom_out_ratio + $card_space
    @line_indent = $card_height*$zoom_out_ratio + $card_space

    # Dessine les zones des cartes 'Quartier'
    i_card = 0
    while i_card < $max_dist
      border = Qt::GraphicsPathItem.new painter, self
      border.setPen pen

      i_card_bis = i_card
      if i_card_bis < 4
        y = 0
      else
        y = -@line_indent
        i_card_bis -= 4
      end
      x = @character_indent + @district_indent*(i_card_bis)
      border.setPos x, y
      border.setZValue 0
      # Définit le contenu de la bulle d'aide
      border.setToolTip "Emplacement 'Quartier'"
      @card_zones << border
      i_card += 1
    end

    # Dessine la zone de la carte 'Personnage'
    border = Qt::GraphicsPathItem.new painter, self
    border.setPen pen
    border.setPos 0, 0
    border.setZValue 0
    # Définit le contenu de la bulle d'aide
    border.setToolTip "Emplacement 'Personnage'"
    @card_zones << border

    # Dessine une 2ème zone de carte 'Personnage' si la partie est composée de 2 ou 3 joueurs
    if $nb_players < 4
      border = Qt::GraphicsPathItem.new painter, self
      border.setPen pen
      border.setPos 0, -@line_indent
      border.setZValue 0
      # Définit le contenu de la bulle d'aide
      border.setToolTip "Emplacement 'Personnage'"
      @card_zones << border
    end
  end

  def re_init_card_zone
    for card_zone in @card_zones
      card_zone.setParentItem nil
      scene.removeItem card_zone
    end
    paint_card_zone
  end

  # Initialise les coordonnées de l'aire de jeu du joueur
  def set_position
    case $nb_players
      # Positionnement pour 2 joueurs
      when 2
        case @player_id
          when 1
            to_the_down_center
          when 2
            to_the_top_center
          else
        end
      # Positionnement pour 3 joueurs
      when 3
        case @player_id
          when 1
            to_the_down_center
          when 2
            to_the_top_left
          when 3
            to_the_top_right
          else
        end
      # Positionnement pour 4 joueurs
      when 4
        case @player_id
          when 1
            to_the_down_center
          when 2
            to_the_top_left_corner
          when 3
            to_the_top_center
          when 4
            to_the_top_right_corner
          else
        end
      # Positionnement pour 5 joueurs
      when 5
        case @player_id
          when 1
            to_the_down_center
          when 2
            to_the_top_left_corner
          when 3
            to_the_top_left
          when 4
            to_the_top_right
          when 5
            to_the_top_right_corner
          else
        end
      # Positionnement pour 6 joueurs
      when 6
        case @player_id
          when 1
            to_the_down_center
          when 2
            to_the_down_left_corner
          when 3
            to_the_top_left_corner
          when 4
            to_the_top_center
          when 5
            to_the_top_right_corner
          when 6
            to_the_down_right_corner
          else
        end
      # Positionnement pour 7 joueurs
      when 7
        case @player_id
          when 1
            to_the_down_center
          when 2
            to_the_down_left_corner
          when 3
            to_the_top_left_corner
          when 4
            to_the_top_left
          when 5
            to_the_top_right
          when 6
            to_the_top_right_corner
          when 7
            to_the_down_right_corner
          else
        end
      # Positionnement pour 8 joueurs
      when 8
        case @player_id
          when 1
            to_the_down_left
          when 2
            to_the_down_left_corner
          when 3
            to_the_top_left_corner
          when 4
            to_the_top_left
          when 5
            to_the_top_right
          when 6
            to_the_top_right_corner
          when 7
            to_the_down_right_corner
          when 8
            to_the_down_right
          else
        end
      else
        # Aucune action
    end
    setRotation @angle
    setPos @x, @y
  end

  def to_the_down_right
    @angle = 0
    @x = $area_space/2
    @y = $scene_height/2 - $border_space - $card_space - $card_height*$zoom_out_ratio
  end

  def to_the_down_center
    @angle = 0
    @x = -$area_width/2
    @y = $scene_height/2 - $border_space - $card_space - $card_height*$zoom_out_ratio
  end

  def to_the_down_left
    @angle = 0
    @x = -$area_width - $area_space/2
    @y = $scene_height/2 - $border_space - $card_space - $card_height*$zoom_out_ratio
  end

  def to_the_top_left
    @angle = 180
    @x = -$area_space/2
    @y = -($scene_height/2 - $border_space - $card_space - $card_height*$zoom_out_ratio)
  end

  def to_the_top_center
    @angle = 180
    @x = $area_width/2
    @y = -($scene_height/2 - $border_space - $card_space - $card_height*$zoom_out_ratio)
  end

  def to_the_top_right
    @angle = 180
    @x = $area_width + $area_space/2
    @y = -($scene_height/2 - $border_space - $card_space - $card_height*$zoom_out_ratio)

  end

  def to_the_down_left_corner
    @angle = 45
    line_indent = $card_height*$zoom_out_ratio/2 + $card_space + $card_height*$zoom_out_ratio
    @x = -($scene_width/2 - $border_space - (line_indent / Math.sqrt(2)).ceil)
    line_indent = $card_space + $card_height*$zoom_out_ratio
    @y = (line_indent / Math.sqrt(2)).ceil + $rotate_area_space/2
  end

  def to_the_down_right_corner
    @angle = -45
    line_indent = $card_space + $card_height*$zoom_out_ratio
    @x = +($scene_width/2 - $border_space - ($rotate_area_wide - (line_indent / Math.sqrt(2)).ceil))
    line_indent = $card_height*$zoom_out_ratio/2 + $card_space + $card_height*$zoom_out_ratio
    @y = +(($rotate_area_wide - (line_indent / Math.sqrt(2)).ceil) + $rotate_area_space/2)
  end

  def to_the_top_left_corner
    @angle = 135
    line_indent = $card_space + $card_height*$zoom_out_ratio
    @x = -($scene_width/2 - $border_space - ($rotate_area_wide - (line_indent / Math.sqrt(2)).ceil))
    line_indent = $card_height*$zoom_out_ratio/2 + $card_space + $card_height*$zoom_out_ratio
    @y = -(($rotate_area_wide - (line_indent / Math.sqrt(2)).ceil) + $rotate_area_space/2)
  end

  def to_the_top_right_corner
    @angle = -135
    line_indent = $card_height*$zoom_out_ratio/2 + $card_space + $card_height*$zoom_out_ratio
    @x = $scene_width/2 - $border_space - (line_indent / Math.sqrt(2)).ceil
    line_indent = $card_space + $card_height*$zoom_out_ratio
    @y = -((line_indent / Math.sqrt(2)).ceil + $rotate_area_space/2)
  end
end