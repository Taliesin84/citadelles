# encoding: utf-8

require 'Qt4'
require './gui/game/submit_banner'

class CardSelection < Qt::GraphicsItem

  attr_reader :cards, :submit_next, :submit_prev
  attr_accessor :min_select, :max_select

  def initialize submit_txt, min_select, max_select, previous, parent, prev_type="prev", prev_txt="Etape Précédente"
    super(parent)
    @min_select = min_select
    @max_select = max_select
    @previous = previous
    # On initie à 0 le nombre de cartes dans la zone de choix
    @cards = []
    # On positionne la zone de choix
    @_x = -(($scene_width - $card_width)/2)
    @_y = -$card_height/2
    setPos @_x, @_y
    setAcceptedMouseButtons Qt::LeftButton
    # On accepete de récupérer de manière indépendante les évènements de type survol de la souris
    setAcceptHoverEvents true
    # On créé le bouton de validation
    unless submit_txt == ""
      @submit_next = SubmitBanner.new "next", submit_txt, self
      if @previous
        @submit_next.setPos ($scene_width-$card_width)/2+$area_space, $card_height*5/4
        @submit_prev = SubmitBanner.new prev_type, prev_txt, self
        @submit_prev.setPos ($scene_width-$card_width-@submit_prev.width)/2, $card_height*5/4
      else
        @submit_next.setPos ($scene_width-$card_width-@submit_next.width)/2, $card_height*5/4
      end
      @submit_next.hide
    end
  end

  def boundingRect
    Qt::RectF.new 0, 0, $scene_width, $card_height*3/2
  end

  def add_char_card card_id, available=true, txt_toot_tip="", revealed=true
    # On récupère la carte correspondant à l'identifiant donné
    card = scene.char_deck_ref.find {|card| card.id == card_id}
    available ? card.available : card.unavailable
    card.setToolTip txt_toot_tip
    add_card card, revealed
  end

  def add_dist_card card_id, available=true, txt_toot_tip="", revealed=true
    # On récupère la carte correspondant à l'identifiant donné
    card = scene.dist_deck_ref.find {|card| card.id == card_id}
    available ? card.available : card.unavailable
    card.setToolTip txt_toot_tip
    add_card card, revealed
  end

  # Ajoute la carte à l'item graphique et organise son affichage
  def add_card card, revealed
    # On incrémente le nombre de cartes
    @cards << card
    # On associe la carte à la zone de choix
    card.setParentItem self
    # On agrandit la taille de la carte
    card.enlarge unless card.zoomed
    # On révèle la carte
    revealed ? card.reveal : card.un_reveal
    # On la déselectionne
    card.selected = false
    # On rend visible la carte
    card.show
    card.i_card = @cards.length
    for item in @cards
      if @cards.length < 4
        indent = (($scene_width-$card_width) - ($card_width + ($card_space*$ratio+$card_width)*(@cards.length-1)))/2
        x = indent + (($card_space*$ratio+$card_width)*(item.i_card-1))
      else
        x = (($scene_width-$card_width*2)/(@cards.length-1)) * (item.i_card-1)
      end
      item.move_to x, 0, item.i_card
    end
  end

  # Réinitialise la zone de choix
  def re_init
    for card in @cards
      card.re_init_effects
      card.reduce
      card.selected = false
      card.available
      card.hide
      card.i_card = 0
      card.setParentItem nil
      card.setToolTip ""
    end
    @cards = []
  end

  def disp_submit
    card_selected = @cards.find_all {|card| card.selected}
    if card_selected.length < @min_select
      @submit_next.hide
      if @previous
        @submit_prev.setPos ($scene_width-$card_width-@submit_prev.width)/2, $card_height*5/4
      end
    else
      if @previous
        @submit_prev.setPos ($scene_width-$card_width)/2-$area_space-@submit_prev.width, $card_height*5/4
      end
      @submit_next.show
    end
  end
end