# encoding: utf-8

require 'Qt4'

class DiscardingDist < Qt::GraphicsItem

  attr_accessor :cards

  def initialize
    super()
    @cards = []
    # On positionne la défausse sur la table de jeu
    @_x = -$area_width/2 - $area_space/2 + $card_width*$zoom_out_ratio
    @_y = -$card_height*$zoom_out_ratio/2
    setPos @_x, @_y
    # Dessine la zone de la 'Défausse'
    paint_card_zone
  end

  def boundingRect
    Qt::RectF.new(-$card_width*0.5, -$card_height*0.5, $card_width*$zoom_out_ratio*1.5, $card_height*$zoom_out_ratio*1.5)
  end

  def add_card dist_id
    card = scene.dist_deck_dup.find {|dist| dist.id == dist_id}
    @cards << card
    card.reveal
    card.reduce if card.zoomed
    if card.id == "mus"
      for dist in card.museum_cards
        dist.show
        add_card dist.id
      end
    end
    card.re_init
    card.setParentItem self
    card.setToolTip ""
    card.move_to (rand(2*20)-20), (rand(2*20)-20), @cards.length, rand(2*30)-30
  end

  # Dessine la zone des emplacements des cartes de la 'Défausse'
  def paint_card_zone
    # Caractéristiques des lignes des cadres de survol
    pen = Qt::Pen.new $grey
    pen.setWidth 1
    pen.setStyle(Qt::DashLine)
    pen.setCapStyle(Qt::RoundCap)
    pen.setJoinStyle(Qt::RoundJoin)

    # Création du cadre de délimitation de la zone de la carte
    painter = Qt::PainterPath.new
    painter.addRoundedRect 0, 0, $card_width*$zoom_out_ratio, $card_height*$zoom_out_ratio, 5, 5
    border = Qt::GraphicsPathItem.new painter, self
    border.setPen pen
    border.setPos 0, 0
    # Définit le contenu de la bulle d'aide
    border.setToolTip "Défausse"
  end
end