# encoding: utf-8

require 'Qt4'
require './gui/game/submit_banner'

class PlayerSelection < Qt::GraphicsItem

  attr_reader :players, :submit_next

  def initialize submit_txt, previous, parent=nil
    super(parent)

    @previous = previous
    # On initie à 0 le nombre de cartes dans la zone de choix
    @players = []
    # On positionne la zone de choix
    @_x = -$scene_width/2
    @_y = -$card_height/2
    setPos @_x, @_y
    setZValue 10
    # On accepete de récupérer de manière indépendante les évènements de type survol de la souris
    setAcceptHoverEvents true
    if submit_txt != ""
      # On créé le bouton de validation
      @submit_next = SubmitBanner.new "next", submit_txt, self
      if @previous
        @submit_next.setPos ($scene_width+$area_space)/2, $card_height*5/4
        @submit_prev = SubmitBanner.new "prev", "Etape Précédente", self
        @submit_prev.setPos ($scene_width-@submit_prev.width)/2, $card_height*5/4
      else
        @submit_next.setPos ($scene_width-@submit_next.width)/2, $card_height*5/4
      end
      @submit_next.hide
    else
      if @previous
        @submit_prev = SubmitBanner.new "prev", "Etape Précédente", self
        @submit_prev.setPos ($scene_width-@submit_prev.width)/2, $card_height*5/4
      end
    end
  end

  def boundingRect
    Qt::RectF.new 0, 0, $scene_width, $card_height*3/2
  end

  # Ajoute le 'Label' du Joueur à l'item graphique et organise son affichage
  #todo Problèmes d'affichage
  def add_player player_id, selectable=true, txt_toot_tip=""
    player = scene.players.find {|player| player.id == player_id}
    player_label = PlayerLabel.new player.id, player.name, 'avatar', true, self
    @players << player_label
    unless selectable
      player_label.unavailable
    end
    player_label.setToolTip txt_toot_tip
    ratio = 2
    player_label.scale ratio, ratio
    player_label.id = @players.length
    for player in @players
      if @players.length < 5
        indent = ($card_height*5/4 - (player.height*ratio)*(@players.length) - $area_space*(@players.length))/2
        x = ($scene_width-player.width*ratio)/2
        y = indent + (player.height*ratio+$area_space)*(player.id-1)
      else
        if player.player_id <= (@players.length/2)+@players.length%2
          # Colonne de Gauche
          indent = ($card_height*5/4 - (player.height*ratio)*((@players.length/2)+@players.length%2) - $area_space*((@players.length/2)+@players.length%2))/2
          x = ($scene_width/2 - player.width*ratio)/2
          y = indent + (player.height*ratio+$area_space)*(player.id-1)
        else
          # Colonne de Droite
          indent = ($card_height*5/4 - (player.height*ratio)*(@players.length/2) - $area_space*(@players.length/2))/2
          x = $scene_width/2 + ($scene_width/2 - player.width*ratio)/2
          y = indent + (player.height*ratio+$area_space)*(player.id-1-((@players.length/2)+@players.length%2))
        end
      end
      player.setPos x, y
    end
  end

  def disp_submit
    player_selected = @players.find {|player| player.selected}
    if player_selected.nil?
      @submit_next.hide
      if @previous
        @submit_prev.setPos ($scene_width-@submit_prev.width)/2, $card_height*5/4
      end
    else
      if @previous
        @submit_prev.setPos ($scene_width-$area_space)/2-@submit_prev.width, $card_height*5/4
      end
      @submit_next.show
    end
  end
end