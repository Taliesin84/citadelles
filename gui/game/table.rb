# encoding: utf-8

require 'Qt4'
require './gui/game/disp_char_power'
require './gui/game/district_card'
require './gui/game/character_card'
require './gui/game/player'
require './gui/game/drawing_dist'
require './gui/game/discarding_dist'
require './gui/game/discarding_char'
require './gui/game/action_panel'
require './gui/game/deck'
require './gui/game/buttons'
require './gui/game/history'
require './gui/game/chat'
require './helpers/game_parameters'
require './helpers/parameters'
require './helpers/table_helpers'

class Table < Qt::GraphicsScene

  include ToggleFunctions
  include GameParameters
  include Parameters
  include Deck

  attr_reader :font,
              :history,
              :chat,
              :action,
              :zoom_card,
              :disp_char_power,
              :zoom_player_hand,
              :draw_dist,
              :discard_dist,
              :discard_char,
              :dist_deck_ref,
              :dist_deck_dup,
              :char_deck_ref,
              :char_deck_dup

  attr_accessor :players,
                :char_deck

  def initialize background, players, characters, districts, parameters, player=nil

    super()

    # Paramètres 'Graphique' de la partie
    $scene_width = 1440
    $scene_height = 810
    $ratio = 5.0 # Définition du ratio de réduction des cartes
    $card_ratio = 323.0/500.0 # Définition du rapport largeur/hauteur des cartes
    $card_space = 4 # Définition de l'espace entre chaque carte
    $icon_wide = 32 # Définition de la taille des icônes des indicateurs

    set_colors
    set_polices
    set_dimensions
    setSceneRect -$scene_width/2, -$scene_height/2, $scene_width, $scene_height

    set_background background

    # Paramètres de jeu
    set_game_parameters players, characters, districts, parameters, player
    #set_game_parameters
    # Mise en place des éléments de la table
    set_table_elements


    for player in @players
      $begin_nb_cards.times {
        player.draw_card @draw_dist
      }
    end

    #1.times {
    #  dist = @players[0].hand.cards.min_by {|card| card.cost}
    #  @players[0].build dist.id
    #}
    #@players[1].earn 2
    #@players[2].earn 3
    #@players[0].enthrone

    #dist = @dist_deck_dup.find {|card| card.id == "mnf"}
    #@players[0].build dist.id
    #dist = @dist_deck_dup.find {|card| card.id == "lab"}
    #@players[0].build dist.id

    #@players[0].earn 10
    #@players[0].enthrone
    #dist = @dist_deck_dup.find {|card| card.id == "mus"}
    #@players[0].build dist.id
    #dist.add_card "mar1"
    #dist = @dist_deck_dup.find {|card| card.id == "don"}
    #@players[1].build dist.id
    #dist = @dist_deck_dup.find {|card| card.id == "pal1"}
    #@players[2].build dist.id
    #dist = @dist_deck_dup.find {|card| card.id == "gm"}
    #@players[2].build dist.id

    @font.show

    @action = ActionPanel.new
    addItem @action
    @history.insert_turn 1
    #@action.current_char = @char_deck_ref[3]

    @action.current_player = @players[0]
    @action.disp_turn_toss

    #@action.call_next_char
  end

  def set_background font
    # Affichage de l'image de fond
    @background = Qt::Pixmap.new "./img/backgrounds/#{font}"
    # Mise aux dimensions de l'image de fond
    @background = @background.scaled $scene_width, $scene_height
  end

  def set_table_elements
    # On créé l'arrière plan translucide pour les zooms et phases d'actions
    set_font
    # On créé les boutons de navigation entre les différents écrans
    set_buttons
    # On créé tous les éléments propre aux Joueurs
    set_players
    # Création de la Pioche
    @draw_dist = DrawingDist.new
    addItem @draw_dist
    # Création des autres Decks
    init_decks
    # Création de la Défausse des cartes 'Quartier'
    @discard_dist = DiscardingDist.new
    addItem @discard_dist
    # Création de la Défausse des cartes 'Personnage'
    @discard_char = DiscardingChar.new
    addItem @discard_char
    # Création de l'écran permettant l'agrandissement des cartes de la table
    @zoom_card = ZoomCard.new
    addItem @zoom_card
    # Création de l'écran permettant l'agrandissement de la 'Main du Joueur'
    @zoom_player_hand = ZoomPlayerHand.new
    addItem @zoom_player_hand
    # Création de l'écran affichant les 'Pouvoirs des Personnages'
    @disp_char_power = DispCharPower.new
    addItem @disp_char_power
    # Création de l'écran affichant l''Historique'
    @history = History.new
    addItem @history
    unless $player.nil?
      # Création de l'écran affichant le 'Chat'
      @chat = Chat.new "127.0.0.1", @players[0]
      addItem @chat
    end
  end

  # On créé tous les éléments propre aux Joueurs
  def set_players
    @players = []
    i = 1
    while i <= $nb_players
      # On créé les Joueurs
      player = Player.new i, @players_name[i-1]
      @players << player
      # On créé les mains des Joueurs
      addItem player.hand
      # On définit les emplacements des 'Citadelles' des Joueurs
      addItem player.citadel
      i += 1
    end
  end

  def set_buttons
    # Création du bouton permettant de permuter entre l''Action' et la 'Table'
    addItem Toggle.new
    # Création du bouton permettant d'afficher l''Historique'
    addItem HistoryButton.new
    # Création du bouton permettant d'afficher le 'Chat'
    addItem ChatButton.new unless $player.nil?
    # Création du bouton permettant d'afficher les 'Pouvoirs des Personnages'
    addItem CharPowerButton.new
  end

  # Initialisation des Decks
  def init_decks
    # Création du Deck 'Personnage'
    @char_deck = create_char_deck
    for card in @char_deck
      card.hide
      addItem card
    end
    # Création du duplicat du Deck 'Personnage'
    @char_deck_dup = @char_deck.dup
    # Création du Deck de référence 'Personnage'
    @char_deck_ref = create_char_deck
    for card in @char_deck_ref
      card.hide
      addItem card
    end
    @dist_deck_dup = @draw_dist.cards.dup
    # Création du Deck de référence 'Quartier'
    @dist_deck_ref = create_dist_deck
    for card in @dist_deck_ref
      card.hide
      addItem card
    end
  end

  # Dessine un arrière plan translucide
  def set_font
    @font = Qt::GraphicsRectItem.new -$scene_width/2, -$scene_height/2, $scene_width, $scene_height
    @font.setPen Qt::Pen.new Qt::NoPen
    grey = Qt::Color.new 50, 50, 50, 210
    @font.setBrush Qt::Brush.new grey
    @font.setZValue 9
    addItem @font
    @font.hide
  end

  # Fonction de gestion de l'affichage de l'image de fond
  def drawBackground painter, zone
    painter.drawPixmap -$scene_width/2, -$scene_height/2, $scene_width, $scene_height, @background
  end

  #def mousePressEvent event
  #  puts items(event.buttonDownScenePos(Qt::LeftButton))
  #end

  def keyPressEvent event
    case event.key
      when Qt::Key_Space
        action_toggle self
      when Qt::Key_C
        chat_toggle self unless $player.nil?
      when Qt::Key_H
        history_toggle self
      when Qt::Key_I
        char_power_toggle self
      else
        # Nothing
    end
  end

end

#app = Qt::Application.new ARGV
#
#font = "fond1.png"
#table = Table.new font
#
#view = Qt::GraphicsView.new table
#view.setFixedSize $scene_width + 10, $scene_height + 10
#view.setWindowTitle "Citadelles"
#view.setRenderHints(Qt::Painter::Antialiasing | Qt::Painter::SmoothPixmapTransform)
#view.show
#
#app.exec