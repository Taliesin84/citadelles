# encoding: utf-8

require 'Qt4'

# Permet l'affichage d'une carte
class DisplayCard < Qt::GraphicsItem

  attr_reader :card

  def initialize parent=nil
    super(parent)
    @card = nil
    @prev_parent = nil
    @prev_revealed = false
    setPos -$card_width/2, -$card_height/2
    setZValue 10
  end

  def boundingRect
    Qt::RectF.new -$card_width/2, -$card_height/2, $card_width, $card_height
  end

  def add_card card, txt_tool_tip=""
    # On réinitialise tous les effets
    card.re_init_effects
    # On ajoute la carte
    @card = card
    # On récupère son état précédent
    @prev_parent = card.parentItem
    @prev_revealed = card.revealed
    # On affiche la carte
    @card.show
    # On associe la carte à l'item
    @card.setParentItem self
    # On agrandie la carte
    @card.enlarge
    # On révèle la carte
    @card.reveal
    # On positionne la carte
    @card.setPos 0, 0
    @card.setToolTip txt_tool_tip
  end

  # Suppression de la carte dans le cas d'une utilisation dans 'ZoomCard'
  def del_card
    # On réinitialise tous les effets
    @card.re_init_effects
    # On remet la carte face cachée si elle l'était précédemment
    @card.un_reveal unless @prev_revealed
    # On réduit la carte
    @card.reduce
    # On rend la carte à son item parent d'origine
    @card.setParentItem @prev_parent
    # On positionne la carte dans sa position précédente
    @card.setPos @card._x, @card._y
    @card.setZValue @card.i_card
    # On retire la carte
    @card = nil
  end

  # Suppression de la carte dans le cas d'une utilisation dans 'ActionPanel'
  def re_init
    # On dissimule la carte
    @card.hide
    setToolTip ""
    del_card
  end

end