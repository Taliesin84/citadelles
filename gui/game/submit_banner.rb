# encoding: utf-8

require 'Qt4'

class SubmitBanner < Qt::GraphicsItem

  attr_reader :type, :width, :height, :scroll_banner

  def initialize type, label_txt, parent=nil
    super(parent)

    @type = type
    # On accepte de récupérer de manière indépendante les évènements de type survol de la souris
    setAcceptHoverEvents true

    if @type == "next"
      setToolTip "Valider pour passer à l'étape suivante"
    elsif @type == "prev"
      setToolTip "Valider pour revenir à l'étape précédente"
    end

    # Création de l'icône de validation
    create_hover_effect
    init_label label_txt
    set_icon
    set_background # Création des effets de survol

    set_positions
    setZValue 2
  end

  def boundingRect
    Qt::RectF.new 0, 0, @width, @height
  end

  def hoverMoveEvent event
    hoverEnterEvent event
  end

  def hoverEnterEvent event
    setCursor Qt::Cursor.new(Qt::PointingHandCursor)
    @red_label_effect.setEnabled true
    @red_icon_effect.setEnabled true
  end

  def hoverLeaveEvent event
    @red_label_effect.setEnabled false
    @red_icon_effect.setEnabled false
  end

  # Création des effets
  def create_hover_effect

    # Caractéristiques des lignes des cadres de survol
    pen = Qt::Pen.new $red
    pen.setWidth 2
    pen.setStyle(Qt::SolidLine)
    pen.setCapStyle(Qt::RoundCap)
    pen.setJoinStyle(Qt::RoundJoin)

    # Création d'un effet Rouge pour l'icône
    @red_label_effect = Qt::GraphicsColorizeEffect.new
    @red_label_effect.setColor $red
    @red_label_effect.setEnabled false

    @red_icon_effect = Qt::GraphicsColorizeEffect.new
    @red_icon_effect.setColor $red
    @red_icon_effect.setEnabled false
  end

  # Affichage du nom/pseudo du joueur
  def init_label label_txt
    @label = Qt::GraphicsTextItem.new label_txt, self
    font = Qt::Font.new $main_font, $icon_wide
    @label.setFont font
    @label.setGraphicsEffect @red_label_effect
    @label.setZValue 2
  end

  # Affichage du fond
  def set_background
    scroll_banner_pix = Qt::Pixmap.new "./img/scroll_banner.png"
    @scroll_banner = Qt::GraphicsPixmapItem.new scroll_banner_pix, self
    @scroll_banner.setTransformationMode Qt::SmoothTransformation
    @scroll_banner.setZValue 1
  end

  def set_icon
    icon_pix = Qt::Pixmap.new "./img/#{@type}.png"
    icon_pix = icon_pix.scaled $icon_wide, $icon_wide, Qt::IgnoreAspectRatio, Qt::SmoothTransformation
    @icon = Qt::GraphicsPixmapItem.new icon_pix, self
    @icon.setTransformationMode Qt::SmoothTransformation
    @icon.setGraphicsEffect @red_icon_effect
    @icon.setZValue 2
  end

  def set_positions
    @width = ((@label.boundingRect.width+@icon.boundingRect.width)*1.2).ceil
    $icon_wide > @label.boundingRect.height ? label_height=$icon_wide : label_height=@label.boundingRect.height
    @height = (label_height*1.34).ceil
    @width_ratio = @width / @scroll_banner.boundingRect.width
    @height_ratio = @height / @scroll_banner.boundingRect.height
    @scroll_banner.scale @width_ratio, @height_ratio
    x = (@label.boundingRect.width*0.1).ceil
    y = (@label.boundingRect.height*0.17).ceil
    if @type == "next"
      @label.setPos x, y
      @icon.setPos x+@label.boundingRect.width, (@height-$icon_wide)/2
    elsif %w(prev clock skip).include? @type
      @icon.setPos x, (@height-$icon_wide)/2
      @label.setPos x+$icon_wide+$card_space, y
    end
  end

  def mousePressEvent event
    if %w(CardSelection PlayerSelection).include? parentItem.class.to_s
      parentItem.parentItem.do_action @type
    elsif parentItem.class.to_s == "ActionPanel"
      parentItem.do_action @type
    end
  end
end