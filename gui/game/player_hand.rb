# encoding: utf-8

require 'Qt4'

class PlayerHand < Qt::GraphicsItem

  attr_reader :player_id
  attr_accessor :cards

  def initialize player_id
    super()
    @player_id = player_id
    @cards = []
    @zoomed = false
    # Facteur de grossissement de la 'Main du Joueur' lors du survol de la souris
    @hand_ratio = 1.6
    # On place l'aire qu'occupera la 'Main du Joueur'
    @_x = $card_width*$zoom_out_ratio
    @_y = -$card_height*$zoom_out_ratio/2
    setPos @_x, @_y
    setAcceptedMouseButtons Qt::LeftButton
    # On accepte de récupérer de manière indépendante les évènements de type survol de la souris
    setAcceptHoverEvents true
    setZValue 2
    @player_id == $player ? show : hide
  end

  def boundingRect
    Qt::RectF.new 0, 0, $area_width-$card_space, $card_height*$zoom_out_ratio
  end

  # Ajoute une carte à la main du Joueur
  def add_card card_id
    # On récupère la carte correspondant à l'identifiant donné
    card = scene.dist_deck_dup.find {|card| card.id == card_id}
    # On retire la carte du référencement de la pioche
    scene.draw_dist.cards.delete_if {|dist| dist.id == card_id} if card.parentItem.class.to_s == "DrawingDist"
    # On ajoute la carte
    @cards << card
    # On définit la carte comme enfant de la 'Main du Joueur'
    card.setParentItem self
    # On révèle la carte
    card.reveal
    # Définit le contenu de la bulle d'aide
    card.setToolTip "Clic 'Droit' pour agrandir la carte\nDouble Clic 'Gauche' pour agrandir toute votre main"
    # On définit l'index de la carte 'Quartier' dans la 'Main du Joueur'
    card.i_card = @cards.length
    # On positionne la carte
    set_position
  end

  # Retire une carte de la main du Joueur
  def remove_card card_id
    @cards.delete_if {|card| card.id == card_id}
    index = 1
    for card in @cards
      card.i_card = index
      set_position
      index += 1
    end
  end

  def set_position
    for item in @cards
      if @cards.length > 5
        indent = ($area_width - $card_space - $card_width*$zoom_out_ratio)/(@cards.length-1)
        x = indent * (item.i_card-1)
      else
        indent = ($area_width - $card_space - $card_width*$zoom_out_ratio - ((@cards.length-1) * ($card_width*$zoom_out_ratio + $card_space)))/2
        x = indent + (item.i_card-1) * ($card_width*$zoom_out_ratio + $card_space)
      end
      # On positionne la carte 'Quartier' dans la 'Main Du Joueur'
      item.move_to x, 0, item.i_card
    end
  end

  def mouseDoubleClickEvent event
    unless scene.zoom_player_hand.isVisible or @cards.empty? or scene.font.isVisible
      hoverLeaveEvent event
      scene.font.show
      scene.zoom_player_hand.show
      scene.zoom_player_hand.add_cards @cards, "Double Clic 'Gauche' pour réduire toute votre main"
    end
  end

  def hoverMoveEvent event
    hoverEnterEvent event
  end

  # Gestion de l'entrée du curseur de la souris
  def hoverEnterEvent event
    unless scene.font.isVisible or @zoomed
      zoom_in
    end
  end

  def zoom_in
    x = $card_width*$zoom_out_ratio - (($area_width-$card_space)*@hand_ratio - ($area_width-$card_space))/2
    y = pos.y*@hand_ratio
    setPos x, y
    scale @hand_ratio, @hand_ratio
    @zoomed = true
  end

  # Gestion de la sortie du curseur de la souris
  def hoverLeaveEvent event
    zoom_out if @zoomed
  end

  def zoom_out
    setPos @_x, @_y
    scale 1/@hand_ratio, 1/@hand_ratio
    @zoomed = false
  end
end