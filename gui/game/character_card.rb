# encoding: utf-8

require './gui/game/card'

class CharacterCard < Card

  attr_reader :number, :name, :cat, :power
  attr_accessor :power_used, :killed, :stolen, :bewitched

  def initialize number, name, cat="neutre"
    @back_pix = Qt::Pixmap.new "./img/cards/others/char_back.png"
    card_path = "characters/#{name}"
    super(name, card_path)

    @name = name
    @number = number
    @cat = cat
    @power_used = false
    @killed = false
    @stolen = false
    @bewitched = false
  end

  # Création des effets
  def create_effects
    super

    # Affichage de l'icône représentant le 'solde' du joueur
    dagger_pix = Qt::Pixmap.new "./img/dagger.png"
    dagger_pix = dagger_pix.scaled $icon_wide*2, $icon_wide*2, Qt::KeepAspectRatioByExpanding, Qt::SmoothTransformation
    @dagger = Qt::GraphicsPixmapItem.new dagger_pix, self
    # Définit le contenu de la bulle d'aide
    @dagger.setToolTip "'Personnage' assassiné(e)"
    @dagger.setPos $card_width-dagger_pix.width*0.7, -dagger_pix.height*0.3
    @dagger.setShapeMode 1
    @dagger.hide

    # Affichage de l'icône représentant le 'solde' du joueur
    treasure_pix = Qt::Pixmap.new "./img/treasure.png"
    treasure_pix = treasure_pix.scaled $icon_wide*2, $icon_wide*2, Qt::KeepAspectRatioByExpanding, Qt::SmoothTransformation
    @treasure = Qt::GraphicsPixmapItem.new treasure_pix, self
    # Définit le contenu de la bulle d'aide
    @treasure.setToolTip "'Personnage' volé(e)"
    @treasure.setPos $card_width-treasure_pix.width*0.7, -treasure_pix.height/2
    @treasure.setShapeMode 1
    @treasure.hide

    # Affichage de l'icône représentant le 'solde' du joueur
    witch_hat_pix = Qt::Pixmap.new "./img/witch_hat.png"
    witch_hat_pix = witch_hat_pix.scaled $icon_wide*2, $icon_wide*2, Qt::KeepAspectRatioByExpanding, Qt::SmoothTransformation
    @witch_hat = Qt::GraphicsPixmapItem.new witch_hat_pix, self
    # Définit le contenu de la bulle d'aide
    @witch_hat.setToolTip "'Personnage' ensorcelé(e)"
    @witch_hat.setPos $card_width-witch_hat_pix.width*0.6, -witch_hat_pix.height/2
    @witch_hat.setShapeMode 1
    @witch_hat.hide
  end

  # Révèle la carte avec ses indicateurs
  def reveal
    super
    @dagger.show if @killed
    @treasure.show if @stolen
    @witch_hat.show if @bewitched
  end

  # Dissimule la carte avec ses indicateurs
  def un_reveal
    super
    @dagger.hide
    @treasure.hide
    @witch_hat.hide
  end

  # Re-initialise les indicateurs
  def re_init
    @power_used = false
    @killed = false
    @stolen = false
    @bewitched = false
    un_reveal
  end

end