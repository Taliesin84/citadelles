# encoding: utf-8

require 'Qt4'
require './gui/game/player_label'

class Podium < Qt::GraphicsItem

  def initialize parent=nil
    super(parent)
    ratio = 2

    ranked_players = scene.players.sort_by {|player| -player.citadel.score}
    gold_cup_pix =  Qt::Pixmap.new "./img/gold_cup.png"
    gold_cup_pix = gold_cup_pix.scaled $icon_wide*8, $icon_wide*8, Qt::KeepAspectRatio, Qt::SmoothTransformation
    gold_cup = Qt::GraphicsPixmapItem.new gold_cup_pix, self
    if $nb_players > 2
      x = -gold_cup.boundingRect.width/2
      y = -$scene_height/2 + $border_space
    else
      x = -gold_cup.boundingRect.width/2
      y = -gold_cup.boundingRect.height
    end
    gold_cup.setPos x, y

    gold_player_label = PlayerLabel.new ranked_players[0].id, ranked_players[0].name, 'avatar', false, self
    gold_player_label.scale ratio, ratio
    if $nb_players > 2
      x = -gold_player_label.boundingRect.width*ratio/2
      y = y + gold_cup.boundingRect.height
    else
      x = -gold_player_label.boundingRect.width*ratio/2
      y = 0
    end
    gold_player_label.setPos x, y

    if $nb_players > 2
      silver_cup_pix =  Qt::Pixmap.new "./img/silver_cup.png"
      silver_cup_pix = silver_cup_pix.scaled $icon_wide*8, $icon_wide*8, Qt::KeepAspectRatio, Qt::SmoothTransformation
      silver_cup = Qt::GraphicsPixmapItem.new silver_cup_pix, self
      x = -$scene_width/2 + ($scene_width/3-silver_cup.boundingRect.width)/2
      y = y - gold_cup.boundingRect.height/3
      silver_cup.setPos x, y

      silver_player_label = PlayerLabel.new ranked_players[1].id, ranked_players[1].name, 'avatar', false, self
      silver_player_label.scale ratio, ratio
      x = -$scene_width/2 + ($scene_width/3-silver_player_label.boundingRect.width*ratio)/2
      y = y + silver_cup.boundingRect.height
      silver_player_label.setPos x, y

      bronze_cup_pix =  Qt::Pixmap.new "./img/bronze_cup.png"
      bronze_cup_pix = bronze_cup_pix.scaled $icon_wide*8, $icon_wide*8, Qt::KeepAspectRatio, Qt::SmoothTransformation
      bronze_cup = Qt::GraphicsPixmapItem.new bronze_cup_pix, self
      x = $scene_width/6 + ($scene_width/3-bronze_cup.boundingRect.width)/2
      y = y - silver_cup.boundingRect.height/3
      bronze_cup.setPos x, y

      bronze_player_label = PlayerLabel.new ranked_players[2].id, ranked_players[2].name, 'avatar', false, self
      bronze_player_label.scale ratio, ratio
      x = $scene_width/6 + ($scene_width/3-bronze_player_label.boundingRect.width*ratio)/2
      y = y + bronze_cup.boundingRect.height
      bronze_player_label.setPos x, y
    end
  end

  def boundingRect
    Qt::RectF.new -$scene_width/2, -$scene_height/2, $scene_width, $scene_height
  end

end