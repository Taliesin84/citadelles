# encoding: utf-8

require 'Qt4'

class SubmitButton < Qt::GraphicsItem

  attr_reader :type, :width, :height

  def initialize type, parent=0, pushable=true
    super(parent)

    @type = type
    @pushable = pushable
    # Création de l'icône de validation
    submit_pix = Qt::Pixmap.new "./img/#@type.png"
    if %w(ban check).include? @type
      ratio = 4
    else
      ratio = 8
    end
    submit_pix = submit_pix.scaled $icon_wide*ratio, $icon_wide*ratio, Qt::KeepAspectRatioByExpanding, Qt::SmoothTransformation
    @width = submit_pix.width
    @height = submit_pix.height
    submit = Qt::GraphicsPixmapItem.new submit_pix, self
    # Evite l'aliasing et la pixelisation
    submit.setTransformationMode Qt::SmoothTransformation
    # Création des effets de survol
    create_effects
    submit.setGraphicsEffect @grey_effect unless @pushable
    submit.setShapeMode 1
    # On accepete de récupérer de manière indépendante les évènements de type survol de la souris
    setAcceptHoverEvents true
  end

  def boundingRect
    Qt::RectF.new 0, 0, @width, @height
  end

  def hoverMoveEvent event
    hoverEnterEvent event
  end

  def hoverEnterEvent event
    setCursor Qt::Cursor.new(Qt::PointingHandCursor) if @pushable
    @border.show
  end

  def hoverLeaveEvent event
    @border.hide
  end

  # Création des effets
  def create_effects

    # Caractéristiques des lignes des cadres de survol
    pen = Qt::Pen.new $red
    pen.setWidth 2
    pen.setStyle(Qt::DashLine)
    pen.setCapStyle(Qt::RoundCap)
    pen.setJoinStyle(Qt::RoundJoin)

    # Création du cadre de survol
    painter = Qt::PainterPath.new
    painter.addRoundedRect 0.0, 0.0, @width.to_f, @height.to_f, 5.0, 5.0
    @border = Qt::GraphicsPathItem.new painter, self
    @border.setPen pen
    @border.hide

    # On 'grise' le bouton si ce dernier est désactivé
    unless @pushable
      # Création de l'effet grisé
      @grey_effect = Qt::GraphicsColorizeEffect.new
      @grey_effect.setColor $dimgrey
    end
  end

  def mousePressEvent event
    parentItem.do_action @type if @pushable
  end
end