# encoding: utf-8

require 'Qt4'

class DiscardingChar < Qt::GraphicsItem

  attr_reader :nb_cards
  attr_accessor :cards

  def initialize
    super()

    @cards = []

    @width = $card_width*$zoom_out_ratio*5 + $card_space*4
    @_x = -$area_width - $card_width*$zoom_out_ratio - $card_space*2
    @_y = $card_width*$zoom_out_ratio*5/2 + $card_space*2
    setPos @_x, @_y
    rotate -90
    setToolTip "Emplacement 'Personnage' défaussé"
    # On dessine les zones des cartes
    paint_card_zone
  end

  def boundingRect
    Qt::RectF.new 0, 0, @width, $card_height*$zoom_out_ratio
  end

  def add_card card_id
    # On récupère la carte d'après le nom du 'Personnage' donné
    card = scene.char_deck.delete_at(scene.char_deck.find_index {|card| card.id == card_id})
    @cards << card
    card.setParentItem self
    # On affiche la carte
    card.show
    # On définit si la carte 'Personnage' défaussée doit être face visible ou non
    set_card_visibility card
    card.i_card = @cards.length
    for item in @cards
      x = (@width - ($card_width*$zoom_out_ratio + ($card_width*$zoom_out_ratio + $card_space)*(@nb_cards-1)))/2 +
          ($card_width*$zoom_out_ratio + $card_space)*(item.i_card-1)
      item.move_to x, 0
    end
  end

  # Dessine les zones des emplacements des cartes de la 'Citadelle'
  def paint_card_zone
    # Caractéristiques des lignes des cadres de survol
    pen = Qt::Pen.new(Qt::Color.new(150, 150, 150, 255))
    pen.setWidth 1
    pen.setStyle(Qt::DashLine)
    pen.setCapStyle(Qt::RoundCap)
    pen.setJoinStyle(Qt::RoundJoin)

    # Création du cadre de délimitation de la zone de la carte
    painter = Qt::PainterPath.new
    painter.addRoundedRect 0, 0, $card_width*$zoom_out_ratio, $card_height*$zoom_out_ratio, 5, 5
    if $nb_char == 8
      case $nb_players
        when 2, 4
          @nb_cards = 4
        when 5
          @nb_cards = 3
        when 3, 6
          @nb_cards = 2
        when 7
          @nb_cards = 1
        else
      end
    elsif $nb_char == 9
      case $nb_players
        when 4
          @nb_cards = 5
        when 5
          @nb_cards = 4
        when 6
          @nb_cards = 3
        when 7
          @nb_cards = 2
        when 8
          @nb_cards = 1
        else
      end
    end
    i = 1
    while i <= @nb_cards
      border = Qt::GraphicsPathItem.new painter, self
      border.setPen pen
      x = (@width - ($card_width*$zoom_out_ratio + ($card_width*$zoom_out_ratio + $card_space)*(@nb_cards-1)))/2 +
          ($card_width*$zoom_out_ratio + $card_space)*(i-1)
      border.setPos x, 0
      i += 1
    end
  end

  # Détermine si la carte 'Personnage' défaussée doit être face visible ou non
  def set_card_visibility card
    if $nb_char == 8
      case $nb_players
        when 2, 3, 6, 7
          card.un_reveal
        when 4
          @cards.length < 3 ? card.reveal : card.un_reveal
        when 5
          @cards.length < 2 ? card.reveal : card.un_reveal
        else
      end
    elsif $nb_char == 9
      case $nb_players
        when 4
          @cards.length < 4 ? card.reveal : card.un_reveal
        when 5
          @cards.length < 3 ? card.reveal : card.un_reveal
        when 6
          @cards.length < 2 ? card.reveal : card.un_reveal
        when 7, 8
          card.un_reveal
        else
      end
    end
  end
end