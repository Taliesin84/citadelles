# encoding: utf-8

require 'Qt4'
require './gui/game/display_text'

class Ranking < DisplayText

  def initialize parent=nil
    super("ranking", parent)

    set_score
    set_icons
  end

  def set_turn nb_turn
    turn = Qt::GraphicsTextItem.new self
    turn.setTextWidth @width*0.2
    turn_font = Qt::Font.new $indicator_font, $scene_height/10
    turn.setFont turn_font
    turn.setPos -@width*0.384, -$card_height*0.81
    turn.setHtml "<center>#{nb_turn.to_s}</center>"
  end

  def set_score
    ranking = Qt::GraphicsTextItem.new self
    ranking.setTextWidth @width*0.86
    ranking_font = Qt::Font.new $script_font, $scene_height/30
    ranking.setFont ranking_font
    ranking.setPos -@width*0.42, -$card_height*0.3

    ranked_players = scene.players.sort_by {|player| -player.citadel.score}
    score_tab = "<table>"
    rank = 1
    for player in ranked_players
      score_tab << "<tr><td width='80%'>#{rank} - <span style='color:rgb(#{$player_rgb_colors[player.id-1]});'>#{player.name}</span></td>"+
          "<td width='5%' align='right'>#{player.citadel.districts.length}</td>"+
          "<td width='15%' align='right'>#{player.citadel.score}</td></tr>"
      rank += 1
    end
    score_tab << "</table>"
    scene.history.insert_ranking ranked_players
    ranking.setHtml score_tab
  end

  def set_icons
    # Affichage de l'icône représentant le 'solde' du joueur
    castle_pix = Qt::Pixmap.new "./img/castle.png"
    castle_pix = castle_pix.scaled $icon_wide, $icon_wide, Qt::IgnoreAspectRatio, Qt::SmoothTransformation
    castle = Qt::GraphicsPixmapItem.new castle_pix, self
    # Définit le contenu de la bulle d'aide
    castle.setToolTip "Nombre de 'Quartier(s)' bâti(s)"
    castle.setPos @width*0.265, -$card_height*0.3-$icon_wide
    castle.setShapeMode 1

    # Affichage de l'icône représentant le 'solde' du joueur
    coin_pix = Qt::Pixmap.new "./img/coin.png"
    coin_pix = coin_pix.scaled $icon_wide, $icon_wide, Qt::IgnoreAspectRatio, Qt::SmoothTransformation
    coin = Qt::GraphicsPixmapItem.new coin_pix, self
    # Définit le contenu de la bulle d'aide
    coin.setToolTip "Score"
    coin.setPos @width*0.39, -$card_height*0.3-$icon_wide
    coin.setShapeMode 1
  end
end