# encoding: utf-8

require 'Qt4'
require './gui/game/display_text'

class History < DisplayText

  attr_reader :index

  def initialize parent=nil
    super("chat_font", parent)
    @index = 0
  end

  def insert_turn turn
    @index = 0
    @text_edit.moveCursor Qt::TextCursor::End
    @text_edit.insertHtml "<table><tr><td width='100%' align='center'><br>*********************************</td></tr>"
    @text_edit.insertHtml "<tr><td width='100%' align='center'><b>Tour #{turn}</b></td></tr>"
    @text_edit.insertHtml "<tr><td width='100%' align='center'>*********************************</td></tr></table><br>"
  end

  def insert_call char_index, char_name, player_name, victim=nil
    @index = 0
    @text_edit.moveCursor Qt::TextCursor::End
    if victim.nil?
      @text_edit.insertHtml "<br><strong>#{char_index}] <u>#{char_name.capitalize}</u></strong> (#{player_name})<br><br>"
    else
      @text_edit.insertHtml "<br><strong>#{char_index}] <u>#{char_name.capitalize}</u></strong> (#{player_name}) [#{victim}]<br><br>"
    end
  end

  def insert_action text, italic=false
    @index += 1
    @text_edit.moveCursor Qt::TextCursor::End
    if italic
      italic_open = "<i>"
      italic_close = "</i>"
    else
      italic_open = ""
      italic_close = ""
    end
    @text_edit.insertHtml "#{@index} - #{italic_open}#{text}#{italic_close}<br>"
  end

  def insert_ranking ranked_players
    @text_edit.moveCursor Qt::TextCursor::End
    @text_edit.insertHtml "<table><tr><td width='100%' align='center'><br>*********************************</td></tr>"+
                              "<tr><td width='100%' align='center'><b>Classement</b></td></tr>"+
                              "<tr><td width='100%' align='center'>*********************************</td></tr></table><br>"+
                              "<table><tr><th width='70%'></th>"+
                              "<th width='10%' align='right'><img src='./img/castle.png' height='#{$icon_wide}' width='#{$icon_wide}' alt='Nombre de 'Quartier(s)' bâti(s)' /></th>"+
                              "<th width='20%' align='right'><img src='./img/coin.png' height='#{$icon_wide}' width='#{$icon_wide}' alt='Score' /></th></tr>"
    rank = 1
    for player in ranked_players
      @text_edit.insertHtml "<tr><td>#{rank} - <span style='color:rgb(#{$player_rgb_colors[player.id-1]});'>#{player.name}</span></td>"+
                                "<td align='right'>#{player.citadel.districts.length}</td>"+
                                "<td align='right'>#{player.citadel.score}</td></tr>"
      rank += 1
    end
    @text_edit.insertHtml "</table><br>"
  end
end