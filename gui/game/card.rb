# encoding: utf-8

require 'Qt4'
require './helpers/graphics_helpers'

class Card < Qt::GraphicsItem

  attr_reader :id, :name, :zoomed, :revealed, :_x, :_y
  attr_accessor :i_card, :selected

  def initialize id, path
    super()
    @id = id
    # on rend la carte sélectionnable
    @selectable = true
    # Carte par défaut non-sélectionnée
    @selected = false
    # Index initial à 0
    @i_card = 0
    @_x = 0
    @_y = 0
    # Création d'un Pixmap contenant l'illustration de la carte
    @pict_pix = Qt::Pixmap.new "./img/cards/#{path}.png"
    # Redimensionnement des illustrations
    @pict_pix = @pict_pix.scaled $card_width, $card_height, Qt::IgnoreAspectRatio, Qt::SmoothTransformation
    @back_pix = @back_pix.scaled $card_width, $card_height, Qt::IgnoreAspectRatio, Qt::SmoothTransformation
    # On instancie le recto (illustration)
    @pict = Qt::GraphicsPixmapItem.new @pict_pix, self
    # On instancie le verso (dos)
    @back = Qt::GraphicsPixmapItem.new @back_pix, self
    # Evite l'aliasing et la pixelisation
    @pict.setTransformationMode Qt::SmoothTransformation
    @back.setTransformationMode Qt::SmoothTransformation
    @pict.setPos 0, 0
    @back.setPos 0, 0
    # On accepete de récupérer de manière indépendante les évènements de type survol de la souris
    setAcceptHoverEvents true
    # Création des effets de survol (loupe + cadre)
    create_effects
    @pict.setGraphicsEffect @grey_effect
    # Carte face cachée
    un_reveal
    # On réduit la carte
    reduce
  end

  def boundingRect
    Qt::RectF.new 0, 0, $card_width, $card_height
  end

  def reveal
    @pict.show
    @back.hide
    @revealed = true
  end

  def un_reveal
    @pict.hide
    @back.show
    @revealed = false
  end

  # Déplace la carte aux coordonnées x, y, z, angle et les enregistre
  def move_to x, y, z=1, angle=0
    @_x = x
    @_y = y
    setPos @_x, @_y
    setZValue z
    setRotation angle
  end

  # Agrandit la carte
  def enlarge
    # On agrandit la carte
    scale $zoom_in_ratio, $zoom_in_ratio
    # On informe que la carte est dans l'état 'agrandit'
    @zoomed = true
  end

  # Réduit la carte
  def reduce
    # On réduit la carte
    scale $zoom_out_ratio, $zoom_out_ratio
    # On informe que la carte est dans l'état 'réduit'
    @zoomed = false
  end

  def mousePressEvent event
    case event.button
      when 1
        if parentItem.class.to_s == "CardSelection"
          if parentItem.submit_next.nil?
            @selected=true
            scene.action.do_action
          else
            if parentItem.max_select == 1
              for card in parentItem.cards
                card.selected = false
                card.re_init_effects unless card == self
              end
            end
            if @selectable
              if @selected
                @selected = false
              else
                cards_selected = parentItem.cards.find_all {|card| card.selected}
                @selected = true if cards_selected.length < parentItem.max_select
              end
            end
            parentItem.disp_submit
          end
        else
          event.ignore
        end
      when 2
        if parentItem.class.to_s == "ZoomCard"
          # On cache l'arrière plan translucide
          scene.font.hide
          scene.zoom_card.hide
          # On propage l'évènement à l'item sélectionné
          scene.zoom_card.del_card
        elsif !scene.font.isVisible and %w(Citadel PlayerHand).include? parentItem.class.to_s
          if scene.action.current_player.nil?
            @revealed ? disp=true : disp=false
          else
            (@revealed or parentItem.player_id == scene.action.current_player.id) ? disp=true : disp=false
          end
          if disp
            # On affiche un arrière plan translucide
            scene.font.show
            scene.zoom_card.show
            parentItem.hoverLeaveEvent event if parentItem.class.to_s == "PlayerHand"
            # On propage l'évènement à l'item sélectionné
            scene.zoom_card.add_card self
          end
        elsif parentItem.class.to_s == "DiscardingChar"
          if @revealed
            scene.font.show
            scene.zoom_card.show
            # On propage l'évènement à l'item sélectionné
            scene.zoom_card.add_card self
          end
        end
      else
        event.ignore
    end
  end

  # Surcharge de la méthode afin que lorsque le pointeur lors d'un zoom est déjà sur la carte
  # le joueur ne doive pas sortir et re-rentré dans la zone afin que l'effet de survol s'affiche
  def hoverMoveEvent event
    hoverEnterEvent event
  end

  # Gestion de l'entrée du curseur de la souris
  def hoverEnterEvent event
    # Changement de la forme du curseur
    setCursor Qt::Cursor.new(Qt::PointingHandCursor)
    # Si la carte est dans une zone de choix
    if parentItem.class.to_s == "CardSelection"
      # Si la carte à agrandir viens de la 'Main du Joueur' alors on la fait passer par dessus les autres cartes
      setZValue parentItem.cards.length+1
      # Réduction de l'épaisseur des contours
      pen = @border.pen
      pen.setWidth 4
      # Gestion des icônes de survol et de la couleur des contours en fonction du type d'action en cours
      case scene.action.name
        when "disp_toss", "choose_char", "choose_dist", "exchange_draw_choice", "disp_power_card", "sorcerer_card_choice",
            "diplomat_dist_choice", "diplomat_victim_dist_choice", "embellish_dist_choice", "bewitch_choice"
          # Affichage de l'icône de sélection
          if @selectable
            @check.show
            pen.setColor $green
          else
            @ban.show
            pen.setColor $red
          end
        when "build_choice"
          # Affichage de l'icône de sélection ou d'interdiction
          if @selectable
            @check.show
            pen.setColor $green
          else
            # Affichage de l'icône de sélection
            @ban.show
            pen.setColor $red
          end
        when "choose_discard_char", "kill_choice", "steal_choice", "destruct_dist_choice", "discard_dist_choice", "choose_dist_to_discard"
          # Affichage de l'icône de suppression
          @selectable ? @delete.show : @ban.show
          pen.setColor $red
        else
      end
      @border.setPen pen
      # Affichage du contour
      @border.show

    elsif parentItem.class.to_s == "ZoomPlayerHand"
      # Réduction de l'épaisseur des contours
      pen = @border.pen
      pen.setWidth 4
      pen.setColor $red
      @border.setPen pen
      # Affichage du contour
      @border.show
      setZValue parentItem.cards.length+1
    else
      # Carte agrandie
      if @zoomed and parentItem.class.to_s == "ZoomCard"
        # Affichage de l'icône de réduction
        @zoom_out.show
        # Réduction de l'épaisseur des contours
        pen = @border.pen
        pen.setWidth 4
        # Changement de la couleur des contours en rouge
        pen.setColor $red
        @border.setPen pen
        # Affichage du contour rouge
        @border.show

        # Carte réduite
      else
        # On vérifie qu'il n'y pas de carte déjà agrandie
        if !scene.font.isVisible and %w(Citadel PlayerHand).include? parentItem.class.to_s
          if scene.action.current_player.nil?
            @revealed ? disp=true : disp=false
          else
            (@revealed or parentItem.player_id == scene.action.current_player.id) ? disp=true : disp=false
          end
          if disp
            # Si la carte à agrandir viens de la 'Main du Joueur' alors on la fait passer par dessus les autres cartes
            setZValue parentItem.cards.length+1 if parentItem.class.to_s == "PlayerHand"
            # Affichage de l'icône d'agrandissement
            @zoom_in.show
            # Réduction de l'épaisseur des contours
            pen = @border.pen
            pen.setWidth 10
            # Changement de la couleur des contours en rouge
            pen.setColor $red
            @border.setPen pen
            # Affichage du contour rouge
            @border.show
          else
            setCursor Qt::Cursor.new(Qt::ArrowCursor)
          end
        elsif parentItem.class.to_s == "DiscardingChar"
          if revealed
            # Affichage de l'icône d'agrandissement
            @zoom_in.show
            # Réduction de l'épaisseur des contours
            pen = @border.pen
            pen.setWidth 10
            # Changement de la couleur des contours en rouge
            pen.setColor $red
            @border.setPen pen
            # Affichage du contour rouge
            @border.show
          end
        else
          setCursor Qt::Cursor.new(Qt::ArrowCursor)
        end
      end
    end
  end

  # Gestion de la sortie du curseur de la souris
  def hoverLeaveEvent event
    re_init_effects unless @selected
    # Si la carte à réduire viens de la 'Main du Joueur' alors on la remet en arrière
    setZValue @i_card if %w(CardSelection PlayerHand ZoomPlayerHand).include? parentItem.class.to_s
  end

  # Initialisation des effets
  def re_init_effects
    # réinitialisation à l'ancienne (plus rapide)
    @border.hide
    @check.hide
    @delete.hide
    @ban.hide
    @zoom_in.hide
    @zoom_out.hide
  end

  # Création des effets
  def create_effects
    # Caractéristiques des lignes des cadres de survol
    pen = Qt::Pen.new $red
    pen.setWidth 4
    pen.setStyle(Qt::SolidLine)
    pen.setCapStyle(Qt::RoundCap)
    pen.setJoinStyle(Qt::RoundJoin)

    # Création du cadre de survol
    painter = Qt::PainterPath.new
    painter.addRoundedRect 0.0, 0.0, $card_width.to_f, $card_height.to_f, 25.0, 25.0
    @border = Qt::GraphicsPathItem.new painter, self
    @border.setPen pen
    @border.hide

    # Création de l'icône de sélection
    check_pix = Qt::Pixmap.new "./img/check.png"
    @check = Qt::GraphicsPixmapItem.new check_pix, self
    @check.setFlag Qt::GraphicsItem::ItemIgnoresTransformations, true
    x = ($card_width - @check.boundingRect.width)/2
    y = ($card_height - @check.boundingRect.height)/2
    @check.setPos x, y
    @check.hide

    # Création de l'icône de suppression
    delete_pix = Qt::Pixmap.new "./img/delete.png"
    @delete = Qt::GraphicsPixmapItem.new delete_pix, self
    @delete.setFlag Qt::GraphicsItem::ItemIgnoresTransformations, true
    x = ($card_width - @delete.boundingRect.width)/2
    y = ($card_height - @delete.boundingRect.height)/2
    @delete.setPos x, y
    @delete.hide

    # Création de l'icône d'interdiction
    ban_pix = Qt::Pixmap.new "./img/ban.png"
    @ban = Qt::GraphicsPixmapItem.new ban_pix, self
    @ban.setFlag Qt::GraphicsItem::ItemIgnoresTransformations, true
    x = ($card_width - @ban.boundingRect.width)/2
    y = ($card_height - @ban.boundingRect.height)/2
    @ban.setPos x, y
    @ban.hide

    # Création de l'icône de réduction
    zoom_out_pix = Qt::Pixmap.new "./img/zoom_out.png"
    @zoom_out = Qt::GraphicsPixmapItem.new zoom_out_pix, self
    @zoom_out.setFlag Qt::GraphicsItem::ItemIgnoresTransformations, true
    x = ($card_width - @zoom_out.boundingRect.width)/2
    y = ($card_height - @zoom_out.boundingRect.height)/2
    @zoom_out.setPos x, y
    @zoom_out.hide

    # Création de l'icône d'agrandissement
    zoom_in_pix = Qt::Pixmap.new "./img/zoom_in.png"
    zoom_in_pix = zoom_in_pix.scaled zoom_in_pix.width*$ratio*0.5, zoom_in_pix.height*$ratio*0.5, Qt::IgnoreAspectRatio, Qt::SmoothTransformation
    @zoom_in = Qt::GraphicsPixmapItem.new zoom_in_pix, self
    x = ($card_width - @zoom_in.boundingRect.width)/2
    y = ($card_height - @zoom_in.boundingRect.height)/2
    @zoom_in.setPos x, y
    @zoom_in.hide

    # Création de l'effet grisé
    @grey_effect = Qt::GraphicsColorizeEffect.new
    @grey_effect.setColor $dimgrey
    @grey_effect.setEnabled false
  end

  def unavailable
    @grey_effect.setEnabled true
    @selectable = false
  end

  def available
    @grey_effect.setEnabled false
    @selectable = true
  end

end