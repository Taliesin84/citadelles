# encoding: utf-8

module ActionPanelDisp

  # Définit le texte d'instruction de l'action en cours
  def set_heading txt_head
    @head_txt = Qt::GraphicsTextItem.new self
    @head_txt.setHtml "<center>#{txt_head}</center>"
    @head_txt.setDefaultTextColor $main_font_color
    head_font = Qt::Font.new $main_font, $scene_height/20
    #head_font.setLetterSpacing Qt::Font::AbsoluteSpacing, 2
    @head_txt.setFont head_font
    @head_txt.setTextWidth $scene_width - $card_width
    x = -@head_txt.textWidth/2
    y = -$scene_height/2 + ($scene_height/4 - @head_txt.boundingRect.height)/2
    @head_txt.setPos x, y
    @head_txt.show
  end

  # Phase de tirage au sort du 'Roi'
  def disp_turn_toss
    @name = "disp_turn_toss"
    disp_turn "Tirage au sort du Roi"
  end

  # Phase de choix des Personnages
  def disp_turn_choose_char
    @name = "disp_turn_choose_char"
    disp_turn "Choix des 'Personnages'"
  end

  # Phase d'appel des Personnages
  def disp_turn_call_char
    @name = "disp_turn_call_char"
    disp_turn "Appel des 'Personnages'"
  end

  # Affiche l'écran Palier de phase'
  def disp_turn phase_txt
    if @nb_turn > 1
      set_heading "#@nb_turn ème Tour"
    else
      set_heading "#@nb_turn  er Tour"
    end

    banner = TextBanner.new phase_txt, $red, self
    banner.setPos -banner.width/2, -banner.height/2
    banner.setToolTip "Prochaine Phase de Jeu"

    # Affichage du bouton de retour en arrière
    submit_next = SubmitBanner.new "next", "Débuter la phase", self
    submit_next.setPos -submit_next.width/2, $card_height*3/4
  end

  # Permet de tirer une carte 'Personnage' au sort
  def disp_toss
    @name = "disp_toss"
    set_heading "Choisissez une carte !"

    @choice_box = CardSelection.new "", 1, 1, false, self
    char_deck_shuffled = scene.char_deck.shuffle
    for char in char_deck_shuffled
      @choice_box.add_char_card char.id, true, "Sélectionner cette carte", false
    end
  end

  # Révèle la carte tirée au sort
  def disp_toss_char char
    @name = "disp_toss_char"
    set_heading "Voici la carte que vous avez tirée !"

    @disp_box = DisplayCard.new self
    # On ajoute la carte
    @disp_box.add_card char, "Carte tirée"

    # Affichage du bouton de retour en arrière
    if %(roi empereur).include? char.id
      submit_next = SubmitBanner.new "next", "Sacrement", self
    else
      submit_next = SubmitBanner.new "next", "Joueur Suivant", self
    end
    submit_next.setPos -submit_next.width/2, $card_height*3/4
  end

  # Révèle le nouveau 'Roi'
  def disp_enthrone
    @name = "disp_enthrone"
    set_heading "Longue vie au Roi !!!"

    crown_pix =  Qt::Pixmap.new "./img/crown.png"
    crown_pix = crown_pix.scaled $icon_wide*2, $icon_wide*2, Qt::KeepAspectRatioByExpanding, Qt::SmoothTransformation
    crown = Qt::GraphicsPixmapItem.new crown_pix, self
    crown.setPos -crown.boundingRect.width/2, -crown.boundingRect.height

    king = scene.players.find {|player| player.king}
    player_label = PlayerLabel.new king.id, king.name, 'avatar', false, self
    player_label.scale 2, 2
    player_label.setPos -player_label.boundingRect.width, 0

    # Affichage du bouton de retour en arrière
    submit_next = SubmitBanner.new "next", "Débuter la partie", self
    submit_next.setPos -submit_next.width/2, $card_height*3/4
  end

  # Affiche les cartes 'Personnage' défaussées
  def disp_discarded_card
    @name = "disp_discarded_card"
    if scene.discard_char.cards.length < 2
      set_heading "Carte défaussée :"

      @disp_box = DisplayCard.new self
      char_ref = scene.char_deck_ref.find {|char| char.id == scene.discard_char.cards[0].id}
      # On ajoute la carte
      @disp_box.add_card char_ref, "Carte défaussée face visible"
    else
      set_heading "Cartes défaussées :"

      @disp_box = DisplayCards.new self
      cards = []
      for card in scene.discard_char.cards
        char = scene.char_deck_ref.find {|char| char.id == card.id}
        char.i_card = card.i_card
        cards << char
      end
      @disp_box.add_cards cards, "Carte défaussée face visible"
    end

    # Affichage du bouton de retour en arrière
    submit_next = SubmitBanner.new "next", "Etape Suivante", self
    submit_next.setPos -submit_next.width/2, $card_height*3/4
  end

  def disp_player_turn_toss
    @name = "disp_player_turn_toss"
    disp_player_turn
  end

  def disp_player_turn_choose_char
    @name = "disp_player_turn_choose_char"
    disp_player_turn
  end

  def disp_player_turn
    set_heading "Le prochain Joueur à choisir sa carte est :"

    call_player
  end

  def disp_secret_card
    @name = "disp_secret_card"
    set_heading "Carte secrète défaussée :"

    # On créé la zone d'affichage de la carte
    @disp_box = DisplayCard.new self
    char_ref = scene.char_deck_ref.find {|char| char.id == scene.discard_char.cards[scene.discard_char.cards.length-1].id}
    # On ajoute la carte
    @disp_box.add_card char_ref

    # Affichage du bouton de validation
    submit_next = SubmitBanner.new "next", "Etape Suivante", self
    submit_next.setPos -submit_next.width/2, $card_height*3/4
  end

  # Réalise la phase de sélection du 'Personnage'
  def choose_char
    @name = "choose_char"
    set_heading "Veuillez choisir un 'Personnage' !"

    if $nb_players > 2
      @choice_box = CardSelection.new "Joueur Suivant", 1, 1, false, self
    else
      @choice_box = CardSelection.new "Etape Suivante", 1, 1, false, self
    end
    for char in scene.char_deck
      @choice_box.add_char_card char.id, true, "Sélectionner ce 'Personnage'"
    end
  end

  # Réalise la phase de sélection du 'Personnage'
  def choose_discard_char
    @name = "choose_discard_char"
    set_heading "Veuillez choisir un 'Personnage' à défausser !"

    @choice_box = CardSelection.new "Joueur Suivant", 1, 1, false, self
    for char in scene.char_deck
      @choice_box.add_char_card char.id, true, "Défausser ce 'Personnage'"
    end
  end

  def king_call_player
    @name = "king_call_player"
    set_heading "Le Roi appelle :"

    # On créé la zone d'affichage de la carte
    @disp_box = DisplayCard.new self
    # On ajoute la carte
    @disp_box.add_card @current_char
    @disp_box.setPos -$card_width/2, -$card_height/2-$icon_wide
    if @current_player.nil?
      if @current_char.killed and $end_killed_char
        banner = TextBanner.new "'Personnage' assassiné", $red, self
        banner.setPos -banner.width/2, $card_height/2-$icon_wide
      else
        banner = TextBanner.new "'Personnage' défaussé", $red, self
        banner.setPos -banner.width/2, $card_height/2-$icon_wide
      end
    else
      player_label = PlayerLabel.new @current_player.id, @current_player.name, 'avatar', false, self
      player_label.scale 2, 2
      player_label.setPos -player_label.width, $card_height/2-$icon_wide
    end

    # Affichage du bouton de validation
    submit_next = SubmitBanner.new "next", "Etape Suivante", self
    submit_next.setPos -submit_next.width/2, $card_height*3/4
  end

  # Appel d'un Joueur
  def call_player
    player_label = PlayerLabel.new @current_player.id, @current_player.name, 'avatar', false, self
    player_label.scale 2, 2
    player_label.setPos -player_label.width, -player_label.height

    # Affichage du bouton de validation
    submit_next = SubmitBanner.new "next", "Etape Suivante", self
    submit_next.setPos -submit_next.width/2, $card_height*3/4
  end

  # Affiche la somme volée par le 'Voleur'
  def disp_gold_stolen
    @name = "disp_gold_stolen"
    set_heading "Le 'Voleur' vous a dérobé :"

    # On affiche la somme dérobée
    disp_gold @money, "del_coins"
  end

  # Réalise la phase de choix entre tirer des cartes ou prendre de l'or
  def gold_vs_card
    @name = "gold_vs_card"
    set_heading "Quelle action souhaitez-vous effectuer ?"

    # Adaptation du choix en fonction des 'Merveilles' construites sur la Citadelle du 'Joueur'
    if @current_player.citadel.districts.find {|card| card.id == "obs"}
      draw_card_pix = "3_to_1_cards"
      draw_card_tool_tip = "Piocher 3 cartes 'Quartier', en choisir une et se défausser des 2 autres."
    elsif @current_player.citadel.districts.find {|card| card.id == "bib"}
      draw_card_pix = "2_cards"
      draw_card_tool_tip = "Piocher 2 cartes 'Quartier'."
    else
      draw_card_pix = "2_to_1_card"
      draw_card_tool_tip = "Piocher 2 cartes 'Quartier', en choisir une et se défausser de l'autre."
    end
    disp_gold_vs_card draw_card_pix, draw_card_tool_tip, "coins", "2"
  end

  # Affiche la somme gagnée
  def disp_gold_earned
    @name = "disp_gold_earned"
    set_heading "Votre personnage '#{@current_char.name.capitalize}' vous rapporte :"

    # On affiche la somme gagnée
    disp_gold @money, "coins"
  end

  # Affiche le montant de la taxe du 'Bailli'
  def disp_gold_taxed
    @name = "disp_gold_taxed"
    set_heading "Paiement de la taxe au 'Bailli' :"

    # On affiche le montant de la taxe
    disp_gold @money, "del_coins"
  end

  def disp_gold_earned_by_queen player_name
    @name = "disp_gold_earned_by_queen"
    set_heading "La 'Reine' ayant été assise à côté du 'Roi' rapporte 3 pièces d'or au joueur #{player_name}"

    # On affiche la somme gagnée
    disp_gold @due_to_queen, "coins"
  end

  def disp_gold_earned_with_hospice
    @name = "disp_gold_earned_with_hospice"
    set_heading "Votre quartier 'Hospice' vous rapporte :"

    # On affiche la somme gagnée
    disp_gold 1, "coins"
  end

  # Affiche la somme gagné
  def disp_gold money, gold_icon
    # Affichage de l'icône du choix des 'Pièces d'or'
    gold_pix = Qt::Pixmap.new "./img/#{gold_icon}.png"
    gold_pix = gold_pix.scaled $icon_wide*8, $icon_wide*8, Qt::KeepAspectRatioByExpanding, Qt::SmoothTransformation
    gold = Qt::GraphicsPixmapItem.new gold_pix, self

    # Affichage du texte de la quantité d'or
    nb_gold = Qt::GraphicsTextItem.new "x #{money.to_s}", self
    nb_gold.setDefaultTextColor $main_font_color
    nb_gold_font = Qt::Font.new $indicator_font, $scene_height/20
    nb_gold.setFont nb_gold_font
    gold.setZValue 1
    x = -($scene_width/2 - gold.boundingRect.width - nb_gold.boundingRect.width)/2
    y = -gold.boundingRect.height/2
    gold.setPos x, y
    nb_gold.setPos x+gold.boundingRect.width, -nb_gold.boundingRect.height/2

    # Affichage du bouton de validation
    submit_next = SubmitBanner.new "next", "Etape Suivante", self
    submit_next.setPos -submit_next.width/2, $card_height*3/4
  end

  # Réalise la phase de sélection du 'Quartier'
  def choose_dist nb_dist
    @name = "choose_dist"
    set_heading "Quel 'Quartier' souhaitez-vous conserver ?"

    @choice_box = CardSelection.new "Etape Suivante", 1, 1, false, self
    i = 1
    while i <= nb_dist
      card = scene.dist_deck_ref.find {|card| card.id == scene.draw_dist.cards[scene.draw_dist.cards.length-i].id}
      @choice_box.add_dist_card card.id, true, "Conserver ce 'Quartier'"
      i += 1
    end
  end

  # Réalise la phase de sélection du 'Quartier'
  def choose_dist_to_discard nb_dist
    @name = "choose_dist_to_discard"
    if nb_dist < 3
      set_heading "Quel 'Quartier' souhaitez-vous défausser ?"
      @choice_box = CardSelection.new "Etape Suivante", 1, 1, false, self
    else
      set_heading "Quels sont les 2 'Quartiers' dont vous souhaitez vous séparer ?"
      @choice_box = CardSelection.new "Etape Suivante", 2, 2, false, self
    end

    for card in @current_player.hand.cards
      dist_ref = scene.dist_deck_ref.find {|dist| dist.id == card.id}
      @choice_box.add_dist_card dist_ref.id, true, "Défausser ce 'Quartier'"
    end
  end

  def disp_drawn_dist nb_dist
    @name = "disp_drawn_dist"
    set_heading "Voici les #{nb_dist} 'Quartier(s)' que vous avez pioché !"

    disp_dist nb_dist
  end

  def disp_park_drawn_dist
    @name = "disp_park_drawn_dist"
    set_heading "Votre quartier 'Parc' vous a permis de piocher les 2 'Quartiers' suivant :"

    disp_dist 2
  end

  # Réalise la phase d'affichage de 'Quartier(s)' piochés
  def disp_dist nb_dist
    @disp_box = DisplayCards.new self
    cards = []
    i = 1
    while i <= nb_dist
      card = scene.dist_deck_ref.find {|card| card.id == scene.draw_dist.cards[scene.draw_dist.cards.length-i].id}
      card.i_card = i
      cards << card
      i += 1
    end
    @disp_box.add_cards cards

    # Affichage du bouton de retour en arrière
    submit_next = SubmitBanner.new "next", "Etape Suivante", self
    submit_next.setPos -submit_next.width/2, $card_height*3/4
  end

  # Réalise la demande de l'application du pouvoir du 'Quartier'
  def dist_power_choice card_id
    @name = "dist_power_choice"
    set_heading "Souhaitez-vous utiliser le pouvoir de votre 'Quartier' maintenant ?"

    # On récupère la carte
    card = scene.dist_deck_ref.find {|card| card.id == card_id}
    power_choice card
  end

  # Réalise la demande de l'application du pouvoir du 'Personnage'
  def char_power_choice
    @name = "char_power_choice"
    set_heading "Souhaitez-vous utiliser le pouvoir de votre 'Personnage' maintenant ?"

    power_choice @current_char
  end

  # Réalise la demande de l'application du pouvoir du quartier 'Beffroi'
  def belfry_power_choice
    @name = "belfry_power_choice"
    set_heading "Souhaitez-vous utiliser le pouvoir du 'Beffroi' ?"

    # On récupère la carte
    card = scene.dist_deck_ref.find {|card| card.id == "bef"}
    power_choice card
  end

  # Réalise l'écarn de choix d'application ou non du pouvoir de la carte
  def power_choice card
    # On créé la zone d'affichage de la carte
    @disp_box = DisplayCard.new self
    @disp_box.setPos -$card_width/2, -$card_height/3
    # On ajoute la carte
    @disp_box.add_card card
    card.class.to_s == "CharacterCard" ? type="Personnage" : type="Quartier"

    # Affichage de l'icône d'acceptation
    accept = SubmitButton.new "check", self
    accept.setToolTip "Utiliser le pouvoir du '#{type}'"
    x = -$card_width/2 - $card_space*$ratio - accept.width
    y = -accept.height/2 + $card_height/6
    accept.setPos x, y

    # Affichage de l'icône de refus
    refuse = SubmitButton.new "ban", self
    refuse.setToolTip "Ne pas utiliser le pouvoir du '#{type}'"
    x = $card_width/2 + $card_space*$ratio
    y = -refuse.height/2 + $card_height/6
    refuse.setPos x, y
  end

  # Réalise la phase d'assassinat
  def kill_choice
    @name = "kill_choice"
    set_heading "Quel 'Personnage' souhaitez-vous assassiner ?"

    @choice_box = CardSelection.new "Assassiner", 1, 1, true, self
    for char_ref in scene.char_deck_ref
      #todo Implémenter la suppression de la carte secrète si on est le roi ou juste avant le roi
      unless char_ref.id=="assassin" or
          scene.discard_char.cards.find {|char| char.id==char_ref.id and char.revealed} or
          (char_ref.id=="roi" and !$killing_king)
        @choice_box.add_char_card char_ref.id, true, "Assassiner ce 'Personnage'"
      end
    end
  end

  # Réalise la phase d'assassinat
  def bewitch_choice
    @name = "bewitch_choice"
    set_heading "Quel 'Personnage' souhaitez-vous ensorceler ?"

    @choice_box = CardSelection.new "Ensorceler", 1, 1, true, self
    for char_ref in scene.char_deck_ref
      #todo Implémenter la suppression de la carte secrète si on est le roi ou juste avant le roi
      unless char_ref.id=="sorciere" or scene.discard_char.cards.find {|char| char.id==char_ref.id and char.revealed}
        @choice_box.add_char_card char_ref.id, true, "Ensorceler ce 'Personnage'"
      end
    end
  end

  def witch_recall
    @name = "witch_recall"
    set_heading "Le Joueur incarnant la 'Sorcière' est rappelée :"

    witch_hat_pix =  Qt::Pixmap.new "./img/witch_hat.png"
    witch_hat_pix = witch_hat_pix.scaled $icon_wide*2, $icon_wide*2, Qt::KeepAspectRatioByExpanding, Qt::SmoothTransformation
    witch_hat = Qt::GraphicsPixmapItem.new witch_hat_pix, self
    witch_hat.setPos -witch_hat.boundingRect.width/2, -witch_hat.boundingRect.height

    player_label = PlayerLabel.new @current_player.id, @current_player.name, 'avatar', false, self
    player_label.scale 2, 2
    player_label.setPos -player_label.width, 0

    # Affichage du bouton de validation
    submit_next = SubmitBanner.new "next", "Etape Suivante", self
    submit_next.setPos -submit_next.width/2, $card_height*3/4
  end

  # Réalise la phase de vol
  def steal_choice
    @name = "steal_choice"
    set_heading "Quel 'Personnage' souhaitez-vous voler !"

    @choice_box = CardSelection.new "Voler", 1, 1, true, self
    for char_ref in scene.char_deck_ref
      #todo Implémenter la suppression de la carte secrète si on est le roi ou juste avant le roi
      unless %w(assassin voleur).include? char_ref.id or scene.discard_char.cards.find {|char| char.id==char_ref.id and char.revealed}
        char = scene.char_deck_dup.find {|card| card.id == char_ref.id}
        if char.killed
          @choice_box.add_char_card char_ref.id, false, "'Personnage' assassiné"
        elsif char.bewitched
          @choice_box.add_char_card char_ref.id, false, "'Personnage' ensorcelé"
        else
          @choice_box.add_char_card char_ref.id, true, "Voler ce 'Personnage'"
        end
      end
    end
  end

  # Permet de définir le type d'échange effectué par le 'Magicien'
  def exchange_type_choice
    @name = "exchange_type_choice"
    set_heading "Quelle action souhaitez-vous effectuer ?"

    exchange_all = SubmitButton.new "exchange_all", self
    exchange_all.setToolTip "Echanger la totalité de ses cartes avec le Joueur de son choix"
    x = -($scene_width/2 - exchange_all.width)/2 - exchange_all.width
    y = -exchange_all.height/2
    exchange_all.setPos x, y

    # Affichage du texte "ou"
    txt_or = Qt::GraphicsTextItem.new "ou", self
    txt_or.setDefaultTextColor $main_font_color
    txt_or_font = Qt::Font.new $main_font, $scene_height/20
    txt_or.setFont txt_or_font
    x = -txt_or.boundingRect.width/2
    y = -txt_or.boundingRect.height/2
    txt_or.setPos x, y

    if @current_player.hand.cards.length > 0
      exchange = SubmitButton.new "exchange_draw", self
      exchange.setToolTip "Echanger des cartes de sa main contre le même nombre de la Pioche"
    else
      exchange = SubmitButton.new "exchange_draw", self, false
      exchange.setToolTip "Vous ne pouvez échanger des cartes de votre main avec la Pioche\ncar vous n'avez pas de cartes 'Quartiers"
    end
    x = ($scene_width/2 - exchange.width)/2
    y = -exchange.height/2
    exchange.setPos x, y

    # Affichage du bouton de retour en arrière
    submit_prev = SubmitBanner.new "prev", "Etape Précédente", self
    submit_prev.setPos -submit_prev.width/2, $card_height*3/4
  end

  # Permet de choisir le Joueur avec qui échanger toute sa main
  def exchange_all_choice
    @name = "exchange_all_choice"
    set_heading "Avec quel Joueur souhaitez-vous échanger toutes vos cartes 'Quartier' ?"

    @choice_box = PlayerSelection.new "Echanger", true, self
    for player in scene.players
      unless player.id == @current_player.id
        if player.hand.cards.length == 0
          selectable=false
          txt_tool_tip = "Ce Joueur ne possède aucune cartes dans sa main !"
        else
          selectable=true
          txt_tool_tip = "Sélectionner ce joueur\n#{player.hand.cards.length} cartes\nScore : #{player.citadel.score}"
        end
        @choice_box.add_player player.id, selectable, txt_tool_tip
      end
    end
  end

  # Affiche la nouvelle main
  def disp_exchange_hand
    @name = "disp_exchange_hand"
    set_heading "Voici votre nouvelle main !"

    @disp_box = DisplayCards.new self
    cards = []
    for card in @current_player.hand.cards
      dist = scene.dist_deck_ref.find {|dist| dist.id == card.id}
      dist.i_card = card.i_card
      cards << dist
    end
    @disp_box.add_cards cards

    # Affichage du bouton de validation
    submit_next = SubmitBanner.new "next", "Etape Suivante", self
    submit_next.setPos -submit_next.width/2, $card_height*3/4
  end

  def exchange_draw_choice
    @name = "exchange_draw_choice"
    set_heading "Quel cartes souhaitez-vous échanger ?"

    @choice_box = CardSelection.new "Etape Suivante", 1, 99, true, self
    player = scene.players.find {|player| player.id == @current_player.id}
    for card in player.hand.cards
      dist = scene.dist_deck_ref.find {|dist| dist.id == card.id}
      @choice_box.add_dist_card dist.id, true, "Sélectionner ce 'Quartier'"
    end
  end

  # Permet au 'Sorcier' de choisir le Joueur à qui il s'apprete de prendre une carte de sa main
  def sorcerer_player_choice
    @name = "sorcerer_player_choice"
    set_heading "A quel Personnage souhaitez-vous prendre une carte ?"

    if $sorcerer_revealed_cards
      @choice_box = PlayerSelection.new "", true, self
    else
      @choice_box = PlayerSelection.new "Choisir une carte", true, self
    end
    for player in scene.players
      if player.hand.cards.length > 0 and player.id != @current_player.id
        txt_tool_tip = "Sélectionner ce joueur\n#{player.hand.cards.length} cartes\nScore : #{player.citadel.score}"
        @choice_box.add_player player.id, true, txt_tool_tip
      end
    end
  end

  # Permet au 'Sorcier' de choisir la carte qu'il souhaite récupérer
  def sorcerer_card_choice player_id
    @name = "sorcerer_card_choice"
    set_heading "Quel carte 'Quartier' souhaitez-vous prendre ?"

    victim = scene.players.find {|player| player.id == player_id}
    @choice_box = CardSelection.new "Etape Suivante", 1, 1, !$sorcerer_revealed_cards, self
    for dist in victim.hand.cards
      @choice_box.add_dist_card dist.id, true, "Prendre cette carte", $sorcerer_revealed_cards
    end
  end

  def sorcerer_build_choice dist_id
    @name = "sorcerer_build_choice"
    card = scene.dist_deck_ref.find {|dist| dist.id == dist_id}
    set_heading "Souhaitez-vous bâtir ce 'Quartier' contre #{card.cost} pièces d'or ?"

    # On créé la zone d'affichage de la carte
    @disp_box = DisplayCard.new self
    @disp_box.setPos -$card_width/2, -$card_height/3
    # On ajoute la carte
    @disp_box.add_card card

    # Affichage de l'icône d'acceptation
    accept = SubmitButton.new "check", self
    accept.setToolTip "Bâtir le 'Quartier'"
    x = -$card_width/2 - $card_space*$ratio - accept.width
    y = -accept.height/2 + $card_height/6
    accept.setPos x, y

    # Affichage de l'icône de refus
    refuse = SubmitButton.new "ban", self
    refuse.setToolTip "Ne pas bâtir le 'Quartier'"
    x = $card_width/2 + $card_space*$ratio
    y = -refuse.height/2 + $card_height/6
    refuse.setPos x, y
  end

  def sorcerer_disp_card dist_id
    @name = "sorcerer_disp_card"
    set_heading "Voici le 'Quartier' que vous avez récupéré !"

    @disp_box = DisplayCard.new self
    card = scene.dist_deck_ref.find {|dist| dist.id == dist_id}
    @disp_box.add_card card

    # Affichage du bouton de retour en arrière
    submit_next = SubmitBanner.new "next", "Etape Suivante", self
    submit_next.setPos -submit_next.width/2, $card_height*3/4
  end

  # Permet à l''Empereur' de choisir le nouveau 'Roi'
  def emperor_player_choice
    @name = "emperor_player_choice"
    set_heading "Quel Personnage souhaitez-vous nommer 'Roi' contre une pièce d'or ou une carte' ?"

    @choice_box = PlayerSelection.new "Intrôniser", true, self
    for player in scene.players
      if player.king
        selectable=false
        txt_tool_tip = "Vous ne pouvez ré-intrôniser le 'Roi' actuel"
      elsif player.id == @current_player.id
        selectable=false
        txt_tool_tip = "Vous ne pouvez vous nommer 'Roi'"
      else
        selectable=true
        txt_tool_tip = "Nommer ce joueur\n#{player.citadel.sold} pièce(s) d'or | #{player.hand.cards.length} cartes\nScore : #{player.citadel.score}"
      end
      @choice_box.add_player player.id, selectable, txt_tool_tip
    end
  end

  # Appelle le Joueur nommé par l''Empereur'
  def emperor_call_player
    @name = "emperor_call_player"
    emperor = scene.players.find {|player| player.citadel.characters.find {|char| char.id == "empereur"}}
    set_heading "#{emperor.name.capitalize} a choisit de nommer 'Roi' :"

    call_player
  end

  # Réalise la phase de choix entre tirer des cartes ou prendre de l'or du 'Navigateur'
  def enthroned_player_choice
    @name = "enthroned_player_choice"
    set_heading "L'Empereur vous a nommé 'Roi' et exige une contribution de votre part :"

    draw_card_pix = "1_card"
    draw_card_tool_tip = "Donner 1 carte 'Quartier'"
    disp_gold_vs_card draw_card_pix, draw_card_tool_tip, "del_coins", "1"
  end

  def enthroned_player_dist_choice prev
    @name = "enthroned_player_dist_choice"
    set_heading "Quel 'Quartier' souhaitez-vous donner à l'Empereur ?"

    @choice_box = CardSelection.new "Etape Suivante", 1, 1, prev, self
    for dist in @current_player.hand.cards
      @choice_box.add_dist_card dist.id, true, "Donner ce 'Quartier'"
    end
  end

  def emperor_recall
    @name = "emperor_recall"
    set_heading "Rappel de :"

    call_player
  end

  def disp_emperor_gain
    @name = "disp_emperor_gain"
    set_heading "Voici la contribution que vous a donné le nouveau 'Roi' :"

    if @due_to_emperor.class.to_s == "Fixnum"
      # On affiche la somme gagnée
      disp_gold @due_to_emperor, "coins"
    elsif @due_to_emperor.class.to_s == "DistrictCard"
      # On créé la zone d'affichage de la carte
      @disp_box = DisplayCard.new self
      @disp_box.setPos -$card_width/2, -$card_height/3
      # On ajoute la carte
      @disp_box.add_card @due_to_emperor
    end

    # Affichage du bouton de validation
    submit_next = SubmitBanner.new "next", "Etape Suivante", self
    submit_next.setPos -submit_next.width/2, $card_height*3/4
  end

  # Réalise la phase de destruction du 'Condottiere'
  def destruct_player_choice
    @name = "destruct_player_choice"
    set_heading "Chez quel Joueur souhaitez-vous détruire un 'Quartier' ?"

    @choice_box = PlayerSelection.new "", true, self
    for player in scene.players
      if player.citadel.characters.find {|char| char.id == "eveque"}
        selectable = false
        txt_tool_tip = "Ce joueur incarne le personnage 'Evêque'"
      elsif player.citadel.districts.length == 0
        selectable = false
        txt_tool_tip = "Ce Joueur n'a encore construit aucun 'Quartier'"
      elsif !$ending_turn_mercenary and player.citadel.districts.length==$max_dist
        selectable = false
        txt_tool_tip = "Vous ne pouvez détruire un 'Quartier' dans une 'Citadelle' achevée"
      else
        selectable = true
        txt_tool_tip = "Sélectionner ce joueur\n#{player.citadel.districts.length} quartier(s)\nScore : #{player.citadel.score}"
      end
      @choice_box.add_player player.id, selectable, txt_tool_tip
    end
  end

  # Permet de sélectionner le 'Quartier' à détruire
  def destruct_dist_choice player_id
    @name = "destruct_dist_choice"
    set_heading "Quel 'Quartier' souhaitez-vous détruire ?"

    @choice_box = CardSelection.new "Etape Suivante", 1, 1, true, self
    player = scene.players.find {|player| player.id == player_id}
    (player.citadel.districts.find {|dist| dist.id == "gm"}) ? great_wall=1 : great_wall=0
    for dist in player.citadel.districts
      if dist.cost+great_wall+dist.upgrade-1 > @current_player.citadel.sold
        @choice_box.add_dist_card dist.id, false, "Vous n'avez pas les moyens de détruire ce 'Quartier'"
      elsif dist.id == "don"
        @choice_box.add_dist_card dist.id, false, "Ce 'Quartier' ne peut être détruit"
      else
        @choice_box.add_dist_card dist.id, true, "Détruire ce 'Quartier'"
      end
    end
  end

  # Réalise la phase de destruction de la 'Poudriere'
  def powder_player_choice
    @name = "powder_player_choice"
    set_heading "Chez quel Joueur souhaitez-vous détruire un 'Quartier' ?"

    @choice_box = PlayerSelection.new "", true, self
    for player in scene.players
      if player.citadel.districts.length == 0
        selectable = false
        txt_tool_tip = "Ce Joueur n'a encore construit aucun 'Quartier'"
      else
        selectable = true
        txt_tool_tip = "Sélectionner ce joueur\n#{player.citadel.districts.length} quartier(s)\nScore : #{player.citadel.score}"
      end
      @choice_box.add_player player.id, selectable, txt_tool_tip
    end
  end

  # Permet de sélectionner le 'Quartier' à détruire
  def powder_dist_choice player_id
    @name = "powder_dist_choice"
    set_heading "Quel 'Quartier' souhaitez-vous détruire ?"

    @choice_box = CardSelection.new "Etape Suivante", 1, 1, true, self
    player = scene.players.find {|player| player.id == player_id}
    for dist in player.citadel.districts
      @choice_box.add_dist_card dist.id, true, "Détruire ce 'Quartier'"
    end
  end

  # Permet au 'Diplomate' de sélectionner un de ses 'Quartiers' pour l'échange
  def diplomat_dist_choice
    @name = "diplomat_dist_choice"
    set_heading "Quel 'Quartier' souhaitez-vous échanger ?"

    @choice_box = CardSelection.new "Echanger", 1, 1, true, self
    for dist in @current_player.citadel.districts
      @choice_box.add_dist_card dist.id, true, "Echanger ce 'Quartier'"
    end
  end

  # Permet au 'Diplomate' de sélectionner le Joueur avec qui il va procéder à l'échange
  def diplomat_player_choice
    @name = "diplomat_player_choice"
    set_heading "Avec quel Joueur souhaitez-vous échanger votre 'Quartier' ?"

    @choice_box = PlayerSelection.new "", true, self
    for player in scene.players
      unless player.id == @current_player.id
        if player.citadel.characters.find {|char| char.id == "eveque"}
          selectable=false
          txt_tool_tip = "Ce joueur incarne le personnage 'Evêque' !"
        elsif player.citadel.districts.length == 0
          selectable=false
          txt_tool_tip = "Ce Joueur n'a encore construit aucun 'Quartier' !"
        else
          selectable=true
          txt_tool_tip = "Sélectionner ce joueur\n#{player.citadel.districts} quartier(s)\nScore : #{player.citadel.score}"
        end
        @choice_box.add_player player.id, selectable, txt_tool_tip
      end
    end
  end

  # Permet au 'Diplomate' de sélectionner le 'Quartier' du Joueur pour l'échange
  def diplomat_victim_dist_choice player_id
    @name = "diplomat_victim_dist_choice"
    set_heading "Quel 'Quartier' souhaitez-vous échanger ?"

    @choice_box = CardSelection.new "Echanger", 1, 1, true, self
    player = scene.players.find {|player| player.id == player_id}
    for dist in player.citadel.districts
      if dist.cost+dist.upgrade > @current_player.citadel.sold+@diplomat_dist.cost
        @choice_box.add_dist_card dist.id, false, "Vous n'avez pas les moyens d'échanger avec ce 'Quartier'"
      elsif dist.id == "don"
        @choice_box.add_dist_card dist.id, false, "Ce 'Quartier' ne peut être échanger"
      else
        @choice_box.add_dist_card dist.id, true, "Echanger ce 'Quartier'"
      end
    end
  end

  def embellish_dist_choice
    @name = "embellish_dist_choice"
    set_heading "Quel 'Quartier' souhaitez-vous embellir d'une pièce d'or ?"

    @choice_box = CardSelection.new "Embellir", 1, 1, true, self
    for card in @current_player.citadel.districts
      @choice_box.add_dist_card card.id, true, "Embellir ce 'Quartier'"
    end
  end


  # Pouvoir du Laboratoire
  # Permet de choisir le 'Quartier' à défausser contre une pièce d'or'
  def discard_dist_choice
    @name = "discard_dist_choice"
    set_heading "Quel 'Quartier' souhaitez-vous défausser contre une pièce d'or ?"

    @choice_box = CardSelection.new "Défausser", 1, 1, true, self
    for card in @current_player.hand.cards
      @choice_box.add_dist_card card.id, true, "Défausser ce 'Quartier'"
    end
  end

  def museum_discard_dist_choice
    @name = "museum_discard_dist_choice"
    set_heading "Quel 'Quartier' souhaitez-vous placer sous le 'Musée' ?"

    @choice_box = CardSelection.new "Défausser", 1, 1, true, self
    for card in @current_player.hand.cards
      @choice_box.add_dist_card card.id, true, "Défausser ce 'Quartier'"
    end
  end

  # Réalise la phase de choix entre tirer des cartes ou prendre de l'or du 'Navigateur'
  def sailor_choice
    @name = "sailor_choice"
    set_heading "Quelle action souhaitez-vous effectuer ?"

    draw_card_pix = "4_cards"
    draw_card_tool_tip = "Piocher 4 cartes 'Quartier'"
    disp_gold_vs_card draw_card_pix, draw_card_tool_tip, "coins", "4"
  end

  def disp_gold_vs_card draw_card_pix, draw_card_tool_tip, gold_icon, nb_gold

    # Affichages de l'icône du choix 'Prendre les Cartes'
    draw_card = SubmitButton.new draw_card_pix, self
    draw_card.setToolTip draw_card_tool_tip
    x = -($scene_width/2 - draw_card.width)/2 - draw_card.width
    y = -draw_card.height/2
    draw_card.setPos x, y

    # Affichage du texte "ou"
    txt_or = Qt::GraphicsTextItem.new "ou", self
    txt_or.setDefaultTextColor $main_font_color
    txt_or_font = Qt::Font.new $main_font, $scene_height/20
    txt_or.setFont txt_or_font
    x = -txt_or.boundingRect.width/2
    y = -txt_or.boundingRect.height/2
    txt_or.setPos x, y

    # Affichage de l'icône du choix des 'Pièces d'or'
    gold = SubmitButton.new gold_icon, self
    if gold_icon == "coins"
      gold.setToolTip "Prendre #{nb_gold} pièces d'or"
    else
      gold.setToolTip "Donner #{nb_gold} pièces d'or"
    end

    # Affichage du texte de la quantité d'or
    nb_gold = Qt::GraphicsTextItem.new "x #{nb_gold}", self
    nb_gold.setDefaultTextColor $main_font_color
    nb_gold_font = Qt::Font.new $indicator_font, $scene_height/20
    nb_gold.setFont nb_gold_font

    x = ($scene_width/2 - gold.width - nb_gold.boundingRect.width)/2
    y = -gold.height/2
    gold.setPos x, y
    nb_gold.setPos x+gold.width, -nb_gold.boundingRect.height/2
  end

  def disp_power_card cards
    @name = "disp_power_card"
    if @_gold_vs_card and @_build
      set_heading "Sélectionnez la Carte dont vous souhaitez utiliser le Pouvoir !"
      @choice_box = CardSelection.new "Utiliser le Pouvoir", 1, 1, true, self, "skip", "Aucune"
    else
      set_heading "Sélectionnez la Carte dont vous souhaitez utiliser le Pouvoir maintenant !"
      @choice_box = CardSelection.new "Utiliser le Pouvoir", 1, 1, true, self, "clock", "Plus tard"
    end

    for card in cards
      if card.class.to_s == "CharacterCard"
        @choice_box.add_char_card card.id, true, "Utiliser le Pouvoir de ce 'Personnage'"
      end
      if card.id == "lab"
        if @current_player.hand.cards.length > 0
          @choice_box.add_dist_card "lab", true, "Utiliser le Pouvoir de ce 'Quartier'"
        else
          @choice_box.add_dist_card "lab", false, "Vous n'avez aucune carte à défausser"
        end
      end
      if card.id == "mnf"
        if @current_player.citadel.sold >= 2
          @choice_box.add_dist_card "mnf", true, "Utiliser le Pouvoir de ce 'Quartier'"
        else
          @choice_box.add_dist_card "mnf", false, "Vous n'avez pas assez d'argent"
        end
      end
    end
  end

  # Réalise la phase de demande de construction
  def confirm_build
    @name = "confirm_build"
    if @current_char.id == "architecte"
      @nb_build ||= 0
      if @nb_build > 0
        set_heading "Souhaitez-vous bâtir un autre 'Quartier' ?"
      else
        set_heading "Souhaitez-vous bâtir un 'Quartier' ?"
      end
    else
      set_heading "Souhaitez-vous bâtir un 'Quartier' ?"
    end

    # Affichage de l'icône d'acceptation
    accept = SubmitButton.new "build", self
    accept.setToolTip "Bâtir un 'Quartier'"
    x = -$card_width/2 - $card_space*$ratio - accept.width
    y = -accept.height/2
    accept.setPos x, y

    # Affichage de l'icône de refus
    refuse = SubmitButton.new "no_build", self
    refuse.setToolTip "Joueur Suivant"
    x = $card_width/2 + $card_space*$ratio
    y = -refuse.height/2
    refuse.setPos x, y
  end

  # Réalise la phase de construction
  def build_choice
    @name = "build_choice"
    set_heading "Veuillez choisir un 'Quartier' à bâtir !"

    if @current_player.hand.cards.empty?
      alert = Qt::GraphicsTextItem.new self
      alert.setDefaultTextColor Qt::Color.new 255, 0, 0
      alert.setTextWidth $scene_width - $card_width
      alert_font = Qt::Font.new $main_font, $scene_height/20
      alert.setFont alert_font
      alert.setHtml "<center>Vous ne possédez aucune carte 'Quartier' dans votre main !</center>"
      alert.setPos -alert.boundingRect.width/2, - alert.boundingRect.height/2

      # Affichage du bouton de retour en arrière
      submit_prev = SubmitBanner.new "prev", "Etape Précédente", self
      submit_prev.setPos -submit_prev.width/2, $card_height*3/4
    else
      @choice_box = CardSelection.new "Bâtir", 1, 1, true, self
      carrier = @current_player.citadel.districts.find {|dist| dist.id == "car" }
      for card in @current_player.hand.cards
        dist_already_build = @current_player.citadel.districts.find {|dist| dist.name == card.name}
        if card.cost > @current_player.citadel.sold
          @choice_box.add_dist_card card.id, false, "Vous n'avez pas les moyens de bâtir ce 'Quartier'"
        elsif !dist_already_build.nil? and carrier.nil?
          @choice_box.add_dist_card card.id, false, "Vous avez déjà bâti un 'Quartier' de ce type"
        else
          (@current_player.citadel.districts.find {|dist| dist.id=="fab"} and card.cat=="prestige") ? cost=card.cost-1 : cost=card.cost
          @choice_box.add_dist_card card.id, true, "Bâtir ce 'Quartier'\nCoût : #{cost} pièce(s) d'or'"
        end
      end
    end
  end

  def cemetery_owner_call
    @name = "cemetery_owner_call"
    set_heading "Le 'Roi' appelle le Joueur possèdant la carte 'Cimetière' :"

    call_player
  end

  # Permet de choisir si'lon veut récupérer le 'Quartier' détruit par le 'Condottiere' dans sa main contre une pièce d'or
  def disp_cemetery_power
    @name = "disp_cemetery_power"
    set_heading "Souhaitez-vous dépenser une pièce d'or pour récupérer ce 'Quartier' ?"

    # On créé la zone d'affichage de la carte
    @disp_box = DisplayCard.new self
    # On ajoute la carte
    card = scene.dist_deck_ref.find {|dist| dist.id == "cim"}
    @disp_box.add_card card

    # Affichage de l'icône d'acceptation
    accept = SubmitButton.new "check", self
    accept.setToolTip "Récupérer ce 'Quartier'"
    x = -$card_width/2 - $card_space*$ratio - accept.width
    y = -accept.height/2
    accept.setPos x, y

    # Affichage de l'icône de refus
    refuse = SubmitButton.new "ban", self
    refuse.setToolTip "Ne pas récupérer ce 'Quartier'"
    x = $card_width/2 + $card_space*$ratio
    y = -refuse.height/2
    refuse.setPos x, y
  end

  def mercenary_recall
    @name = "mercenary_recall"
    set_heading "Rappel de :"

    call_player
  end

  # Révèle le Joueur assassiné
  def disp_killed_player killed_char
    @name = "disp_killed_player"
    set_heading "Voici le Joueur assassiné :"

    # On créé la zone d'affichage de la carte
    @disp_box = DisplayCard.new self
    # On ajoute la carte
    char_ref = scene.char_deck_ref.find {|char| char.id == killed_char.id}
    char_ref.killed = true
    @disp_box.add_card char_ref
    @disp_box.setPos -$card_width/2, -$card_height/2-$icon_wide

    # On indique le Joueur en possession de la carte
    player = killed_char.parentItem
    player_label = PlayerLabel.new player.player_id, player.player_name, 'avatar', false, self
    player_label.scale 2, 2
    player_label.setPos -player_label.width, $card_height/2-$icon_wide

    # Affichage du bouton de validation
    submit_next = SubmitBanner.new "next", "Etape Suivante", self
    submit_next.setPos -submit_next.width/2, $card_height*3/4
  end

  # Affiche le classement des joueurs
  def disp_ranking end_game=false
    @name = "disp_ranking"
    set_heading ""
    ranking = Ranking.new self
    ranking.show
    ranking.setZValue 1
    ranking.set_turn @nb_turn
    # Affichage du bouton de validation
    if end_game
      the_end = Qt::GraphicsTextItem.new "Partie Terminée !", self
      the_end.setDefaultTextColor $red
      the_end_font = Qt::Font.new $main_font, $scene_height/20
      the_end.setFont the_end_font
      the_end.setPos -the_end.boundingRect.width/2, $card_height*3/4
      the_end.setZValue 2
    else
      submit_next = SubmitBanner.new "next", "Tour Suivant", self
      submit_next.setPos -submit_next.width/2, $card_height*3/4
      submit_next.scroll_banner.hide
      submit_next.setZValue 2
    end
  end

  # Affiche le Podium
  def disp_podium
    @name = "disp_podium"
    set_heading ""

    podium = Podium.new self
    podium.setZValue 1

    submit_next = SubmitBanner.new "next", "Voir Classement", self
    submit_next.setPos -submit_next.width/2, $card_height*3/4
  end

end