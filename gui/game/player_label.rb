# encoding: utf-8

require 'Qt4'

class PlayerLabel < Qt::GraphicsItem

  attr_reader :player_id, :name, :width, :height
  attr_accessor :id, :selected

  def initialize player_id, name, avatar_name='@avatar', hover_effect=false, parent=0
    super(parent)

    @player_id = player_id
    @name = name

    avatar_path = "./img/#{avatar_name}.png"

    set_avatar avatar_path
    set_player_name name
    set_background
    set_positions

    @label.setGraphicsEffect @red_effect

    if hover_effect
      @selectable = true
      @selected = false
      setAcceptedMouseButtons Qt::LeftButton
      # On accepte de récupérer de manière indépendante les évènements de type survol de la souris
      setAcceptHoverEvents true
      setGraphicsEffect @grey_effect
    end
  end

  def boundingRect
    Qt::RectF.new 0, 0, @width, @height
  end

  def mousePressEvent event
    if event.button == 1 and parentItem.class.to_s == "PlayerSelection" and @selectable
      if parentItem.submit_next.nil?
        @selected = true
        scene.action.do_action
      else
        for player in parentItem.players
          player.selected = false
          player.deactivate unless player == self
        end
        if @selectable
          @selected ? @selected=false : @selected=true
        end
        parentItem.disp_submit
      end
    else
      event.ignore
    end
  end

  def hoverMoveEvent event
    hoverEnterEvent event
  end

  def hoverEnterEvent event
    if @selectable
      setCursor Qt::Cursor.new(Qt::PointingHandCursor)
      activate
    end
  end

  def hoverLeaveEvent event
    deactivate unless @selected
  end

  # Affichage de l'@avatar du joueur
  def set_avatar avatar_path
    avatar_pix = Qt::Pixmap.new avatar_path
    @avatar = Qt::GraphicsPixmapItem.new avatar_pix, self
    @avatar.setTransformationMode Qt::SmoothTransformation
    w_ratio = $icon_wide.to_f/avatar_pix.width
    h_ratio = $icon_wide.to_f/avatar_pix.height
    #todo Intégrer un zoom sur l'@avatar lors du survol de ce dernier
    @avatar.scale w_ratio, h_ratio

    # Création de l'effet grisé
    @color_effect = Qt::GraphicsColorizeEffect.new
    @color_effect.setColor $player_colors[@player_id-1]
    @avatar.setGraphicsEffect @color_effect
  end

  # Affichage du nom/pseudo du joueur
  def set_player_name name
    @label = Qt::GraphicsTextItem.new name.to_s, self
    font = Qt::Font.new $main_font, $icon_wide/2
    @label.setFont font
  end

  # Affichage du fond
  def set_background
    scroll_banner_pix = Qt::Pixmap.new "./img/scroll_banner.png"
    @scroll_banner = Qt::GraphicsPixmapItem.new scroll_banner_pix, self
    @scroll_banner.setTransformationMode Qt::SmoothTransformation
  end

  # Création de l'effet d'activation du tour du Joueur
  def set_hover_effect x, y, width, height
    # Caractéristiques de la ligne du pourtour de l'icône lors d'un survol
    pen = Qt::Pen.new($red)
    pen.setWidth 2
    pen.setStyle(Qt::SolidLine)
    pen.setCapStyle(Qt::RoundCap)
    pen.setJoinStyle(Qt::RoundJoin)

    # Dessine un pourtour autour de l'icône
    painter = Qt::PainterPath.new
    painter.addRoundedRect x, y, width, height, 5.0, 5.0
    @square = Qt::GraphicsPathItem.new painter, self
    @square.setPen pen
    @square.setZValue 3
    @square.hide

    # Création de l'effet grisé
    @grey_effect = Qt::GraphicsColorizeEffect.new
    @grey_effect.setColor $dimgrey
    @grey_effect.setEnabled false

    @red_effect = Qt::GraphicsColorizeEffect.new
    @red_effect.setColor $red
    @red_effect.setEnabled false
  end

  def activate
    @square.show
    @red_effect.setEnabled true
  end

  def deactivate
    @square.hide
    @red_effect.setEnabled false
  end

  def set_positions
    label_width = $icon_wide + $card_space + @label.boundingRect.width
    @width = (label_width*1.2).ceil
    @height = ($icon_wide*1.34).ceil
    width_ratio = @width / @scroll_banner.boundingRect.width
    height_ratio = @height / @scroll_banner.boundingRect.height
    @scroll_banner.scale width_ratio, height_ratio

    x = (label_width*0.1).ceil
    y = ($icon_wide*0.17).ceil
    @avatar.setPos x, y
    @label.setPos x+$icon_wide+$card_space, (@height-@label.boundingRect.height)/2

    set_hover_effect x, y, $icon_wide, $icon_wide

    @scroll_banner.setZValue 1
    @avatar.setZValue 2
    @label.setZValue 2
    setZValue 2
  end

  def unavailable
    @grey_effect.setEnabled true
    @selectable = false
  end

  def available
    @grey_effect.setEnabled false
    @selectable = true
  end
end