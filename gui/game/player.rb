# encoding: utf-8

require './gui/game/citadel'
require './gui/game/player_hand'

class Player

  attr_reader :id, :name, :citadel, :hand, :king
  attr_accessor :first

  def initialize id, name
    # Numéro du Joueur
    @id = id
    # Nom du Joueur
    @name = name
    # Indique si le Joueur est le Roi pour le tour de jeu
    @king = false
    # Indique si le Joueur est le premier à avoir construit le nombre maximal de 'Quartier'
    @first = false
    # Instanciation de la 'Main du Joueur' en premier pour un affichage correct
    # du nombre de cartes de la 'Main du Joueur' lors de la création de sa 'Citadelle'
    @hand = PlayerHand.new @id
    # Instanciation d'une 'Citadelle' pour le joueur
    @citadel = Citadel.new  @id, @name
  end

  # Définit le joueur comme actif ou non-actif
  def set_current activate
    activate ? @citadel.activate : @citadel.deactivate
  end

  # Fait du Joueur le Roi du tour de jeu
  def enthrone
    @king = true
    @citadel.crown.show
  end

  # Retire au Joueur sa souveraineté
  def dethrone
    @king = false
    @citadel.crown.hide
  end

  # Le Joueur tire une carte dans la 'Pioche'
  def draw_card draw_dist
    card = draw_dist.draw_card
    add_card card.id
  end

  # Ajout d'une carte 'Quartier' à la 'Main du Joueur'
  def add_card card_id
    # Ajout de la carte 'Quartier' à la 'Main du Joueur'
    @hand.add_card card_id
    # On met à jour  l'indicateur du nombre de cartes 'Quartier' de la 'Citadelle' du Joueur
    @citadel.refresh_nb_cards @hand.cards.length.to_s
  end

  # Le Joueur se défausse d'une carte
  def discard_card card_id, discard_dist
    # On supprime la carte de la 'Main du Joueur'
    remove_card card_id
    # On ajoute la carte à la 'Défausse'
    discard_dist.add_card card_id
  end

  # Retrait d'une carte de la main du Joueur
  def remove_card card_id
    # Retrait de la carte 'Quartier' de la 'Main du Joueur'
    @hand.remove_card card_id
    # On met à jour l'indicateur du nombre de cartes 'Quartier' de la 'Citadelle' du Joueur
    @citadel.refresh_nb_cards @hand.cards.length.to_s
  end

  # Construit un 'Quartier' dans la 'Citadelle'
  def build card_id
    @citadel.build_dist card_id
    @hand.remove_card card_id
    @citadel.refresh_nb_cards @hand.cards.length.to_s
  end

  # Ajoute la somme au solde du Joueur
  def earn amount
    @citadel.sold += amount
    @citadel.refresh_sold
  end

  # Retire la somme au solde du Joueur
  def pay amount
    @citadel.sold -= amount
    @citadel.refresh_sold
  end
end