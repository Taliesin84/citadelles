# encoding: utf-8

require './gui/game/card'

class DistrictCard < Card

  attr_reader :name, :cat, :cost, :upgrade, :museum_cards
  attr_accessor :power_used

  def initialize id, cat, name, cost
    @back_pix = Qt::Pixmap.new "./img/cards/others/dist_back.png"
    card_path = "districts/#{cat}/#{name}"
    super(id, card_path)

    # Nom du 'Quartier'
    @name = name
    # Catégorie du 'Quartier'
    @cat = cat
    # Coût de construction du 'Quartier'
    @cost = cost
    # Indique si le pouvoir du 'Quartier' a été utilisé lors du tour
    @power_used = false
    # Indique la valeur de l'embélissement par l'Artiste du 'Quartier'
    @upgrade = 0
    if id == "mus"
      @museum_cards = []
      disp_museum_indicator
    end
  end

  # Embéllit le 'Quartier' d'une pièce d'or
  def embellish
    @upgrade += 1
    disp_embellish
  end

  # Affiche les pièces d'embellissement du 'Quartier' par l'artiste
  def disp_embellish
    i_coin = 0
    coin_wide = $card_height/11
    while i_coin < @upgrade
      coin = Coin.new self
      ratio = coin_wide.to_f/coin.boundingRect.width
      coin.scale ratio, ratio
      x = boundingRect.width*0.82 - (coin_wide* (i_coin/6))
      y = $card_height*0.02 + (coin_wide*(i_coin%6))
      coin.setPos x, y
      i_coin += 1
    end
  end

  def disp_museum_indicator
    # Affichage de l'icône représentant le nombre de cartes de la 'Main du Joueur'
    cards_pix = Qt::Pixmap.new "./img/cards.png"
    cards_pix = cards_pix.scaled $icon_wide, $icon_wide, Qt::IgnoreAspectRatio, Qt::SmoothTransformation
    @cards_icon = Qt::GraphicsPixmapItem.new cards_pix, self
    @cards_icon.setToolTip "Nombre de cartes placées sous le 'Musée'"
    # Affichage du montant du nombre de cartes de la 'Main du Joueur'
    @txt_nb_cards = Qt::GraphicsTextItem.new @museum_cards.length.to_s, self
    font = Qt::Font.new $indicator_font, $icon_wide/2
    @txt_nb_cards.setFont font
    @txt_nb_cards.setDefaultTextColor Qt::Color.new 0, 139, 0
    x = $card_width*0.7
    y = $card_height*0.86
    @cards_icon.setPos x, y
    @txt_nb_cards.setPos x+@cards_icon.boundingRect.width, y
  end

  def add_card card_id
    dist = scene.dist_deck_dup.find {|card| card.id == card_id}
    dist.un_reveal
    dist.setParentItem self
    dist.hide
    @museum_cards << dist
    refresh_museum_cards
    parentItem.set_score
  end

  def refresh_museum_cards
    @txt_nb_cards.setPlainText @museum_cards.length.to_s
    @museum_cards.length>9 ? x=$card_width*0.65 : x=$card_width*0.7
    y = $card_height*0.86
    @cards_icon.setPos x, y
    @txt_nb_cards.setPos x+@cards_icon.boundingRect.width, y
  end

  def reveal
    super
    disp_embellish
  end

  def un_reveal
    super
    for item in childItems
      if item.class.to_s == "Coin"
        item.setParentItem nil
        scene.removeItem item
      end
    end
  end

  def re_init
    @upgrade = 0
    if id == "mus"
      @museum_cards = []
      refresh_museum_cards
    end
    for item in childItems
      if item.class.to_s == "Coin"
        item.setParentItem nil
        scene.removeItem item
      end
    end
  end

end

class Coin < Qt::GraphicsPixmapItem
  def initialize parent=nil
    super(parent)
    coin_pix = Qt::Pixmap.new "./img/coin.png"
    setPixmap coin_pix
    setTransformationMode Qt::SmoothTransformation
  end
end