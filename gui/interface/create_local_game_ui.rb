# encoding: utf-8

require './gui/interface/portrait'

=begin
** Form generated from reading ui file 'create_local_game.ui'
**
** Created: sam. juil. 14 00:15:33 2012
**      by: Qt User Interface Compiler version 4.8.1
**
** WARNING! All changes made in this file will be lost when recompiling ui file!
=end

class Ui_CreateLocalGame
    attr_reader :grp_box_characters
    attr_reader :horizontalLayoutWidget_2
    attr_reader :layout_characters
    attr_reader :char_list
    attr_reader :line
    attr_reader :grp_box_players
    attr_reader :gridLayoutWidget_2
    attr_reader :gridLayout
    attr_reader :tab_widget_param
    attr_reader :tab_game_parameters
    attr_reader :gridLayoutWidget
    attr_reader :layout_game_parameters
    attr_reader :lbl_nb_dist
    attr_reader :nb_dist
    attr_reader :nb_gold
    attr_reader :nb_players
    attr_reader :lbl_nb_cards
    attr_reader :lbl_nb_gold
    attr_reader :nb_cards
    attr_reader :lbl_nb_players
    attr_reader :button_default
    attr_reader :button_dist_deck
    attr_reader :tab_options
    attr_reader :verticalLayoutWidget_2
    attr_reader :layout_options
    attr_reader :check_easy_draw
    attr_reader :check_kill_crown
    attr_reader :check_end_killed_char
    attr_reader :check_total_bewitch
    attr_reader :check_bewitch_crown
    attr_reader :check_build_bewitch
    attr_reader :check_end_owner
    attr_reader :check_sorcerer_revealed_cards
    attr_reader :check_toss_king
    attr_reader :check_killing_king
    attr_reader :check_oblige_to_enthrone
    attr_reader :check_all_wealthy_pay
    attr_reader :check_ending_turn_mercenary
    attr_reader :check_pay_diff
    attr_reader :check_treasure_add
    attr_reader :check_end_powder
    attr_reader :buttonBox

    def setupUi(createLocalGame)
    if createLocalGame.objectName.nil?
        createLocalGame.objectName = "createLocalGame"
    end
    createLocalGame.resize(669, 636)
    createLocalGame.minimumSize = Qt::Size.new(669, 636)
    createLocalGame.maximumSize = Qt::Size.new(669, 636)
    window_icon = Qt::Icon.new
    window_icon.addPixmap Qt::Pixmap.new('./img/icons/toolbar/home.png'), Qt::Icon::Normal, Qt::Icon::Off
    createLocalGame.windowIcon = window_icon
    @grp_box_characters = Qt::GroupBox.new(createLocalGame)
    @grp_box_characters.objectName = "grp_box_characters"
    @grp_box_characters.geometry = Qt::Rect.new(10, 10, 651, 111)
    @grp_box_characters.flat = false
    @horizontalLayoutWidget_2 = Qt::Widget.new(@grp_box_characters)
    @horizontalLayoutWidget_2.objectName = "horizontalLayoutWidget_2"
    @horizontalLayoutWidget_2.geometry = Qt::Rect.new(10, 30, 651, 66)
    @layout_characters = Qt::HBoxLayout.new(@horizontalLayoutWidget_2)
    @layout_characters.objectName = "layout_characters"
    @layout_characters.setContentsMargins(0, 0, 0, 0)
    @char_list = []
    i = 0
    while i <= 8
      char = Portrait.new i, @layout_portraits
      @char_list << char
      @layout_characters.addWidget(char)
      i += 1
    end
    @line = Qt::Frame.new(createLocalGame)
    @line.objectName = "line"
    @line.geometry = Qt::Rect.new(10, 570, 651, 20)
    @line.setFrameShape(Qt::Frame::HLine)
    @line.setFrameShadow(Qt::Frame::Sunken)
    @grp_box_players = Qt::GroupBox.new(createLocalGame)
    @grp_box_players.objectName = "grp_box_players"
    @grp_box_players.geometry = Qt::Rect.new(10, 130, 281, 441)
    @gridLayoutWidget_2 = Qt::Widget.new(@grp_box_players)
    @gridLayoutWidget_2.objectName = "gridLayoutWidget_2"
    @gridLayoutWidget_2.geometry = Qt::Rect.new(10, 30, 261, 401)
    @gridLayout = Qt::GridLayout.new(@gridLayoutWidget_2)
    @gridLayout.objectName = "gridLayout"
    @gridLayout.setContentsMargins(0, 0, 0, 0)
    @tab_widget_param = Qt::TabWidget.new(createLocalGame)
    @tab_widget_param.objectName = "tab_widget_param"
    @tab_widget_param.geometry = Qt::Rect.new(300, 130, 361, 441)
    @tab_game_parameters = Qt::Widget.new()
    @tab_game_parameters.objectName = "tab_game_parameters"
    @gridLayoutWidget = Qt::Widget.new(@tab_game_parameters)
    @gridLayoutWidget.objectName = "gridLayoutWidget"
    @gridLayoutWidget.geometry = Qt::Rect.new(10, 10, 331, 141)
    @layout_game_parameters = Qt::GridLayout.new(@gridLayoutWidget)
    @layout_game_parameters.objectName = "layout_game_parameters"
    @layout_game_parameters.setContentsMargins(0, 0, 0, 0)
    @lbl_nb_dist = Qt::Label.new(@gridLayoutWidget)
    @lbl_nb_dist.objectName = "lbl_nb_dist"

    @layout_game_parameters.addWidget(@lbl_nb_dist, 3, 0, 1, 1)

    @nb_dist = Qt::SpinBox.new(@gridLayoutWidget)
    @nb_dist.objectName = "nb_dist"
    @nb_dist.minimum = 6
    @nb_dist.maximum = 8
    @nb_dist.value = 8

    @layout_game_parameters.addWidget(@nb_dist, 3, 1, 1, 1)

    @nb_gold = Qt::SpinBox.new(@gridLayoutWidget)
    @nb_gold.objectName = "nb_gold"
    @nb_gold.maximum = 10
    @nb_gold.value = 2

    @layout_game_parameters.addWidget(@nb_gold, 2, 1, 1, 1)

    @nb_players = Qt::SpinBox.new(@gridLayoutWidget)
    @nb_players.objectName = "nb_players"
    @nb_players.minimum = 2
    @nb_players.maximum = 8

    @layout_game_parameters.addWidget(@nb_players, 0, 1, 1, 1)

    @lbl_nb_cards = Qt::Label.new(@gridLayoutWidget)
    @lbl_nb_cards.objectName = "lbl_nb_cards"

    @layout_game_parameters.addWidget(@lbl_nb_cards, 1, 0, 1, 1)

    @lbl_nb_gold = Qt::Label.new(@gridLayoutWidget)
    @lbl_nb_gold.objectName = "lbl_nb_gold"

    @layout_game_parameters.addWidget(@lbl_nb_gold, 2, 0, 1, 1)

    @nb_cards = Qt::SpinBox.new(@gridLayoutWidget)
    @nb_cards.objectName = "nb_cards"
    @nb_cards.maximum = 8
    @nb_cards.value = 4

    @layout_game_parameters.addWidget(@nb_cards, 1, 1, 1, 1)

    @lbl_nb_players = Qt::Label.new(@gridLayoutWidget)
    @lbl_nb_players.objectName = "lbl_nb_players"

    @layout_game_parameters.addWidget(@lbl_nb_players, 0, 0, 1, 1)

    @button_default = Qt::PushButton.new(@tab_game_parameters)
    @button_default.objectName = "button_default"
    @button_default.geometry = Qt::Rect.new(10, 380, 331, 24)
    @button_dist_deck = Qt::PushButton.new(@tab_game_parameters)
    @button_dist_deck.objectName = "button_dist_deck"
    @button_dist_deck.geometry = Qt::Rect.new(120, 220, 111, 91)
    icon = Qt::Icon.new
    icon.addPixmap(Qt::Pixmap.new("./img/2_to_1_card.png"), Qt::Icon::Normal, Qt::Icon::Off)
    @button_dist_deck.icon = icon
    @button_dist_deck.iconSize = Qt::Size.new(64, 64)
    @tab_widget_param.addTab @tab_game_parameters, Qt::Application.translate("CreateLocalGame", "Paramètres de la partie", nil, Qt::Application::UnicodeUTF8)
    @tab_options = Qt::Widget.new()
    @tab_options.objectName = "tab_options"
    @verticalLayoutWidget_2 = Qt::Widget.new(@tab_options)
    @verticalLayoutWidget_2.objectName = "verticalLayoutWidget_2"
    @verticalLayoutWidget_2.geometry = Qt::Rect.new(10, 10, 331, 401)
    @layout_options = Qt::VBoxLayout.new(@verticalLayoutWidget_2)
    @layout_options.objectName = "layout_options"
    @layout_options.setContentsMargins(0, 0, 0, 0)
    @check_easy_draw = Qt::CheckBox.new(@verticalLayoutWidget_2)
    @check_easy_draw.objectName = "check_easy_draw"

    @layout_options.addWidget(@check_easy_draw)

    @check_kill_crown = Qt::CheckBox.new(@verticalLayoutWidget_2)
    @check_kill_crown.objectName = "check_kill_crown"

    @layout_options.addWidget(@check_kill_crown)

    @check_end_killed_char = Qt::CheckBox.new(@verticalLayoutWidget_2)
    @check_end_killed_char.objectName = "check_end_killed_char"

    @layout_options.addWidget(@check_end_killed_char)

    @check_total_bewitch = Qt::CheckBox.new(@verticalLayoutWidget_2)
    @check_total_bewitch.objectName = "check_total_bewitch"

    @layout_options.addWidget(@check_total_bewitch)

    @check_bewitch_crown = Qt::CheckBox.new(@verticalLayoutWidget_2)
    @check_bewitch_crown.objectName = "check_bewitch_crown"

    @layout_options.addWidget(@check_bewitch_crown)

    @check_build_bewitch = Qt::CheckBox.new(@verticalLayoutWidget_2)
    @check_build_bewitch.objectName = "check_build_bewitch"

    @layout_options.addWidget(@check_build_bewitch)

    @check_end_owner = Qt::CheckBox.new(@verticalLayoutWidget_2)
    @check_end_owner.objectName = "check_end_owner"

    @layout_options.addWidget(@check_end_owner)

    @check_sorcerer_revealed_cards = Qt::CheckBox.new(@verticalLayoutWidget_2)
    @check_sorcerer_revealed_cards.objectName = "check_sorcerer_revealed_cards"

    @layout_options.addWidget(@check_sorcerer_revealed_cards)

    @check_toss_king = Qt::CheckBox.new(@verticalLayoutWidget_2)
    @check_toss_king.objectName = "check_toss_king"

    @layout_options.addWidget(@check_toss_king)

    @check_killing_king = Qt::CheckBox.new(@verticalLayoutWidget_2)
    @check_killing_king.objectName = "check_killing_king"

    @layout_options.addWidget(@check_killing_king)

    @check_oblige_to_enthrone = Qt::CheckBox.new(@verticalLayoutWidget_2)
    @check_oblige_to_enthrone.objectName = "check_oblige_to_enthrone"

    @layout_options.addWidget(@check_oblige_to_enthrone)

    @check_all_wealthy_pay = Qt::CheckBox.new(@verticalLayoutWidget_2)
    @check_all_wealthy_pay.objectName = "check_all_wealthy_pay"

    @layout_options.addWidget(@check_all_wealthy_pay)

    @check_ending_turn_mercenary = Qt::CheckBox.new(@verticalLayoutWidget_2)
    @check_ending_turn_mercenary.objectName = "check_ending_turn_mercenary"

    @layout_options.addWidget(@check_ending_turn_mercenary)

    @check_pay_diff = Qt::CheckBox.new(@verticalLayoutWidget_2)
    @check_pay_diff.objectName = "check_pay_diff"

    @layout_options.addWidget(@check_pay_diff)

    @check_treasure_add = Qt::CheckBox.new(@verticalLayoutWidget_2)
    @check_treasure_add.objectName = "check_treasure_add"

    @layout_options.addWidget(@check_treasure_add)

    @check_end_powder = Qt::CheckBox.new(@verticalLayoutWidget_2)
    @check_end_powder.objectName = "check_end_powder"

    @layout_options.addWidget(@check_end_powder)

    @tab_widget_param.addTab(@tab_options, Qt::Application.translate("CreateLocalGame", "Variantes", nil, Qt::Application::UnicodeUTF8))
    @buttonBox = Qt::DialogButtonBox.new(createLocalGame)
    @buttonBox.objectName = "buttonBox"
    @buttonBox.geometry = Qt::Rect.new(10, 590, 651, 31)
    @buttonBox.standardButtons = Qt::DialogButtonBox::Cancel|Qt::DialogButtonBox::Ok
    @buttonBox.centerButtons = true
    Qt::Widget.setTabOrder(@tab_widget_param, @nb_players)
    Qt::Widget.setTabOrder(@nb_players, @nb_cards)
    Qt::Widget.setTabOrder(@nb_cards, @nb_gold)
    Qt::Widget.setTabOrder(@nb_gold, @nb_dist)
    Qt::Widget.setTabOrder(@nb_dist, @button_dist_deck)
    Qt::Widget.setTabOrder(@button_dist_deck, @button_default)
    Qt::Widget.setTabOrder(@button_default, @check_easy_draw)
    Qt::Widget.setTabOrder(@check_easy_draw, @check_kill_crown)
    Qt::Widget.setTabOrder(@check_kill_crown, @check_end_killed_char)
    Qt::Widget.setTabOrder(@check_end_killed_char, @check_total_bewitch)
    Qt::Widget.setTabOrder(@check_total_bewitch, @check_bewitch_crown)
    Qt::Widget.setTabOrder(@check_bewitch_crown, @check_build_bewitch)
    Qt::Widget.setTabOrder(@check_build_bewitch, @check_end_owner)
    Qt::Widget.setTabOrder(@check_end_owner, @check_sorcerer_revealed_cards)
    Qt::Widget.setTabOrder(@check_sorcerer_revealed_cards, @check_toss_king)
    Qt::Widget.setTabOrder(@check_toss_king, @check_killing_king)
    Qt::Widget.setTabOrder(@check_killing_king, @check_oblige_to_enthrone)
    Qt::Widget.setTabOrder(@check_oblige_to_enthrone, @check_all_wealthy_pay)
    Qt::Widget.setTabOrder(@check_all_wealthy_pay, @check_ending_turn_mercenary)
    Qt::Widget.setTabOrder(@check_ending_turn_mercenary, @check_pay_diff)
    Qt::Widget.setTabOrder(@check_pay_diff, @check_treasure_add)
    Qt::Widget.setTabOrder(@check_treasure_add, @check_end_powder)

    retranslateUi(createLocalGame)

    @tab_widget_param.setCurrentIndex(0)


    Qt::MetaObject.connectSlotsByName(createLocalGame)
    end # setupUi

    def setup_ui(createLocalGame)
        setupUi(createLocalGame)
    end

    def retranslateUi(createLocalGame)
    createLocalGame.windowTitle = Qt::Application.translate("CreateLocalGame", "Création d'une Partie", nil, Qt::Application::UnicodeUTF8)
    createLocalGame.toolTip = Qt::Application.translate("CreateLocalGame", "Définit si la destruction d'un 'Quartier' dans une 'Citadelle' achevée par la 'Poudrière' repousse la fin de la partie.", nil, Qt::Application::UnicodeUTF8)
    @grp_box_characters.title = Qt::Application.translate("CreateLocalGame", "Deck des 'Personnages'", nil, Qt::Application::UnicodeUTF8)
    @grp_box_players.title = Qt::Application.translate("CreateLocalGame", "Liste des Joueurs", nil, Qt::Application::UnicodeUTF8)
    @tab_widget_param.toolTip = Qt::Application.translate("CreateLocalGame", "Choir les cartes qui feront parties du deck des 'Quartiers'.", nil, Qt::Application::UnicodeUTF8)
    @lbl_nb_dist.toolTip = Qt::Application.translate("CreateLocalGame", "Détermine le nombre de 'Quartiers' à bâtir pour que la partie s'achève.", nil, Qt::Application::UnicodeUTF8)
    @lbl_nb_dist.text = Qt::Application.translate("CreateLocalGame", "Nombre de 'Quartiers' :", nil, Qt::Application::UnicodeUTF8)
    @lbl_nb_cards.toolTip = Qt::Application.translate("CreateLocalGame", "Définit le nombre de cartes distribuées en début de partie à chaque joueurs.", nil, Qt::Application::UnicodeUTF8)
    @lbl_nb_cards.text = Qt::Application.translate("CreateLocalGame", "Nombre de cartes :", nil, Qt::Application::UnicodeUTF8)
    @lbl_nb_gold.toolTip = Qt::Application.translate("CreateLocalGame", "Définit la quantité d'argent donnée à chaque joueur en début de partie.", nil, Qt::Application::UnicodeUTF8)
    @lbl_nb_gold.text = Qt::Application.translate("CreateLocalGame", "Solde de départ :", nil, Qt::Application::UnicodeUTF8)
    @lbl_nb_players.toolTip = Qt::Application.translate("CreateLocalGame", "Définit le nombre maximal de joueurs pouvant rejoindre la partie.", nil, Qt::Application::UnicodeUTF8)
    @lbl_nb_players.text = Qt::Application.translate("CreateLocalGame", "Joueurs max :", nil, Qt::Application::UnicodeUTF8)
    @button_default.text = Qt::Application.translate("CreateLocalGame", "Rétablir les paramètres par défauts", nil, Qt::Application::UnicodeUTF8)
    @button_dist_deck.text = ''
    @tab_widget_param.setTabText(@tab_widget_param.indexOf(@tab_game_parameters), Qt::Application.translate("CreateLocalGame", "Paramètres de la partie", nil, Qt::Application::UnicodeUTF8))
    @check_easy_draw.toolTip = Qt::Application.translate("CreateLocalGame", "Variante permettant de choisir la carte à défausser parmis toutes ses cartes.", nil, Qt::Application::UnicodeUTF8)
    @check_easy_draw.text = Qt::Application.translate("CreateLocalGame", "Pioche facile", nil, Qt::Application::UnicodeUTF8)
    @check_kill_crown.toolTip = Qt::Application.translate("CreateLocalGame", "Définit si l'assassinat du 'Roi' entraîne la perte de la couronne.", nil, Qt::Application::UnicodeUTF8)
    @check_kill_crown.text = Qt::Application.translate("CreateLocalGame", "Couronne des mort-vivants", nil, Qt::Application::UnicodeUTF8)
    @check_end_killed_char.toolTip = Qt::Application.translate("CreateLocalGame", "Définit si le 'Personnage' assassiné se révèle à la fin du tour.", nil, Qt::Application::UnicodeUTF8)
    @check_end_killed_char.text = Qt::Application.translate("CreateLocalGame", "Que les morts se révèlent... à la fin !", nil, Qt::Application::UnicodeUTF8)
    @check_total_bewitch.toolTip = Qt::Application.translate("CreateLocalGame", "Définit si la 'Sorcière' récupère la totalité du tour du Joueur ou seulement le pouvoir de son 'Personnage'.", nil, Qt::Application::UnicodeUTF8)
    @check_total_bewitch.text = Qt::Application.translate("CreateLocalGame", "Véritable marionnettiste", nil, Qt::Application::UnicodeUTF8)
    @check_bewitch_crown.toolTip = Qt::Application.translate("CreateLocalGame", "Définit si la 'Sorcière' récupère la couronne en ensorcellant le 'Roi'.", nil, Qt::Application::UnicodeUTF8)
    @check_bewitch_crown.text = Qt::Application.translate("CreateLocalGame", "Couronne ensorcelée", nil, Qt::Application::UnicodeUTF8)
    @check_build_bewitch.toolTip = Qt::Application.translate("CreateLocalGame", "Définit si la 'Sorcière' peut bâtir un 'Quartier' au cours de son tour.", nil, Qt::Application::UnicodeUTF8)
    @check_build_bewitch.text = Qt::Application.translate("CreateLocalGame", "Entrepreuneuse au balai", nil, Qt::Application::UnicodeUTF8)
    @check_end_owner.toolTip = Qt::Application.translate("CreateLocalGame", "Définit si le 'Bailli' est appelé à la toute fin du tour de jeu.", nil, Qt::Application::UnicodeUTF8)
    @check_end_owner.text = Qt::Application.translate("CreateLocalGame", "Collecte des impôts", nil, Qt::Application::UnicodeUTF8)
    @check_sorcerer_revealed_cards.toolTip = Qt::Application.translate("CreateLocalGame", "Définit si le 'Sorcier' peut voir les cartes lors de l'utilisation de son pouvoir.", nil, Qt::Application::UnicodeUTF8)
    @check_sorcerer_revealed_cards.text = Qt::Application.translate("CreateLocalGame", "Oracle", nil, Qt::Application::UnicodeUTF8)
    @check_toss_king.toolTip = Qt::Application.translate("CreateLocalGame", "Définit si on tire au hasard le 'Roi' lorsqu'il se retrouve parmis les personnages défaussés.", nil, Qt::Application::UnicodeUTF8)
    @check_toss_king.text = Qt::Application.translate("CreateLocalGame", "Tirage au sort du Roi", nil, Qt::Application::UnicodeUTF8)
    @check_killing_king.toolTip = Qt::Application.translate("CreateLocalGame", "Définit si le 'Roi' peut-être assassiné.", nil, Qt::Application::UnicodeUTF8)
    @check_killing_king.text = Qt::Application.translate("CreateLocalGame", "Roi immortel", nil, Qt::Application::UnicodeUTF8)
    @check_oblige_to_enthrone.toolTip = Qt::Application.translate("CreateLocalGame", "Définit si l''Empereur' est obligé d'intrôniser un Joueur au début de son tour.", nil, Qt::Application::UnicodeUTF8)
    @check_oblige_to_enthrone.text = Qt::Application.translate("CreateLocalGame", "Empereur à la main forcée", nil, Qt::Application::UnicodeUTF8)
    @check_all_wealthy_pay.toolTip = Qt::Application.translate("CreateLocalGame", "Définit si tous les Joueurs les plus riches payent l''Abbe'.", nil, Qt::Application::UnicodeUTF8)
    @check_all_wealthy_pay.text = Qt::Application.translate("CreateLocalGame", "Tous les riches passent à la caisse !", nil, Qt::Application::UnicodeUTF8)
    @check_ending_turn_mercenary.toolTip = Qt::Application.translate("CreateLocalGame", "Définit si le 'Condottiere' peut détruire le 'Quartier' d'un Joueur ayant achevé sa 'Citadelle'.", nil, Qt::Application::UnicodeUTF8)
    @check_ending_turn_mercenary.text = Qt::Application.translate("CreateLocalGame", "Ce n'est pas encore fini !", nil, Qt::Application::UnicodeUTF8)
    @check_pay_diff.toolTip = Qt::Application.translate("CreateLocalGame", "Définit si le 'Diplomate' paye la différence si son 'Quartier' a un coût inférieur à celui de sa victime.", nil, Qt::Application::UnicodeUTF8)
    @check_pay_diff.text = Qt::Application.translate("CreateLocalGame", "On paie la différence !", nil, Qt::Application::UnicodeUTF8)
    @check_treasure_add.toolTip = Qt::Application.translate("CreateLocalGame", "Définit si le coût de la construction du 'Quartier' 'Trésor Impérial' est compris dans le décompte final ou pas.", nil, Qt::Application::UnicodeUTF8)
    @check_treasure_add.text = Qt::Application.translate("CreateLocalGame", "Trésor doublement impérial", nil, Qt::Application::UnicodeUTF8)
    @check_end_powder.text = Qt::Application.translate("CreateLocalGame", "Faîtes parler la poudre !", nil, Qt::Application::UnicodeUTF8)
    @tab_widget_param.setTabText(@tab_widget_param.indexOf(@tab_options), Qt::Application.translate("CreateLocalGame", "Variantes", nil, Qt::Application::UnicodeUTF8))
    end # retranslateUi

    def retranslate_ui(createLocalGame)
        retranslateUi(createLocalGame)
    end

end

module Ui
    class CreateLocalGame < Ui_CreateLocalGame
    end
end  # module Ui

