# encoding: utf-8

require 'Qt4'
require 'socket'
require 'thread'
require './gui/interface/waiting_room_ui'
require './gui/interface/char_select'
require './gui/interface/dist_select'
require './lib/servers/tchat_server'

class WaitingRoom < Qt::Widget

  include TchatServer

  slots :send_msg, 'send_nb_j_max(int)', 'char_select(int)', :dist_select, :set_dist_options, :disp_char_selected, :set_default, :exit

  def initialize role, game_server, parent=nil
    super(parent)
    @role = role
    @game_server = game_server

    @wait_room_ui = Ui::WaitingRoom.new
    @wait_room_ui.setup_ui self

    if @role == "client"
      for checkbox in findChildren Qt::CheckBox
        checkbox.setEnabled false
      end
      for spinbox in findChildren Qt::SpinBox
        spinbox.setEnabled false
      end
      @wait_room_ui.button_default.hide
    end

    @wait_room_ui.pseudo.text = "Invité(e)"
    @wait_room_ui.nb_max_players.value = @game_server['nb_j_max']
    set_default
    set_char_list

    # On démarre le serveur de tchat si on est hébergeur
    if @role == "server"
      start_tchat_server
    else
      @wait_room_ui.tchat.append "<em>Connexion à <strong>#{@game_server['name']}</strong> en cours...</em>"
    end

    # On établit une connexion cliente avec le tchat
    start_tchat_socket

    # On établit une connexion cliente avec le serveur de jeu
    start_game_server_socket

    connections
  end

  def connections
    connect @wait_room_ui.message, SIGNAL(:returnPressed), SLOT(:send_msg)
    connect @wait_room_ui.send, SIGNAL(:clicked), SLOT(:send_msg)
    connect @wait_room_ui.nb_max_players, SIGNAL('valueChanged(int)'), SLOT('send_nb_j_max(int)') if @role == "server"
    connect @wait_room_ui.button_dist_deck, SIGNAL(:clicked), SLOT(:dist_select)
    connect @wait_room_ui.button_default, SIGNAL(:clicked), SLOT(:set_default)
    connect @wait_room_ui.leave, SIGNAL(:clicked), SLOT(:close)
  end

  # Démarre le serveur de tchat
  def start_tchat_server
    @wait_room_ui.tchat.append "<em>Démarrage du Serveur de Tchat en cours...</em>"
    @th_tchat = Thread.new do
      EventMachine::run do
        host = '0.0.0.0'
        port = 2001
        EventMachine::start_server host, port, TchatServer
        @wait = true
        puts "Serveur de Tchat démarré sur #{host}:#{port}..."
      end
    end
    # On patiente jusqu'à que la connexion soit établie
    i = 1
    until @wait
      @wait_room_ui.tchat.append "<em>#{i}...</em>"
      sleep 1
      i += 1
    end
  end

  # Ouvre un socket client avec le serveur de tchat
  def start_tchat_socket
    # Ouverture d'un socket avec le Serveur de Tchat
    @tchat_socket = TCPSocket.open @game_server[:host], 2001
    @wait_room_ui.tchat.append "<em>Connexion réussie !</em>\n"

    # Désolidarisation de la récupération des messages du tchat du reste de l'application'
    @th_msg = Thread.new do
      while !(@tchat_socket.closed?) and (server_msg = @tchat_socket.gets)
        # Affichage des messages du Serveur
        @wait_room_ui.tchat.append server_msg.strip
      end
    end
  end

  # Ouvre un socket client avec le serveur de jeu
  def start_game_server_socket
    begin
      @game_socket = TCPSocket.open @game_server[:host], 2000
      @th_game = Thread.new do
        while !(@game_socket.closed?) and (action = @game_socket.gets)
          puts "ACTION DU SERVEUR : #{action}"
          if @role == "server"
            if action == /!add_player!/

            end
          else
            case action
              when /!refresh!/
                if action.strip.split('!').length > 4
                  refresh action.strip.split('!')[2], action.strip.split('!')[3..-1]
                else
                  refresh action.strip.split('!')[2], action.strip.split('!')[3].to_i
                end
                puts action
              else
                puts action
            end
          end
        end
      end
    rescue Exception => e
      puts "Erreur : #{e.message}"
      puts "Backtrace :"
      for msg in e.backtrace
        puts " - #{msg}"
      end
      Qt::MessageBox.critical self, "Critical", "Echec de connexion au serveur de jeu !\n#{$!}"
      exit
    end
  end

  def refresh name, value
    case name
      when /!check!/
        item = findChild Qt::CheckBox, name
        value=="true" ? item.setChecked(true) : item.setChecked(false)
      when /!portrait!/
        item = findChild Portrait, name
        @char_list[name.split('_')[1].to_i] = value
        item.set_portrait value
      when /!cards!/
        @dist_list = value
      else
        item = findChild Qt::SpinBox, name
        item.setValue value
    end
  end

  # Envoit un message du client au serveur de tchat
  def send_msg
    pseudo = @wait_room_ui.pseudo.text
    msg = @wait_room_ui.message.text
    @tchat_socket.puts "<b>#{pseudo}</b> : #{msg}"
    @wait_room_ui.message.clear
  end

  def add_player id, name
    layout_player = Qt::HBoxLayout.new(@wait_room_ui.verticalLayoutWidget)
    layout_player.objectName = "layout_player_#{id}"
    layout_player.setContentsMargins(0, 0, 0, 0)
    avatar
  end

  # Envoit le nombre maximal de joueurs au Serveur de Jeu
  def send_nb_j_max nb_j_max
    if nb_j_max < 4
      @wait_room_ui.char_list[-1].hide
    else
      @wait_room_ui.char_list[-1].show
    end
    if nb_j_max == 8 and @wait_room_ui.char_list[-1].char_name == "nobody"
      @wait_room_ui.char_list[-1].set_portrait "reine"
      @char_list[-1] = "reine"
    end
    puts "!refresh!nb_max_players!#{nb_j_max}"
    start_game_server_socket if @game_socket.closed?
    @game_socket.puts "!refresh!nb_max_players!#{nb_j_max}"
  end

  def set_char_list
    i = 0
    for char_ui in @wait_room_ui.char_list
      char_ui.set_portrait @char_list[i]
      connect @wait_room_ui.char_list[i], SIGNAL('clicked(int)'), SLOT('char_select(int)') if @role == "server"
      i += 1
    end
  end

  def char_select index
    @char_select = CharSelect.new index, @char_list[index], @wait_room_ui.nb_max_players.value
    @char_select.setModal true
    @char_select.show
    connect @char_select, SIGNAL(:accepted), SLOT(:disp_char_selected)
  end

  def disp_char_selected
    @char_list[@char_select.index_char] = @char_select.selected_char
    @wait_room_ui.char_list[@char_select.index_char].set_portrait @char_select.selected_char
    set_char_options
  end

  def set_char_options
    for char in @char_list
      case char
        when "assassin"
          @wait_room_ui.check_end_killed_char.show
          @wait_room_ui.check_total_bewitch.hide
          @wait_room_ui.check_build_bewitch.hide
        when "sorciere"
          @wait_room_ui.check_end_killed_char.hide
          @wait_room_ui.check_total_bewitch.show
          @wait_room_ui.check_build_bewitch.show
        when "voleur"
          @wait_room_ui.check_end_owner.hide
        when "bailli"
          @wait_room_ui.check_end_owner.show
        when "magicien"
          @wait_room_ui.check_sorcerer_revealed_cards.hide
        when "sorcier"
          @wait_room_ui.check_sorcerer_revealed_cards.show
        when "roi"
          @wait_room_ui.check_oblige_to_enthrone.hide
        when "empereur"
          @wait_room_ui.check_oblige_to_enthrone.show
        when "eveque"
          @wait_room_ui.check_all_wealthy_pay.hide
        when "abbe"
          @wait_room_ui.check_all_wealthy_pay.show
        when "condottiere"
          @wait_room_ui.check_ending_turn_mercenary.show
          @wait_room_ui.check_pay_diff.hide
        when "diplomate"
          @wait_room_ui.check_ending_turn_mercenary.hide
          @wait_room_ui.check_pay_diff.show
        else
          # do nothing
      end
      if @char_list[0]=="assassin" and @char_list[3]=="roi"
        @wait_room_ui.check_kill_crown.show
        @wait_room_ui.check_killing_king.show
      else
        @wait_room_ui.check_kill_crown.hide
        @wait_room_ui.check_killing_king.hide
      end
      (@char_list[0]=="sorciere" and @char_list[3]=="roi") ? @wait_room_ui.check_bewitch_crown.show : @wait_room_ui.check_bewitch_crown.hide
    end
  end

  def dist_select
    @dist_select = DistSelect.new @dist_list, @role
    @dist_select.setModal true
    @dist_select.show
    connect @dist_select, SIGNAL(:accepted), SLOT(:set_dist_options)
  end

  def set_dist_options
    @dist_list = @dist_select.selected_dist
    @dist_list.find {|dist| dist == "tresor_imperial"} ? @wait_room_ui.check_treasure_add.show : @wait_room_ui.check_treasure_add.hide
    @dist_list.find {|dist| dist == "poudriere"} ? @wait_room_ui.check_end_powder.show : @wait_room_ui.check_end_powder.hide
  end

  def set_default
    @wait_room_ui.nb_cards.value = 4
    @wait_room_ui.nb_gold.value = 2
    @wait_room_ui.nb_dist.value = 8

    if @wait_room_ui.nb_max_players.value < 8
      @char_list = %w(assassin voleur magicien roi eveque marchand architecte condottiere nobody)
    else
      @char_list = %w(assassin voleur magicien roi eveque marchand architecte condottiere reine)
    end

    i = 0
    for char in @char_list
      @wait_room_ui.char_list[i].set_portrait char
      i += 1
    end
    set_char_options

    @dist_list = %w(cour_des_miracles donjon tresor_imperial carriere cimetiere fontaine_aux_souhaits laboratoire manufacture observatoire bibliotheque dracoport ecole_de_magie grande_muraille universite)

    @wait_room_ui.check_easy_draw.setChecked false
    @wait_room_ui.check_kill_crown.setChecked true
    @wait_room_ui.check_end_killed_char.setChecked true
    @wait_room_ui.check_total_bewitch.setChecked false
    @wait_room_ui.check_bewitch_crown.setChecked false
    @wait_room_ui.check_build_bewitch.setChecked false
    @wait_room_ui.check_end_owner.setChecked false
    @wait_room_ui.check_sorcerer_revealed_cards.setChecked true
    @wait_room_ui.check_toss_king.setChecked false
    @wait_room_ui.check_killing_king.setChecked true
    @wait_room_ui.check_oblige_to_enthrone.setChecked false
    @wait_room_ui.check_all_wealthy_pay.setChecked false
    @wait_room_ui.check_ending_turn_mercenary.setChecked false
    @wait_room_ui.check_pay_diff.setChecked true
    @wait_room_ui.check_treasure_add.setChecked false
    @wait_room_ui.check_end_powder.setChecked false
  end

  def closeEvent event
    exit ? event.accept : event.ignore
  end

  def exit
    puts "exit"
    if @role == "server"
      @th_tchat.exit
      puts "Serveur de Tchat arrêté."
      # On éteind le serveur de jeu et on supprime son référencement par le serveur central
      socket = TCPSocket.open('192.168.0.111', 2000)
      socket.puts "!shutdown!"
      socket.close
    end
    @th_msg.exit
    close()
  end

end

#app = Qt::Application.new(ARGV)
#
#Qt::TextCodec::setCodecForCStrings (Qt::TextCodec::codecForName "UTF-8")
#Qt::TextCodec::setCodecForTr (Qt::TextCodec::codecForName "UTF-8")
#Qt::TextCodec::setCodecForLocale (Qt::TextCodec::codecForName "UTF-8")
#server_param = {'id' => "plop", 'name' => "plop", 'mdp' => "", 'host' => "127.0.0.1", 'port' => 2000, 'nb_j_max' => 8}
#server = WaitingRoom.new "client", server_param
#server.show
#
#app.exec