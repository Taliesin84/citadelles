# encoding: utf-8

require 'Qt4'
require './gui/interface/dist_select_ui'
require './gui/interface/card_label'

class DistSelect < Qt::Dialog

  attr_reader :selected_dist

  slots 'set_selected_card(int)', :set_selected_dist, :uncheck_all, :check_all

  def initialize districts, user, parent=nil
    super(parent)

    @selected_dist = districts

    @districts = %w(beffroi bibliotheque carriere cimetiere cour_des_miracles donjon dracoport ecole_de_magie fabrique fontaine_aux_souhaits grande_muraille hopital hospice laboratoire manufacture musee observatoire parc phare poudriere salle_des_cartes salle_du_trone tresor_imperial universite)

    @dist_select_ui = Ui::DistSelect.new
    @dist_select_ui.setup_ui(self)

    @grid_layout_widget = Qt::Widget.new @dist_select_ui.scrollAreaWidgetContents
    @grid_layout_widget.objectName = "gridLayoutWidget"
    @grid_layout_widget.geometry = Qt::Rect.new 10, 10, 701, 527
    @grid_layout = Qt::GridLayout.new @grid_layout_widget
    @grid_layout.objectName = "gridLayout"
    @grid_layout.setContentsMargins 0, 0, 0, 0

    @check_boxes = []
    wide = 0
    i = 0
    while i < @districts.length
      card = CardLabel.new i+1, "cards/districts/prestige/", @districts[i], @grid_layout_widget
      @grid_layout.addWidget card, 0, i*2, 1, 1
      wide += 323

      connect card, SIGNAL('clicked(int)'), SLOT('set_selected_card(int)') if user == "server"

      check_box = Qt::CheckBox.new @grid_layout_widget
      check_box.objectName = "check_box_#{i+1}"
      @grid_layout.addWidget(check_box, 1, i*2, 1, 1, Qt::AlignCenter)
      check_box.setChecked true if @selected_dist.find {|dist| dist == @districts[i]}
      check_box.setEnabled false if user == "client"
      @check_boxes << check_box

      if i < @districts.length-1
        horizontal_spacer = Qt::SpacerItem.new(40, 20, Qt::SizePolicy::Expanding, Qt::SizePolicy::Minimum)
        @grid_layout.addItem horizontal_spacer, 0, 1+i*2, 1, 1
        wide += 20
      end
      i += 1
    end
    @dist_select_ui.scrollAreaWidgetContents.minimumSize = Qt::Size.new(wide+20, 521)
    @grid_layout_widget.geometry = Qt::Rect.new 10, 10, wide, 527

    connections
  end

  def connections
    connect @dist_select_ui.uncheck_all, SIGNAL(:clicked), SLOT(:uncheck_all)
    connect @dist_select_ui.check_all, SIGNAL(:clicked), SLOT(:check_all)
    connect @dist_select_ui.button_box, SIGNAL(:accepted), SLOT(:set_selected_dist)
    connect @dist_select_ui.button_box, SIGNAL(:rejected), SLOT(:reject)
  end

  def set_selected_card index
    @check_boxes[index-1].isChecked ? @check_boxes[index-1].setChecked(false) : @check_boxes[index-1].setChecked(true)
  end

  def set_selected_dist
    @selected_dist = []
    i = 0
    while i < @check_boxes.length
      @selected_dist << @districts[i] if @check_boxes[i].isChecked
      i += 1
    end
    accept
  end

  def uncheck_all
    for check_box in @check_boxes
      check_box.setChecked false
    end
  end

  def check_all
    for check_box in @check_boxes
      check_box.setChecked true
    end
  end
end
