# encoding: utf-8
require 'Qt4'
require './gui/interface/join_room_ui'
require 'socket'
require 'benchmark'

class JoinRoom < Qt::Dialog

  slots 'set_enabled(QListWidgetItem *)', :refresh, :join_game, 'join_game_item(QListWidgetItem *)'

  attr_reader :selected_server

  def initialize parent=nil
    super(parent)

    @servers = []

    @net_room_ui = Ui::JoinRoom.new
    @net_room_ui.setup_ui(self)
    @net_room_ui.join.setEnabled false
    set_games_list
    connections
  end

  def connections
    connect @net_room_ui.cancel, SIGNAL(:rejected), SLOT(:reject)
    connect @net_room_ui.gamesList, SIGNAL("itemClicked(QListWidgetItem *)"), SLOT("set_enabled(QListWidgetItem *)")
    connect @net_room_ui.refresh, SIGNAL(:clicked), SLOT(:refresh)
    # Redirection vers la salle d'attente correspondante'
    connect @net_room_ui.join, SIGNAL(:clicked), SLOT(:join_game)
    connect @net_room_ui.gamesList, SIGNAL("itemDoubleClicked(QListWidgetItem *)"), SLOT("join_game_item(QListWidgetItem *)")
  end


  # Active le bouton 'Rejoindre' quand une partie a été sélectionnée
  def set_enabled item
    @net_room_ui.join.setEnabled true
  end

  # Rafraichit la liste des Serveurs de jeu
  def refresh
    @net_room_ui.gamesList.clear
    set_games_list
  end

  def join_game_item item
    join_game
  end

  def join_game
    index_item = @net_room_ui.gamesList.currentRow
    if @servers[index_item-1]['mdp'].empty?
      @selected_server = @servers[index_item-1]
      accept
    else
      # Affichage d'une InputDialog pour insérer le mot de passe du serveur
      mdp = Qt::InputDialog.getText self, "Input Dialog", "Entrer le mot de passe :"
      #Vérification du mot de passe
      if mdp.nil? or mdp.strip != @servers[index_item-1]['mdp']
        Qt::MessageBox.warning self, "Warning", "Mot de passe incorrect !"
      elsif mdp.strip == @servers[index_item-1]['mdp']
        puts @servers[index_item-1]
        @selected_server = @servers[index_item-1]
        accept
      end
    end
  end

  # Demande au Serveur Central la liste des parties hébergées
  def set_games_list host="192.168.0.111", port=2010
    begin
      #session = TCPSocket.open host, port
      session = connect_to host, port
      session.puts "!get_games!"
      while !(session.closed?) and (server_msg = session.gets)
        if server_msg =~ /!disconnect!/
          session.close
        else
          game_item = server_msg.strip.split('|')
          game = {
              'id' => game_item[0],
              'name' => game_item[1],
              'mdp' => game_item[2],
              'host' => game_item[3],
              'port' => game_item[4],
              'nb_j_max' => game_item[5]
          }
          @servers << game
          game['mdp'].empty? ? mdp="sans" : mdp="avec"
          @net_room_ui.gamesList.add_item "#{game['id']}|#{game['name']}|#{mdp}|#{game['host']}|#{game['port']}|#{game['nb_j_max']}"
        end
      end
    rescue
      Qt::MessageBox.critical self, "Critical", "Echec de connexion au Serveur Central !\n\n#{$!}"
      self.reject
    end
  end

  # Connexion par un socket brut
  def connect_to host, port, timeout=nil
    addr = Socket.getaddrinfo host, nil
    sock = Socket.new Socket.const_get(addr[0][0]), Socket::SOCK_STREAM, 0

    if timeout
      secs = Integer(timeout)
      u_secs = Integer((timeout - secs) * 1_000_000)
      optval = [secs, u_secs].pack("l_2")
      sock.setsockopt Socket::SOL_SOCKET, Socket::SO_RCVTIMEO, optval
      sock.setsockopt Socket::SOL_SOCKET, Socket::SO_SNDTIMEO, optval
    end

    sock.connect(Socket.pack_sockaddr_in(port, addr[0][3]))
    sock
  end

end
