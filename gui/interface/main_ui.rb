# encoding: utf-8

=begin
** Form generated from reading ui file 'main.ui'
**
** Created: jeu. juin 21 19:59:52 2012
**      by: Qt User Interface Compiler version 4.8.0
**
** WARNING! All changes made in this file will be lost when recompiling ui file!
=end

class Ui_MainWindow
  attr_reader :stats
  attr_reader :options
  attr_reader :design
  attr_reader :quit
  attr_reader :help
  attr_reader :about
  attr_reader :net
  attr_reader :lan
  attr_reader :join
  attr_reader :create_local
  attr_reader :create_network
  attr_reader :central_widget
  attr_reader :menu_bar
  attr_reader :menu_game
  attr_reader :menu_create
  attr_reader :menu_infos
  attr_reader :status_bar

  def setupUi(mainWindow)
    if mainWindow.objectName.nil?
      mainWindow.objectName = "mainWindow"
    end
    mainWindow.resize(1450, 860)
    mainWindow.minimumSize = Qt::Size.new(1450, 860)
    mainWindow.maximumSize = Qt::Size.new(1450, 860)
    main_icon = Qt::Icon.new
    main_icon.addPixmap Qt::Pixmap.new('./img/icons/citadel.png'), Qt::Icon::Normal, Qt::Icon::Off
    mainWindow.windowIcon = main_icon
    @stats = Qt::Action.new(mainWindow)
    @stats.objectName = "stats"
    stats_icon = Qt::Icon.new
    stats_icon.addPixmap Qt::Pixmap.new('./img/icons/toolbar/fav.png'), Qt::Icon::Normal, Qt::Icon::Off
    @stats.icon = stats_icon
    @options = Qt::Action.new(mainWindow)
    @options.objectName = "options"
    options_icon = Qt::Icon.new
    options_icon.addPixmap Qt::Pixmap.new('./img/icons/toolbar/configure.png'), Qt::Icon::Normal, Qt::Icon::Off
    @options.icon = options_icon
    @design = Qt::Action.new(mainWindow)
    @design.objectName = "design"
    design_icon = Qt::Icon.new
    design_icon.addPixmap Qt::Pixmap.new('./img/icons/toolbar/paint.png'), Qt::Icon::Normal, Qt::Icon::Off
    @design.icon = design_icon
    @quit = Qt::Action.new(mainWindow)
    @quit.objectName = "quit"
    quit_icon = Qt::Icon.new
    quit_icon.addPixmap Qt::Pixmap.new('./img/icons/toolbar/close.png'), Qt::Icon::Normal, Qt::Icon::Off
    @quit.icon = quit_icon
    @help = Qt::Action.new(mainWindow)
    @help.objectName = "help"
    help_icon = Qt::Icon.new
    help_icon.addPixmap Qt::Pixmap.new('./img/icons/toolbar/help.png'), Qt::Icon::Normal, Qt::Icon::Off
    @help.icon = help_icon
    @about = Qt::Action.new(mainWindow)
    @about.objectName = "about"
    about_icon = Qt::Icon.new
    about_icon.addPixmap Qt::Pixmap.new('./img/icons/toolbar/infos.png'), Qt::Icon::Normal, Qt::Icon::Off
    @about.icon = about_icon
    @join = Qt::Action.new(mainWindow)
    @join.objectName = "join"
    join_icon = Qt::Icon.new
    join_icon.addPixmap Qt::Pixmap.new('./img/icons/toolbar/join.png'), Qt::Icon::Normal, Qt::Icon::Off
    @join.icon = join_icon
    @create_local = Qt::Action.new(mainWindow)
    @create_local.objectName = "create_local"
    create_local_icon = Qt::Icon.new
    create_local_icon.addPixmap Qt::Pixmap.new('./img/icons/toolbar/home.png'), Qt::Icon::Normal, Qt::Icon::Off
    @create_local.icon = create_local_icon
    @create_network = Qt::Action.new(mainWindow)
    @create_network.objectName = "create_network"
    create_network_icon = Qt::Icon.new
    create_network_icon.addPixmap Qt::Pixmap.new('./img/icons/toolbar/network.png'), Qt::Icon::Normal, Qt::Icon::Off
    @create_network.icon = create_network_icon
    @central_widget = Qt::Widget.new(mainWindow)
    @central_widget.objectName = "central_widget"
    mainWindow.centralWidget = @central_widget
    mainWindow.centralWidget.setStyleSheet("background-image: url('./img/intro.png'); background-repeat:no-repeat; background-position:center center;")
    @menu_bar = Qt::MenuBar.new(mainWindow)
    @menu_bar.objectName = "menu_bar"
    @menu_bar.geometry = Qt::Rect.new(0, 0, 1450, 21)
    @menu_game = Qt::Menu.new(@menu_bar)
    @menu_game.objectName = "menu_game"
    @menu_create = Qt::Menu.new(@menu_game)
    @menu_create.objectName = "menu_create"
    menu_create_icon = Qt::Icon.new
    menu_create_icon.addPixmap Qt::Pixmap.new('./img/icons/toolbar/add.png'), Qt::Icon::Normal, Qt::Icon::Off
    @menu_create.icon = menu_create_icon
    @menu_infos = Qt::Menu.new(@menu_bar)
    @menu_infos.objectName = "menu_infos"
    mainWindow.setMenuBar(@menu_bar)
    @status_bar = Qt::StatusBar.new(mainWindow)
    @status_bar.objectName = "status_bar"
    mainWindow.statusBar = @status_bar

    @menu_bar.addAction(@menu_game.menuAction())
    @menu_bar.addAction(@menu_infos.menuAction())
    @menu_game.addAction(@menu_create.menuAction())
    @menu_game.addAction(@join)
    @menu_game.addSeparator()
    @menu_game.addAction(@stats)
    @menu_game.addAction(@options)
    @menu_game.addAction(@design)
    @menu_game.addSeparator()
    @menu_game.addAction(@quit)
    @menu_create.addAction(@create_local)
    @menu_create.addAction(@create_network)
    @menu_infos.addAction(@help)
    @menu_infos.addAction(@about)

    retranslateUi(mainWindow)

    Qt::MetaObject.connectSlotsByName(mainWindow)
  end # setupUi

  def setup_ui(mainWindow)
    setupUi(mainWindow)
  end

  def retranslateUi(mainWindow)
    mainWindow.windowTitle = Qt::Application.translate("MainWindow", "Citadelles", nil, Qt::Application::UnicodeUTF8)
    @stats.text = Qt::Application.translate("MainWindow", "Statistiques", nil, Qt::Application::UnicodeUTF8)
    @stats.shortcut = Qt::Application.translate("MainWindow", "F5", nil, Qt::Application::UnicodeUTF8)
    @options.text = Qt::Application.translate("MainWindow", "Options", nil, Qt::Application::UnicodeUTF8)
    @options.shortcut = Qt::Application.translate("MainWindow", "F6", nil, Qt::Application::UnicodeUTF8)
    @design.text = Qt::Application.translate("MainWindow", "Apparences", nil, Qt::Application::UnicodeUTF8)
    @design.shortcut = Qt::Application.translate("MainWindow", "F7", nil, Qt::Application::UnicodeUTF8)
    @quit.text = Qt::Application.translate("MainWindow", "Quitter", nil, Qt::Application::UnicodeUTF8)
    @help.text = Qt::Application.translate("MainWindow", "Aide", nil, Qt::Application::UnicodeUTF8)
    @help.shortcut = Qt::Application.translate("MainWindow", "F1", nil, Qt::Application::UnicodeUTF8)
    @about.text = Qt::Application.translate("MainWindow", "A propos", nil, Qt::Application::UnicodeUTF8)
    @about.shortcut = Qt::Application.translate("MainWindow", "F12", nil, Qt::Application::UnicodeUTF8)
    @join.text = Qt::Application.translate("MainWindow", "Rejoindre Partie", nil, Qt::Application::UnicodeUTF8)
    @join.shortcut = Qt::Application.translate("MainWindow", "F4", nil, Qt::Application::UnicodeUTF8)
    @create_local.text = Qt::Application.translate("MainWindow", "Locale", nil, Qt::Application::UnicodeUTF8)
    @create_local.shortcut = Qt::Application.translate("MainWindow", "F2", nil, Qt::Application::UnicodeUTF8)
    @create_network.text = Qt::Application.translate("MainWindow", "Internet", nil, Qt::Application::UnicodeUTF8)
    @create_network.shortcut = Qt::Application.translate("MainWindow", "F3", nil, Qt::Application::UnicodeUTF8)
    @menu_game.title = Qt::Application.translate("MainWindow", "Partie", nil, Qt::Application::UnicodeUTF8)
    @menu_create.title = Qt::Application.translate("MainWindow", "Créer Partie", nil, Qt::Application::UnicodeUTF8)
    @menu_infos.title = Qt::Application.translate("MainWindow", "?", nil, Qt::Application::UnicodeUTF8)
  end # retranslateUi

  def retranslate_ui(mainWindow)
    retranslateUi(mainWindow)
  end

end

module Ui
  class MainWindow < Ui_MainWindow
  end
end  # module Ui

