# encoding: utf-8

require './gui/interface/main_ui'
require './gui/interface/create_local_game'
require './gui/interface/create_network_game'
require './gui/interface/join_room'
require './gui/interface/waiting_room'
require './gui/game/table'

class Main < Qt::MainWindow

  slots :create_local_game, :start_local_game, :create_network_game, :join_room, :wait_room, :design_dial, :about, :exit

  def initialize parent=nil
    super(parent)

    #On oblige l'application à utiliser le charset UTF-8
    Qt::TextCodec::setCodecForCStrings (Qt::TextCodec::codecForName "UTF-8")
    Qt::TextCodec::setCodecForTr (Qt::TextCodec::codecForName "UTF-8")
    Qt::TextCodec::setCodecForLocale (Qt::TextCodec::codecForName "UTF-8")

    @main_ui = Ui::MainWindow.new
    @main_ui.setup_ui self

    connections
  end

  # Etablissement de toutes les intéractions avec l'interface
  def connections
    #connect @main_ui.quit, SIGNAL(:triggered), $qApp, SLOT(:closeAllWindows)
    connect @main_ui.quit, SIGNAL(:triggered), SLOT(:exit)
    connect @main_ui.create_network, SIGNAL(:triggered), SLOT(:create_network_game)
    connect @main_ui.create_local, SIGNAL(:triggered), SLOT(:create_local_game)
    connect @main_ui.join, SIGNAL(:triggered), SLOT(:join_room)
    connect @main_ui.design, SIGNAL(:triggered), SLOT(:design_dial)
    connect @main_ui.about, SIGNAL(:triggered), SLOT(:about)
  end

  # Ouverture de la fenêtre de dialogue pour créer une partie locale
  def create_local_game
    @create_local_game = CreateLocalGame.new
    @create_local_game.setModal true
    @create_local_game.show

    connect @create_local_game, SIGNAL(:accepted), SLOT(:start_local_game)
  end

  def start_local_game
    font = 'fond1.png'
    table = Table.new font,@create_local_game.players ,@create_local_game.characters, @create_local_game.districts, @create_local_game.parameters
    view = Qt::GraphicsView.new table
    view.setFixedSize 1450, 820
    view.setRenderHints(Qt::Painter::Antialiasing | Qt::Painter::SmoothPixmapTransform)
    view.show
    setCentralWidget view
  end

  # Ouverture de la fenêtre de dialogue pour créer une partie réseau
  def create_network_game
    @create_network_game = CreateNetworkGame.new
    @create_network_game.setModal true
    @create_network_game.show

    connect @create_network_game, SIGNAL(:accepted), SLOT(:wait_room)
  end

  # Ouverture de la fenêtre de dialogue pour rejoindre une partie
  def join_room
    begin
      @join_room = JoinRoom.new
      @join_room.setModal true
      @join_room.show

      connect @join_room, SIGNAL(:accepted), SLOT(:wait_room)
    rescue Errno::ECONNREFUSED
      Qt::MessageBox.critical self, "Critical", "Connexion échouée !"
    end
  end

  # Ouverture de la fenêtre de dialogue de la salle d'attente de la partie
  def wait_room
    begin
      if @join_room
        @game_server = @join_room.selected_server
        @wait_room = WaitingRoom.new "client", @join_room.selected_server
      elsif @create_network_game
        @game_server = @create_network_game.created_server
        @wait_room = WaitingRoom.new "server", @create_network_game.created_server
      end
      @wait_room.show
        #setCentralWidget @wait_room
    rescue Exception => e
      puts "Erreur : #{e.message}"
      puts "Backtrace :"
      for msg in e.backtrace
        puts " - #{msg}"
      end
      # On vérifie si un serveur a été créé
      if @create_network_game
        # On éteind le serveur de jeu et on supprime son référencement par le serveur central
        puts "#{@game_server[:host]}:#{@game_server[:port]}"
        socket = TCPSocket.open(@game_server[:host], @game_server[:port])
        socket.puts "!shutdown!"
        socket.close
      end
      Qt::MessageBox.critical self, "Critical", "Echec d'accès à la salle d'attente !"
    end
  end

  # Ouverture de la fenêtre de dialogue pour régler l'apparence du jeu
  def design_dial
    @main_ui.central_widget.setStyleSheet "background-image: url('./img/fond1-1024.jpg'); background-repeat:no-repeat;"
  end

  # Override de la fonction de fermeture d'une fenêtre pour que celle-ci ferme toutes ses filles
  def closeEvent event
    exit ? event.accept : event.ignore
  end

  def exit
    exit_msg_box = Qt::MessageBox.question self, "Quitter Citadelles", "Etes-vous sûr(e) de vouloir quitter" , Qt::MessageBox::Yes | Qt::MessageBox::No, Qt::MessageBox::Yes
    if exit_msg_box == Qt::MessageBox::Yes
      close_all
    else
      false
    end
  end

  # Permet de gérer la fermeture de l'application
  def close_all
    #todo Informer le serveur central de la clôture du serveur de jeu s'il existe
    $qApp.quit()
  end

  def about
    Qt::MessageBox.about self, "A Propos de Citadelles",
                         "<center><h1>Citadelles</h1> <i>version 1.0</i></center><br>" +
                             "<center>Un jeu de cartes de Bruno Faidutti</center><br>" +
                             "<center>Design et programmation par Tali3syn</center>" +
                             "<center><i>(Ruby 1.9.3p194 / Qt 4.8)</i></center>"
  end

end