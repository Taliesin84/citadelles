# encoding: utf-8

=begin
** Form generated from reading ui file 'network_room.ui'
**
** Created: Tue Apr 3 22:21:36 2012
**      by: Qt User Interface Compiler version 4.8.0
**
** WARNING! All changes made in this file will be lost when recompiling ui file!
=end

class Ui_JoinRoom
  attr_reader :verticalLayoutWidget
  attr_reader :verticalLayout
  attr_reader :refresh
  attr_reader :verticalSpacer
  attr_reader :insertIP
  attr_reader :join
  attr_reader :cancel
  attr_reader :gamesList

  def setupUi(joinRoom)
    if joinRoom.objectName.nil?
      joinRoom.objectName = "joinRoom"
    end
    joinRoom.resize(500, 300)
    joinRoom.minimumSize = Qt::Size.new(500, 300)
    joinRoom.maximumSize = Qt::Size.new(500, 300)
    window_icon = Qt::Icon.new
    window_icon.addPixmap Qt::Pixmap.new('./img/icons/toolbar/join.png'), Qt::Icon::Normal, Qt::Icon::Off
    joinRoom.windowIcon = window_icon
    joinRoom.cursor = Qt::Cursor.new(Qt::PointingHandCursor)
    @verticalLayoutWidget = Qt::Widget.new(joinRoom)
    @verticalLayoutWidget.objectName = "verticalLayoutWidget"
    @verticalLayoutWidget.geometry = Qt::Rect.new(370, 10, 124, 271)
    @verticalLayout = Qt::VBoxLayout.new(@verticalLayoutWidget)
    @verticalLayout.objectName = "verticalLayout"
    @verticalLayout.setContentsMargins(0, 0, 0, 0)
    @refresh = Qt::PushButton.new(@verticalLayoutWidget)
    @refresh.objectName = "refresh"
    @sizePolicy = Qt::SizePolicy.new(Qt::SizePolicy::Fixed, Qt::SizePolicy::Fixed)
    @sizePolicy.setHorizontalStretch(0)
    @sizePolicy.setVerticalStretch(0)
    @sizePolicy.heightForWidth = @refresh.sizePolicy.hasHeightForWidth
    @refresh.sizePolicy = @sizePolicy
    icon = Qt::Icon.new
    icon.addPixmap(Qt::Pixmap.new("./img/refresh.png"), Qt::Icon::Normal, Qt::Icon::Off)
    @refresh.icon = icon
    @refresh.iconSize = Qt::Size.new(48, 48)
    @refresh.flat = false

    @verticalLayout.addWidget(@refresh)

    @verticalSpacer = Qt::SpacerItem.new(20, 40, Qt::SizePolicy::Minimum, Qt::SizePolicy::Expanding)

    @verticalLayout.addItem(@verticalSpacer)

    @insertIP = Qt::PushButton.new(@verticalLayoutWidget)
    @insertIP.objectName = "insertIP"

    @verticalLayout.addWidget(@insertIP)

    @join = Qt::PushButton.new(@verticalLayoutWidget)
    @join.objectName = "join"
    icon1 = Qt::Icon.new
    icon1.addPixmap(Qt::Pixmap.new("./img/door_closed.png"), Qt::Icon::Normal, Qt::Icon::Off)
    icon1.addPixmap(Qt::Pixmap.new("./img/door_opened.png"), Qt::Icon::Normal, Qt::Icon::On)
    @join.icon = icon1
    @join.iconSize = Qt::Size.new(32, 32)

    @verticalLayout.addWidget(@join)

    @cancel = Qt::DialogButtonBox.new(@verticalLayoutWidget)
    @cancel.objectName = "cancel"
    @sizePolicy1 = Qt::SizePolicy.new(Qt::SizePolicy::Expanding, Qt::SizePolicy::Fixed)
    @sizePolicy1.setHorizontalStretch(0)
    @sizePolicy1.setVerticalStretch(0)
    @sizePolicy1.heightForWidth = @cancel.sizePolicy.hasHeightForWidth
    @cancel.sizePolicy = @sizePolicy1
    @cancel.standardButtons = Qt::DialogButtonBox::Cancel
    @cancel.centerButtons = true

    @verticalLayout.addWidget(@cancel)

    @gamesList = Qt::ListWidget.new(joinRoom)
    @gamesList.objectName = "gamesList"
    @gamesList.geometry = Qt::Rect.new(10, 10, 351, 281)
    @gamesList.dragDropOverwriteMode = false

    retranslateUi(joinRoom)

    Qt::MetaObject.connectSlotsByName(joinRoom)
  end # setupUi

  def setup_ui(joinRoom)
    setupUi(joinRoom)
  end

  def retranslateUi(joinRoom)
    joinRoom.windowTitle = Qt::Application.translate("NetworkRoom", "Salle de Jeux", nil, Qt::Application::UnicodeUTF8)
    @refresh.text = ''
    @refresh.shortcut = Qt::Application.translate("NetworkRoom", "F5", nil, Qt::Application::UnicodeUTF8)
    @insertIP.text = Qt::Application.translate("NetworkRoom", "Insérer IP", nil, Qt::Application::UnicodeUTF8)
    @join.text = Qt::Application.translate("NetworkRoom", "Rejoindre", nil, Qt::Application::UnicodeUTF8)
  end # retranslateUi

  def retranslate_ui(joinRoom)
    retranslateUi(joinRoom)
  end

end

module Ui
  class JoinRoom < Ui_JoinRoom
  end
end  # module Ui