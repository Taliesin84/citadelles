# encoding: utf-8

=begin
** Form generated from reading ui file 'char_select.ui'
**
** Created: Thu Jul 5 15:03:11 2012
**      by: Qt User Interface Compiler version 4.8.1
**
** WARNING! All changes made in this file will be lost when recompiling ui file!
=end

class Ui_CharSelect
  attr_reader :scrollArea
  attr_reader :scrollAreaWidgetContents
  attr_reader :button_box

  def setupUi(charSelect)
    if charSelect.objectName.nil?
      charSelect.objectName = "charSelect"
    end
    charSelect.resize(717, 582)
    charSelect.minimumSize = Qt::Size.new(717, 620)
    charSelect.maximumSize = Qt::Size.new(717, 620)
    charSelect.modal = true
    @scrollArea = Qt::ScrollArea.new(charSelect)
    @scrollArea.objectName = "scrollArea"
    @scrollArea.geometry = Qt::Rect.new(10, 10, 701, 561)
    @scrollArea.widgetResizable = true
    @scrollAreaWidgetContents = Qt::Widget.new()
    @scrollAreaWidgetContents.objectName = "scrollAreaWidgetContents"
    @scrollAreaWidgetContents.geometry = Qt::Rect.new(0, 0, 701, 555)
    @scrollArea.setWidget(@scrollAreaWidgetContents)

    @button_box = Qt::DialogButtonBox.new(charSelect)
    @button_box.objectName = "button_box"
    @button_box.geometry = Qt::Rect.new(10, 580, 701, 32)
    @button_box.orientation = Qt::Horizontal
    @button_box.standardButtons = Qt::DialogButtonBox::Cancel|Qt::DialogButtonBox::Ok
    @button_box.centerButtons = true

    retranslateUi(charSelect)
    Qt::Object.connect(@button_box, SIGNAL('accepted()'), charSelect, SLOT('accept()'))
    Qt::Object.connect(@button_box, SIGNAL('rejected()'), charSelect, SLOT('reject()'))

    Qt::MetaObject.connectSlotsByName(charSelect)
  end # setupUi

  def setup_ui(charSelect)
    setupUi(charSelect)
  end

  def retranslateUi(charSelect)
    charSelect.windowTitle = Qt::Application.translate("charSelect", "Choix des cartes 'Personnage'", nil, Qt::Application::UnicodeUTF8)
  end # retranslateUi

  def retranslate_ui(charSelect)
    retranslateUi(charSelect)
  end

end

module Ui
  class CharSelect < Ui_CharSelect
  end
end  # module Ui

