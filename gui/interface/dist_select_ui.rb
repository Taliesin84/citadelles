# encoding: utf-8

=begin
** Form generated from reading ui file 'dist_select.ui'
**
** Created: sam. août 25 13:37:00 2012
**      by: Qt User Interface Compiler version 4.8.1
**
** WARNING! All changes made in this file will be lost when recompiling ui file!
=end

class Ui_DistSelect
    attr_reader :button_box
    attr_reader :scrollArea
    attr_reader :scrollAreaWidgetContents
    attr_reader :gridLayoutWidget
    attr_reader :gridLayout
    attr_reader :horizontalLayoutWidget
    attr_reader :horizontalLayout
    attr_reader :uncheck_all
    attr_reader :check_all
    attr_reader :line

    def setupUi(distSelect)
    if distSelect.objectName.nil?
        distSelect.objectName = "distSelect"
    end
    distSelect.resize(1000, 640)
    distSelect.minimumSize = Qt::Size.new(1000, 640)
    distSelect.maximumSize = Qt::Size.new(1000, 640)
    distSelect.modal = true
    @button_box = Qt::DialogButtonBox.new(distSelect)
    @button_box.objectName = "button_box"
    @button_box.geometry = Qt::Rect.new(10, 610, 981, 32)
    @button_box.orientation = Qt::Horizontal
    @button_box.standardButtons = Qt::DialogButtonBox::Cancel|Qt::DialogButtonBox::Ok
    @button_box.centerButtons = true
    @scrollArea = Qt::ScrollArea.new(distSelect)
    @scrollArea.objectName = "scrollArea"
    @scrollArea.geometry = Qt::Rect.new(10, 10, 981, 561)
    @scrollArea.widgetResizable = true
    @scrollAreaWidgetContents = Qt::Widget.new()
    @scrollAreaWidgetContents.objectName = "scrollAreaWidgetContents"
    @scrollAreaWidgetContents.geometry = Qt::Rect.new(0, 0, 975, 555)
    @gridLayoutWidget = Qt::Widget.new(@scrollAreaWidgetContents)
    @gridLayoutWidget.objectName = "gridLayoutWidget"
    @gridLayoutWidget.geometry = Qt::Rect.new(20, 10, 941, 531)
    @gridLayout = Qt::GridLayout.new(@gridLayoutWidget)
    @gridLayout.objectName = "gridLayout"
    @gridLayout.setContentsMargins(0, 0, 0, 0)
    @scrollArea.setWidget(@scrollAreaWidgetContents)
    @horizontalLayoutWidget = Qt::Widget.new(distSelect)
    @horizontalLayoutWidget.objectName = "horizontalLayoutWidget"
    @horizontalLayoutWidget.geometry = Qt::Rect.new(10, 570, 981, 41)
    @horizontalLayout = Qt::HBoxLayout.new(@horizontalLayoutWidget)
    @horizontalLayout.objectName = "horizontalLayout"
    @horizontalLayout.setContentsMargins(0, 0, 0, 0)
    @uncheck_all = Qt::PushButton.new(@horizontalLayoutWidget)
    @uncheck_all.objectName = "uncheck_all"
    @sizePolicy = Qt::SizePolicy.new(Qt::SizePolicy::Fixed, Qt::SizePolicy::Fixed)
    @sizePolicy.setHorizontalStretch(0)
    @sizePolicy.setVerticalStretch(0)
    @sizePolicy.heightForWidth = @uncheck_all.sizePolicy.hasHeightForWidth
    @uncheck_all.sizePolicy = @sizePolicy

    @horizontalLayout.addWidget(@uncheck_all)

    @check_all = Qt::PushButton.new(@horizontalLayoutWidget)
    @check_all.objectName = "check_all"
    @sizePolicy.heightForWidth = @check_all.sizePolicy.hasHeightForWidth
    @check_all.sizePolicy = @sizePolicy

    @horizontalLayout.addWidget(@check_all)

    @line = Qt::Frame.new(distSelect)
    @line.objectName = "line"
    @line.geometry = Qt::Rect.new(10, 600, 981, 16)
    @line.setFrameShape(Qt::Frame::HLine)
    @line.setFrameShadow(Qt::Frame::Sunken)

    retranslateUi(distSelect)
    Qt::Object.connect(@button_box, SIGNAL('accepted()'), distSelect, SLOT('accept()'))
    Qt::Object.connect(@button_box, SIGNAL('rejected()'), distSelect, SLOT('reject()'))

    Qt::MetaObject.connectSlotsByName(distSelect)
    end # setupUi

    def setup_ui(distSelect)
        setupUi(distSelect)
    end

    def retranslateUi(distSelect)
    distSelect.windowTitle = Qt::Application.translate("distSelect", "Choix des cartes 'Quartier' à insérer dans le deck de jeu", nil, Qt::Application::UnicodeUTF8)
    @uncheck_all.text = Qt::Application.translate("distSelect", "Tout décocher", nil, Qt::Application::UnicodeUTF8)
    @check_all.text = Qt::Application.translate("distSelect", "Tout cocher", nil, Qt::Application::UnicodeUTF8)
    end # retranslateUi

    def retranslate_ui(distSelect)
        retranslateUi(distSelect)
    end

end

module Ui
    class DistSelect < Ui_DistSelect
    end
end  # module Ui