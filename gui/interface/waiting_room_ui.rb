# encoding: utf-8

require './gui/interface/portrait'

=begin
** Form generated from reading ui file 'untitled.ui'
**
** Created: sam. juil. 7 14:26:06 2012
**      by: Qt User Interface Compiler version 4.8.1
**
** WARNING! All changes made in this file will be lost when recompiling ui file!
=end

class Ui_WaitingRoom
    attr_reader :horizontalLayoutWidget
    attr_reader :layout_validation
    attr_reader :message
    attr_reader :send
    attr_reader :horizontalSpacer
    attr_reader :leave
    attr_reader :grp_box_characters
    attr_reader :horizontalLayoutWidget_2
    attr_reader :layout_characters
    attr_reader :char_list
    attr_reader :line
    attr_reader :tchat
    attr_reader :grp_box_players
    attr_reader :verticalLayoutWidget
    attr_reader :layout_players_list
    attr_reader :tab_widget_param
    attr_reader :tab_game_parameters
    attr_reader :gridLayoutWidget
    attr_reader :layout_game_parameters
    attr_reader :lbl_nb_max_players
    attr_reader :lbl_nb_gold
    attr_reader :nb_cards
    attr_reader :lbl_nb_cards
    attr_reader :nb_max_players
    attr_reader :nb_gold
    attr_reader :nb_dist
    attr_reader :lbl_nb_dist
    attr_reader :button_default
    attr_reader :button_dist_deck
    attr_reader :tab_options
    attr_reader :verticalLayoutWidget_2
    attr_reader :layout_options
    attr_reader :check_easy_draw
    attr_reader :check_kill_crown
    attr_reader :check_end_killed_char
    attr_reader :check_total_bewitch
    attr_reader :check_bewitch_crown
    attr_reader :check_build_bewitch
    attr_reader :check_end_owner
    attr_reader :check_sorcerer_revealed_cards
    attr_reader :check_toss_king
    attr_reader :check_killing_king
    attr_reader :check_oblige_to_enthrone
    attr_reader :check_all_wealthy_pay
    attr_reader :check_ending_turn_mercenary
    attr_reader :check_pay_diff
    attr_reader :check_treasure_add
    attr_reader :check_end_powder
    attr_reader :horizontalLayoutWidget_3
    attr_reader :layout_player
    attr_reader :lbl_pseudo
    attr_reader :pseudo
    attr_reader :ready

    def setupUi(waitingRoom)
    if waitingRoom.objectName.nil?
        waitingRoom.objectName = "waitingRoom"
    end
    waitingRoom.resize(1000, 636)
    waitingRoom.minimumSize = Qt::Size.new(1000, 636)
    waitingRoom.maximumSize = Qt::Size.new(1000, 636)
    window_icon = Qt::Icon.new
    window_icon.addPixmap Qt::Pixmap.new('./img/icons/toolbar/network.png'), Qt::Icon::Normal, Qt::Icon::Off
    waitingRoom.windowIcon = window_icon
    @horizontalLayoutWidget = Qt::Widget.new(waitingRoom)
    @horizontalLayoutWidget.objectName = "horizontalLayoutWidget"
    @horizontalLayoutWidget.geometry = Qt::Rect.new(10, 590, 981, 41)
    @layout_validation = Qt::HBoxLayout.new(@horizontalLayoutWidget)
    @layout_validation.objectName = "layout_validation"
    @layout_validation.setContentsMargins(0, 0, 0, 0)
    @message = Qt::LineEdit.new(@horizontalLayoutWidget)
    @message.objectName = "message"
    @message.maximumSize = Qt::Size.new(341, 16777215)

    @layout_validation.addWidget(@message)

    @send = Qt::PushButton.new(@horizontalLayoutWidget)
    @send.objectName = "send"
    icon = Qt::Icon.new
    icon.addPixmap(Qt::Pixmap.new("./img/bulle.png"), Qt::Icon::Normal, Qt::Icon::Off)
    @send.icon = icon
    @send.iconSize = Qt::Size.new(25, 25)

    @layout_validation.addWidget(@send)

    @horizontalSpacer = Qt::SpacerItem.new(40, 20, Qt::SizePolicy::Expanding, Qt::SizePolicy::Minimum)

    @layout_validation.addItem(@horizontalSpacer)

    @leave = Qt::PushButton.new(@horizontalLayoutWidget)
    @leave.objectName = "leave"
    icon1 = Qt::Icon.new
    icon1.addPixmap(Qt::Pixmap.new("./img/door_closed.png"), Qt::Icon::Normal, Qt::Icon::Off)
    icon1.addPixmap(Qt::Pixmap.new("./img/door_opened.png"), Qt::Icon::Normal, Qt::Icon::On)
    @leave.icon = icon1
    @leave.iconSize = Qt::Size.new(25, 25)

    @layout_validation.addWidget(@leave)

    @grp_box_characters = Qt::GroupBox.new(waitingRoom)
    @grp_box_characters.objectName = "grp_box_characters"
    @grp_box_characters.geometry = Qt::Rect.new(360, 10, 631, 111)
    @grp_box_characters.flat = false
    @horizontalLayoutWidget_2 = Qt::Widget.new(@grp_box_characters)
    @horizontalLayoutWidget_2.objectName = "horizontalLayoutWidget_2"
    @horizontalLayoutWidget_2.geometry = Qt::Rect.new(10, 30, 631, 66)
    @layout_characters = Qt::HBoxLayout.new(@horizontalLayoutWidget_2)
    @layout_characters.objectName = "layout_characters"
    @layout_characters.setContentsMargins(0, 0, 0, 0)
    @char_list = []
    i = 0
    while i <= 8
      char = Portrait.new i, @layout_portraits
      @char_list << char
      @layout_characters.addWidget(char)
      i += 1
    end
    @line = Qt::Frame.new(waitingRoom)
    @line.objectName = "line"
    @line.geometry = Qt::Rect.new(360, 570, 631, 20)
    @line.setFrameShape(Qt::Frame::HLine)
    @line.setFrameShadow(Qt::Frame::Sunken)
    @tchat = Qt::TextEdit.new(waitingRoom)
    @tchat.objectName = "tchat"
    @tchat.geometry = Qt::Rect.new(10, 10, 340, 581)
    @tchat.readOnly = true
    @grp_box_players = Qt::GroupBox.new(waitingRoom)
    @grp_box_players.objectName = "grp_box_players"
    @grp_box_players.geometry = Qt::Rect.new(360, 130, 261, 391)
    @verticalLayoutWidget = Qt::Widget.new(@grp_box_players)
    @verticalLayoutWidget.objectName = "verticalLayoutWidget"
    @verticalLayoutWidget.geometry = Qt::Rect.new(10, 30, 241, 351)
    @layout_players_list = Qt::VBoxLayout.new(@verticalLayoutWidget)
    @layout_players_list.objectName = "layout_players_list"
    @layout_players_list.setContentsMargins(0, 0, 0, 0)
    @tab_widget_param = Qt::TabWidget.new(waitingRoom)
    @tab_widget_param.objectName = "tab_widget_param"
    @tab_widget_param.geometry = Qt::Rect.new(630, 130, 361, 441)
    @tab_game_parameters = Qt::Widget.new()
    @tab_game_parameters.objectName = "tab_game_parameters"
    @gridLayoutWidget = Qt::Widget.new(@tab_game_parameters)
    @gridLayoutWidget.objectName = "gridLayoutWidget"
    @gridLayoutWidget.geometry = Qt::Rect.new(10, 10, 331, 141)
    @layout_game_parameters = Qt::GridLayout.new(@gridLayoutWidget)
    @layout_game_parameters.objectName = "layout_game_parameters"
    @layout_game_parameters.setContentsMargins(0, 0, 0, 0)
    @lbl_nb_max_players = Qt::Label.new(@gridLayoutWidget)
    @lbl_nb_max_players.objectName = "lbl_nb_max_players"

    @layout_game_parameters.addWidget(@lbl_nb_max_players, 0, 0, 1, 1)

    @lbl_nb_gold = Qt::Label.new(@gridLayoutWidget)
    @lbl_nb_gold.objectName = "lbl_nb_gold"

    @layout_game_parameters.addWidget(@lbl_nb_gold, 2, 0, 1, 1)

    @nb_cards = Qt::SpinBox.new(@gridLayoutWidget)
    @nb_cards.objectName = "nb_cards"
    @nb_cards.maximum = 8
    @nb_cards.value = 4

    @layout_game_parameters.addWidget(@nb_cards, 1, 1, 1, 1)

    @lbl_nb_cards = Qt::Label.new(@gridLayoutWidget)
    @lbl_nb_cards.objectName = "lbl_nb_cards"

    @layout_game_parameters.addWidget(@lbl_nb_cards, 1, 0, 1, 1)

    @nb_max_players = Qt::SpinBox.new(@gridLayoutWidget)
    @nb_max_players.objectName = "nb_max_players"
    @nb_max_players.minimum = 2
    @nb_max_players.maximum = 8
    @nb_max_players.value = 4

    @layout_game_parameters.addWidget(@nb_max_players, 0, 1, 1, 1)

    @nb_gold = Qt::SpinBox.new(@gridLayoutWidget)
    @nb_gold.objectName = "nb_gold"
    @nb_gold.maximum = 10
    @nb_gold.value = 2

    @layout_game_parameters.addWidget(@nb_gold, 2, 1, 1, 1)

    @nb_dist = Qt::SpinBox.new(@gridLayoutWidget)
    @nb_dist.objectName = "nb_dist"
    @nb_dist.minimum = 6
    @nb_dist.maximum = 8
    @nb_dist.value = 8

    @layout_game_parameters.addWidget(@nb_dist, 3, 1, 1, 1)

    @lbl_nb_dist = Qt::Label.new(@gridLayoutWidget)
    @lbl_nb_dist.objectName = "lbl_nb_dist"

    @layout_game_parameters.addWidget(@lbl_nb_dist, 3, 0, 1, 1)

    @button_default = Qt::PushButton.new(@tab_game_parameters)
    @button_default.objectName = "button_default"
    @button_default.geometry = Qt::Rect.new(10, 380, 331, 24)
    @button_dist_deck = Qt::PushButton.new(@tab_game_parameters)
    @button_dist_deck.objectName = "button_dist_deck"
    @button_dist_deck.geometry = Qt::Rect.new(120, 220, 111, 91)
    icon2 = Qt::Icon.new
    icon2.addPixmap(Qt::Pixmap.new("./img/2_to_1_card.png"), Qt::Icon::Normal, Qt::Icon::Off)
    @button_dist_deck.icon = icon2
    @button_dist_deck.iconSize = Qt::Size.new(64, 64)
    @tab_widget_param.addTab(@tab_game_parameters, Qt::Application.translate("waitingRoom", "Paramètres de la partie", nil, Qt::Application::UnicodeUTF8))
    @tab_options = Qt::Widget.new()
    @tab_options.objectName = "tab_options"
    @verticalLayoutWidget_2 = Qt::Widget.new(@tab_options)
    @verticalLayoutWidget_2.objectName = "verticalLayoutWidget_2"
    @verticalLayoutWidget_2.geometry = Qt::Rect.new(10, 10, 331, 401)
    @layout_options = Qt::VBoxLayout.new(@verticalLayoutWidget_2)
    @layout_options.objectName = "layout_options"
    @layout_options.setContentsMargins(0, 0, 0, 0)
    @check_easy_draw = Qt::CheckBox.new(@verticalLayoutWidget_2)
    @check_easy_draw.objectName = "check_easy_draw"

    @layout_options.addWidget(@check_easy_draw)

    @check_kill_crown = Qt::CheckBox.new(@verticalLayoutWidget_2)
    @check_kill_crown.objectName = "check_kill_crown"

    @layout_options.addWidget(@check_kill_crown)

    @check_end_killed_char = Qt::CheckBox.new(@verticalLayoutWidget_2)
    @check_end_killed_char.objectName = "check_end_killed_char"

    @layout_options.addWidget(@check_end_killed_char)

    @check_total_bewitch = Qt::CheckBox.new(@verticalLayoutWidget_2)
    @check_total_bewitch.objectName = "check_total_bewitch"

    @layout_options.addWidget(@check_total_bewitch)

    @check_bewitch_crown = Qt::CheckBox.new(@verticalLayoutWidget_2)
    @check_bewitch_crown.objectName = "check_bewitch_crown"

    @layout_options.addWidget(@check_bewitch_crown)

    @check_build_bewitch = Qt::CheckBox.new(@verticalLayoutWidget_2)
    @check_build_bewitch.objectName = "check_build_bewitch"

    @layout_options.addWidget(@check_build_bewitch)

    @check_end_owner = Qt::CheckBox.new(@verticalLayoutWidget_2)
    @check_end_owner.objectName = "check_end_owner"

    @layout_options.addWidget(@check_end_owner)

    @check_sorcerer_revealed_cards = Qt::CheckBox.new(@verticalLayoutWidget_2)
    @check_sorcerer_revealed_cards.objectName = "check_sorcerer_revealed_cards"

    @layout_options.addWidget(@check_sorcerer_revealed_cards)

    @check_toss_king = Qt::CheckBox.new(@verticalLayoutWidget_2)
    @check_toss_king.objectName = "check_toss_king"

    @layout_options.addWidget(@check_toss_king)

    @check_killing_king = Qt::CheckBox.new(@verticalLayoutWidget_2)
    @check_killing_king.objectName = "check_killing_king"

    @layout_options.addWidget(@check_killing_king)

    @check_oblige_to_enthrone = Qt::CheckBox.new(@verticalLayoutWidget_2)
    @check_oblige_to_enthrone.objectName = "check_oblige_to_enthrone"

    @layout_options.addWidget(@check_oblige_to_enthrone)

    @check_all_wealthy_pay = Qt::CheckBox.new(@verticalLayoutWidget_2)
    @check_all_wealthy_pay.objectName = "check_all_wealthy_pay"

    @layout_options.addWidget(@check_all_wealthy_pay)

    @check_ending_turn_mercenary = Qt::CheckBox.new(@verticalLayoutWidget_2)
    @check_ending_turn_mercenary.objectName = "check_ending_turn_mercenary"

    @layout_options.addWidget(@check_ending_turn_mercenary)

    @check_pay_diff = Qt::CheckBox.new(@verticalLayoutWidget_2)
    @check_pay_diff.objectName = "check_pay_diff"

    @layout_options.addWidget(@check_pay_diff)

    @check_treasure_add = Qt::CheckBox.new(@verticalLayoutWidget_2)
    @check_treasure_add.objectName = "check_treasure_add"

    @layout_options.addWidget(@check_treasure_add)

    @check_end_powder = Qt::CheckBox.new(@verticalLayoutWidget_2)
    @check_end_powder.objectName = "check_end_powder"

    @layout_options.addWidget(@check_end_powder)

    @tab_widget_param.addTab(@tab_options, Qt::Application.translate("waitingRoom", "Variantes", nil, Qt::Application::UnicodeUTF8))
    @horizontalLayoutWidget_3 = Qt::Widget.new(waitingRoom)
    @horizontalLayoutWidget_3.objectName = "horizontalLayoutWidget_3"
    @horizontalLayoutWidget_3.geometry = Qt::Rect.new(360, 530, 261, 41)
    @layout_player = Qt::HBoxLayout.new(@horizontalLayoutWidget_3)
    @layout_player.objectName = "layout_player"
    @layout_player.setContentsMargins(0, 0, 0, 0)
    @lbl_pseudo = Qt::Label.new(@horizontalLayoutWidget_3)
    @lbl_pseudo.objectName = "lbl_pseudo"

    @layout_player.addWidget(@lbl_pseudo)

    @pseudo = Qt::LineEdit.new(@horizontalLayoutWidget_3)
    @pseudo.objectName = "pseudo"
    @pseudo.maxLength = 16

    @layout_player.addWidget(@pseudo)

    @ready = Qt::CheckBox.new(@horizontalLayoutWidget_3)
    @ready.objectName = "ready"

    @layout_player.addWidget(@ready)

    Qt::Widget.setTabOrder(@pseudo, @ready)
    Qt::Widget.setTabOrder(@ready, @message)
    Qt::Widget.setTabOrder(@message, @send)
    Qt::Widget.setTabOrder(@send, @leave)
    Qt::Widget.setTabOrder(@leave, @tchat)
    Qt::Widget.setTabOrder(@tchat, @tab_widget_param)
    Qt::Widget.setTabOrder(@tab_widget_param, @nb_max_players)
    Qt::Widget.setTabOrder(@nb_max_players, @nb_cards)
    Qt::Widget.setTabOrder(@nb_cards, @nb_gold)
    Qt::Widget.setTabOrder(@nb_gold, @nb_dist)
    Qt::Widget.setTabOrder(@nb_dist, @button_dist_deck)
    Qt::Widget.setTabOrder(@button_dist_deck, @button_default)
    Qt::Widget.setTabOrder(@button_default, @check_easy_draw)
    Qt::Widget.setTabOrder(@check_easy_draw, @check_kill_crown)
    Qt::Widget.setTabOrder(@check_kill_crown, @check_end_killed_char)
    Qt::Widget.setTabOrder(@check_end_killed_char, @check_total_bewitch)
    Qt::Widget.setTabOrder(@check_total_bewitch, @check_bewitch_crown)
    Qt::Widget.setTabOrder(@check_bewitch_crown, @check_build_bewitch)
    Qt::Widget.setTabOrder(@check_build_bewitch, @check_end_owner)
    Qt::Widget.setTabOrder(@check_end_owner, @check_sorcerer_revealed_cards)
    Qt::Widget.setTabOrder(@check_sorcerer_revealed_cards, @check_toss_king)
    Qt::Widget.setTabOrder(@check_toss_king, @check_killing_king)
    Qt::Widget.setTabOrder(@check_killing_king, @check_oblige_to_enthrone)
    Qt::Widget.setTabOrder(@check_oblige_to_enthrone, @check_all_wealthy_pay)
    Qt::Widget.setTabOrder(@check_all_wealthy_pay, @check_ending_turn_mercenary)
    Qt::Widget.setTabOrder(@check_ending_turn_mercenary, @check_pay_diff)
    Qt::Widget.setTabOrder(@check_pay_diff, @check_treasure_add)
    Qt::Widget.setTabOrder(@check_treasure_add, @check_end_powder)

    retranslateUi(waitingRoom)

    @tab_widget_param.setCurrentIndex(0)


    Qt::MetaObject.connectSlotsByName(waitingRoom)
    end # setupUi

    def setup_ui(waitingRoom)
        setupUi(waitingRoom)
    end

    def retranslateUi(waitingRoom)
    waitingRoom.windowTitle = Qt::Application.translate("waitingRoom", "Salle d'attente", nil, Qt::Application::UnicodeUTF8)
    waitingRoom.toolTip = Qt::Application.translate("waitingRoom", "Définit si la destruction d'un 'Quartier' dans une 'Citadelle' achevée par la 'Poudrière' repousse la fin de la partie.", nil, Qt::Application::UnicodeUTF8)
    @send.text = Qt::Application.translate("waitingRoom", "Envoyer", nil, Qt::Application::UnicodeUTF8)
    @leave.text = Qt::Application.translate("waitingRoom", "Quitter la salle", nil, Qt::Application::UnicodeUTF8)
    @grp_box_characters.title = Qt::Application.translate("waitingRoom", "Deck des 'Personnages'", nil, Qt::Application::UnicodeUTF8)
    @grp_box_players.title = Qt::Application.translate("waitingRoom", "Liste des Joueurs", nil, Qt::Application::UnicodeUTF8)
    @tab_widget_param.toolTip = Qt::Application.translate("waitingRoom", "Choir les cartes qui feront parties du deck des 'Quartiers'.", nil, Qt::Application::UnicodeUTF8)
    @lbl_nb_max_players.toolTip = Qt::Application.translate("waitingRoom", "Définit le nombre maximal de joueurs pouvant rejoindre la partie.", nil, Qt::Application::UnicodeUTF8)
    @lbl_nb_max_players.text = Qt::Application.translate("waitingRoom", "Joueurs max :", nil, Qt::Application::UnicodeUTF8)
    @lbl_nb_gold.toolTip = Qt::Application.translate("waitingRoom", "Définit la quantité d'argent donnée à chaque joueur en début de partie.", nil, Qt::Application::UnicodeUTF8)
    @lbl_nb_gold.text = Qt::Application.translate("waitingRoom", "Solde de départ :", nil, Qt::Application::UnicodeUTF8)
    @lbl_nb_cards.toolTip = Qt::Application.translate("waitingRoom", "Définit le nombre de cartes distribuées en début de partie à chaque joueurs.", nil, Qt::Application::UnicodeUTF8)
    @lbl_nb_cards.text = Qt::Application.translate("waitingRoom", "Nombre de cartes :", nil, Qt::Application::UnicodeUTF8)
    @lbl_nb_dist.toolTip = Qt::Application.translate("waitingRoom", "Détermine le nombre de 'Quartiers' à bâtir pour que la partie s'achève.", nil, Qt::Application::UnicodeUTF8)
    @lbl_nb_dist.text = Qt::Application.translate("waitingRoom", "Nombre de 'Quartiers' :", nil, Qt::Application::UnicodeUTF8)
    @button_default.text = Qt::Application.translate("waitingRoom", "Rétablir les paramètres par défauts", nil, Qt::Application::UnicodeUTF8)
    @button_dist_deck.text = ''
    @tab_widget_param.setTabText(@tab_widget_param.indexOf(@tab_game_parameters), Qt::Application.translate("waitingRoom", "Paramètres de la partie", nil, Qt::Application::UnicodeUTF8))
    @check_easy_draw.toolTip = Qt::Application.translate("waitingRoom", "Variante permettant de choisir la carte à défausser parmis toutes ses cartes.", nil, Qt::Application::UnicodeUTF8)
    @check_easy_draw.text = Qt::Application.translate("waitingRoom", "Pioche facile", nil, Qt::Application::UnicodeUTF8)
    @check_kill_crown.toolTip = Qt::Application.translate("waitingRoom", "Définit si l'assassinat du 'Roi' entraîne la perte de la couronne.", nil, Qt::Application::UnicodeUTF8)
    @check_kill_crown.text = Qt::Application.translate("waitingRoom", "Couronne des mort-vivants", nil, Qt::Application::UnicodeUTF8)
    @check_end_killed_char.toolTip = Qt::Application.translate("waitingRoom", "Définit si le 'Personnage' assassiné se révèle à la fin du tour.", nil, Qt::Application::UnicodeUTF8)
    @check_end_killed_char.text = Qt::Application.translate("waitingRoom", "Que les morts se révèlent... à la fin !", nil, Qt::Application::UnicodeUTF8)
    @check_total_bewitch.toolTip = Qt::Application.translate("waitingRoom", "Définit si la 'Sorcière' récupère la totalité du tour du Joueur ou seulement le pouvoir de son 'Personnage'.", nil, Qt::Application::UnicodeUTF8)
    @check_total_bewitch.text = Qt::Application.translate("waitingRoom", "Véritable marionnettiste", nil, Qt::Application::UnicodeUTF8)
    @check_bewitch_crown.toolTip = Qt::Application.translate("waitingRoom", "Définit si la 'Sorcière' récupère la couronne en ensorcellant le 'Roi'.", nil, Qt::Application::UnicodeUTF8)
    @check_bewitch_crown.text = Qt::Application.translate("waitingRoom", "Couronne ensorcelée", nil, Qt::Application::UnicodeUTF8)
    @check_build_bewitch.toolTip = Qt::Application.translate("waitingRoom", "Définit si la 'Sorcière' peut bâtir un 'Quartier' au cours de son tour.", nil, Qt::Application::UnicodeUTF8)
    @check_build_bewitch.text = Qt::Application.translate("waitingRoom", "Entrepreuneuse au balai", nil, Qt::Application::UnicodeUTF8)
    @check_end_owner.toolTip = Qt::Application.translate("waitingRoom", "Définit si le 'Bailli' est appelé à la toute fin du tour de jeu.", nil, Qt::Application::UnicodeUTF8)
    @check_end_owner.text = Qt::Application.translate("waitingRoom", "Collecte des impôts", nil, Qt::Application::UnicodeUTF8)
    @check_sorcerer_revealed_cards.toolTip = Qt::Application.translate("waitingRoom", "Définit si le 'Sorcier' peut voir les cartes lors de l'utilisation de son pouvoir.", nil, Qt::Application::UnicodeUTF8)
    @check_sorcerer_revealed_cards.text = Qt::Application.translate("waitingRoom", "Oracle", nil, Qt::Application::UnicodeUTF8)
    @check_toss_king.toolTip = Qt::Application.translate("waitingRoom", "Définit si on tire au hasard le 'Roi' lorsqu'il se retrouve parmis les personnages défaussés.", nil, Qt::Application::UnicodeUTF8)
    @check_toss_king.text = Qt::Application.translate("waitingRoom", "Tirage au sort du Roi", nil, Qt::Application::UnicodeUTF8)
    @check_killing_king.toolTip = Qt::Application.translate("waitingRoom", "Définit si le 'Roi' peut-être assassiné.", nil, Qt::Application::UnicodeUTF8)
    @check_killing_king.text = Qt::Application.translate("waitingRoom", "Roi immortel", nil, Qt::Application::UnicodeUTF8)
    @check_oblige_to_enthrone.toolTip = Qt::Application.translate("waitingRoom", "Définit si l''Empereur' est obligé d'intrôniser un Joueur au début de son tour.", nil, Qt::Application::UnicodeUTF8)
    @check_oblige_to_enthrone.text = Qt::Application.translate("waitingRoom", "Empereur à la main forcée", nil, Qt::Application::UnicodeUTF8)
    @check_all_wealthy_pay.toolTip = Qt::Application.translate("waitingRoom", "Définit si tous les Joueurs les plus riches payent l''Abbe'.", nil, Qt::Application::UnicodeUTF8)
    @check_all_wealthy_pay.text = Qt::Application.translate("waitingRoom", "Tous les riches passent à la caisse !", nil, Qt::Application::UnicodeUTF8)
    @check_ending_turn_mercenary.toolTip = Qt::Application.translate("waitingRoom", "Définit si le 'Condottiere' peut détruire le 'Quartier' d'un Joueur ayant achevé sa 'Citadelle'.", nil, Qt::Application::UnicodeUTF8)
    @check_ending_turn_mercenary.text = Qt::Application.translate("waitingRoom", "Ce n'est pas encore fini !", nil, Qt::Application::UnicodeUTF8)
    @check_pay_diff.toolTip = Qt::Application.translate("waitingRoom", "Définit si le 'Diplomate' paye la différence si son 'Quartier' a un coût inférieur à celui de sa victime.", nil, Qt::Application::UnicodeUTF8)
    @check_pay_diff.text = Qt::Application.translate("waitingRoom", "On paie la différence !", nil, Qt::Application::UnicodeUTF8)
    @check_treasure_add.toolTip = Qt::Application.translate("waitingRoom", "Définit si le coût de la construction du 'Quartier' 'Trésor Impérial' est compris dans le décompte final ou pas.", nil, Qt::Application::UnicodeUTF8)
    @check_treasure_add.text = Qt::Application.translate("waitingRoom", "Trésor doublement impérial", nil, Qt::Application::UnicodeUTF8)
    @check_end_powder.text = Qt::Application.translate("waitingRoom", "Faîtes parler la poudre !", nil, Qt::Application::UnicodeUTF8)
    @tab_widget_param.setTabText(@tab_widget_param.indexOf(@tab_options), Qt::Application.translate("waitingRoom", "Variantes", nil, Qt::Application::UnicodeUTF8))
    @lbl_pseudo.text = Qt::Application.translate("waitingRoom", "Pseudo :", nil, Qt::Application::UnicodeUTF8)
    @pseudo.text = ''
    @ready.text = Qt::Application.translate("waitingRoom", "Prêt", nil, Qt::Application::UnicodeUTF8)
    end # retranslateUi

    def retranslate_ui(waitingRoom)
        retranslateUi(waitingRoom)
    end

end

module Ui
    class WaitingRoom < Ui_WaitingRoom
    end
end  # module Ui
