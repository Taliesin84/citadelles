# encoding: utf-8

=begin
** Form generated from reading ui file 'create_network_game.ui'
**
** Created: dim. juin 24 21:22:40 2012
**      by: Qt User Interface Compiler version 4.8.0
**
** WARNING! All changes made in this file will be lost when recompiling ui file!
=end

class Ui_CreateNetworkGame

  attr_reader :line
  attr_reader :horizontalLayoutWidget
  attr_reader :horizontalLayout
  attr_reader :horizontalSpacer
  attr_reader :create
  attr_reader :buttonBox
  attr_reader :formLayoutWidget
  attr_reader :formLayout
  attr_reader :label_name
  attr_reader :server_name
  attr_reader :label_mdp
  attr_reader :mdp_1
  attr_reader :confirm_mdp
  attr_reader :mdp_2
  attr_reader :line_2

  def setupUi(createNetworkGame)
    if createNetworkGame.objectName.nil?
      createNetworkGame.objectName = "createNetworkGame"
    end
    createNetworkGame.resize(400, 180)
    createNetworkGame.minimumSize = Qt::Size.new(400, 180)
    createNetworkGame.maximumSize = Qt::Size.new(400, 180)
    window_icon = Qt::Icon.new
    window_icon.addPixmap Qt::Pixmap.new('./img/icons/toolbar/network.png'), Qt::Icon::Normal, Qt::Icon::Off
    createNetworkGame.windowIcon = window_icon
    @line = Qt::Frame.new(createNetworkGame)
    @line.objectName = "line"
    @line.geometry = Qt::Rect.new(11, 120, 371, 20)
    @line.setFrameShape(Qt::Frame::HLine)
    @line.setFrameShadow(Qt::Frame::Sunken)
    @horizontalLayoutWidget = Qt::Widget.new(createNetworkGame)
    @horizontalLayoutWidget.objectName = "horizontalLayoutWidget"
    @horizontalLayoutWidget.geometry = Qt::Rect.new(10, 140, 371, 31)
    @horizontalLayout = Qt::HBoxLayout.new(@horizontalLayoutWidget)
    @horizontalLayout.objectName = "horizontalLayout"
    @horizontalLayout.setContentsMargins(0, 0, 0, 0)
    @horizontalSpacer = Qt::SpacerItem.new(40, 20, Qt::SizePolicy::Expanding, Qt::SizePolicy::Minimum)

    @horizontalLayout.addItem(@horizontalSpacer)

    @create = Qt::PushButton.new(@horizontalLayoutWidget)
    @create.objectName = "create"
    icon = Qt::Icon.new
    icon.addPixmap(Qt::Pixmap.new("../Projects/Ruby/Citadelles/img/create.png"), Qt::Icon::Normal, Qt::Icon::Off)
    @create.icon = icon

    @horizontalLayout.addWidget(@create)

    @buttonBox = Qt::DialogButtonBox.new(@horizontalLayoutWidget)
    @buttonBox.objectName = "buttonBox"
    @sizePolicy = Qt::SizePolicy.new(Qt::SizePolicy::Fixed, Qt::SizePolicy::Fixed)
    @sizePolicy.setHorizontalStretch(0)
    @sizePolicy.setVerticalStretch(0)
    @sizePolicy.heightForWidth = @buttonBox.sizePolicy.hasHeightForWidth
    @buttonBox.sizePolicy = @sizePolicy
    @buttonBox.orientation = Qt::Horizontal
    @buttonBox.standardButtons = Qt::DialogButtonBox::Cancel

    @horizontalLayout.addWidget(@buttonBox)

    @formLayoutWidget = Qt::Widget.new(createNetworkGame)
    @formLayoutWidget.objectName = "formLayoutWidget"
    @formLayoutWidget.geometry = Qt::Rect.new(11, 10, 371, 101)
    @formLayout = Qt::FormLayout.new(@formLayoutWidget)
    @formLayout.objectName = "formLayout"
    @formLayout.fieldGrowthPolicy = Qt::FormLayout::ExpandingFieldsGrow
    @formLayout.setContentsMargins(0, 0, 0, 0)
    @label_name = Qt::Label.new(@formLayoutWidget)
    @label_name.objectName = "label_name"

    @formLayout.setWidget(0, Qt::FormLayout::LabelRole, @label_name)

    @server_name = Qt::LineEdit.new(@formLayoutWidget)
    @server_name.objectName = "server_name"
    @server_name.maxLength = 32

    @formLayout.setWidget(0, Qt::FormLayout::FieldRole, @server_name)

    @label_mdp = Qt::Label.new(@formLayoutWidget)
    @label_mdp.objectName = "label_mdp"

    @formLayout.setWidget(3, Qt::FormLayout::LabelRole, @label_mdp)

    @mdp_1 = Qt::LineEdit.new(@formLayoutWidget)
    @mdp_1.objectName = "mdp_1"
    @mdp_1.maxLength = 32
    @mdp_1.echoMode = Qt::LineEdit::Password

    @formLayout.setWidget(3, Qt::FormLayout::FieldRole, @mdp_1)

    @confirm_mdp = Qt::Label.new(@formLayoutWidget)
    @confirm_mdp.objectName = "confirm_mdp"

    @formLayout.setWidget(4, Qt::FormLayout::LabelRole, @confirm_mdp)

    @mdp_2 = Qt::LineEdit.new(@formLayoutWidget)
    @mdp_2.objectName = "mdp_2"
    @mdp_2.maxLength = 32
    @mdp_2.echoMode = Qt::LineEdit::Password

    @formLayout.setWidget(4, Qt::FormLayout::FieldRole, @mdp_2)

    @line_2 = Qt::Frame.new(@formLayoutWidget)
    @line_2.objectName = "line_2"
    @sizePolicy1 = Qt::SizePolicy.new(Qt::SizePolicy::Expanding, Qt::SizePolicy::Expanding)
    @sizePolicy1.setHorizontalStretch(12)
    @sizePolicy1.setVerticalStretch(12)
    @sizePolicy1.heightForWidth = @line_2.sizePolicy.hasHeightForWidth
    @line_2.sizePolicy = @sizePolicy1
    @line_2.minimumSize = Qt::Size.new(120, 0)
    @line_2.lineWidth = 1
    @line_2.setFrameShape(Qt::Frame::HLine)
    @line_2.setFrameShadow(Qt::Frame::Sunken)

    @formLayout.setWidget(2, Qt::FormLayout::FieldRole, @line_2)

    Qt::Widget.setTabOrder(@server_name, @mdp_1)
    Qt::Widget.setTabOrder(@mdp_1, @mdp_2)
    Qt::Widget.setTabOrder(@mdp_2, @create)
    Qt::Widget.setTabOrder(@create, @buttonBox)

    retranslateUi(createNetworkGame)
    Qt::Object.connect(@buttonBox, SIGNAL('accepted()'), createNetworkGame, SLOT('accept()'))
    Qt::Object.connect(@buttonBox, SIGNAL('rejected()'), createNetworkGame, SLOT('reject()'))

    Qt::MetaObject.connectSlotsByName(createNetworkGame)
  end # setupUi

  def setup_ui(createNetworkGame)
    setupUi(createNetworkGame)
  end

  def retranslateUi(createNetworkGame)
    createNetworkGame.windowTitle = Qt::Application.translate("CreateNetworkGame", "Création d'une Partie", nil, Qt::Application::UnicodeUTF8)
    @create.text = Qt::Application.translate("CreateNetworkGame", "Créer Partie", nil, Qt::Application::UnicodeUTF8)
    @label_name.text = Qt::Application.translate("CreateNetworkGame", "Nom de la partie :", nil, Qt::Application::UnicodeUTF8)
    @label_mdp.text = Qt::Application.translate("CreateNetworkGame", "Mot de passe :", nil, Qt::Application::UnicodeUTF8)
    @confirm_mdp.text = Qt::Application.translate("CreateNetworkGame", "Confirmation :", nil, Qt::Application::UnicodeUTF8)
  end # retranslateUi

  def retranslate_ui(createNetworkGame)
    retranslateUi(createNetworkGame)
  end

end

module Ui
  class CreateNetworkGame < Ui_CreateNetworkGame
  end
end  # module Ui

