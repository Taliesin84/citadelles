# encoding: utf-8

require 'Qt4'

class Portrait < Qt::Label

  attr_reader :index, :char_name

  signals 'clicked(int)'

  def initialize index, parent=nil
    super(parent)
    @index = index
    @char_name = ""
    setObjectName "portrait_#{index}"
    setContentsMargins 0, 0, 0, 0
  end

  def set_portrait char_name
    @char_name = char_name
    char_pix = Qt::Pixmap.new "./img/portraits/#{char_name}.png"
    char_pix = char_pix.scaled 41, 64, Qt::IgnoreAspectRatio, Qt::SmoothTransformation
    setPixmap char_pix
    case char_name
      when "eveque"
        setToolTip "Evêque"
      when "condottiere"
        setToolTip "Condottière"
      when "sorciere"
        setToolTip "Sorcière"
      when "abbe"
        setToolTip "Abbé"
      else
        setToolTip char_name.capitalize
    end
  end

  def mousePressEvent event
    emit clicked(@index)
  end

end