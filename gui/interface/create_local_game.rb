# encoding: utf-8

require 'Qt4'
require './gui/interface/create_local_game_ui'
require './gui/interface/char_select'
require './gui/interface/dist_select'

class CreateLocalGame < Qt::Dialog

  slots 'set_disp_nb_j_max(int)' ,'char_select(int)', :dist_select, :set_dist_options, :disp_char_selected, :set_default, :valid_parameters

  attr_reader :players, :characters, :districts, :parameters

  def initialize parent=nil
    super(parent)

    @create_local_game_ui = Ui::CreateLocalGame.new
    @create_local_game_ui.setup_ui self
    set_players
    @create_local_game_ui.nb_players.value = 4
    set_default
    set_char_list

    connections
  end

  def connections
    connect @create_local_game_ui.nb_players, SIGNAL('valueChanged(int)'), SLOT('set_disp_nb_j_max(int)')
    connect @create_local_game_ui.button_dist_deck, SIGNAL(:clicked), SLOT(:dist_select)
    connect @create_local_game_ui.button_default, SIGNAL(:clicked), SLOT(:set_default)
    connect @create_local_game_ui.buttonBox, SIGNAL(:accepted), SLOT(:valid_parameters)
    connect @create_local_game_ui.buttonBox, SIGNAL(:rejected), SLOT(:reject)
  end

  def set_disp_nb_j_max nb_j_max
    if nb_j_max < 4
      @create_local_game_ui.char_list[-1].hide
    else
      @create_local_game_ui.char_list[-1].show
    end
    i = 0
    while i < @players_item.length
      i < nb_j_max ? @players_item[i][:label].show : @players_item[i][:label].hide
      i < nb_j_max ? @players_item[i][:pseudo].show : @players_item[i][:pseudo].hide
      i += 1
    end
    if nb_j_max == 8 and @create_local_game_ui.char_list[-1].char_name == "nobody"
      @create_local_game_ui.char_list[-1].set_portrait "reine"
      @characters[-1] = "reine"
    end
  end

  def set_char_list
    i = 0
    for char_ui in @create_local_game_ui.char_list
      char_ui.set_portrait @characters[i]
      connect @create_local_game_ui.char_list[i], SIGNAL('clicked(int)'), SLOT('char_select(int)')
      i += 1
    end
  end

  def char_select index
    @char_select = CharSelect.new index, @characters[index], @create_local_game_ui.nb_players.value
    @char_select.setModal true
    @char_select.show
    connect @char_select, SIGNAL(:accepted), SLOT(:disp_char_selected)
  end

  def disp_char_selected
    @characters[@char_select.index_char] = @char_select.selected_char
    @create_local_game_ui.char_list[@char_select.index_char].set_portrait @char_select.selected_char
    set_char_options
  end

  def set_char_options
    for char in @characters
      case char
        when "assassin"
          @create_local_game_ui.check_end_killed_char.show
          @create_local_game_ui.check_total_bewitch.hide
          @create_local_game_ui.check_build_bewitch.hide
        when "sorciere"
          @create_local_game_ui.check_end_killed_char.hide
          @create_local_game_ui.check_total_bewitch.show
          @create_local_game_ui.check_build_bewitch.show
        when "voleur"
          @create_local_game_ui.check_end_owner.hide
        when "bailli"
          @create_local_game_ui.check_end_owner.show
        when "magicien"
          @create_local_game_ui.check_sorcerer_revealed_cards.hide
        when "sorcier"
          @create_local_game_ui.check_sorcerer_revealed_cards.show
        when "roi"
          @create_local_game_ui.check_oblige_to_enthrone.hide
        when "empereur"
          @create_local_game_ui.check_oblige_to_enthrone.show
        when "eveque"
          @create_local_game_ui.check_all_wealthy_pay.hide
        when "abbe"
          @create_local_game_ui.check_all_wealthy_pay.show
        when "condottiere"
          @create_local_game_ui.check_ending_turn_mercenary.show
          @create_local_game_ui.check_pay_diff.hide
        when "diplomate"
          @create_local_game_ui.check_ending_turn_mercenary.hide
          @create_local_game_ui.check_pay_diff.show
        else
          # do nothing
      end
      if @characters[0]=="assassin" and @characters[3]=="roi"
        @create_local_game_ui.check_kill_crown.show
        @create_local_game_ui.check_killing_king.show
      else
        @create_local_game_ui.check_kill_crown.hide
        @create_local_game_ui.check_killing_king.hide
      end
      (@characters[0]=="sorciere" and @characters[3]=="roi") ? @create_local_game_ui.check_bewitch_crown.show : @create_local_game_ui.check_bewitch_crown.hide
    end
  end

  def dist_select
    @dist_select = DistSelect.new @districts, 'server'
    @dist_select.setModal true
    @dist_select.show
    connect @dist_select, SIGNAL(:accepted), SLOT(:set_dist_options)
  end

  def set_dist_options
    @districts = @dist_select.selected_dist
    @districts.find {|dist| dist == "tresor_imperial"} ? @create_local_game_ui.check_treasure_add.show : @create_local_game_ui.check_treasure_add.hide
    @districts.find {|dist| dist == "poudriere"} ? @create_local_game_ui.check_end_powder.show : @create_local_game_ui.check_end_powder.hide
  end

  def set_players
    @players_item = []
    i = 1
    8.times {
      lbl_player = Qt::Label.new(@create_local_game_ui.gridLayoutWidget_2)
      lbl_player.objectName = "lbl_player_#{i}"
      lbl_player.text = Qt::Application.translate("CreateLocalGame", "Joueur #{i} :", nil, Qt::Application::UnicodeUTF8)
      i <= 4 ? lbl_player.show : lbl_player.hide

      @create_local_game_ui.gridLayout.addWidget(lbl_player, i-1, 0, 1, 1)

      pseudo = Qt::LineEdit.new(@create_local_game_ui.gridLayoutWidget_2)
      pseudo.objectName = "pseudo_#{i}"
      pseudo.maxLength = 16
      pseudo.text = "Joueur #{i}"
      i <= 4 ? pseudo.show : pseudo.hide

      @create_local_game_ui.gridLayout.addWidget(pseudo, i-1, 1, 1, 1)

      @players_item << {:label => lbl_player, :pseudo => pseudo}
      i += 1
    }
  end

  def set_default
    @create_local_game_ui.nb_cards.value = 4
    @create_local_game_ui.nb_gold.value = 2
    @create_local_game_ui.nb_dist.value = 8

    if @create_local_game_ui.nb_players.value < 8
      @characters = %w(assassin voleur magicien roi eveque marchand architecte condottiere nobody)
    else
      @characters = %w(assassin voleur magicien roi eveque marchand architecte condottiere reine)
    end

    i = 0
    for char in @characters
      @create_local_game_ui.char_list[i].set_portrait char
      i += 1
    end
    set_char_options

    @districts = %w(cour_des_miracles donjon tresor_imperial carriere cimetiere fontaine_aux_souhaits laboratoire manufacture observatoire bibliotheque dracoport ecole_de_magie grande_muraille universite)

    @create_local_game_ui.check_easy_draw.setChecked false
    @create_local_game_ui.check_kill_crown.setChecked true
    @create_local_game_ui.check_end_killed_char.setChecked true
    @create_local_game_ui.check_total_bewitch.setChecked false
    @create_local_game_ui.check_bewitch_crown.setChecked false
    @create_local_game_ui.check_build_bewitch.setChecked false
    @create_local_game_ui.check_end_owner.setChecked false
    @create_local_game_ui.check_sorcerer_revealed_cards.setChecked true
    @create_local_game_ui.check_toss_king.setChecked false
    @create_local_game_ui.check_killing_king.setChecked true
    @create_local_game_ui.check_oblige_to_enthrone.setChecked false
    @create_local_game_ui.check_all_wealthy_pay.setChecked false
    @create_local_game_ui.check_ending_turn_mercenary.setChecked false
    @create_local_game_ui.check_pay_diff.setChecked true
    @create_local_game_ui.check_treasure_add.setChecked false
    @create_local_game_ui.check_end_powder.setChecked false
  end

  def valid_parameters
    @characters.delete_if {|char| char == "nobody"}
    @players = []
    for player_item in @players_item
      @players << player_item[:pseudo].text.to_s if player_item[:pseudo].isVisible
    end
    @parameters = {
        # Paramètres de 'Table'
        :begin_sold => @create_local_game_ui.nb_gold.value,
        :begin_nb_cards => @create_local_game_ui.nb_cards.value,
        :max_dist => @create_local_game_ui.nb_dist.value,
        :easy_draw => @create_local_game_ui.check_easy_draw.isChecked,

        # Paramètres des 'Personnages'
        :kill_crown => @create_local_game_ui.check_kill_crown.isChecked,
        :end_killed_char => @create_local_game_ui.check_end_killed_char.isChecked,
        :total_bewitch => @create_local_game_ui.check_total_bewitch.isChecked,
        :bewitch_crown => @create_local_game_ui.check_bewitch_crown.isChecked,
        :build_bewitch => @create_local_game_ui.check_build_bewitch.isChecked,
        :end_owner => @create_local_game_ui.check_end_owner.isChecked,
        :sorcerer_revealed_cards => @create_local_game_ui.check_sorcerer_revealed_cards.isChecked,
        :toss_king => @create_local_game_ui.check_toss_king.isChecked,
        :killing_king => @create_local_game_ui.check_killing_king.isChecked,
        :oblige_to_enthrone => @create_local_game_ui.check_oblige_to_enthrone.isChecked,
        :all_wealthy_pay => @create_local_game_ui.check_all_wealthy_pay.isChecked,
        :ending_turn_mercenary => @create_local_game_ui.check_ending_turn_mercenary.isChecked,
        :pay_diff => @create_local_game_ui.check_pay_diff.isChecked,

        # Paramètres des 'Quartiers'
        :treasure_add => @create_local_game_ui.check_treasure_add.isChecked,
        :end_powder => @create_local_game_ui.check_end_powder.isChecked,
    }
    accept
  end

end