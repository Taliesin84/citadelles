# encoding: utf-8

require 'Qt4'

class CardLabel < Qt::Label

  signals 'clicked(int)'

  def initialize index, path, name, parent=nil
    super(parent)
    setObjectName "card_#{index}"
    setContentsMargins 0, 0, 0, 0

    @index = index
    @name = name

    card_pix = Qt::Pixmap.new "./img/#{path}/#{name}.png"
    card_pix = card_pix.scaled 323, 500, Qt::IgnoreAspectRatio, Qt::SmoothTransformation
    setPixmap card_pix
  end

  def mousePressEvent event
    emit clicked(@index)
  end
end