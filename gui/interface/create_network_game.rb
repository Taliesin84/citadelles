# encoding: utf-8

require 'Qt4'
require './gui/interface/create_network_game_ui'
require './lib/servers/game_server'
require 'win32/process'

class CreateNetworkGame < Qt::Dialog

  slots :validation, 'enable_create(const QString &)'

  attr_reader :created_server

  def initialize parent=nil
    super(parent)

    @create_game_ui = Ui::CreateNetworkGame.new
    @create_game_ui.setup_ui self
    @create_game_ui.create.setEnabled false

    connections
  end

  def connections
    connect @create_game_ui.server_name, SIGNAL('textChanged(const QString &)'), SLOT('enable_create(const QString &)')
    connect @create_game_ui.create, SIGNAL(:clicked), SLOT(:validation)
    connect @create_game_ui.buttonBox, SIGNAL(:rejected), SLOT(:reject)
  end

  def enable_create text
    if text.empty?
      @create_game_ui.create.setEnabled false
    else
      @create_game_ui.create.setEnabled true
    end
  end

  def validation
    mdp_1 = @create_game_ui.mdp_1.text
    mdp_2 = @create_game_ui.mdp_2.text
    name = @create_game_ui.server_name.text
    if mdp_1.empty? and mdp_2.empty?
      create_server name
    else
      if mdp_1 == mdp_2
        create_server name, mdp_1
      else
        if mdp_1.empty?
          Qt::MessageBox.warning self, "Warning", "Veuillez saisir le mot de passe originel !"
        elsif mdp_2.empty?
          Qt::MessageBox.warning self, "Warning", "Veuillez saisir la confirmation de votre mot de passe !"
        elsif mdp_1 != mdp_2
          Qt::MessageBox.warning self, "Warning", "Votre mot de passe et sa confirmation ne concordent pas !"
        end
      end
    end
  end

  def create_server name, mdp=''
    begin
      rd, wr = IO.pipe
      server_pid = fork do
        rd.close
        server = GameServer.new name, mdp
        server.start
        wr.write "#{server.id}|#{server.name}|#{server.mdp}|#{server.host}|192.168.0.111|#{server.port}|#{server.nb_j_max}"
        wr.close
      end
      puts "Pid : #{server_pid}"
      #@created_server = {
      #    :id => server.id,
      #    :name => server.name,
      #    :mdp => server.mdp,
      #    :host => "192.168.0.111",
      #    :port => server.port,
      #    :nb_j_max => server.nb_j_max
      #}
      wr.close
      server_params = rd.read.split('|')
      @created_server = {
          :id => server_params[0],
          :name => server_params[1],
          :mdp => server_params[2],
          :host => server_params[3],
          :port => server_params[4],
          :nb_j_max => server_params[5]
      }
      rd.close
      Process.waitpid server_pid
      puts @created_server
      #Qt::MessageBox.information self, "Information", "Votre serveur est démarré !\nVous allez à présent être redirigé vers le tableau de bord de partie."
      accept
    rescue Exception => e
      puts "Erreur : #{e.message}"
      puts "Backtrace :"
      for msg in e.backtrace
        puts " - #{msg}"
      end
      #Qt::MessageBox.critical self, "Critical", "Echec de démarrage du serveur de jeu !"
      Qt::MessageBox.critical self, "Critical", "#{$!}"
    end
  end

end