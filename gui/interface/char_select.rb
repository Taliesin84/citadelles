# encoding: utf-8

require 'Qt4'
require './gui/interface/char_select_ui'
require './gui/interface/card_label'

class CharSelect < Qt::Dialog

  attr_reader :index_char, :selected_char, :label_select

  slots 'set_selected_card(int)', :set_selected_char, 'toggle(bool)'

  def initialize index_char, char_checked, nb_players, parent=nil
    super(parent)
    @index_char = index_char
    @selected_char = char_checked
    set_characters nb_players

    @char_select_ui = Ui::CharSelect.new
    @char_select_ui.setup_ui(self)

    @grid_layout_widget = Qt::Widget.new @char_select_ui.scrollAreaWidgetContents
    @grid_layout_widget.objectName = "gridLayoutWidget"
    @grid_layout_widget.geometry = Qt::Rect.new 10, 10, 701, 527
    @grid_layout = Qt::GridLayout.new @grid_layout_widget
    @grid_layout.objectName = "gridLayout"
    @grid_layout.setContentsMargins 0, 0, 0, 0

    @radio_buttons = []
    wide = 0
    i = 0
    while i < @characters.length
      card = CardLabel.new i+1, "cards/characters/", @characters[i], @grid_layout_widget
      @grid_layout.addWidget card, 0, i*2, 1, 1
      wide += 323
      connect card, SIGNAL('clicked(int)'), SLOT('set_selected_card(int)')

      radio_button = Qt::RadioButton.new @grid_layout_widget
      radio_button.objectName = "radio_button_#{i+1}"
      @grid_layout.addWidget(radio_button, 1, i*2, 1, 1, Qt::AlignCenter)
      radio_button.setChecked true if @characters[i] == char_checked
      connect radio_button, SIGNAL('toggled(bool)'), SLOT('toggle(bool)')
      #connect radio_button, SIGNAL(:toggled), SLOT(:set_selected_char)
      @radio_buttons << radio_button

      if i < @characters.length-1
        horizontal_spacer = Qt::SpacerItem.new(40, 20, Qt::SizePolicy::Expanding, Qt::SizePolicy::Minimum)
        @grid_layout.addItem horizontal_spacer, 0, 1+i*2, 1, 1
        wide += 20
      end
      i += 1
    end
    @char_select_ui.scrollAreaWidgetContents.minimumSize = Qt::Size.new(wide+20, 521)
    @grid_layout_widget.geometry = Qt::Rect.new 10, 10, wide, 527

    connections
  end

  def connections
    connect @char_select_ui.button_box, SIGNAL(:accepted), SLOT(:accept)
    connect @char_select_ui.button_box, SIGNAL(:rejected), SLOT(:reject)
  end

  # Permet la sélection par les label 'carte'
  def set_selected_card index
    @radio_buttons[index-1].setChecked true
    set_selected_char
  end

  # Permet la sélection par les boutons 'radio'
  def toggle bool
    set_selected_char
  end

  def set_selected_char
    i = 0
    while i < @radio_buttons.length
      @selected_char=@characters[i] if @radio_buttons[i].isChecked
      i += 1
    end
  end

  def set_characters nb_players
    case @index_char
      when 0
        @characters = %w(assassin sorciere)
      when 1
        @characters = %w(voleur bailli)
      when 2
        @characters = %w(magicien sorcier)
      when 3
        @characters = %w(roi empereur)
      when 4
        @characters = %w(eveque abbe)
      when 5
        @characters = %w(marchand alchimiste)
      when 6
        @characters = %w(architecte navigateur)
      when 7
        @characters = %w(condottiere diplomate)
      when 8
        nb_players < 8 ? @characters=%w(nobody reine artiste) : @characters=%w(reine artiste)
      else
    end
  end
end
