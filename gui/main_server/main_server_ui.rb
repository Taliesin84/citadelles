# encoding: utf-8
=begin
** Form generated from reading ui file 'main_server.ui'
**
** Created: Mon Apr 2 20:39:58 2012
**      by: Qt User Interface Compiler version 4.8.0
**
** WARNING! All changes made in this file will be lost when recompiling ui file!
=end

class Ui_MainServer
  attr_reader :horizontalLayoutWidget
  attr_reader :horizontalLayout_3
  attr_reader :reInit
  attr_reader :launch
  attr_reader :shutdown
  attr_reader :horizontalLayoutWidget_2
  attr_reader :horizontalLayout_1
  attr_reader :radioGServer
  attr_reader :radioEvent
  attr_reader :typeServer
  attr_reader :horizontalLayoutWidget_3
  attr_reader :horizontalLayout_2
  attr_reader :nbServers
  attr_reader :labelNbServers
  attr_reader :line
  attr_reader :line_2
  attr_reader :listServers
  attr_reader :line_3
  attr_reader :refresh

  def setupUi(mainServer)
    if mainServer.objectName.nil?
      mainServer.objectName = "mainServer"
    end
    mainServer.resize(400, 450)
    mainServer.minimumSize = Qt::Size.new(400, 450)
    mainServer.maximumSize = Qt::Size.new(400, 450)
    main_icon = Qt::Icon.new
    main_icon.addPixmap Qt::Pixmap.new('./img/icons/main_server.png'), Qt::Icon::Normal, Qt::Icon::Off
    mainServer.windowIcon = main_icon
    @horizontalLayoutWidget = Qt::Widget.new(mainServer)
    @horizontalLayoutWidget.objectName = "horizontalLayoutWidget"
    @horizontalLayoutWidget.geometry = Qt::Rect.new(10, 410, 381, 41)
    @horizontalLayout_3 = Qt::HBoxLayout.new(@horizontalLayoutWidget)
    @horizontalLayout_3.objectName = "horizontalLayout_3"
    @horizontalLayout_3.setContentsMargins(0, 0, 0, 0)
    @reInit = Qt::PushButton.new(@horizontalLayoutWidget)
    @reInit.objectName = "reInit"

    @horizontalLayout_3.addWidget(@reInit)

    @launch = Qt::PushButton.new(@horizontalLayoutWidget)
    @launch.objectName = "launch"

    @horizontalLayout_3.addWidget(@launch)

    @shutdown = Qt::PushButton.new(@horizontalLayoutWidget)
    @shutdown.objectName = "shutdown"

    @horizontalLayout_3.addWidget(@shutdown)

    @horizontalLayoutWidget_2 = Qt::Widget.new(mainServer)
    @horizontalLayoutWidget_2.objectName = "horizontalLayoutWidget_2"
    @horizontalLayoutWidget_2.geometry = Qt::Rect.new(30, 30, 361, 41)
    @horizontalLayout_1 = Qt::HBoxLayout.new(@horizontalLayoutWidget_2)
    @horizontalLayout_1.objectName = "horizontalLayout_1"
    @horizontalLayout_1.setContentsMargins(0, 0, 0, 0)
    @radioGServer = Qt::RadioButton.new(@horizontalLayoutWidget_2)
    @radioGServer.objectName = "radioGServer"
    @radioGServer.checked = true

    @horizontalLayout_1.addWidget(@radioGServer)

    @radioEvent = Qt::RadioButton.new(@horizontalLayoutWidget_2)
    @radioEvent.objectName = "radioEvent"

    @horizontalLayout_1.addWidget(@radioEvent)

    @typeServer = Qt::Label.new(mainServer)
    @typeServer.objectName = "typeServer"
    @typeServer.geometry = Qt::Rect.new(30, 10, 361, 20)
    @horizontalLayoutWidget_3 = Qt::Widget.new(mainServer)
    @horizontalLayoutWidget_3.objectName = "horizontalLayoutWidget_3"
    @horizontalLayoutWidget_3.geometry = Qt::Rect.new(63, 100, 331, 41)
    @horizontalLayout_2 = Qt::HBoxLayout.new(@horizontalLayoutWidget_3)
    @horizontalLayout_2.objectName = "horizontalLayout_2"
    @horizontalLayout_2.setContentsMargins(0, 0, 0, 0)
    @nbServers = Qt::LCDNumber.new(@horizontalLayoutWidget_3)
    @nbServers.objectName = "nbServers"
    @sizePolicy = Qt::SizePolicy.new(Qt::SizePolicy::Fixed, Qt::SizePolicy::Minimum)
    @sizePolicy.setHorizontalStretch(0)
    @sizePolicy.setVerticalStretch(0)
    @sizePolicy.heightForWidth = @nbServers.sizePolicy.hasHeightForWidth
    @nbServers.sizePolicy = @sizePolicy
    @nbServers.minimumSize = Qt::Size.new(100, 0)
    @nbServers.frameShape = Qt::Frame::StyledPanel

    @horizontalLayout_2.addWidget(@nbServers)

    @labelNbServers = Qt::Label.new(@horizontalLayoutWidget_3)
    @labelNbServers.objectName = "labelNbServers"

    @horizontalLayout_2.addWidget(@labelNbServers)

    @line = Qt::Frame.new(mainServer)
    @line.objectName = "line"
    @line.geometry = Qt::Rect.new(10, 70, 381, 16)
    @line.setFrameShape(Qt::Frame::HLine)
    @line.setFrameShadow(Qt::Frame::Sunken)
    @line_2 = Qt::Frame.new(mainServer)
    @line_2.objectName = "line_2"
    @line_2.geometry = Qt::Rect.new(10, 150, 381, 16)
    @line_2.setFrameShape(Qt::Frame::HLine)
    @line_2.setFrameShadow(Qt::Frame::Sunken)
    @listServers = Qt::ListWidget.new(mainServer)
    @listServers.objectName = "listServers"
    @listServers.geometry = Qt::Rect.new(10, 170, 381, 221)
    @line_3 = Qt::Frame.new(mainServer)
    @line_3.objectName = "line_3"
    @line_3.geometry = Qt::Rect.new(10, 390, 381, 20)
    @line_3.setFrameShape(Qt::Frame::HLine)
    @line_3.setFrameShadow(Qt::Frame::Sunken)
    @refresh = Qt::PushButton.new(mainServer)
    @refresh.objectName = "refresh"
    @refresh.geometry = Qt::Rect.new(20, 100, 41, 41)
    @sizePolicy1 = Qt::SizePolicy.new(Qt::SizePolicy::Fixed, Qt::SizePolicy::Fixed)
    @sizePolicy1.setHorizontalStretch(0)
    @sizePolicy1.setVerticalStretch(0)
    @sizePolicy1.heightForWidth = @refresh.sizePolicy.hasHeightForWidth
    @refresh.sizePolicy = @sizePolicy1
    icon = Qt::Icon.new
    icon.addPixmap(Qt::Pixmap.new("./img/refresh.png"), Qt::Icon::Normal, Qt::Icon::Off)
    @refresh.icon = icon
    @refresh.iconSize = Qt::Size.new(24, 24)
    @refresh.flat = false

    retranslateUi(mainServer)

    Qt::MetaObject.connectSlotsByName(mainServer)
  end # setupUi

  def setup_ui(mainServer)
    setupUi(mainServer)
  end

  def retranslateUi(mainServer)
    mainServer.windowTitle = Qt::Application.translate("mainServer", "Serveur Central", nil, Qt::Application::UnicodeUTF8)
    @reInit.text = Qt::Application.translate("mainServer", "Réinitialiser", nil, Qt::Application::UnicodeUTF8)
    @launch.text = Qt::Application.translate("mainServer", "Démarrer", nil, Qt::Application::UnicodeUTF8)
    @shutdown.text = Qt::Application.translate("mainServer", "Eteindre", nil, Qt::Application::UnicodeUTF8)
    @radioGServer.text = Qt::Application.translate("mainServer", "GServer", nil, Qt::Application::UnicodeUTF8)
    @radioEvent.text = Qt::Application.translate("mainServer", "EventMachine", nil, Qt::Application::UnicodeUTF8)
    @typeServer.text = Qt::Application.translate("mainServer", "Serveur Central basé sur la technologie :", nil, Qt::Application::UnicodeUTF8)
    @labelNbServers.text = Qt::Application.translate("mainServer", "Serveur(s) de Jeu connecté(s)", nil, Qt::Application::UnicodeUTF8)
    @refresh.text = ''
    @refresh.shortcut = Qt::Application.translate("mainServer", "F5", nil, Qt::Application::UnicodeUTF8)
  end # retranslateUi

  def retranslate_ui(mainServer)
    retranslateUi(mainServer)
  end

end

module Ui
  class MainServer < Ui_MainServer
  end
end  # module Ui