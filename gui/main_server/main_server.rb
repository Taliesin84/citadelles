# encoding: utf-8

require 'Qt4'
require 'socket'
require 'thread'
require './gui/main_server/main_server_ui'
require './lib/servers/main_g_server'
require './lib/servers/main_event_server'

class MainServer < Qt::MainWindow

  include MainEventServer

  slots :re_init, :launch, :shutdown, :refresh

  def initialize parent=nil
    super(parent)

    #On oblige l'application à utiliser le charset UTF-8
    Qt::TextCodec::setCodecForCStrings (Qt::TextCodec::codecForName "UTF-8")
    Qt::TextCodec::setCodecForTr (Qt::TextCodec::codecForName "UTF-8")
    Qt::TextCodec::setCodecForLocale (Qt::TextCodec::codecForName "UTF-8")

    @servers = {}

    @main_server_ui = Ui::MainServer.new
    @main_server_ui.setup_ui(self)

    @main_server_ui.reInit.setEnabled false
    @main_server_ui.refresh.setEnabled false

    connections
  end

  def connections
    connect @main_server_ui.refresh, SIGNAL(:clicked), SLOT(:refresh)
    connect @main_server_ui.reInit, SIGNAL(:clicked), SLOT(:re_init)
    connect @main_server_ui.launch, SIGNAL(:clicked), SLOT(:launch)
    connect @main_server_ui.shutdown, SIGNAL(:clicked), SLOT(:shutdown)
  end

  def re_init
    if @main_server_ui.radioEvent.checked
      $games = {}
    elsif @main_server_ui.radioGServer.checked
      @main_server.games = {}
    end
    refresh
  end

  def launch
    @host = '0.0.0.0'
    @port = 2010
    if @main_server_ui.radioEvent.checked
      # Démarrage du Serveur Central avec EventMachine
      @th_server = Thread.new do
        EventMachine::run do
          $games = {}
          EventMachine::start_server @host, @port, MainEventServer
        end
      end
    elsif @main_server_ui.radioGServer.checked
      # Démarrage du Serveur Central avec GServer
      @th_server = Thread.new do
        nb_servers_max = 100
        debug = true
        # On instancie le Serveur Central
        @main_server = MainGServer.new @port, @host, nb_servers_max, $stderr, true, debug
        # On demande au Serveur Central de démarrer et de se mettre en écoute
        @main_server.start
        enabled_controls
        auto_refresh
      end
    end
  end

  def enabled_controls
    @main_server_ui.reInit.setEnabled true
    @main_server_ui.refresh.setEnabled true
    @main_server_ui.launch.setEnabled false
    @main_server_ui.radioEvent.setEnabled false
    @main_server_ui.radioGServer.setEnabled false
  end

  def auto_refresh
    @th_auto_refresh = Thread.new do
      until @main_server.stopped?
        sleep 1
        refresh
      end
    end
  end

  # Override de la fonction de fermeture d'une fenêtre pour que celle-ci ferme toutes ses filles
  def closeEvent event
    shutdown ? event.accept : event.ignore
  end

  def shutdown
    exit_msg_box = Qt::MessageBox.question self, "Quitter Citadelles", "Etes-vous sûr(e) de vouloir éteindre le 'Serveur Central' ?" , Qt::MessageBox::Yes | Qt::MessageBox::No, Qt::MessageBox::Yes

    if exit_msg_box == Qt::MessageBox::Yes
      unless @main_server.nil?
        @th_auto_refresh.exit
        @th_server.exit
        if @main_server_ui.radioGServer.checked
          @main_server.shutdown
        end
      end
      $qApp.quit
    else
      false
    end
  end

  # Rafraichit la liste des Serveurs de jeu
  def refresh
    @main_server_ui.listServers.clear
    set_games_list
  end

  # Demande au Serveur Central la liste des parties hébergées
  def set_games_list
    if @main_server_ui.radioEvent.checked
      servers = $games
    elsif @main_server_ui.radioGServer.checked
      servers = @main_server.games
    end
    for id, server in servers
      server[1].empty? ? mdp="sans" : mdp="avec"
      @main_server_ui.listServers.add_item "#{id}|#{server[0]}|#{server[1]}|#{mdp}|#{server[3]}|#{server[4]}|#{server[5]}"
    end
    @main_server_ui.nbServers.display servers.length
  end

end